/**************************************************************************************************************
 * memm is a bayesian estimation of pollen dispersal kernel and variance in male fecundity.                   *
 * copyright (c) 2009-2016 etienne klein <etienne.klein@paca.inra.fr>                                         *
 *                                                                                                            *
 * this file is part of memm.                                                                                 *
 *                                                                                                            *
 * memm is free software: you can redistribute it and/or modify                                               *
 * it under the terms of the gnu general public license as published by                                       *
 * the free software foundation, either version 3 of the license, or                                          *
 * (at your option) any later version.                                                                        *
 *                                                                                                            *
 * memm is distributed in the hope that it will be useful,                                                    *
 * but without any warranty; without even the implied warranty of                                             *
 * merchantability or fitness for a particular purpose.  see the                                              *
 * gnu general public license for more details.                                                               *
 *                                                                                                            *
 * you should have received a copy of the gnu general public license                                          *
 * along with memm.  if not, see <http://www.gnu.org/licenses/>.                                              *
 **************************************************************************************************************/

#ifndef __MEMM_POPULATION_HPP__
#define __MEMM_POPULATION_HPP__

/*!
 * \file Population.hpp
 * \brief Populations class
 * \author EK
 * \version 1.0
 * \date 30/10/2019
 */

#include <string>
#include <vector>
#include <limits>
#include <iostream>

using namespace std;

/*! \class Population
 * \brief Population class define parameter object
 *
 * The class define a population for multipop versions of MEMM
 *
 */
class Population {

  public:
    /*! \brief Population Constructor */
    Population(string filename, int nql, int nqt, int nw, int y0, int yobs);
    /*! \brief Population Destructor */
    ~Population();

    string filename_;   //!< Parameter name (ID)
    int nql_;           //!< Number of value for this parameter (vector)
    int nqt_;           //!< Number of value for this parameter (vector)
    int nw_;           //!< Number of value for this parameter (vector)
    int y0_;           //!< Number of value for this parameter (vector)
    int yobs_;           //!< Number of value for this parameter (vector)



};
#endif
