/**************************************************************************************************************
 * memm is a bayesian estimation of pollen dispersal kernel and variance in male fecundity.                   *
 * copyright (c) 2009-2016 etienne klein <etienne.klein@paca.inra.fr>                                         *
 *                                                                                                            *
 * this file is part of memm.                                                                                 *
 *                                                                                                            *
 * memm is free software: you can redistribute it and/or modify                                               *
 * it under the terms of the gnu general public license as published by                                       *
 * the free software foundation, either version 3 of the license, or                                          *
 * (at your option) any later version.                                                                        *
 *                                                                                                            *
 * memm is distributed in the hope that it will be useful,                                                    *
 * but without any warranty; without even the implied warranty of                                             *
 * merchantability or fitness for a particular purpose.  see the                                              *
 * gnu general public license for more details.                                                               *
 *                                                                                                            *
 * you should have received a copy of the gnu general public license                                          *
 * along with memm.  if not, see <http://www.gnu.org/licenses/>.                                              *
 **************************************************************************************************************/

#ifndef __MEMM_LOGLIK_MULTISPE_COVAR_PREDICT_HPP__
#define __MEMM_LOGLIK_MULTISPE_COVAR_PREDICT_HPP__

/*!
 * \file MEMM_logLik_mulitSpe_covar_predict.hpp
 * \brief MEMM_logLik_mulitSpe_covar_predict class
 * \author Etienne Klein
 * \version 1.0
 * \date 12/01/2022
 *
 * The MEMM_loglik_mulitSpe_covar class enables to estimate the pollen dispersal kernel and the variance in male fecundity from spatial and genetic data (microsatellites) concerning adult plants and sampled seeds
 * Derives from the MEMM_loglik class but integrates
 * - multi-species/multi-type and fecundation barriers
 * - several ramets per clone within each species/type
 * - a likelihood for seed set as a function of pollen amount
 *
 * * The MEMM_loglik_mulitSpe_covar_predic class enables to predict the mating patterns given a file for adults positions and covariates and a file of parameters values
 */

#include "MEMM.hpp"
#include "MEMM_logLik.hpp"

using namespace std;

/*! \class MEMM_logLik_mulitSpe_covar_predict
 * \brief MEMM_logLik_mulitSpe_covar_predict predicts mating patterns
 *
 * This class enables to predict mating patterns.
 */
class MEMM_logLik_multiSpe_covar_predict : public MEMM_logLik {

  public :

    /*! \brief Constructor
     *
     * MEMM_logLik_mulitSpe_covar constructor
     */
    MEMM_logLik_multiSpe_covar_predict();

    /*! \brief Destructor
     *
     * MEMM_logLik_mulitSpe_covar class destructor
     */
    ~MEMM_logLik_multiSpe_covar_predict();

    virtual void mcmc(int nbIteration, int thinStep);
    virtual void mcmc_dyn(int nbIteration, int thinStep);

  protected :

    // EK 2020_02 including multispecies/multitype
    int Ntype;
    vector < int > Type, Clone;
    vector < int > NClone;
    double GamC, mGamC, MGamC;                  //!< parameter for variance of the clone random effect
    vector < vector < double > > Vec_Fec_Clone;
    vector < vector < vector < int > > > CloneList;
    MEMM_loi * pLoiClone;  //!< distribution for individual fecundities

    double bSI, mbSI, MbSI;         //!< parameter for the genetical self-incompatibility between two ramets of the same clone

    double Delta_opt1, mDelta_opt1, MDelta_opt1;         //!< parameter for the optimal phenological lag male flower 1
    double Delta_opt2, mDelta_opt2, MDelta_opt2;         //!< parameter for the optimal phenological lag male flower 2
    double Sigma_pheno1, mSigma_pheno1, MSigma_pheno1;         //!< parameter for the widht of flowering widow flower 1
    double Sigma_pheno2, mSigma_pheno2, MSigma_pheno2;         //!< parameter for the widht of flowering widow flower 2
    double Weight12, mWeight12, MWeight12;         //!< parameter for the relative weights of male flowers 1 and 2

    double Blimpol, mBlimpol, MBlimpol;         //!< parameter for the sigmoid of the "pollen amount-empty seed rate" relationship
    int polSatur;
    double TotSeuilA, mTotSeuilA, MTotSeuilA;         //!< parameter for the sigmoid of the "pollen amount-empty seed rate" relationship
    double TotSeuilL, mTotSeuilL, MTotSeuilL;         //!< parameter for the sigmoid of the "pollen amount-empty seed rate" relationship
//    double AlRed, mAlRed, MAlRed;         //!< parameter for the zero-deflated binomial modelling empty fruits (i.e; 0/3 full seeds)
    vector < vector < int > > SeedSet;
    vector < int > SeedSetTot;
    vector < vector <double> > DistPP;
    vector < vector <double> > AzimPP;
    vector < vector < long double > > PHENOPP, PHENO ;
    vector < vector < long double > > DISPPP ;

    // EK 2019_11 including fixed effects in
    int Nvar=0;
    vector < vector <double> > Covar;       //!< matrix contining the covariates for all adults
    vector < int > FlowType;
    
    vector < vector < double > > PhenoMP1, PhenoMP2, PhenoPP1, PhenoPP2;   //!< matrix contining the phenological lags for all pairs of trees
    
    vector < vector <double> > Vec_FixedEff;    //!< vector of the coefficients of the fixed effects .
                                                //!En ligne le param visé.
                                                //!En colonne l'effet visé
    vector<double> Vec_Fec_All;  //!< individual male fecundities  Random and deterministic parts

    vector < vector <double> > Vec_FixedEffSS;    //!< vector of the coefficients of the fixed effects on seed-set success
                                                //!En ligne le param visé.
                                                //!En colonne l'effet visé
    vector<double> Vec_ODD;  //!< computing once the Sum(Vec_SS[i]*Cov[p][i] to avoid repeating the computation

    vector < vector < double > > mat_Barr;         //!< Matrix of the parameters for the interspecific barriers
    
    // NEW EK -> Computing posterior probabilities of parents
    vector<vector< long double >> PosteriorPeres;  //!< posterior prob of all fathers for a seed i
    vector< double > PosteriorOut; //< posterior prob of coming from outside for a seed i
    vector< double > PosteriorSelf; //< posterior prob of coming from outside for a seed i
    int Nposterior; // Number of steps used to compute posteriors
    //

    vector < vector < vector < double> > > AllFreq;  //!< individual male fecundities  Random and deterministic parts

    
    // Parameters

    
    
    virtual void loadSeeds(Configuration * p);
    virtual void loadParentFile(Configuration * p);
    virtual void loadAllelicFreq(Configuration * p);
    virtual void calculDistances(Configuration * p);
    virtual void calculTransitionMatrix(Configuration *p);
    virtual void loadParameters(Configuration *p);
    virtual void exportPosteriors();
    void exportNEP(int iteration);
    
    /*! \brief Calcul the likelihood of the model
     *
     * Will calcul the log likelihood of the model
     */
    long double compute(int posterior=NULL);
    long double computeDyn();
    long double logLik_Genet(int posterior=NULL);
    long double logLik_SeedSet(int posterior=NULL);
    void computeDISP();
    void computePHENO();
    void computeODD();


    ofstream * PolLimFile;
    ofstream * NepFile;
    ofstream * SummaryStats;
    ofstream * SummaryStatsAll;
  private :


};



#endif
