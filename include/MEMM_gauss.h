/**************************************************************************************************************
 * memm is a bayesian estimation of pollen dispersal kernel and variance in male fecundity.                   *
 * copyright (c) 2009-2016 etienne klein <etienne.klein@paca.inra.fr>                                         *
 *                                                                                                            *
 * this file is part of memm.                                                                                 *
 *                                                                                                            *
 * memm is free software: you can redistribute it and/or modify                                               *
 * it under the terms of the gnu general public license as published by                                       *
 * the free software foundation, either version 3 of the license, or                                          *
 * (at your option) any later version.                                                                        *
 *                                                                                                            *
 * memm is distributed in the hope that it will be useful,                                                    *
 * but without any warranty; without even the implied warranty of                                             *
 * merchantability or fitness for a particular purpose.  see the                                              *
 * gnu general public license for more details.                                                               *
 *                                                                                                            *
 * you should have received a copy of the gnu general public license                                          *
 * along with memm.  if not, see <http://www.gnu.org/licenses/>.                                              *
 **************************************************************************************************************/

#ifndef MEMM_GAUSS_H
#define MEMM_GAUSS_H
#include <fstream>
#include <vector>
#include <boost/random/variate_generator.hpp>
#include <boost/random/lagged_fibonacci.hpp>
#include <boost/random/uniform_real.hpp>
#include <boost/random/normal_distribution.hpp>
#include <boost/math/distributions/normal.hpp>
#include "MEMM_loi.h"
/** 
 * \brief Cette classe implemente la loi gamma.
 */
class MEMM_gauss : public MEMM_loi
{
 private:
    double _pmu, _pvar;
    boost::math::normal_distribution<> _dgauss;
    boost::variate_generator<boost::lagged_fibonacci19937&, boost::uniform_real<> >& _accept;
 public:
  /** Constructeur
      \pmu permentant de construire la loi gauss(mu,var).
   \pvar permentant de construire la loi gauss(mu,var).
   */
  MEMM_gauss(double& pmu, double& pvar,
	     boost::variate_generator<boost::lagged_fibonacci19937&, boost::uniform_real<> >& accept);
    
  /** faire un tirage
  */
  virtual void tirage(double& v);
    
  /** Calcul la vraisemblance de l'echantillon ech avec les parametres de loi palpha et pbeta
   * \param [in] npar, taille de l'echantillon
   * \param [in] y, vecteur de valeurs réelles
   * \pmu [in] parametres de loi Gaussin
   * \pvar [in] parametre de loi Gaussian
   * \return la vraisemblance
   */
  virtual double logLik(int npar,const std::vector<double> & y, double& pmu, double & pvar);
    virtual double logLik(int npar,const std::vector<double> & prop, double& palpha);
   
    /** Calcul la vraisemblance de plusieurs ech avec le parametre de loi param
      * \param [in] ech, echantillon, matrice
      * \param [in] parametre de loi
      * \return la vraisemblance
      */
    virtual double logLik(const std::vector< std::vector<double> > & fec, double& palpha);

  /** Modifier le parametre d'indice indiceParam.
      \param [in] indice du parametre (MEMM_LOI_MU ou MEMM_LOI_VAR
      \param [in] valeur a prendre en compte.
   */
  virtual void setDParam(unsigned int indiceParam, double & value);
  virtual ~MEMM_gauss();
};

#endif // MEMM_BETA_H
