/**************************************************************************************************************
 * memm is a bayesian estimation of pollen dispersal kernel and variance in male fecundity.                   *
 * copyright (c) 2009-2016 etienne klein <etienne.klein@paca.inra.fr>                                         *
 *                                                                                                            *
 * this file is part of memm.                                                                                 *
 *                                                                                                            *
 * memm is free software: you can redistribute it and/or modify                                               *
 * it under the terms of the gnu general public license as published by                                       *
 * the free software foundation, either version 3 of the license, or                                          *
 * (at your option) any later version.                                                                        *
 *                                                                                                            *
 * memm is distributed in the hope that it will be useful,                                                    *
 * but without any warranty; without even the implied warranty of                                             *
 * merchantability or fitness for a particular purpose.  see the                                              *
 * gnu general public license for more details.                                                               *
 *                                                                                                            *
 * you should have received a copy of the gnu general public license                                          *
 * along with memm.  if not, see <http://www.gnu.org/licenses/>.                                              *
 **************************************************************************************************************/

#ifndef __CONFIG_XML_HPP__
#define __CONFIG_XML_HPP__

/*!
 * \file ConfigXML.hpp
 * \brief XML Configuration manager
 * \author Jean-Francois REY
 * \version 1.2
 * \date 19 Jul 2018
 *
 * The Config class manage parameters from arguments and XML file (for MEMM project).
 * TODO implement arguments managing.
 */
#include <string>
#include <string.h>
#include <iostream>
#include <typeinfo>
#include <unordered_map>
#include <map>
#include "XMLInterface.hh"
#include "Configuration.hpp"
#include "Parameter.hpp"
#include "Population.hpp"
#include "Tools.hpp"
#include "ModelsList.hpp"

using namespace std;

/*! \defgroup XMLVariables XPath acces XML variables
 * @{
 */
#define MEMM_ROOT "//MEMM"
#define MEMM_ROOT_MODE "mode"
#define MEMM_ROOT_TYPE "type"
#define INPUT_ROOT "//MEMM/input"
#define INPUT_NUMBER_POPS "multipop"
#define INPUT_PARENTS_FILE_NAME "//MEMM/input/file[@description='parents']/filename"
#define INPUT_PARENTS_FILE_NUMBERS_OF_LOCUS "//MEMM/input/file[@description='parents']/numbers_of_locus"
#define INPUT_NUMBERS_OF_LOCUS "//MEMM/input/numbers_of_locus"
#define INPUT_PARENTS_FILE_CLASS_COV "//MEMM/input/file[@description='parents']/class_covariates"
#define INPUT_PARENTS_FILE_QT_COV "//MEMM/input/file[@description='parents']/quantitative_covariables"
#define INPUT_PARENTS_FILE_WEIGHT_VAR "//MEMM/input/file[@description='parents']/weighting_variables"
#define INPUT_OFFSPRING_FILE_NAME "//MEMM/input/file[@description='offspring']/filename"
#define INPUT_OFFSPRING_FILE_NUMBERS_OF_LOCUS "//MEMM/input/file[@description='offspring']/numbers_of_locus"
#define INPUT_AF "//MEMM/input/AF[@enable='true']"
#define INPUT_AF_FILE_NAME "//MEMM/input/AF/filename"
#define INPUT_KERNEL "//MEMM/input/Kernel"
#define INPUT_DIST "//MEMM/input/Dist[@enable='true']"
#define INPUT_DIST_FILE_NAME "//MEMM/input/Dist/filename"
#define INPUT_MEMM_LOCUS_ERROR "//MEMM/input/LE[@enable='true']"
#define INPUT_MEMM_LOCUS_ERROR_FILE "//MEMM/input/LE/filename"
#define INPUT_MEMM_LOCUS_ERROR_NULL "//MEMM/input/LE[@enable='true']/null"
#define PARAM_LOCUS_ERROR_NMISTYPEMAX "//MEMM/input/LE[@enable='true']/nmistypemax"
#define INPUT_COVAR "//MEMM/input/Covar[@enable='true']"
#define INPUT_PRIOR_COVAR_FILE "//MEMM/input/Covar/filename"
#define INPUT_COVAR_NUMBER "//MEMM/input/Covar/ncovars"

// NEW COLONIZ
//#define INPUT_COLONIZ_Y0 "//MEMM/input/Chronos[@enable='true']/Y0" // NEW COLONIZ/DENDRO
//#define INPUT_COLONIZ_YOBS "//MEMM/input/Chronos[@enable='true']/Yobs" // NEW COLONIZ/DENDRO
#define INPUT_COLONIZ_Y0 "//MEMM/input/file[@description='parents']/Y0" // NEW COLONIZ/DENDRO
#define INPUT_COLONIZ_YOBS "//MEMM/input/file[@description='parents']/Yobs" // NEW COLONIZ/DENDRO

#define INPUT_PARAM_POLLIM "//MEMM/parameters/pollim"

#define PARAM_SEED "//MEMM/options/seed"
#define PARAM_BURNIN "//MEMM/options/burnin"
#define PARAM_ITE "//MEMM/options/ite"
#define PARAM_THIN "//MEMM/options/thin"
#define OUTPUT_DIRECTORY_PATH "//MEMM/output/directory"
#define OUTPUT_GAMA_FILE_NAME "//MEMM/output/file[@type='GamA']"
#define OUTPUT_GAMA_FILE_NAME_ATTRIBUT "name"
#define OUTPUT_IND_FEC_FILE_NAME "//MEMM/output/file[@type='IndFec']"
#define OUTPUT_IND_FEC_FILE_NAME_ATTRIBUT "name"
#define OUTPUT_DISP_FILE_NAME "//MEMM/output/file[@type='Disp']"
#define OUTPUT_DISP_FILE_NAME_ATTRIBUT "name"

#define OUTPUT_POSTERIOR_FILE_NAME "//MEMM/output/file[@type='Posterior']" // NEW EK -> Computing posterior probabilities of parents
#define OUTPUT_POSTERIOR_FILE_NAME_ATTRIBUT "name"// NEW EK -> Computing posterior probabilities of parents
#define OUTPUT_POSTERIOR_TYPE "//MEMM/output/posterior_type" // NEW EK -> Exporting different types of posteriors

/*!@}*/


/*! \class ConfigXML
 * \brief Configuration manager for XML format
 *
 * This class manage parameters as arguments or as XML file.
 */
class ConfigXML : public Configuration {

    private :
        XMLInterface * param_xml; ///< XML tools
        string param_filename; ///< paramater file name

    public :
        /*!
         * \brief Constructor
         *
         * ConfigXML class constructor
         */
        ConfigXML();

        /*! \brief Constructor
         * \param filename : parameters XML file to open
         *
         *  ConfigXML class constructor
         */
        ConfigXML(string filename);
        ConfigXML(char * filename);

        /*! \brief Destructor
         *
         *  ConfigXML class destructor
         */
        ~ConfigXML();

        /*! \brief Create and load default parameters as XML format
         *
         * Create new xml document and load DEFAULT_XML_CONFIG_MEMM as default parameters
         */
        void createDefaultXMLConfig();

        /*! \brief Return information about available models
         * \return model name, description and xml
         */
        map<string,pair<string,string>> getModels();

        /*! \brief Return information about available models
         * \return model name, description and xml
         */
        pair<string,string> getModel(string s);

        /*! \brief set verbose mode
         * \param b : true active verbose
         */
        void setVerbose(bool b);

        /*! \brief Load an xml parameters file
         * \param filename : xml parameters file
         * \return 1 if ok
         *
         */
        bool loadXMLConfig(string filename);

        /*! \brief Load an xml 
         * \param xml : a xml string
         * \return 1 if ok
         *
         */
        bool readXMLConfig(string xml);

        /*! \brief Print xml file in stdout
         */
        bool printXMLConfig();

        /*! \brief Print help message
         *
         * Print help message for parameters
         */
        void printHelpMessage();

        /*! \brief Add an element and value in xml format
         * \param expr : XPath expression
         * \param name : element name to add
         * \param text : element value, default ""
         * \return 1 if OK otherwise return value < 1
         *
         * Add an element name with value text in a xml node represented as XPath expression.
         *
         */
        bool addElement(string expr, string name, string text="");

        /*! \brief change enable attribut of an element
         * \param name : element (tag) name
         * \param enable : default enable value is true
         * \return true if find the element
         */
        bool enableElement(string name, bool enable=true);

        /*! \brief Get value of a parameter
         * \param expr : XPath expression
         * \param attribut : element attribut name of the value to return
         * \return the value as a string.
         *
         * Get the value of an element or an element attribut value.
         */
        string getValue(string expr, string attribut="");

        /*! \brief Get value of a parameter
         * \param valueName : Predefine parameter identifier from MEMM_parameters_t
         * \return the value as aar *.
         *
         * Get the value of an element or an element attribut value.
         */
        virtual char * getValue(MEMM_parameters_t valueName);

        /*! \brief Set value of a parameter
         * \param expr : XPath expression
         * \param value : parameter value to set
         * \param attribut : parameter attribut to set value, if none ""
         *
         * Set the value of an element or an element attribut value.
         */
        bool setValue(string expr,string value, string attribut="");

        /*! \brief Set value of a parameter MEMM_parameter_t
         * \param valueName : a MEMM_paramter_t variable
         * \param value : parameter value to set
         *
         * Set the value of an element or an element attribut value.
         */
        virtual bool setValue(MEMM_parameters_t valueName, const char * value);

        /*! \brief Save parameters
         * \return 1 if OK
         *
         * Save parameters as XML file if already specified.
         */
        bool save();

        /*! \brief Save parameters in file
         * \param filename : file name to save parameters
         * \return 1 if OK
         * 
         * Save parameters as XML file if already specified.
         */
        bool saveFile(string filename);

        /*! \brief Get filename
         * \return filename as string
         *
         * Get the filename of the parameters record if any.
         */
        string getFileName();

        /*! \brief load Parameters 
         * \retunr a map of loaded paramter
         */
        unordered_map <string, Parameter *> loadParameters();

        /*! \brief get all variable parameters names
         * \return a vecotr of name
         */
        vector<string> getParametersName();

        /*! \brief remove a parameter 
         * \param name : parameter name to delete
         */
        bool deleteParameter(string name);

        /*! \brief Add or modifiy a parameter variable
         * \param name : parameter name
         * \param init : initial value
         * \param min : minimal value
         * \param max : maximal value
         * \param size : vector size (covariable)
         * \param prior : prior name
         *
         * Add or modify a parameter variable in XML file. If size > 1 that will create a vector.
         */
        bool setParameter(string name, double init, double min, double max, int size, string prior );

        /*! \brief get a parameter variable initial value
         * \param param_name : the parameter name
         * \return a double
         */
        double getParameterInitValue(string param_name);

        /*! \brief get a parameter variable minimum value
         * \param param_name : the paramter name
         * \return a double
         */
        double getParameterMinValue(string param_name);

        /*! \brief get a parameter variable maximum value
         * \param param_name : the paramter name
         * \return a double
         */
        double getParameterMaxValue(string param_name);

    /*! \brief get a parameter variable tuning value
     * \param param_name : the paramter name
     * \return a double
     */
    double getParameterTuneValue(string param_name);

        /*! \brief get a parameter variable size
         * \param param_name : the paramter name
         * \return an interger
         */
        int getParameterSizeValue(string param_name);

        /*! \brief get a parameter variable prior name
         * \param param_name : the paramter name
         * \return a string
         */
        string getParameterPriorValue(string param_name);

        /*! \brief Get Distribution name
         * \return a vector of string 
         */
        vector<string> getDistributionsName();

        /*! \brief get distribution type
         * \param namelaw : name of the distribution law
         * \return law type
         */
        string getDistributionType(string namelaw);

        /*! \brief get Distribution parameters list
         * \param lawname
         * \return a map matching distribution parameter name and a parameter name
         */
        map<string,string> getDistributionParameters(string lawname);

        /*! \brief remove a distribution
         * \param name : a distribution name
         */
        bool deleteDistribution(string name);

        /*! \brief add or modify a distribution 
         * \param name : distribution name
         * \param law : distribution law name
         * \param name_ref : map of parameter name and parameter reference name in the xml
         */
        bool setDistribution(string name, string law, map<string,string> name_ref);
    /*! \brief load Populations
     * \return a map of loaded populations
     */
    unordered_map <string, Population *> loadPopulations();


    private :

    
    /*! \brief load a population section
     * \param paramNode : pointer on xml tag param
     * \return a Population object
     */
    Population * loadPopulationD(xmlNodePtr paramNode);

    
        /*! \brief load a parameter section
         * \param paramNode : pointer on xml tag param
         * \return a Parameter object
         */
        Parameter * loadParameterD(xmlNodePtr paramNode);
};



#endif


