#ifndef __MEMM_COLONIZ_DENDRO_MULTIPOP_HPP__
#define __MEMM_COLONIZ_DENDRO_MULTIPOP_HPP__

#include "MEMM.hpp"
#include "MEMM_coloniz_dendro.hpp"
#include "Population.hpp"

//struct CouplePot { int mere_pot; int pere_pot; double couple_prob;};

/*! \class MEMM_coloniz_dendro_multipop
 * \brief MEMM_coloniz_dendro_multipop class
 *
 * Mother and Father are unknow.
 *
 */
class MEMM_coloniz_dendro_multipop : public MEMM {

  public :

    /*! \brief Constructor
     */
    MEMM_coloniz_dendro_multipop();

    /*! \brief destructor
     */
    virtual ~MEMM_coloniz_dendro_multipop();

       /*! \brief initialize variables from parameters
        * \param p : a Configuration pointer
        *
        * Load parameters and initialize variables and model.
        */
        void init(Configuration * p);

       /*! \brief run mcmc
        *  Run mcmc algorithm with nbIteration and thin
        */
       void run();

       /*! \brief run burn in
        * Run mcmc with burn in iteration
        */
       void burnin();

       /*! \brief mcmc algorithm
        * \param nbIteration : number of iteration
        * \param thinStep : number of step for output (0 print nothings)
        *
        * Run mcmc algorithm.
        */

    //       void mcmc(int nbIteration, int thinStep) = 0;
    

    virtual void mcmc(int nbIteration, int thinStep);
    virtual void mcmc_dyn(int nbIteration, int thinStep);

    
    vector<double *> Scale;
    double mScale, MScale; //!< scale parameter initial, dimension of distance (a)
    vector<double *> Shape;
    double mShape, MShape; //!< shape parameter initial for dispersal kernel (b)
    vector<double *>  Mig;
    double mMig, MMig;   //!< initial value of probabilities to mother to be fertilized by pollen grain from uncensored father form outside the area
    vector<double *>  Self;
    double mSelf, MSelf;  //!< initial probabilites to mother to be self fertilized
    vector<double *>  GamA;
    double mGamA, MGamA;  //!< initial Variance of fecundities

    // NEW ANISOTROPIE
    vector<double *>  Theta;
    double mTheta, MTheta; //!< anisotropy parameter initial for dispersal kernel
    vector<double *>  Kappa;
    double mKappa, MKappa; //!< anisotropy parameter initial for dispersal kernel

    // NEW COLONIZ
    vector<double *>  Mig0P;
    double mMig0P, MMig0P; //!< migration amount at year -100
    vector<double *>  bMigP;
    double mbMigP, MbMigP; //!< rate of increase of migration amount
    vector<double *>  AgeThP;
    double mAgeThP, MAgeThP; //!< age threshold for pollen production
    vector<double *>  bFecP;
    double mbFecP, MbFecP; //!< rate of increase of pollen production

    vector<double *> Scales;  //!< scales parameter initial, dimension of distance (as);
    double mScales, MScales; //!< min scales value
     vector<double *> Shapes;  //!< shapes parameter initial value, dispersal kernel (bs)
    double mShapes, MShapes; //!< min shapes value

    // NEW ANISOTROPIE
     vector<double *> Thetas; //!< anisotropy parameter initial for dispersal kernel
    double mThetas, MThetas; //!< min value of anisotropy parameter for dispersal kernel
     vector<double *> Kappas; //!< anisotropy parameter initial for dispersal kernel
    double mKappas, MKappas; //!< min value of anisotropy parameter for dispersal kernel

    // NEW COLONIZ
    vector<double *> Mig0S;
    double mMig0S, MMig0S; //!< seed migration amount at year 0
    vector<double *> bMigS;
    double mbMigS, MbMigS; //!< rate of increase of seed migration amount
    vector<double  *> AgeThS;
    double mAgeThS, MAgeThS; //!< age threshold for sezed production
    vector<double *> bFecS;
    double mbFecS, MbFecS; //!< rate of increase of seed production
    vector<double *> Age;   //!< pollen donor weight
    
    vector<double *> Migs;  //!< migs initial value;
    double mMigs, MMigs; //!< min migs value
    vector<double *> GamAs; //!< GamAs initial value
    double mGamAs, MGamAs;//!< min GamAs value

    vector<double *> as; //!< scale parameter
    vector<double *> abs; //!< exponential power dispersal kernel (1/as)^bs
    vector<double *> Kabs; //!< exponential power dispersal kernel Shapes/2/Pi/as^2/Gamma(2/Shapes)
//    vector<double *> ap; //!<  mean dispersal distance (delta)
//    vector<double *> abp; //!< exponential power dispersal kernel (1/a)^b
//    vector<double *> Kabp; //!< exponential power dispersal kernel Shapep/2/Pi/a^2/Gamma(2/Shapep)


  protected :
//    vector<vector<double>> DistSM;  //!< Distances between Seed Mother
//    vector<vector<double>> AzimSM;  //!< Distances between Seed Mother
//    vector<int> NbMeres;  //!< number of mothers for a seed i
//    vector<int> NbCouples;  //!< number of couple (mother-father) for a seed i
//    vector<double> ProbMig;   //!< proba of outside site fertilization
//    vector<vector< PerePot >> ProbMeres;  //!< potential mothers for a seed i
//    vector<vector< CouplePot >> ProbCouples;  //!< potential couples for a seed i
//
//    // NEW EK -> Computing posterior probabilities of parents
//    vector<vector< long double >> PosteriorMeres;  //!< posterior prob of all mothers for a seed i
//    vector<vector< double >> PosteriorCouples;  //!< posterior prob of all couples for a seed i
//    vector< double > PosteriorOut; //< posterior prob of coming from outside for a seed i
//    int Nposterior; // Number of steps used to compute posteriors
//    //

    vector< MEMM_coloniz_dendro > MEMM_pops;
    unordered_map<string, Population *> allPopulations;
    int Npop;

    void loadOptions(Configuration *p);
    void loadAllelicFreq(Configuration * p);
    void loadParentFile(Configuration *p);
    void loadParameters(Configuration *p);

//    void loadSeeds(Configuration * p);
//    void calculDistances(Configuration *p);
//    void calculTransitionMatrix(Configuration *p);
//    void calculWeightPollenDonor(Configuration *p);
    virtual void exportPosteriors(); // NEW EK -> Computing posterior probabilities of parents

    /*! \brief Calcul model log likelihood
     */
    long double logLik(int posterior=NULL);
  
    //Parameters
//    unordered_map<string, Parameter *> allParameters;
    // allParameters contains all parameters use below to access parameters directly from variable name.

    // NEW COLONIZ DENDRO
//    vector< vector<double> > DBH;   //!< DBH of all trees at all years
//    double NYear, Y0, Yobs;
//
//    vector<double> Vec_Fecs;  //!< mother fecundity
//    vector<double> Vec_Fecp;  //!< male fecunditiy
//
//    MEMM_loi * pLois; //!< distribution for individual fecundities

 private :
    
};

#endif

