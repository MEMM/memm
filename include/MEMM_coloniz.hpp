#ifndef __MEMM_COLONIZ_HPP__
#define __MEMM_COLONIZ_HPP__

#include "MEMM.hpp"

//struct CouplePot { int mere_pot; int pere_pot; double couple_prob;};


/*! \class MEMM_seedlings
 * \brief MEMM_seedlings class
 *
 * Mother and Father are unknow.
 *
 */
class MEMM_coloniz : public MEMM {

  public :

    /*! \brief Constructor
     */
    MEMM_coloniz();

    /*! \brief destructor
     */
    virtual ~MEMM_coloniz();

    virtual void mcmc(int nbIteration, int thinStep);
    virtual void mcmc_dyn(int nbIteration, int thinStep);


  protected :
    vector<vector<double>> DistSM;  //!< Distances between Seed Mother
    vector<vector<double>> AzimSM;  //!< Distances between Seed Mother
    vector<int> NbMeres;  //!< number of mothers for a seed i
    vector<int> NbCouples;  //!< number of couple (mother-father) for a seed i
    vector<double> ProbMig;   //!< proba of outside site fertilization
    vector<vector< PerePot >> ProbMeres;  //!< potential mothers for a seed i
    vector<vector< CouplePot >> ProbCouples;  //!< potential couples for a seed i

    // NEW EK -> Computing posterior probabilities of parents
    vector<vector< long double >> PosteriorMeres;  //!< posterior prob of all mothers for a seed i
    vector<vector< double >> PosteriorCouples;  //!< posterior prob of all couples for a seed i
    int Nposterior; // Number of steps used to compute posteriors
    //
    
    void loadSeeds(Configuration * p);
    void calculDistances(Configuration *p);
    void calculTransitionMatrix(Configuration *p);
//    void loadPrior(Configuration * p);
    void loadParameters(Configuration *p);
    void calculWeightPollenDonor(Configuration *p);
    virtual void exportPosteriors(Configuration *p); // NEW EK -> Computing posterior probabilities of parents

    /*! \brief Calcul model log likelihood
     */
    long double logLik(int posterior=NULL);
  
    

    double Scales;  //!< scales parameter initial, dimension of distance (as);
    double mScales; //!< min scales value
    double MScales; //!< max scales value
    double Shapes;  //!< shapes parameter initial value, dispersal kernel (bs)
    double mShapes; //!< min shapes value
    double MShapes; //!< max shapes value

    // NEW ANISOTROPIE
    double Thetas; //!< anisotropy parameter initial for dispersal kernel
    double mThetas; //!< min value of anisotropy parameter for dispersal kernel
    double MThetas; //!< max value of anisotropy parameter for dispersal kernel
    double Kappas; //!< anisotropy parameter initial for dispersal kernel
    double mKappas; //!< min value of anisotropy parameter for dispersal kernel
    double MKappas; //!< max value of anisotropy parameter for dispersal kernel

    // NEW COLONIZ
    // NEW COLONIZ
    double Mig0S, mMig0S, MMig0S; //!< seed migration amount at year -100
    double bMigS, mbMigS, MbMigS; //!< rate of increase of seed migration amount
    double AgeThS, mAgeThS, MAgeThS; //!< age threshold for sezed production
    double bFecS, mbFecS, MbFecS; //!< rate of increase of seed production
    vector<double> Age;   //!< pollen donor weight

    
    double Migs;  //!< migs initial value;
    double mMigs; //!< min migs value
    double MMigs; //!< max migs value
    double GamAs; //!< GamAs initial value
    double mGamAs;//!< min GamAs value
    double MGamAs;//!< max GamAs value
    double as; //!< mean dispersal distance (deltas)
    double abs; //!< exponential power dispersal kernel (1/as)^bs
    double Kabs; //!< exponential power dispersal kernel Shapes/2/Pi/as^2/Gamma(2/Shapes)
    double ap; //!<  mean dispersal distance (delta)
    double abp; //!< exponential power dispersal kernel (1/a)^b
    double Kabp; //!< exponential power dispersal kernel Shapep/2/Pi/a^2/Gamma(2/Shapep)

    //Début modif
    double Pi0s, mPi0s, MPi0s; // initial, min and max values for the zero-inflated parameter (seed)
    // Fin modif

    vector<double> Vec_Fecs;  //!< mother fecundity
    vector<double> Vec_Fecp;  //!< male fecunditiy
    
    MEMM_loi * pLois; //!< distribution for individual fecundities

 private :
    
};

#endif

