/**************************************************************************************************************
 * memm is a bayesian estimation of pollen dispersal kernel and variance in male fecundity.                   *
 * copyright (c) 2009-2016 etienne klein <etienne.klein@paca.inra.fr>                                         *
 *                                                                                                            *
 * this file is part of memm.                                                                                 *
 *                                                                                                            *
 * memm is free software: you can redistribute it and/or modify                                               *
 * it under the terms of the gnu general public license as published by                                       *
 * the free software foundation, either version 3 of the license, or                                          *
 * (at your option) any later version.                                                                        *
 *                                                                                                            *
 * memm is distributed in the hope that it will be useful,                                                    *
 * but without any warranty; without even the implied warranty of                                             *
 * merchantability or fitness for a particular purpose.  see the                                              *
 * gnu general public license for more details.                                                               *
 *                                                                                                            *
 * you should have received a copy of the gnu general public license                                          *
 * along with memm.  if not, see <http://www.gnu.org/licenses/>.                                              *
 **************************************************************************************************************/

/**
 * \file XMLInterface.hh
 * \brief Class XMLInterface definition
 * \author Jean-François REY
 * \version 1.5
 * \date 04 dec 2013
 *
 * XMLInterface class is a little API to manipulate xml using libxml2
 */

#ifndef __XML_INTERFACE__
#define __XML_INTERFACE__

#include <string>
#include <string.h>
#include <time.h>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <list>
#include <vector>

extern "C" {
#include <libxml/encoding.h>
#include <libxml/parser.h>
#include <libxml/tree.h>
#include <libxml/xpath.h>
//#include <libxml/xpathInternals.h>
}

#include "Myexception.hh"

using namespace std;

#define MY_ENCODING "UTF-8" //!< Default encoding

static string string_null("");

/**
 *  \class XMLInterface
 *  \brief Little interface to manipulate macaon xml structure
 *
 *  XLMInterface give a little interface to manipulate xml structure.
 */
class XMLInterface {

	private : 
		xmlDocPtr doc;	//!< xml document pointer
		xmlNodePtr root_node;	//!< root node of doc
		xmlDtdPtr dtd;  //!< DTD pointer
		bool verbose;	//!< if true verbose enable
	
	public :
        /**
         * \brief Constructor
         *
         * XMLInterface class constructor
         */
        XMLInterface();

        /**
         * \brief Destructor
         *
         * XMLInterface class destructor
         */
		virtual ~XMLInterface();

        /**
         * \brief Set verbose variable
         * \param b : true to enable verbose
         */
		void setVerbose(bool b);

        /**
         * \brief Check a xml DTD
         */
		int checkDTD();

    /**
     * \brief return root node
     */
    xmlNodePtr getRootNode();

        /**
         * \brief Create a new DOM document
         * \return pointer on xml document
         *
         *	Create a new DOM document with \<document\> as root
         */
		xmlDocPtr createNewDocument() throw (Myexception);

        /**
         * \brief Set file name attribut to filename
         * \param filename : name of the file
         * \return pointer on xml Attribut pointer
         *
         *	Create a new DOM document with macaon structure
         */
		xmlAttrPtr setFileNameAttribut(string filename);

        /**
         * \brief Find a tag and add text to it
         * \param tag : tag name
         * \param value : pointer to string to copy in tag
         * \return pointer on xml node
         *
         *	Will find the fisrt tag with name 'tag' and will add 'value' to this tag
         */
		xmlNodePtr addTextToNode(string tag, string value);

    /**
     * \brief add text to an xml node
     * \param value : text value
     * \param node xml node pointer
     * \return pointer on the xml node
     *
     *	Will add 'value' to this node
     */
    xmlNodePtr addTextToNode(string value, xmlNodePtr node);

    /*! \brief get text
     * \param node : an xml node pointer
     * \param tag : a tag name
     * \return text from node or child node which name is tag.
     */
    string getText(xmlNodePtr node, string tag);

    /*! \brief get text
     * \param node : an xml node pointer
     * \return text in node
     */
    string getText(xmlNodePtr node);

        /**
         * \brief Add an Element and content using XPath
         * \param xpath_expr : XPath expression
         * \param elt : element name to add
         * \param text : element content as a string
         * \return pointer on the new node element
         *
         * Create a new element node and add content (as text) using the XPath expression.
         *
         */
        xmlNodePtr addXPathElement(string& xpath_expr, string& elt, string& text=string_null);

        /**
         * \brief Get an attribut value of a node
         * \param node : node where to get attribut
         * \param name : attribut name
         * \return attribut value or "" if nothings
         *
         *	Return an attribut value or "" if nothing of a node.
         */
		string getAttribut(xmlNodePtr node, string name);

    /**
     * \brief add, modify, remove an attribut
     * \param node : an xml node pointer
     * \param attribut : attribut id
     * \param value : attribut value
     *
     * Add or modify an attribut value of a node.
     * If value is an empty string "" that will remove the attribut from the naoe.
     */
    void addAttribut(xmlNodePtr node, string attribut, string value);

        /**
         * \brief Get Value of a node element or his attribut
         * \param xpath_expr : XPath expression
         * \param attribut : attribut name
         * \return value as a string, empty if nothing found
         *
         * Get the value of a node (as string) or his attribut if attribut parameter is set.
         *
         */
        string getXPathValue(string& xpath_expr, string& attribut=string_null);
        
        /**
         * \brief Set Value of a node element or his attribut
         * \param xpath_element : XPath expression to the node
         * \param value : the value to affect
         * \param attribut : attribut name
         * \return 1 if OK, Otherwise 0
         *
         * Set the value of a node (as string) or his attribut if attribut parameter is set.
         *
         */
        bool setXPathValue(string& xpath_element, string& value, string& attribut=string_null);

        /**
         * \brief Add a tag child to a node
         * \param tag : tag name
         * \param node : parent node
         * \return an xml node pointer to the new node created
         */
        xmlNodePtr addTagNode(string tag, xmlNodePtr node);

        /**
         * \brief Find the first tag from a node
         * \param tag : tag to find
         * \param node : node from where to start
         * \return pointer on node tag or NULL
         *
         *	Will find the first tag node.
         */
        xmlNodePtr find(string tag, xmlNodePtr node = NULL);

        /**
         * \brief Find all tags from a node
         * \param tag : tag to find
         * \param node : node from where to start
         * \return list of pointer on node tag
         *
         *	Will finds all tag nodes.
         */
    		std::list<xmlNodePtr> findAll(string tag, xmlNodePtr node);

        /**
         * \brief get a node pointer from name and attribut value
         * \param tag : a tag name
         * \param attribut : an attribut id
         * \param value : attribut value
         * \param node : a node pointer 
         * \return a pointer on a node with tag name and attribut=value, otherwise retrun NULL
         */
        xmlNodePtr findTagAttribut(string tag, string attribut, string value,xmlNodePtr node);
        
        /**
         * \brief load an xml file
         * \param filename: file to load
         * \return pointer on document tree
         *
         *	load an xml file.
         */
		xmlDocPtr loadFile(string filename) throw (Myexception);
        
        /**
         * \brief parse an XML in-memory block and build a tree.
         * \param buffer: memory block
         * \return pointer on document tree
         *
         *	Parse an XML in-memory block and build a tree.
         */
		xmlDocPtr loadString(string buffer) throw (Myexception);
        
        /**
         * \brief Save document tree in a file
         * \param filename : file where to save
         * \return true if ok
         *
         *	Save the doc tree in a file with MY_ENCODING.
         */
		bool save(string& filename);
        
        /**
        * \brief Print on stdout document tree
        * \return true if ok
        *
        *	Print the doc tree on stdout
        */
		bool print();
        
        /**
         * \brief Add Stamp to xml document
         * \param module : tool name
         * \param version : tool version
         * \return pointer on xml node new stamp
         *
         *	Will add a stamp to the document.
         */
		xmlNodePtr stampDocument(string module, string version);

    /**
     * \brief remove a node
     * \param node : a node pointer
     */
    void deleteNode(xmlNodePtr node);
			
	private : 
        /**
         * \brief return current date and time
         * \return current date and time (Tue May  4 16:58:38 2010)
         *
         *	Save the doc tree in a file with MY_ENCODING.
         */
		std::string getTimestamp();

        /**
         * \brief Convert encoding
         * \param in : char * to convert
         * \param encoding : in initial encoding 
         * \return encoded char
         *
         *	Convert encoding
         */
		xmlChar * ConvertInput(const char *in, const char *encoding);

};

#endif
