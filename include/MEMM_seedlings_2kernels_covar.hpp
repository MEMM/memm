/**************************************************************************************************************
 * memm is a bayesian estimation of pollen dispersal kernel and variance in male fecundity.                   *
 * copyright (c) 2009-2016 etienne klein <etienne.klein@paca.inra.fr>                                         *
 *                                                                                                            *
 * this file is part of memm.                                                                                 *
 *                                                                                                            *
 * memm is free software: you can redistribute it and/or modify                                               *
 * it under the terms of the gnu general public license as published by                                       *
 * the free software foundation, either version 3 of the license, or                                          *
 * (at your option) any later version.                                                                        *
 *                                                                                                            *
 * memm is distributed in the hope that it will be useful,                                                    *
 * but without any warranty; without even the implied warranty of                                             *
 * merchantability or fitness for a particular purpose.  see the                                              *
 * gnu general public license for more details.                                                               *
 *                                                                                                            *
 * you should have received a copy of the gnu general public license                                          *
 * along with memm.  if not, see <http://www.gnu.org/licenses/>.                                              *
 **************************************************************************************************************/

#ifndef __MEMM_SEEDLINGS_2KERNELS_COVAR_HPP__
#define __MEMM_SEEDLINGS_2KERNELS_COVAR_HPP__

#include "MEMM.hpp"
#include "MEMM_seedlings.hpp"
#include "MEMM_beta.h"
#include "MEMM_zigamma.h"
#include "MEMM_gauss.h"

/*! \class MEMM_seedlings_2kernels_covar
 * \brief MEMM_seedlings_2kernels_covar class
 *
 * Mother and Father are unknow.
 * Two kernels for SDD and LDD are mixed with variable weights
 * Some covariates are included in the deterministic part of fecundities or weight of mixture of LDD
 *
 */
class MEMM_seedlings_2kernels_covar : public MEMM_seedlings {

  public :

    /*! \brief Constructor
     */
    MEMM_seedlings_2kernels_covar();

    /*! \brief destructor
     */
    virtual ~MEMM_seedlings_2kernels_covar();

    virtual void mcmc(int nbIteration, int thinStep);
    virtual void mcmc_dyn(int nbIteration, int thinStep);

  
  protected :

    void loadParentFile(Configuration * p);
    void loadParameters(Configuration * p);
    void exportPosteriors();

    /*! \brief Calcul model log likelihood
     */
    long double logLik(int posterior=NULL);
  
    
private :

    double ScalesLDD;  //!< LDD scales parameter initial, dimension of distance (as);
    double mScalesLDD; //!< min LDD scales value
    double MScalesLDD; //!< max LDD scales value
    double ShapesLDD;  //!< LDD shapes parameter initial value, dispersal kernel (bs)
    double mShapesLDD; //!< min LDD shapes value
    double MShapesLDD; //!< max LDD shapes value

    double asLDD, absLDD; // A VERIFIER
    double KnormLDD, Knorm; // A VERIFIER
  
    
    double AlphaLDD;  //!< Beta-distribution for LDD: alpha parameter initial value
    double mAlphaLDD; //!< Beta-distribution for LDD: min alpha value
    double MAlphaLDD; //!< Beta-distribution for LDD: max alpha value
    double BetaLDD;  //!< Beta-distribution for LDD: beta parameter initial value
    double mBetaLDD; //!< Beta-distribution for LDD: min beta value
    double MBetaLDD; //!< Beta-distribution for LDD: max beta value
    
    
    vector<double> Vec_FreqLDD;  //!< frequency of long distance dispersal Random part only
     
    MEMM_loi * pLoiLDD; //!< distribution for individual frequencies of LDD

    // EK 2019_11 including fixed effects in
    int Nvar=0;
    vector < vector <double> > Covar;       //!< matrix contining the covariates for all adults
    vector < vector <double> > Vec_FixedEff;    //!< vector of the coefficients of the fixed effects .
                                                //!En ligne le param visé.
                                                //!En colonne l'effet visé
    vector < vector <double> > mFixedEff, MFixedEff; //!< vectors of min and max values for the coefficients of the fixed effects
    vector < vector <bool> > In_FixedEff;       //!< vector coding for the presence of the fixed effects in the model... For RJMCMC purposes
    vector < vector <double> > PriorIn_FixedEff;       //!< vector with the prior probabilities of presence of the fixed effects... For RJMCMC purposes
    vector < vector <double> > qMean_FixedEff,  qSD_FixedEff; //!< mean and SD for the proposals of the Fixed Effect parameters.
    vector<double> Vec_FreqLDD_All;  //!< frequency of long distance dispersal Random and deterministic parts
    vector<double> Vec_Fecs_All;  //!< individual female fecundities Random and deterministic parts
    vector<double> Vec_Fecp_All;  //!< individual male fecundities  Random and deterministic parts
    
    ofstream * IndivFecAll;

    
};

#endif

