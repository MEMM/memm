#ifndef __UTILS_HPP__
#define __UTILS_HPP__

#include <string>
#include <sys/stat.h>

using namespace std;

bool fileIsValid(const char *file);

bool directoryIsValid(const char *directory);


#endif
