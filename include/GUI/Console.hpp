/**************************************************************************************************************
 * memm is a bayesian estimation of pollen dispersal kernel and variance in male fecundity.                   *
 * copyright (c) 2009-2016 etienne klein <etienne.klein@paca.inra.fr>                                         *
 *                                                                                                            *
 * this file is part of memm.                                                                                 *
 *                                                                                                            *
 * memm is free software: you can redistribute it and/or modify                                               *
 * it under the terms of the gnu general public license as published by                                       *
 * the free software foundation, either version 3 of the license, or                                          *
 * (at your option) any later version.                                                                        *
 *                                                                                                            *
 * memm is distributed in the hope that it will be useful,                                                    *
 * but without any warranty; without even the implied warranty of                                             *
 * merchantability or fitness for a particular purpose.  see the                                              *
 * gnu general public license for more details.                                                               *
 *                                                                                                            *
 * you should have received a copy of the gnu general public license                                          *
 * along with memm.  if not, see <http://www.gnu.org/licenses/>.                                              *
 **************************************************************************************************************/

/**
 * \file Console.hpp
 * \brief Run memm and emulate the console output
 * \author Jean-Francois Rey
 * \version 1.0
 * \date 17 Jul 2018
 */

#ifndef __CONSOLE_HPP__
#define __CONSOLE_HPP__

#include <QString>
#include <QStringList>
#include <QGroupBox>
#include <QPushButton>
#include <QBoxLayout>
#include <QWidget>
#include <QPlainTextEdit>
#include <QScrollBar>
#include <QProcess>
#include <iostream>


using namespace std;

/*
+------------------------
| mainContainer
| +-----------------------
| | QGroupBox
| | +---------------------
| | | QHBoxLayout
| | | + QPushButton
| | |
| | QPlainTexTEdit
*/

/*! \class Console
 * \brief Class consoel emulation
 *
 * This class implement a console output and start/stop memm process run.
 */
class Console : public QWidget
{
  Q_OBJECT
  public : 
    /*! \brief Constructor */
    Console();

    /*! \brief Constructor
     * \param parent a parent widget
     */
    Console(QWidget *parent);

    /*! \brief Destructor */
    ~Console();

    /*! \brief set program name to execute
     * \param exec program name
     */
    void setExec(QString exec);

    /*! \brief set program parameters
     * \param param a list of paramters
     */
    void setParameters(QStringList param);

    /*! \brief set console can run program
     * \param state a boolean default true, can run program
     */
    void setRunMode(bool state);

  public slots :

    /*! \brief run or stop process */
    void runProcess();

    /*! \brief stop process
     * \param status 0 normal exited, otherwose error
     */
    void stopProcess(int status);

    /*! \brief write process output (std and err) into console*/
    void writeConsole();

  private :

    /*! \brief Clean console */
    void cleanConsole();

    /*! \brief init console with process program and paramters */
    void initConsole();

    /*! \brief write into console
     * \param st string to write into
     */
    void writeConsole(QString st);
    
    bool isRunning;   ///< boolean to know if process is running
    QProcess * process; ///< a process pointer
    QVBoxLayout * mainContainer;  ///< main Console layout
    QGroupBox * buttonContainer;  ///< top button container
    QHBoxLayout * buttonLineContainer;  ///< top button layout
    QPushButton * startStopButton;  ///< RUN/STOP button
    QPlainTextEdit * console; ///< the console

};

#endif
