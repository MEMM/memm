/**************************************************************************************************************
 * memm is a bayesian estimation of pollen dispersal kernel and variance in male fecundity.                   *
 * copyright (c) 2009-2016 etienne klein <etienne.klein@paca.inra.fr>                                         *
 *                                                                                                            *
 * this file is part of memm.                                                                                 *
 *                                                                                                            *
 * memm is free software: you can redistribute it and/or modify                                               *
 * it under the terms of the gnu general public license as published by                                       *
 * the free software foundation, either version 3 of the license, or                                          *
 * (at your option) any later version.                                                                        *
 *                                                                                                            *
 * memm is distributed in the hope that it will be useful,                                                    *
 * but without any warranty; without even the implied warranty of                                             *
 * merchantability or fitness for a particular purpose.  see the                                              *
 * gnu general public license for more details.                                                               *
 *                                                                                                            *
 * you should have received a copy of the gnu general public license                                          *
 * along with memm.  if not, see <http://www.gnu.org/licenses/>.                                              *
 **************************************************************************************************************/

#ifndef __MODELS_PARAM_WIDGET__
#define __MODELS_PARAM_WIDGET__

/** \file ModelsParamWidget
 * \brief Models selector
 * \author Jean-Francois Rey
 * \version 1.0
 * \date 02 Aug 2018
 */

#include <string>
#include <iostream>
#include <QApplication>
#include <QWidget>
#include <QFormLayout>
#include <QLabel>
#include <QComboBox>
#include <QTextEdit>

#include "ConfigXML.hpp"

/**
 * \class ModelsParamWidget
 * \brief a widget containnning Models selector
 */
class ModelsParamWidget : public QWidget
{
  Q_OBJECT
  public :

    /** \breif Constructor
     * \param p : a ConfigXML pointer
     */
    ModelsParamWidget(ConfigXML * p);

    /** \brief destructor
     */
    ~ModelsParamWidget();
    /*!
    * \brief Set "controler-modele"
    * \param p : a ConfigXML object
    */
    void setParam(ConfigXML * p);

    /*!
    * \brief Initialize Widget
    *
    * Initialize widget with ConfigXML value.
    */
    void initValue();

  public slots :

    /*! \brief Change Model value
    * \param v : value
    */
    void ChangeModel(const QString & m);

    signals:

    /*! \brief signal when value changed
     */
    void valueChanged(const QString &);

  private :
    ConfigXML * param_; ///< A ConfigXML pointer object.

    QFormLayout * vLayout_; ///< widget main layout
    QComboBox * models_;
    QTextEdit * description_;

};

#endif
