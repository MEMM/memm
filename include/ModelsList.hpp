#include <string>
#include <map>

using namespace std;

#ifndef __MODELS_LIST__HPP
#define __MODELS_LIST__HPP

#define DEFAULT_CONFIG_MEMM_TYPE "static"
#define DEFAULT_CONFIG_MEMM_MODE "default" 

#define DEFAULT_CONFIG_MEMM_DESC "Model to estimate the pollen dispersal kernel and the variance in male fecundity from spatial and genetic data (microsatellites) concerning adult plants and sampled seeds"

/*! \def DEFAULT_XML_CONFIG_MEMM
 * Constant containing default parameters/settings as XML.
 */
#define DEFAULT_XML_CONFIG_MEMM "<?xml version='1.0' encoding='UTF-8'?>\
<document>\
<!--default MEMM Method and type dynamic to use dynamic parameters-->\
<MEMM mode='default' type='static'>\
    <input>\
        <!-- file containing information about parents -->\
        <file description='parents'>\
            <filename>AlisierPar.txt</filename>\
            <numbers_of_locus>6</numbers_of_locus>\
            <class_covariates>6</class_covariates>\
            <quantitative_covariables>2</quantitative_covariables>\
            <weighting_variables>1</weighting_variables>\
        </file>\
        <!-- file containing information about offspring -->\
        <file description='offspring'>\
            <filename>AlisierDesc.txt</filename>\
            <numbers_of_locus>6</numbers_of_locus>\
        </file>\
        <!-- mode of computation of AF and file containing AF -->\
        <AF enable='true'>\
            <filename>freqall.txt</filename>\
        </AF>\
        <Dist enable='true'>\
          <filename>distfile.txt</filename>\
        </Dist>\
        <LE enable='false'>\
          <filename>LocusError.txt</filename>\
        </LE>\
    </input>\
    <parameters>\
        <!-- individual fecundities distribution -->\
        <individual_fecundities distribution='LN' />\
         <!-- GamA dobs/dep -->\
        <param name='gama' type='double'>\
          <init>2.0000</init>\
          <min>1.0000</min>\
          <max>1000.0000</max>\
          <prior name='powerminusone' type='default'/>\
        </param>\
        <!-- mean dispersal distance delta -->\
        <param name='scale' type='double'>\
          <init>100.0</init>\
          <min>0.0</min>\
          <max>10000.0000</max>\
          <prior name='powerminusone' type='default'/>\
        </param>\
        <!-- shape b -->\
        <param name='shape' type='double'>\
          <init>1.0000</init>\
          <min>0.1</min>\
          <max>10.0000</max>\
          <prior name='powerminusone' type='default'/>\
        </param>\
        <!-- migration rate m -->\
        <param name='mig' type='double'>\
          <init>0.5000</init>\
          <min>0.1000</min>\
          <max>1.0000</max>\
          <prior name='powerminusone' type='default'/>\
        </param>\
        <!-- selfting rate s -->\
        <param name='self' type='double'>\
          <init>0.0500</init>\
          <min>0.0</min>\
          <max>0.1000</max>\
          <prior name='powerminusone' type='default'/>\
        </param>\
        <!-- individual fecundities distribution -->\
        <param name='individual_fecundities' type='distribution'>\
          <law name='gamma'>\
          <law_parameters>\
            <lp type='param' name='gama'>gama</lp>\
            <!--<lp type='double' name='gama'>1.00</lp>-->\
            <!--<lp type='double' name='sigma'>1.00</lp>-->\
            <!--<lp type='double' name='mu'>1.00</lp>-->\
          </law_parameters>\
        </law>\
      </param>\
      <param name='kernel_dispersion' type='model'>\
        <model_parameters>\
          <mp type='param' name='scale'>scale</mp>\
          <mp type='param' name='shape'>shape</mp>\
          </model_parameters>\
      </param>\
    </parameters>\
    <options>\
        <seed>12345</seed>\
        <!-- burn-in iteration -->\
        <burnin>5000</burnin>\
        <ite>50000</ite>\
        <thin>20</thin>\
    </options>\
    <!-- Output file -->\
    <output>\
        <!-- output directory -->\
        <directory>./</directory>\
        <!-- dobs/dep -->\
        <file type='GamA' name='ParamFec.txt' />\
        <!-- Individual fecundities -->\
        <file type='IndFec' name='IndivFec.txt' />\
        <!-- Dispersal parameters -->\
        <file type='Disp' name='ParamDisp.txt' />\
    </output>\
</MEMM>\
</document>"


#define CONFIG_SEEDLING_DESC "Model to estimate individual female and male fecundities, based on genotypes and positions of established seedlings and of their candidate parent plants, and accounting for genotyping error."

#define MEMM_XML_CONFIG_SEEDLING "<?xml version='1.0' encoding='UTF-8'?>\
<document>\
  <MEMM mode='seedlings' type='static'>\
    <input>\
      <!-- file containing information about parents -->\
      <file description='parents'>\
        <filename>parentsSeedlings.txt</filename>\
        <numbers_of_locus>6</numbers_of_locus>\
        <class_covariates>2</class_covariates>\
        <quantitative_covariables>2</quantitative_covariables>\
        <weighting_variables>1</weighting_variables>\
      </file>\
      <!-- file containing information about offspring -->\
      <file description='offspring'>\
        <filename>seedlings.txt</filename>\
        <numbers_of_locus>6</numbers_of_locus>\
      </file>\
      <!-- mode of computation of AF and file containing AF -->\
      <AF enable='true'>\
        <filename>freqall.txt</filename>\
      </AF>\
       <Dist enable='false'>\
         <filename>distfile.txt</filename>\
       </Dist>\
       <LE enable='false'>\
         <filename>LocusError.txt</filename>\
       </LE>\
    </input>\
    <parameters>\
      <!-- GamAs dobs/dep -->\
      <param name='gamas' type='double'>\
        <init>2.000000</init>\
        <min>1.000000</min>\
        <max>1000.000000</max>\
        <prior name='powerminusone' type='default'/>\
      </param>\
      <!-- Pi0 -->\
      <param name='Pi0' type='double'>\
        <init>0.5</init>\
        <min>0.0</min>\
        <max>1.0</max>\
        <prior name='powerminusone' type='default'/>\
      </param>\
      <!-- Pi0s -->\
      <param name='Pi0s' type='double'>\
        <init>0.5</init>\
        <min>0.0</min>\
        <max>1.0</max>\
        <prior name='powerminusone' type='default'/>\
      </param>\
      <!-- mean dispersal distance delta -->\
      <param name='scales' type='double'>\
        <init>100.0</init>\
        <min>0.0</min>\
        <max>10000.0</max>\
        <prior name='powerminusone' type='default'/>\
      </param>\
      <!-- shape b -->\
      <param name='shapes' type='double'>\
        <init>1.0</init>\
        <min>0.1</min>\
        <max>10.0</max>\
        <prior name='powerminusone' type='default'/>\
      </param>\
      <!-- migration rate m -->\
      <param name='migs' type='double'>\
        <init>0.5</init>\
        <min>1.0</min>\
        <max>1.0</max>\
        <prior name='powerminusone' type='default'/>\
      </param>\
       <!-- GamA dobs/dep -->\
      <param name='gama' type='double'>\
        <init>2.000000</init>\
        <min>1.000000</min>\
        <max>1000.000000</max>\
        <prior name='powerminusone' type='default'/>\
      </param>\
      <!-- mean dispersal distance delta -->\
      <param name='scale' type='double'>\
        <init>100.0</init>\
        <min>0.0</min>\
        <max>10000.000000</max>\
        <prior name='powerminusone' type='default'/>\
      </param>\
      <!-- shape b -->\
      <param name='shape' type='double'>\
        <init>1.000000</init>\
        <min>0.100000</min>\
        <max>10.000000</max>\
        <prior name='powerminusone' type='default'/>\
      </param>\
      <!-- migration rate m -->\
      <param name='mig' type='double'>\
        <init>0.500000</init>\
        <min>0.100000</min>\
        <max>1.000000</max>\
        <prior name='powerminusone' type='default'/>\
      </param>\
      <!-- selfting rate s -->\
      <param name='self' type='double'>\
        <init>0.050000</init>\
        <min>0.0</min>\
        <max>0.100000</max>\
        <prior name='powerminusone' type='default'/>\
      </param>\
      <!-- individual fecundities distribution -->\
      <param name='individual_fecundities' type='distribution'>\
        <law name='gamma'>\
          <law_parameters>\
            <lp type='param' name='gama'>gama</lp>\
            <!--<lp type='double' name='gama'>1.00</lp>-->\
            <!--<lp type='double' name='sigma'>1.00</lp>-->\
            <!--<lp type='double' name='mu'>1.00</lp>-->\
          </law_parameters>\
        </law>\
      </param>\
      <!-- individual fecundities distribution -->\
      <param name='individual_fecundities_seeds' type='distribution'>\
        <law name='gamma'>\
          <law_parameters>\
            <lp type='param' name='gama'>gamas</lp>\
          </law_parameters>\
        </law>\
      </param>\
    </parameters>\
    <options>\
      <seed>2154</seed>\
      <!-- burn-in iteration -->\
      <burnin>0</burnin>\
      <ite>50000</ite>\
      <thin>20</thin>\
    </options>\
    <!-- Output file -->\
    <output>\
      <directory>./</directory>\
      <!-- dobs/dep -->\
      <file type='GamA' name='ParamFecSeedlings.txt'/>\
      <!-- Individual fecundities -->\
      <file type='IndFec' name='IndivFecSeedlings.txt'/>\
      <!-- Dispersal parameters -->\
      <file type='Disp' name='ParamDispSeedlings.txt'/>\
    </output>\
  </MEMM>\
</document>"

const map<string,pair<string,string>> models({
    {DEFAULT_CONFIG_MEMM_MODE,{DEFAULT_CONFIG_MEMM_DESC,DEFAULT_XML_CONFIG_MEMM}},
    {"seedlings",{CONFIG_SEEDLING_DESC,MEMM_XML_CONFIG_SEEDLING}},
    });

#endif
