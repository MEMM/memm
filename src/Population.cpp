/**************************************************************************************************************
 * memm is a bayesian estimation of pollen dispersal kernel and variance in male fecundity.                   *
 * copyright (c) 2009-2016 etienne klein <etienne.klein@paca.inra.fr>                                         *
 *                                                                                                            *
 * this file is part of memm.                                                                                 *
 *                                                                                                            *
 * memm is free software: you can redistribute it and/or modify                                               *
 * it under the terms of the gnu general public license as published by                                       *
 * the free software foundation, either version 3 of the license, or                                          *
 * (at your option) any later version.                                                                        *
 *                                                                                                            *
 * memm is distributed in the hope that it will be useful,                                                    *
 * but without any warranty; without even the implied warranty of                                             *
 * merchantability or fitness for a particular purpose.  see the                                              *
 * gnu general public license for more details.                                                               *
 *                                                                                                            *
 * you should have received a copy of the gnu general public license                                          *
 * along with memm.  if not, see <http://www.gnu.org/licenses/>.                                              *
 **************************************************************************************************************/

/*!
* \file Population.cpp
* \brief Populations class
* \author EK
* \version 1.0
* \date 30/10/2019
*/

#include "Population.hpp"

Population::Population(string filename, int nql, int nqt, int nw, int y0, int yobs) :
    filename_(filename), nql_(nql), nqt_(nqt), nw_(nw), y0_(y0), yobs_(yobs)
{
    cout << "Population to be created from xml file : " << filename_ << " | Nquali = " << nql_ << " | Nquanti = " << nqt_<< " | Nweights = " << nw_<< " | Y0 = " << y0_<< " | Yobs = " << yobs_<<endl;
};

Population::~Population()
{};

