/**************************************************************************************************************
 * memm is a bayesian estimation of pollen dispersal kernel and variance in male fecundity.                   *
 * copyright (c) 2009-2016 etienne klein <etienne.klein@paca.inra.fr>                                         *
 *                                                                                                            *
 * this file is part of memm.                                                                                 *
 *                                                                                                            *
 * memm is free software: you can redistribute it and/or modify                                               *
 * it under the terms of the gnu general public license as published by                                       *
 * the free software foundation, either version 3 of the license, or                                          *
 * (at your option) any later version.                                                                        *
 *                                                                                                            *
 * memm is distributed in the hope that it will be useful,                                                    *
 * but without any warranty; without even the implied warranty of                                             *
 * merchantability or fitness for a particular purpose.  see the                                              *
 * gnu general public license for more details.                                                               *
 *                                                                                                            *
 * you should have received a copy of the gnu general public license                                          *
 * along with memm.  if not, see <http://www.gnu.org/licenses/>.                                              *
 **************************************************************************************************************/

/*! \file MEMM_lokLik.cpp
 * \brief MEMM_logLik class implementation
 * \author Jean-François REY
 * \vers//ion 1.0
 * \date 23 Sept 2014
 */

#include "MEMM_logLik_multiSpe_covar.hpp"
#include <ctime>
#include <algorithm>
#include <cstdlib>
#include <boost/math/special_functions/gamma.hpp>

MEMM_logLik_multiSpe_covar::MEMM_logLik_multiSpe_covar() : MEMM_logLik()
{
  cout<< "MEMM : multi-species mode"<<endl;
  Nm = 0;
  Nposterior = 0;
  pLoiClone = NULL;
}

MEMM_logLik_multiSpe_covar::~MEMM_logLik_multiSpe_covar()
{
  if(IDmere.size() != 0) IDmere.clear();
  if(Meres.size() != 0) Meres.clear();
  if(MerDesc.size() != 0) MerDesc.clear();
  if(pLoiClone) delete pLoiClone;
}

void MEMM_logLik_multiSpe_covar::mcmc(int nbIteration, int thinStep)
{
    ofstream cSuivi ("suiviPasAPas.txt");
    int suivi=1;
    int iteStop=50000;
    int trackIndiv=55;
    
    std::stringstream ss;
//    ss << outputDirectory << "/";
//    temp = p->getValue(MEMM_GAMA_FILE_NAME);
//    ss << temp;
    ss.str("");
    ss << outputDirectory << "/Pollen_Limitation.txt";
    PolLimFile = new std::ofstream(ss.str(), ofstream::out);
    
    ss.str("");
    ss << outputDirectory << "/Summary_Statistics_PerMother.txt";
    SummaryStats = new std::ofstream(ss.str(), ofstream::out);
    ss.str("");
    ss << outputDirectory << "/Summary_Statistics_PerFather.txt";
    SummaryStatsAll = new std::ofstream(ss.str(), ofstream::out);

    ss.str("");
    ss << outputDirectory << "/Effective_Numbers_PollenDonors.txt";
    NepFile = new std::ofstream(ss.str(), ofstream::out);

    ss.str("");
    ss << outputDirectory << "/Clone_BreedingValues.txt";
    ofstream CloneGval(ss.str(), ofstream::out);

    double lastFecLogLik, nextFecLogLik;
    double lastFecCLogLik, nextFecCLogLik;
    double previousValue, nextValue, previousValue2;
    long double previousLogLik, nextLogLik;
    long double previousLogLik_SeedSet, nextLogLik_SeedSet;
    vector < double > Vec_Previous(Nvar, 0.);
    vector < double > Vec_BarrTemp(Ntype, 0.);
    vector < vector < long double > > previousDISPPP (NPar, vector < long double >(NPar, 0));
    vector < vector < long double > > previousDISP (Nm, vector < long double >(NPar, 0));
    vector < vector < long double > > previousPHENOPP(NPar, vector < long double >(NPar, 0));
    vector < vector < long double > > previousPHENO(Nm, vector < long double >(NPar, 0));
    double temp;
    
    bool inside;
    bool inRange=true;
    int blocSize = 10;
    int Nbloc = NPar/blocSize;
    vector<double> previousBloc1 (NPar, 0.);
    vector<double> previousFec (blocSize, 0.);
    vector<double> nextFec (blocSize, 0.);
    double ratio=1;
    vector<int> myvector;
    for (int i=0; i<NPar; ++i) myvector.push_back(i); // Vector of indices to generate permuted samples
    srand ( unsigned ( time(0) ) );

    
    
    double Tune=0.1;
    double fineTune=0.01;
//    double gamaTune = Tune;
//    double gamcTune = Tune;
//    double scaleTune = Tune;
//    double shapeTune = Tune;
//    double migTune = Tune;
//    double selfTune = Tune;
//    double bsiTune = Tune;
//    double totseuilaTune = Tune;
//    double totseuillTune = Tune;
//    double blimpolTune = Tune;
//    double deltaopt1Tune = Tune;
//    double deltaopt2Tune = Tune;
//    double sigmapheno1Tune = Tune;
//    double sigmapheno2Tune = Tune;
//    double weight12Tune = Tune;
    double gamaTune = allParameters["gama"]->TUNE;
    double gamcTune = allParameters["Gam_Clone"]->TUNE;
    double scaleTune = allParameters["scale"]->TUNE;
    double shapeTune = allParameters["shape"]->TUNE;
    double migTune = allParameters["mig"]->TUNE;
    double selfTune = allParameters["self"]->TUNE;
    double bsiTune = allParameters["b_Self_Incompat"]->TUNE;
    double totseuilaTune = allParameters["Tot_Seuil_Asta"]->TUNE;
    double totseuillTune = allParameters["Tot_Seuil_Longi"]->TUNE;
    double blimpolTune = allParameters["B_lim_pol"]->TUNE;
    double deltaopt1Tune = allParameters["Delta_opt1"]->TUNE;
    double deltaopt2Tune = allParameters["Delta_opt2"]->TUNE;
    double sigmapheno1Tune = allParameters["Sigma_pheno1"]->TUNE;
    double sigmapheno2Tune = allParameters["Sigma_pheno2"]->TUNE;
    double weight12Tune = allParameters["Weight12"]->TUNE;

    
    accept_ratio gamaRatio = allParameters["gama"]->accept_ratio_;
    accept_ratio gamcRatio = allParameters["Gam_Clone"]->accept_ratio_;
    accept_ratio scaleRatio = allParameters["scale"]->accept_ratio_;
    accept_ratio shapeRatio = allParameters["shape"]->accept_ratio_;
    accept_ratio migRatio = allParameters["mig"]->accept_ratio_;
    accept_ratio selfRatio = allParameters["self"]->accept_ratio_;
    accept_ratio bsiRatio = allParameters["b_Self_Incompat"]->accept_ratio_;
    accept_ratio totseuilaRatio = allParameters["Tot_Seuil_Asta"]->accept_ratio_;
    accept_ratio totseuillRatio = allParameters["Tot_Seuil_Longi"]->accept_ratio_;
    accept_ratio blimpolRatio = allParameters["B_lim_pol"]->accept_ratio_;
    accept_ratio deltaopt1Ratio = allParameters["Delta_opt1"]->accept_ratio_;
    accept_ratio deltaopt2Ratio = allParameters["Delta_opt2"]->accept_ratio_;
    accept_ratio sigmapheno1Ratio = allParameters["Sigma_pheno1"]->accept_ratio_;
    accept_ratio sigmapheno2Ratio = allParameters["Sigma_pheno2"]->accept_ratio_;
    accept_ratio weight12Ratio = allParameters["Weight12"]->accept_ratio_;

    cout << posterior << endl; cout.flush();

    bool gamaFixed, gamcFixed;
    bool scaleFixed, shapeFixed;
    bool migFixed, selfFixed, bsiFixed;
    bool totseuilaFixed, totseuillFixed, blimpolFixed;
    bool deltaopt1Fixed, deltaopt2Fixed, sigmapheno1Fixed, sigmapheno2Fixed, weight12Fixed ;

    gamaFixed = ( gamaRatio(1.0,1.0) == 0 );
    gamcFixed = ( gamcRatio(1.0,1.0) == 0 );
    scaleFixed = ( scaleRatio(1.0,1.0) == 0 );
    shapeFixed = ( shapeRatio(1.0,1.0) == 0 );
    selfFixed = ( selfRatio(1.0,1.0) == 0 );
    bsiFixed = ( bsiRatio(1.0,1.0) == 0 );
    migFixed = ( migRatio(1.0,1.0) == 0 );
    totseuilaFixed = ( totseuilaRatio(1.0,1.0) == 0 );
    totseuillFixed = ( totseuillRatio(1.0,1.0) == 0 );
    blimpolFixed = ( blimpolRatio(1.0,1.0) == 0 );
      deltaopt1Fixed = ( deltaopt1Ratio(1.0,1.0) == 0 );
      deltaopt2Fixed = ( deltaopt2Ratio(1.0,1.0) == 0 );
      sigmapheno1Fixed = ( sigmapheno1Ratio(1.0,1.0) == 0 );
      sigmapheno2Fixed = ( sigmapheno2Ratio(1.0,1.0) == 0 );
      weight12Fixed = ( weight12Ratio(1.0,1.0) == 0 );
    
    int xpourCent = nbIteration/20;
    if(!xpourCent) xpourCent=1;
  
    cout << "Log likelihoods at it=0 : " ;
    cout.flush();

    pLoi->setDParam(MEMM_LOI_GAMA,GamA);
    lastFecLogLik = pLoi->logLik(NPar, Vec_Fec,GamA);
    cout << lastFecLogLik << " " ;cout.flush();
    
    computeDISP(); cout << "Compute DISP OK " ;cout.flush();
    computePHENO(); cout << "Compute PHENO OK " ;cout.flush();
    previousLogLik = compute(); cout << "Compute likelihood OK " ;cout.flush();
    cout << previousLogLik ; cout.flush();

    computeODD(); cout << "Compute ODD OK " ;cout.flush();
    
    lastFecCLogLik=0;
    if (GamC > 0){
    pLoiClone->setDParam(MEMM_LOI_GAMA,GamC);
    lastFecCLogLik = pLoi->logLik(Vec_Fec_Clone, GamC);
    }
    previousLogLik = compute();
    cout << " " << lastFecCLogLik << endl;
    cout.flush();
    
    // output start value
    if(thinStep){
        
        *ParamFec << "iteration  LogLikp(FecClone)  Var(FecClone) LogLikp(FecRamet)  Var(FecRamet)" << endl;
        *IndivFec << "iteration " ;
        for (int p=0; p<NPar; p++) {*IndivFec << "Ep_" << dynamic_cast<parent*>(allParents[p])->getName() << " ";}
        for (int p=0; p<NPar; p++) {*IndivFec << "Fp_" << dynamic_cast<parent*>(allParents[p])->getName() << " ";}
        for (int p=0; p<NPar; p++) {*IndivFec << "Gp_" << dynamic_cast<parent*>(allParents[p])->getName() << " ";}
        *IndivFec << endl;
        
        CloneGval << "iteration ";
        for (int t=0; t<Ntype; t++)
            for(int c=0; c<NClone[t]; c++) {CloneGval << "G_" << t << "_" << c << " ";}
        CloneGval << endl;
        
        *ParamDisp << "iteration  LogLik LL_Genet LL_SeedSet "
        << "Scale  Shape "
        //      << "KappaP  ThetaP "
        << "Mig " << "Self " << "bSI "
        << "Delta_opt1 Delta_opt2 Sigma_pheno1 Sigma_pheno2 Weight12 "
        << "TotSeuilA TotSeuilL Blimpol ";
        for (int v=0; v<Nvar; v++) *ParamDisp  << "V" << v+1 << " ";
        *ParamDisp  << "SxCS SxC SxM SxO ";
        *ParamDisp  << "CSxS CSxC CSxM CSxO ";
        *ParamDisp  << "CxS CxCS CxM CxO ";
        *ParamDisp  << "MxS MxCS MxC MxO ";
        *ParamDisp  << "OxS OxCS OxC OxM";
        if (polSatur == 4 || polSatur == 14 || polSatur == 24 || polSatur == 5 || polSatur == 15 || polSatur == 25 || polSatur == 6 || polSatur == 16 || polSatur == 26) for (int v=0; v<Nvar; v++) *ParamDisp  << " W" << v+1;
        *ParamDisp << endl;
        
        *NepFile << "iteration ";
        for (int p=0; p<NPar; p++)
            {
                //if (SeedSetTot[p]>0) *PolLimFile << "Qpol_" << dynamic_cast<parent*>(allParents[p])->getName() << " " << "Ppol_" << dynamic_cast<parent*>(allParents[p])->getName() << " ";
                *NepFile << "Nep_" << dynamic_cast<parent*>(allParents[p])->getName() << " ";
            }
        *PolLimFile << endl;
        PolLimFile->flush();
        *NepFile << endl;
        NepFile->flush();
        *SummaryStats << "ID MeanDist Nep QAutof_pool QAllof_pool QAutof_tubes QAllof_tubes QAutof_seeds QAllof_seeds ";
        *SummaryStatsAll << "ID MeanDist Nep QAutof_pool QAllof_pool QAutof_tubes QAllof_tubes QAutof_seeds QAllof_seeds ";
        for (int t=0; t<Ntype; t++) *SummaryStats << "Q" << t << "_pool ";
        for (int t=0; t<Ntype; t++) *SummaryStats << "Q" << t << "_tubes ";
        for (int t=0; t<Ntype; t++) *SummaryStats << "Q" << t << "_seeds ";
        for (int t=0; t<Ntype; t++) *SummaryStatsAll << "Q" << t << "_pool ";
        for (int t=0; t<Ntype; t++) *SummaryStatsAll << "Q" << t << "_tubes ";
        for (int t=0; t<Ntype; t++) *SummaryStatsAll << "Q" << t << "_seeds ";
        *SummaryStats << "Q_Asta Q_Semi Q_Longi";
        *SummaryStats << endl;
        SummaryStats->flush();
        *SummaryStatsAll << endl;
        SummaryStatsAll->flush();

        *ParamFec << "0 " << lastFecCLogLik << " " << GamC << " " << lastFecLogLik << " " << GamA << endl;
        ParamFec->flush();
        *IndivFec << "0 " ;
        for (int p=0; p<NPar; p++) {*IndivFec << Vec_Fec[p] << " ";}
        for (int p=0; p<NPar; p++) {*IndivFec << Vec_Fec_All[p]  << " ";}
        for (int p=0; p<NPar; p++) {*IndivFec << Vec_Fec_Clone[Type[p]][Clone[p]] << " ";}
        *IndivFec << endl;
        
        CloneGval << "0 ";
        for (int t=0; t<Ntype; t++)
            for(int c=0; c<NClone[t]; c++) {CloneGval << Vec_Fec_Clone[t][c] << " ";}
        CloneGval << endl;
        CloneGval.flush();
        
        *ParamDisp << "0 " << previousLogLik
        << " " << logLik_Genet() << " " << logLik_SeedSet()
        << " " << Scale
        << " " << Shape
        //        << " " << Kappa << " " << ThetaP
        << " " << Mig << " " << Self << " " << bSI
        << " " << Delta_opt1 << " " << Delta_opt2 << " " << Sigma_pheno1 << " " << Sigma_pheno2 << " " << Weight12
        << " " << TotSeuilA
        << " " << TotSeuilL
        << " " << Blimpol;
        for ( int v=0; v<Nvar; v++) *ParamDisp << " " << Vec_FixedEff[0][v];
        for ( int t=0; t<Ntype; t++)
            for ( int tt=0; tt<Ntype; tt++)
                if (t!=tt)
                *ParamDisp << " " << mat_Barr[t][tt];
        if (polSatur == 4 || polSatur == 14 || polSatur == 24 || polSatur == 5 || polSatur == 15 || polSatur == 25 || polSatur == 6 || polSatur == 16 || polSatur == 26) for (int v=0; v<Nvar; v++) *ParamDisp  << " " << Vec_FixedEffSS[0][v];
        *ParamDisp << endl;
        ParamDisp->flush();
    }
    
    
    if(nbIteration) cout << "it=1..."<< std::endl;
    for(int ite=1; ite<=nbIteration; ite++)
    {
        if (ite % xpourCent == 0) {cout << "it="<<ite << "..."<< std::endl;}
        
        if(thinStep && (ite % (thinStep * 100) == 0)) previousLogLik=compute(posterior);
        else previousLogLik=compute();
        
        // GamC - fecundities distribution
        if( !gamcFixed )
        {
            nextValue = GamC*exp(gamcTune*gauss());
            if( nextValue>mGamC && nextValue<MGamC )
            {
                lastFecCLogLik = pLoiClone->logLik(Vec_Fec_Clone, GamC);
                nextFecCLogLik = pLoiClone->logLik(Vec_Fec_Clone, nextValue);
                if (suivi>0 && ite<iteStop) cSuivi << ite << " GamE " << nextValue << " "  << lastFecCLogLik << " " << nextFecCLogLik << " " << nextFecCLogLik-lastFecCLogLik << " ";
                if( accept() < exp(nextFecCLogLik-lastFecCLogLik) * gamcRatio(GamC,nextValue))
                {
                    //cerr<<"New GamA "<<nextValue<<endl;
                    GamC = nextValue;
                    lastFecCLogLik = nextFecCLogLik;
                    pLoiClone->setDParam(MEMM_LOI_GAMA,GamC);
                    if (suivi>0 && ite<iteStop) cSuivi << "Accept" << endl;
                }
                else if (suivi>0 && ite<iteStop) cSuivi << "Reject" << endl;
            }
        }
        //if(thinStep && (ite % thinStep == 0))*ParamFec << ite << " " << lastFecLogLik << " " << GamA << endl;
        if(thinStep && (ite % thinStep == 0))
        {
            //cerr<< ite << " " << lastFecLogLik << " " << GamA << endl;
            *ParamFec << ite << " " << lastFecCLogLik << " " << GamC ;//<< endl;
            ParamFec->flush();
        }
        
        
        // GamA - fecundities distribution - Variance of fecundities
        //    nextValue = pow(GamA, exp(0.2*gauss()));
        if( !gamaFixed )
        {
            nextValue = GamA*exp(gamaTune*gauss());
            if( nextValue>mGamA && nextValue<MGamA )
            {
                lastFecLogLik = pLoi->logLik(NPar, Vec_Fec,GamA);
                nextFecLogLik = pLoi->logLik(NPar, Vec_Fec, nextValue);
                if (suivi>0 && ite<iteStop) cSuivi << ite << " GamF " << nextValue << " "  << lastFecLogLik << " " << nextFecLogLik << " " << nextFecLogLik-lastFecLogLik << " ";
                if( accept() < exp(nextFecLogLik-lastFecLogLik) * gamaRatio(GamA,nextValue))
                {
                    //cerr<<"New GamA "<<nextValue<<endl;
                    GamA = nextValue;
                    lastFecLogLik = nextFecLogLik;
                    pLoi->setDParam(MEMM_LOI_GAMA,GamA);
                    if (suivi>0 && ite<iteStop) cSuivi << "Accept" << endl;
                }
                else if (suivi>0 && ite<iteStop) cSuivi << "Reject" << endl;
            }
        }
        //if(thinStep && (ite % thinStep == 0))*ParamFec << ite << " " << lastFecLogLik << " " << GamA << endl;
        if(thinStep && (ite % thinStep == 0))
        {
            //cerr<< ite << " " << lastFecLogLik << " " << GamA << endl;
            //          *ParamFec << ite << " " << lastFecLogLik << " " << GamA << endl;
            *ParamFec << " " << lastFecLogLik << " " << GamA << endl;
            ParamFec->flush();
        }
        
        // Change male fecundities // Clone level
        // previousLogLik = compute();
        if( GamC > 0 )
        {
            for(int t=0; t<Ntype; t++)
                for(int c=0; c<NClone[t] ; c++)
                {
                    previousValue = Vec_Fec_Clone[t][c];
                    pLoiClone->tirage(nextValue);
                    Vec_Fec_Clone[t][c] = nextValue;
                    for (int p=0; p<CloneList[t][c].size(); p++) Vec_Fec_All[CloneList[t][c][p]] *= Vec_Fec_Clone[t][c]/previousValue;
                    nextLogLik = compute();
                    if (suivi>0 && ite<iteStop && t==Type[trackIndiv] && c==Clone[trackIndiv]) cSuivi << ite << " E(" << trackIndiv << ") " << previousValue << " " << nextValue << " "  << previousLogLik << " " << nextLogLik << " " << nextLogLik-previousLogLik << " ";
                    if( accept() < exp(nextLogLik-previousLogLik))
                    {
                        previousLogLik = nextLogLik;
                        if (suivi>0 && ite<iteStop && t==Type[trackIndiv] && c==Clone[trackIndiv]) cSuivi << "Accept " << Vec_Fec_All[trackIndiv] << endl;
                    }
                    else
                    {
                        for (int p=0; p<CloneList[t][c].size(); p++) Vec_Fec_All[CloneList[t][c][p]] *= previousValue/Vec_Fec_Clone[t][c];
                        Vec_Fec_Clone[t][c] = previousValue;
                        if (suivi>0 && ite<iteStop && t==Type[trackIndiv] && c==Clone[trackIndiv]) cSuivi << "Reject " << Vec_Fec_All[trackIndiv] << endl;
                    }
                }
        }
        
        // Change male fecundities // Ramet level
        //previousLogLik = compute();
        if( !gamaFixed || (GamA != 0)) {
            random_shuffle ( myvector.begin(), myvector.end() );
            for (int bloc=0; bloc<Nbloc; bloc++)
            {
                inside = true;
                bool track = false;
                previousFec.clear();
                nextFec.clear();
                ratio=1;
                for (vector<int>::iterator it=myvector.begin()+bloc*blocSize; it!=myvector.begin()+(bloc+1)*blocSize; ++it) {
                    previousBloc1[*it] = Vec_Fec[*it];
                    previousFec.push_back(Vec_Fec[*it]);
                    Vec_Fec[*it] *= exp(Tune*gauss());
                    Vec_Fec_All[*it] *= Vec_Fec[*it]/previousBloc1[*it];
                    inside = inside && (Vec_Fec[*it]<10000) && (Vec_Fec[*it]>0.0000001);
                    ratio *= Vec_Fec[*it]/previousBloc1[*it];
                    nextFec.push_back(Vec_Fec[*it]);
                    if (*it == trackIndiv) track = true;
                    if (suivi>0 && ite<iteStop && *it == trackIndiv) cSuivi << ite << " Vec_Fec(" << *it << ") "  << previousBloc1[*it] << " " << Vec_Fec[*it] << " ";
                }
                
                if (inside) {
                    nextLogLik = compute();
                    if (suivi>0 && ite<iteStop && track) cSuivi << bloc << " "  << previousLogLik << " " << nextLogLik << " " << nextLogLik-previousLogLik << " ";
                    if(accept()< ratio*exp(nextLogLik - previousLogLik +
                                           pLoi->logLik(blocSize, nextFec, GamA) - pLoi->logLik(blocSize, previousFec, GamA)))
                    { previousLogLik = nextLogLik;
                        if (suivi>0 && ite<iteStop && track) cSuivi << "Accept " << Vec_Fec_All[trackIndiv] << endl;
                    }
                    else
                    {
                        for (vector<int>::iterator it=myvector.begin()+bloc*blocSize; it!=myvector.begin()+(bloc+1)*blocSize; ++it) {
                            Vec_Fec_All[*it] *= previousBloc1[*it]/Vec_Fec[*it];
                            Vec_Fec[*it] = previousBloc1[*it];
                        }
                        if (suivi>0 && ite<iteStop && track) cSuivi << "Reject " << Vec_Fec_All[trackIndiv] << endl;
                    }
                }
                else
                {
                    for (vector<int>::iterator it=myvector.begin()+bloc*blocSize; it!=myvector.begin()+(bloc+1)*blocSize; ++it) {
                        Vec_Fec_All[*it] *= previousBloc1[*it]/Vec_Fec[*it];
                        Vec_Fec[*it] = previousBloc1[*it];
                    }
                    if (suivi>0 && ite<iteStop && track) cSuivi << "Outside " << Vec_Fec_All[trackIndiv] << endl;
                }
            }
            
            if(thinStep && (ite % thinStep == 0))
            {
                *IndivFec << ite << " " ;
                for (int p=0; p<NPar; p++) {*IndivFec << Vec_Fec[p] << " ";}
                for (int p=0; p<NPar; p++) {*IndivFec << Vec_Fec_All[p] << " ";}
                for (int p=0; p<NPar; p++) {*IndivFec << Vec_Fec_Clone[Type[p]][Clone[p]] << " ";}
                *IndivFec << endl;
                IndivFec->flush();
                
                CloneGval << ite << " ";
                for (int t=0; t<Ntype; t++)
                    for(int c=0; c<NClone[t]; c++) {CloneGval << Vec_Fec_Clone[t][c] << " ";}
                CloneGval << endl;
                CloneGval.flush();
            }
        }
        
        // change Scale
        if( !scaleFixed )
        {
            previousValue = Scale;
            Scale = previousValue*exp(scaleTune*gauss());
            
            if( Scale>mScale && Scale<MScale)
            {
                previousDISPPP = DISPPP;
                previousDISP = DISP;
                computeDISP();
                nextLogLik = compute();
                if (suivi>0 && ite<iteStop) cSuivi << ite << " MaleParam1 " << Scale << " "  << previousLogLik << " " << nextLogLik << " " << nextLogLik-previousLogLik << " ";
                if( accept() < exp(nextLogLik-previousLogLik) * scaleRatio(previousValue,Scale) )
                {
                    previousLogLik = nextLogLik;
                    if (suivi>0 && ite<iteStop) cSuivi << "Accept" << endl;
                }
                else
                {
                    Scale = previousValue;
                    a=Scale*exp(gammln(2/Shape)-gammln(3/Shape));
                    ab=-pow(a,-Shape);
                    DISPPP = previousDISPPP;
                    DISP = previousDISP;
                    if (suivi>0 && ite<iteStop) cSuivi << "Reject" << endl;
                }
            }
            else Scale = previousValue;
        }
        
        // change Shape
        if( !shapeFixed )
        {
            previousValue = Shape;
            Shape = previousValue*exp(shapeTune*gauss());
            if( Shape>mShape && Shape<MShape)
            {
                previousDISPPP = DISPPP;
                previousDISP = DISP;
                computeDISP();
                nextLogLik = compute();
                if (suivi>0 && ite<iteStop) cSuivi << ite << " MaleParam2 " << Shape << " "  << previousLogLik << " " << nextLogLik << " " << nextLogLik-previousLogLik << " ";
                if( accept() < exp(nextLogLik-previousLogLik) * shapeRatio(previousValue,Shape) )
                {
                    previousLogLik = nextLogLik;
                    if (suivi>0 && ite<iteStop) cSuivi << "Accept" << endl;
                }
                else{
                    Shape = previousValue;
                    a=Scale*exp(gammln(2/Shape)-gammln(3/Shape));
                    ab=-pow(a,-Shape);
                    DISPPP = previousDISPPP;
                    DISP = previousDISP;
                    if (suivi>0 && ite<iteStop) cSuivi << "Reject" << endl;
                }
            }
            else Shape = previousValue;
        }
        
        // change Mig
        if( !migFixed )
        {
            previousValue = Mig;
            Mig = previousValue*exp(migTune*gauss());
            if( Mig>mMig && Mig<MMig)
            {
                nextLogLik = compute();
                if (suivi>0 && ite<iteStop) cSuivi << ite << " Mig_Pollen " << Mig << " "  << previousLogLik << " " << nextLogLik << " " << nextLogLik-previousLogLik << " ";
                if( accept() < exp(nextLogLik-previousLogLik) * migRatio(previousValue,Mig))
                {
                    previousLogLik = nextLogLik;
                    if (suivi>0 && ite<iteStop) cSuivi << "Accept" << endl;
                }
                else
                {
                    Mig = previousValue;
                    if (suivi>0 && ite<iteStop) cSuivi << "Reject" << endl;
                }
            }
            else Mig = previousValue;
        }
        
             // change Self (In models 6/16/26, Self is used for pre-zygotic barrier of auto pollen
             if( !selfFixed  )
             {
                 previousValue = Self;
                 Self = previousValue * exp(selfTune*gauss());
                 if( Self>mSelf && Self<MSelf )
                 {
     //                previousDISPPP = DISPPP;
     //                previousDISP = DISP;
     //                computeDISP();
                     nextLogLik = compute();
                     if (suivi>0) cSuivi << ite << " SelfingRate " << Self << " "  << previousLogLik << " " << nextLogLik << " " << nextLogLik-previousLogLik << " ";
                     if( accept() < exp(nextLogLik-previousLogLik) * selfRatio(previousValue,Self) )
                     {
                         previousLogLik = nextLogLik;
                         if (suivi>0) cSuivi << "Accept" << endl;
                     }
                     else
                     {
                         Self = previousValue;
     //                    DISPPP = previousDISPPP;
     //                    DISP = previousDISP;
                         if (suivi>0) cSuivi << "Reject" << endl;
                     }
                 }
                 else {
                     Self = previousValue;
                 }
             }

        // change bSI (Self is now computed from the local intergration of the kernel
        if( !bsiFixed )
        {
            previousValue2 = bSI;
            bSI = previousValue2*exp(bsiTune*gauss());
            if( bSI>mbSI && bSI<MbSI)
            {
                previousDISPPP = DISPPP;
                previousDISP = DISP;
                computeDISP();
                nextLogLik = compute();
                if (suivi>0 && ite<iteStop) cSuivi << ite << " Self-incompat " << bSI << " "  << previousLogLik << " " << nextLogLik << " " << nextLogLik-previousLogLik << " ";
                if( accept() < exp(nextLogLik-previousLogLik) * bsiRatio(previousValue,bSI) )
                {
                    previousLogLik = nextLogLik;
                    if (suivi>0 && ite<iteStop) cSuivi << "Accept" << endl;
                }
                else
                {
                    bSI = previousValue2;
                    DISPPP = previousDISPPP;
                    DISP = previousDISP;
                    if (suivi>0 && ite<iteStop) cSuivi << "Reject" << endl;
                }
            }
            else {
                bSI = previousValue2;
            }
        }

        
        //        EK - 2020/02    //////////////////////////////////
        //        Ajout des effets fixes des covariables
        //        //////////////////////////////////////////////////

        // change Pheno1
        if( !deltaopt1Fixed || !sigmapheno1Fixed )
        {
            previousValue = Delta_opt1;
            previousValue2 = Sigma_pheno1;
            Delta_opt1 = previousValue + deltaopt1Tune*gauss();
            Sigma_pheno1 = previousValue2*exp(sigmapheno1Tune*gauss());
            if( Delta_opt1>mDelta_opt1 && Delta_opt1<MDelta_opt1 && Sigma_pheno1>mSigma_pheno1 && Sigma_pheno1<MSigma_pheno1 )
            {
                previousPHENOPP = PHENOPP;
                previousPHENO = PHENO;
                computePHENO();
                nextLogLik = compute();
                if (suivi>0 && ite<iteStop) cSuivi << ite << " Pheno1 " << Delta_opt1 << " "  << previousLogLik << " " << nextLogLik << " " << nextLogLik-previousLogLik << " ";
                if( accept() < exp(nextLogLik-previousLogLik) /* * deltaopt1Ratio(previousValue,Delta_opt1) */ * sigmapheno1Ratio(previousValue,Sigma_pheno1))
                {
                    previousLogLik = nextLogLik;
                    if (suivi>0 && ite<iteStop) cSuivi << "Accept" << endl;
                }
                else{
                    Delta_opt1 = previousValue;
                    Sigma_pheno1 = previousValue2;
                    PHENOPP = previousPHENOPP;
                    PHENO = previousPHENO;
                    if (suivi>0 && ite<iteStop) cSuivi << "Reject" << endl;

                }
            }
            else {
                Delta_opt1 = previousValue;
                Sigma_pheno1 = previousValue2;
            }
        }
         
        // change Pheno2
        if( !deltaopt2Fixed || !sigmapheno2Fixed )
        {
            previousValue = Delta_opt2;
            previousValue2 = Sigma_pheno2;
            Delta_opt2 = previousValue + deltaopt2Tune*gauss();
            Sigma_pheno2 = previousValue2*exp(sigmapheno2Tune*gauss());
            if( Delta_opt2>mDelta_opt1 && Delta_opt2<MDelta_opt2 && Sigma_pheno2>mSigma_pheno2 && Sigma_pheno2<MSigma_pheno2 )
            {
                previousPHENOPP = PHENOPP;
                previousPHENO = PHENO;
                computePHENO();
                nextLogLik = compute();
                if (suivi>0 && ite<iteStop) cSuivi << ite << " Pheno2 " << Delta_opt2 << " "  << previousLogLik << " " << nextLogLik << " " << nextLogLik-previousLogLik << " ";
                if( accept() < exp(nextLogLik-previousLogLik) /* * deltaopt2Ratio(previousValue,Delta_opt2) */ * sigmapheno2Ratio(previousValue,Sigma_pheno2))
                {
                    previousLogLik = nextLogLik;
                    if (suivi>0 && ite<iteStop) cSuivi << "Accept" << endl;
                }
                else{
                    PHENOPP = previousPHENOPP;
                    PHENO = previousPHENO;
                    Delta_opt2 = previousValue;
                    Sigma_pheno2 = previousValue2;
                    if (suivi>0 && ite<iteStop) cSuivi << "Reject" << endl;
                }
            }
            else {
                Delta_opt2 = previousValue;
                Sigma_pheno2 = previousValue2;
            }
        }
        
        // change Weight12
         if( !weight12Fixed )
         {
             previousValue = Weight12;
             Weight12 = previousValue*exp(weight12Tune*gauss());
             if( Weight12>mWeight12 && Weight12<MWeight12)
             {
                 previousPHENOPP = PHENOPP;
                 previousPHENO = PHENO;
                 computePHENO();
                 nextLogLik = compute();
                 if (suivi>0 && ite<iteStop) cSuivi << ite << " PhenoW " << Weight12 << " "  << previousLogLik << " " << nextLogLik << " " << nextLogLik-previousLogLik << " ";
                 if( accept() < exp(nextLogLik-previousLogLik) * weight12Ratio(previousValue,Weight12) )
                 {
                     previousLogLik = nextLogLik;
                     if (suivi>0 && ite<iteStop) cSuivi << "Accept" << endl;

                 }
                 else
                 {
                     PHENOPP = previousPHENOPP;
                     PHENO = previousPHENO;
                     Weight12 = previousValue;
                     if (suivi>0 && ite<iteStop) cSuivi << "Reject" << endl;

                  }
             }
             else Weight12 = previousValue;
         }

        
        //        EK - 2020/02    //////////////////////////////////
        //        Ajout des effets fixes des covariables
        //        //////////////////////////////////////////////////
        
        //        RJMCMC add and remove fixed effects on Vec_Fec
        if ( !gamaFixed || GamA>0.00001)
        {
            int dim = 0;
            for (int v=0; v<Nvar; v++){
                if ( (PriorIn_FixedEff[dim][v] > 0.0) && (PriorIn_FixedEff[dim][v] <1.0)  && (In_FixedEff[dim][v]))
                {
                    In_FixedEff[dim][v]=false;
                    previousValue = Vec_FixedEff[dim][v];
                    Vec_FixedEff[dim][v]=0;
                    for (int m=0; m<NPar; m++) Vec_Fec_All[m] *= exp(-previousValue * Covar[m][v]);
                    nextLogLik = compute();
                    if( accept() < exp(nextLogLik-previousLogLik) ) previousLogLik = nextLogLik;
                    else
                    {
                        In_FixedEff[dim][v]=true;
                        Vec_FixedEff[dim][v] = previousValue;
                        for (int m=0; m<NPar; m++) Vec_Fec_All[m] *= exp(previousValue * Covar[m][v]);
                    }
                }
                else if ( (PriorIn_FixedEff[dim][v] > 0.0) && (PriorIn_FixedEff[dim][v] <1.0) )
                {
                    In_FixedEff[dim][v]=true;
                    Vec_FixedEff[dim][v]= 0.1 + 0.1 * gauss();
                    for (int m=0; m<NPar; m++) Vec_Fec_All[m] *= exp( Vec_FixedEff[dim][v] * Covar[m][v]);
                    nextLogLik = compute();
                    if( accept() < exp(nextLogLik-previousLogLik) ) previousLogLik = nextLogLik;
                    else
                    {
                        In_FixedEff[dim][v]=false;
                        Vec_FixedEff[dim][v] = 0;
                        for (int m=0; m<NPar; m++) Vec_Fec_All[m] *= exp( -Vec_FixedEff[dim][v] * Covar[m][v]);
                    }
                }
            }
        }
        
        //        change the fixed effects on Vec_Fec
        if ( !gamaFixed || GamA>0.00001)
        {
            int dim = 0;
            inRange=true;
            for (int v=0; v<Nvar; v++){
                Vec_Previous[v]=Vec_FixedEff[dim][v];
                if ((PriorIn_FixedEff[dim][v] > 0) && (In_FixedEff[dim][v]))
                {
                    Vec_FixedEff[dim][v] += vecfixedeffTune[dim][v] * gauss();
                    inRange = inRange && (Vec_FixedEff[dim][v] > mFixedEff[dim][v]) && (Vec_FixedEff[dim][v] < MFixedEff[dim][v]);
                    //                ratio *= covarRatio[dim][v](Vec_Previous[v], Vec_FixedEff[dim][v])
                }
            }
            if (inRange)
            {
                for (int m=0; m<NPar; m++)
                {
                    temp = 0;
                    for (int v=0; v<Nvar; v++) temp += Vec_FixedEff[dim][v] * Covar[m][v];
                    Vec_Fec_All[m] = Vec_Fec[m] * exp( temp );
                }
                nextLogLik = compute();
                if (suivi>0 && ite<iteStop)
                {cSuivi << ite << " CovEffect ";
                    for (int v=0; v<Nvar; v++) cSuivi << Vec_Previous[v] << " ";
                    cSuivi << "|";
                    for (int v=0; v<Nvar; v++) cSuivi << " " << Vec_FixedEff[dim][v];
                    cSuivi << previousLogLik << " " << nextLogLik << " " << nextLogLik-previousLogLik << " ";
                }
                if( accept() < exp(nextLogLik-previousLogLik) )
                    {previousLogLik = nextLogLik;
                        if (suivi>0 && ite<iteStop) cSuivi << "Accept" << endl;}
                else
                {
                    for (int v=0; v<Nvar; v++) Vec_FixedEff[dim][v] = Vec_Previous[v];
                    for (int m=0; m<NPar; m++)
                    {
                        temp = 0;
                        for (int v=0; v<Nvar; v++) temp += Vec_FixedEff[dim][v] * Covar[m][v];
                        Vec_Fec_All[m] = Vec_Fec[m] * exp( temp );
                    }
                    if (suivi>0 && ite<iteStop) cSuivi << "Reject" << endl;
                }
            }
            else
            {
                for (int v=0; v<Nvar; v++) Vec_FixedEff[dim][v] = Vec_Previous[v];
                if (suivi>0 && ite<iteStop) cSuivi << "Reject" << endl;
            }
        }

        
        //        EK - 2020/02    //////////////////////////////////
        //        Ajout des barrières entre espèces-types
        //        //////////////////////////////////////////////////
        
        //        RJMCMC add and remove fixed effects on mat_Barr

        for (int t=0; t<Ntype-1; t++)
            for (int tt=0; tt<Ntype; tt++)
                if (t!=tt) {
                    if ( (PriorIn_Barr[t][tt] > 0.0) && (PriorIn_Barr[t][tt] <1.0)  && (In_Barr[t][tt]))
                    {
                        In_Barr[t][tt]=false;
                        previousValue = mat_Barr[t][tt];
                        mat_Barr[t][tt]=0;
                        nextLogLik = compute();
                        if (suivi>0 && ite<iteStop) cSuivi << ite << " Barrier_RJ " << t << "-" << tt <<  " "  << previousLogLik << " " << nextLogLik << " " << nextLogLik-previousLogLik << " ";
                        if( accept() < exp(nextLogLik-previousLogLik) )
                        {
                            previousLogLik = nextLogLik;
                            if (suivi>0 && ite<iteStop) cSuivi << "Accept" << endl;

                        }
                        else
                        {
                            In_Barr[t][tt]=true;
                            mat_Barr[t][tt] = previousValue;
                            if (suivi>0 && ite<iteStop) cSuivi << "Reject" << endl;

                        }
                    }
                    else if ( (PriorIn_Barr[t][tt] > 0.0) && (PriorIn_Barr[t][tt] <1.0) )
                    {
                        In_Barr[t][tt]=true;
                        mat_Barr[t][tt]= qMean_Barr[t][tt] + qSD_Barr[t][tt] * gauss();
                        nextLogLik = compute();
                        if (suivi>0 && ite<iteStop) cSuivi << ite << " Barrier_RJ " << t << "-" << tt <<  " "  << previousLogLik << " " << nextLogLik << " " << nextLogLik-previousLogLik << " ";
                        if( accept() < exp(nextLogLik-previousLogLik) )
                        {
                            previousLogLik = nextLogLik;
                            if (suivi>0 && ite<iteStop) cSuivi << "Accept" << endl;
                        }
                        else
                        {
                            In_Barr[t][tt]=false;
                            mat_Barr[t][tt] = 0;
                            if (suivi>0 && ite<iteStop) cSuivi << "Reject" << endl;
                        }
                    }
                }
        

        //        change the barriers values on mat_Barr : interaction version
        
        for (int t=0; t<Ntype; t++){
            Vec_BarrTemp=mat_Barr[t];
            inRange=true;
            if (suivi>0 && ite<iteStop) cSuivi << ite << " Barrier_values" << t << " ";
            for(int tt=0; tt<Ntype; tt++)
                if (tt != t)
                {
                    if ((PriorIn_Barr[t][tt] > 0) && (In_Barr[t][tt]))
                    {
                        mat_Barr[t][tt] *= exp( matbarrTune[t][tt] * gauss());
                        inRange = inRange && (mat_Barr[t][tt] > mBarr[t][tt]) && (mat_Barr[t][tt] < MBarr[t][tt]);
                        if (suivi>0 && ite<iteStop) cSuivi << mat_Barr[0][1] <<  " " << inRange << " ";
                    }
                }
            if (inRange)
            {
                nextLogLik = compute();
                if (suivi>0 && ite<iteStop) cSuivi << previousLogLik << " " << nextLogLik << " " << nextLogLik-previousLogLik << " ";
                if( accept() < exp(nextLogLik-previousLogLik) )
                {previousLogLik = nextLogLik;
                    if (suivi>0 && ite<iteStop) cSuivi << "Accept" << endl;
                }
                else
                {
                    mat_Barr[t] = Vec_BarrTemp;
                    if (suivi>0 && ite<iteStop) cSuivi << "Reject" << endl;

                }
            }
            else
            {
                mat_Barr[t] = Vec_BarrTemp;
                if (suivi>0 && ite<iteStop) cSuivi << "OutsideRange" << endl;
            }
        }

//        //        change the barriers values on mat_Barr : main effect version
//        bool inRange=true;
//        for (int t=0; t<Ntype; t++){
//            Vec_BarrTemp=mat_Barr[t];
//                    if ((PriorIn_Barr[t][0] > 0) && (In_Barr[t][0]))
//                    {
//                        mat_Barr[t][0] *= exp( matbarrTune[t][0] * gauss());
//                        inRange = inRange && (mat_Barr[t][0] > mBarr[t][0]) && (mat_Barr[t][0] < MBarr[t][0]);
//                    }
//            if (inRange)
//            {
//                for (int tt=1; tt<Ntype; tt++){ mat_Barr[t][tt] = mat_Barr[t][0]; }
//                nextLogLik = compute();
//                if (suivi>0 && ite<iteStop) cSuivi << ite << " Barrier_values " << mat_Barr[t][0] <<  " "  << previousLogLik << " " << nextLogLik << " " << nextLogLik-previousLogLik << " ";
//                if( accept() < exp(nextLogLik-previousLogLik) )
//                {previousLogLik = nextLogLik;
//                    if (suivi>0 && ite<iteStop) cSuivi << "Accept" << endl;
//                }
//                else
//                {
//                    for (int tt=1; tt<Ntype; tt++){ mat_Barr[t][tt] = Vec_BarrTemp[0]; }
//                    if (suivi>0 && ite<iteStop) cSuivi << "Reject" << endl;
//                }
//            }
//            else
//            {
//                mat_Barr[t][0] = Vec_BarrTemp[0];
//            }
//        }

        
        
        if (suivi>0 && ite<iteStop) {
            for (int t=0; t<Ntype; t++)
                for(int tt=0; tt<Ntype; tt++)
                    cSuivi << mat_Barr[t][tt] << " " ;
            cSuivi << endl;
        }

        
        //        EK - 2020/02    //////////////////////////////////
        //        Ajout des observations de limitation pollinique
        //        //////////////////////////////////////////////////
        
//        previousLogLik_SeedSet = logLik_SeedSet();
        previousLogLik_SeedSet = compute();
        // change TotSeuilA
        if( !totseuilaFixed )
        {
            previousValue = TotSeuilA;
            TotSeuilA = previousValue + totseuilaTune * gauss();
            if( TotSeuilA>mTotSeuilA && TotSeuilA<MTotSeuilA)
            {
//                nextLogLik_SeedSet = logLik_SeedSet();
                nextLogLik_SeedSet = compute();
                if (suivi>0 && ite<iteStop) cSuivi << ite << " TotSeuilA " << TotSeuilA <<  " "  << previousLogLik_SeedSet << " " << nextLogLik_SeedSet << " " << nextLogLik_SeedSet-previousLogLik_SeedSet << " ";
                if( accept() < exp(nextLogLik_SeedSet-previousLogLik_SeedSet) /* * totseuilaRatio(previousValue,TotSeuilA) */)
                {
                    previousLogLik_SeedSet = nextLogLik_SeedSet;
                    if (suivi>0 && ite<iteStop) cSuivi << "Accept" << endl;
                }
                else
                {
                    TotSeuilA = previousValue;
                    if (suivi>0 && ite<iteStop) cSuivi << "Reject" << endl;
                }
            }
            else TotSeuilA = previousValue;
        }
        
        if( !totseuillFixed )
        {
            previousValue = TotSeuilL;
            TotSeuilL =  previousValue + totseuillTune*gauss();
            if( TotSeuilL>mTotSeuilL && TotSeuilL<MTotSeuilL)
            {
                //                nextLogLik_SeedSet = logLik_SeedSet();
                nextLogLik_SeedSet = compute();
                if (suivi>0 && ite<iteStop) cSuivi << ite << " TotSeuilL " << TotSeuilL <<  " "  << previousLogLik_SeedSet << " " << nextLogLik_SeedSet << " " << nextLogLik_SeedSet-previousLogLik_SeedSet << " ";
                if( accept() < exp(nextLogLik_SeedSet-previousLogLik_SeedSet) /* * totseuillRatio(previousValue,TotSeuilL) */ )
                {
                    previousLogLik_SeedSet = nextLogLik_SeedSet;
                    if (suivi>0 && ite<iteStop) cSuivi << "Accept" << endl;

                }
                else
                {
                    TotSeuilL = previousValue;
                    if (suivi>0 && ite<iteStop) cSuivi << "Reject" << endl;

                }
            }
            else TotSeuilL = previousValue;
        }
        
        if( !blimpolFixed )
        {
            previousValue = Blimpol;
//            Blimpol = previousValue * exp(blimpolTune*gauss());
            Blimpol = previousValue + blimpolTune*gauss();
            if( Blimpol>mBlimpol && Blimpol<MBlimpol)
            {
                //                nextLogLik_SeedSet = logLik_SeedSet();
                nextLogLik_SeedSet = compute();
                if (suivi>0 && ite<iteStop) cSuivi << ite << " bLimPol " << Blimpol <<  " "  << previousLogLik_SeedSet << " " << nextLogLik_SeedSet << " " << nextLogLik_SeedSet-previousLogLik_SeedSet << " ";
                if( accept() < exp(nextLogLik_SeedSet-previousLogLik_SeedSet) /* * blimpolRatio(previousValue,Blimpol) */ )
                {
                    previousLogLik_SeedSet = nextLogLik_SeedSet;
                    if (suivi>0 && ite<iteStop) cSuivi << "Accept" << endl;

                }
                else
                {
                    Blimpol = previousValue;
                    if (suivi>0 && ite<iteStop) cSuivi << "Reject" << endl;

                }
            }
            else Blimpol = previousValue;
        }
        
        //        change the fixed effects Vec_fixedEffSS on Vec_ODD
        if ( polSatur == 4 || polSatur == 14 || polSatur == 24 || polSatur == 5 || polSatur == 15 || polSatur == 25 || polSatur == 6 || polSatur == 16 || polSatur == 26)
        {
            int dim = 0;
            inRange=true;
            for (int v=0; v<Nvar; v++){
                Vec_Previous[v]=Vec_FixedEffSS[dim][v];
                if ((PriorIn_FixedEffSS[dim][v] > 0) && (In_FixedEffSS[dim][v]))
                {
                    Vec_FixedEffSS[dim][v] += vecfixedeffTuneSS[dim][v] * gauss();
                    inRange = inRange && (Vec_FixedEffSS[dim][v] > mFixedEffSS[dim][v]) && (Vec_FixedEffSS[dim][v] < MFixedEffSS[dim][v]);
                    //                ratio *= covarRatio[dim][v](Vec_Previous[v], Vec_FixedEff[dim][v])
                }
            }
            if (inRange)
            {
                computeODD();
                //                nextLogLik_SeedSet = logLik_SeedSet();
                nextLogLik_SeedSet = compute();
                if( accept() < exp(nextLogLik_SeedSet-previousLogLik_SeedSet) ) previousLogLik_SeedSet = nextLogLik_SeedSet;
                else
                {
                    for (int v=0; v<Nvar; v++) Vec_FixedEffSS[dim][v] = Vec_Previous[v];
                    computeODD();
                }
            }
            else
            {
                for (int v=0; v<Nvar; v++) Vec_FixedEffSS[dim][v] = Vec_Previous[v];
            }
        }
        
        if(thinStep && (ite % thinStep == 0))
//        if(thinStep)
        {
            *ParamDisp << ite << " " << previousLogLik
            << " " << logLik_Genet() << " " << logLik_SeedSet()
            << " " << Scale
            << " " << Shape
            << " " << Mig
            << " " << Self
            << " " << bSI
            << " " << Delta_opt1 << " " << Delta_opt2
            << " " << Sigma_pheno1 << " " << Sigma_pheno2
            << " " << Weight12
            << " " << TotSeuilA
            << " " << TotSeuilL
            << " " << Blimpol;
            
            for ( int v=0; v<Nvar; v++) *ParamDisp << " " << Vec_FixedEff[0][v];
            for ( int t=0; t<Ntype; t++)
                for ( int tt=0; tt<Ntype; tt++)
                    if (t!=tt)
                    *ParamDisp << " " << mat_Barr[t][tt];
            if (polSatur == 4 || polSatur == 14 || polSatur == 24 || polSatur == 5 || polSatur == 15 || polSatur == 25 || polSatur == 6 || polSatur == 16 || polSatur == 26) for ( int v=0; v<Nvar; v++) *ParamDisp << " " << Vec_FixedEffSS[0][v];
            *ParamDisp << endl;
            ParamDisp->flush();
            
            exportNEP(ite);
        }
    }
    if (thinStep) exportPosteriors();
}

void MEMM_logLik_multiSpe_covar::mcmc_dyn(int nbIteration, int thinStep)
{
    cout << "Dynamic mode not supported." << endl;
}


/************************ PRIVATE ************************************/
long double MEMM_logLik_multiSpe_covar::compute(int posterior)
{
    long double liktemp=0;
    
    if (polSatur < 20) liktemp += logLik_Genet(posterior); //cout << "loglik_Genet OK ..." << liktemp; cout.flush();
    if (polSatur < 10 || polSatur > 19) liktemp += logLik_SeedSet(posterior); //cout << "... loglik_SeedSet OK ..." << liktemp  << endl; cout.flush();
    if (polSatur >9 && polSatur < 20 && posterior) logLik_SeedSet(posterior);
    if (polSatur > 20 && posterior) logLik_Genet(posterior);
        
    return liktemp;
}

long double MEMM_logLik_multiSpe_covar::logLik_Genet(int posterior)
{
    vector < vector<long double> > mat (Nm, vector < long double > (NPar, 0.));
    vector <long double> tot (Nm, 0.);
    long double bb, liktemp=0, pip=0;
    long double piptot=0;
    int pbm=0;
    vector <long double> totType0 (Ntype, 0.);
    vector <long double> totType1 (Ntype, 0.);
    vector <long double> totType2 (Ntype, 0.);
    vector <long double> totType3 (Ntype, 0.);
    vector <long double> totFlowType (3, 0.);
    long double moyDispDist, nep;
    vector <long double> propAutof0 (2, 0.);
    vector <long double> propAutof1 (2, 0.);
    vector <long double> propAutof2 (2, 0.);
    vector <long double> propAutof3 (2, 0.);
    
    if(posterior == 1 || posterior == 4) Nposterior++;
    
    //cout << "entering in loglik_Genet..."; cout.flush();
    for (int m=0; m<Nm; m++)
    {
        if (posterior) {
            totType1= totType0; totType2= totType0; totType3= totType0;
            totFlowType[0]=0; totFlowType[1]=0; totFlowType[2]=0;
            propAutof1=propAutof0; propAutof2=propAutof0; propAutof3=propAutof0;
            moyDispDist=0;nep=0;
        }
        if (polSatur == 2 || polSatur == 12 || polSatur == 22 ) // modèle de compétition neutre avec SI post-zygotiques et barrières interspé pré-zygotique
        {
            for (int p=0; p<NPar; p++)
            {
                if ((Type[p]==Type[Meres[m]]) && (Clone[p]==Clone[Meres[m]])) mat[m][p] = (1-bSI)* DISP[m][p] * PHENO[m][p] * Vec_Fec_All[p];
                else mat[m][p] = (1-Blimpol) * DISP[m][p] * PHENO[m][p] * mat_Barr[Type[Meres[m]]][Type[p]] * Vec_Fec_All[p];
                if (isnan(mat[m][p])){mat[m][p]=0; pbm=1;}
                if (posterior)
                {
                    totType1[Type[p]] += DISP[m][p] * PHENO[m][p] * Vec_Fec_All[p];
                    if ((Type[p]==Type[Meres[m]]) && (Clone[p]==Clone[Meres[m]])) totType2[Type[p]] += DISP[m][p] * PHENO[m][p] * Vec_Fec_All[p];
                    else totType2[Type[p]] += DISP[m][p] * PHENO[m][p] * mat_Barr[Type[Meres[m]]][Type[p]] * Vec_Fec_All[p];
                    totType3[Type[p]] += mat[m][p];
                    if ((Type[p]==Type[Meres[m]]) && (Clone[p]==Clone[Meres[m]]))
                    {
                        propAutof1[0] += DISP[m][p] * PHENO[m][p] * Vec_Fec_All[p];
                        propAutof2[0] += DISP[m][p] * PHENO[m][p] * Vec_Fec_All[p];
                        propAutof3[0] += mat[m][p];
                    }
                    else
                    {
                        propAutof1[1] += DISP[m][p] * PHENO[m][p] * Vec_Fec_All[p];
                        propAutof2[1] += DISP[m][p] * PHENO[m][p] * mat_Barr[Type[Meres[m]]][Type[p]] * Vec_Fec_All[p];
                        propAutof3[1] += mat[m][p];
                    }
                    moyDispDist += DistMP[m][p] * mat[m][p];
                    nep += mat[m][p]*mat[m][p];
                }
                tot[m]+=mat[m][p];
                totFlowType[FlowType[p]]+=mat[m][p];
                //                    if (m==38) cout << p << " ";cout.flush();
            }
        }
        
        else if (polSatur == 4 || polSatur == 14 || polSatur == 24
                 || polSatur == 5 || polSatur == 15 || polSatur == 25) // modèle1 avec limitation && SI post-zygotiques && barrières interspé pré-zygotique
        {
            for (int p=0; p<NPar; p++)
            {
                if ((Type[p]==Type[Meres[m]]) && (Clone[p]==Clone[Meres[m]])) mat[m][p] = (1-bSI)* DISP[m][p] * PHENO[m][p] * Vec_Fec_All[p];
                else mat[m][p] = DISP[m][p] * PHENO[m][p] * mat_Barr[Type[Meres[m]]][Type[p]] * Vec_Fec_All[p];
                if (isnan(mat[m][p])){mat[m][p]=0; pbm=1;}
                if (posterior)
                {
                    totType1[Type[p]] += DISP[m][p] * PHENO[m][p] * Vec_Fec_All[p];
                    if ((Type[p]==Type[Meres[m]]) && (Clone[p]==Clone[Meres[m]])) totType2[Type[p]] += DISP[m][p] * PHENO[m][p] * Vec_Fec_All[p];
                    else totType2[Type[p]] += mat[m][p];
                    totType3[Type[p]] += mat[m][p];
                    if ((Type[p]==Type[Meres[m]]) && (Clone[p]==Clone[Meres[m]]))
                    {
                        propAutof1[0] += DISP[m][p] * PHENO[m][p] * Vec_Fec_All[p];
                        propAutof2[0] += DISP[m][p] * PHENO[m][p] * Vec_Fec_All[p];
                        propAutof3[0] += mat[m][p];
                    }
                    else
                    {
                        propAutof1[1] += DISP[m][p] * PHENO[m][p] * Vec_Fec_All[p];
                        propAutof2[1] += mat[m][p];
                        propAutof3[1] += mat[m][p];
                    }
                    moyDispDist += DistMP[m][p] * mat[m][p];
                    nep += mat[m][p]*mat[m][p];
                }
                tot[m]+=mat[m][p];
                totFlowType[FlowType[p]]+=mat[m][p];
                //                    if (m==38) cout << p << " ";cout.flush();
            }
        }

        else if (polSatur == 6 || polSatur == 16 || polSatur == 26) // modèle avec limitation && SI pré && post-zygotiques && barrières interspé pré-zygotique
        {
            for (int p=0; p<NPar; p++)
            {
                if ((Type[p]==Type[Meres[m]]) && (Clone[p]==Clone[Meres[m]])) mat[m][p] = (1-bSI) * Self * DISP[m][p] * PHENO[m][p] * Vec_Fec_All[p];
                else mat[m][p] = (1-TotSeuilL) * DISP[m][p] * PHENO[m][p] * mat_Barr[Type[Meres[m]]][Type[p]] * Vec_Fec_All[p];
                if (isnan(mat[m][p])){mat[m][p]=0; pbm=1;}
                if (posterior)
                {
                    totType1[Type[p]] += DISP[m][p] * PHENO[m][p] * Vec_Fec_All[p];
                    if ((Type[p]==Type[Meres[m]]) && (Clone[p]==Clone[Meres[m]])) totType2[Type[p]] += Self * DISP[m][p] * PHENO[m][p] * Vec_Fec_All[p];
                    else totType2[Type[p]] += mat[m][p];
                    totType3[Type[p]] += mat[m][p];
                    if ((Type[p]==Type[Meres[m]]) && (Clone[p]==Clone[Meres[m]]))
                    {
                        propAutof1[0] += DISP[m][p] * PHENO[m][p] * Vec_Fec_All[p];
                        propAutof2[0] += Self * DISP[m][p] * PHENO[m][p] * Vec_Fec_All[p];
                        propAutof3[0] += mat[m][p];
                    }
                    else
                    {
                        propAutof1[1] += DISP[m][p] * PHENO[m][p] * Vec_Fec_All[p];
                        propAutof2[1] += mat[m][p];
                        propAutof3[1] += mat[m][p];
                    }
                    moyDispDist += DistMP[m][p] * mat[m][p];
                    nep += mat[m][p]*mat[m][p];
                }
                tot[m]+=mat[m][p];
                totFlowType[FlowType[p]]+=mat[m][p];
                //                    if (m==38) cout << p << " ";cout.flush();
            }
        }

        
        else
        {
            for (int p=0; p<NPar; p++)
            {
                if ((Type[p]==Type[Meres[m]]) && (Clone[p]==Clone[Meres[m]])) mat[m][p] = bSI * DISP[m][p] * PHENO[m][p] * Vec_Fec_All[p];
                else mat[m][p] = DISP[m][p] * PHENO[m][p] * mat_Barr[Type[Meres[m]]][Type[p]] * Vec_Fec_All[p];
                if (isnan(mat[m][p])){mat[m][p]=0; pbm=1;}
                if (posterior)
                {
                    totType1[Type[p]] += DISP[m][p] * PHENO[m][p] * Vec_Fec_All[p];
                    if ((Type[p]==Type[Meres[m]]) && (Clone[p]==Clone[Meres[m]])) totType2[Type[p]] += DISP[m][p] * PHENO[m][p] * Vec_Fec_All[p];
                    else totType2[Type[p]] += DISP[m][p] * PHENO[m][p] * mat_Barr[Type[Meres[m]]][Type[p]] * Vec_Fec_All[p];
                    totType3[Type[p]] += mat[m][p];
                    if ((Type[p]==Type[Meres[m]]) && (Clone[p]==Clone[Meres[m]]))
                    {
                        propAutof1[0] += DISP[m][p] * PHENO[m][p] * Vec_Fec_All[p];
                        propAutof2[0] += DISP[m][p] * PHENO[m][p] * Vec_Fec_All[p];
                        propAutof3[0] += mat[m][p];
                    }
                    else
                    {
                        propAutof1[1] += DISP[m][p] * PHENO[m][p] * Vec_Fec_All[p];
                        propAutof2[1] += DISP[m][p] * PHENO[m][p] * mat_Barr[Type[Meres[m]]][Type[p]] * Vec_Fec_All[p];
                        propAutof3[1] += mat[m][p];
                    }
                    moyDispDist += DistMP[m][p] * mat[m][p];
                    nep += mat[m][p] * mat[m][p];
                }
                tot[m]+=mat[m][p];
                totFlowType[FlowType[p]]+=mat[m][p];
            }
        }
        if (tot[m]==0) pbm=1;
        if (posterior)
        {
            *SummaryStats << dynamic_cast<parent*>(allParents[Meres[m]])->getName() << " " << moyDispDist/tot[m] << " ";
            *SummaryStats << tot[m]*tot[m]/nep << " ";
            *SummaryStats << propAutof1[0] << " " << propAutof1[1] << " ";
            *SummaryStats << propAutof2[0] << " " << propAutof2[1] << " ";
            *SummaryStats << propAutof3[0] << " " << propAutof3[1] << " ";
            for (int t=0; t<Ntype; t++) *SummaryStats << totType1[t] << " ";
            for (int t=0; t<Ntype; t++) *SummaryStats << totType2[t] << " ";
            for (int t=0; t<Ntype; t++) *SummaryStats << totType3[t] << " ";
            for (int t=0; t<3; t++) *SummaryStats << totFlowType[t] << " ";
            *SummaryStats << endl;
            SummaryStats->flush();
        }
    }
    if (pbm==1) { std::cout << " Warning: Overflow. One component of the mating matrix was not defined or tot=0 " << std::endl;}
    
    
    
    // cout << "compute mat OK in loglik_Genet..." << liktemp; cout.flush();
    for (int s=0; s<Ns; s++){
        if( ProbMig[s]>0 && tot[MerDesc[s]]>0)
        {
            pip=0;
            for (int p=0; p<NbPeres[s]; p++)
            {
                pip += (ProbPeres[s][p].pere_prob)*mat[MerDesc[s]][ProbPeres[s][p].pere_pot];
            }
            piptot = ProbMig[s]*Mig+(1-Mig)*pip/tot[MerDesc[s]];
            liktemp += log(piptot);
            
            if(posterior == 1 || posterior == 4){
                PosteriorOut[s] += ProbMig[s]*Mig/piptot;
                for (int p=0; p<NbPeres[s]; p++)
                {
                    PosteriorPeres[s][p] += (ProbPeres[s][p].pere_prob)*mat[MerDesc[s]][ProbPeres[s][p].pere_pot]*(1-Mig)/tot[MerDesc[s]]/piptot;
                }
            }
        }
    }
    // cout << "liktemp compted OK ..." << liktemp; cout.flush();
    
    return liktemp;
}

long double MEMM_logLik_multiSpe_covar::logLik_SeedSet(int posterior)
{
    int pbm=0;
    vector < vector<long double> > mat (NPar, vector < long double > (NPar, 0.));
    vector <long double> tot (NPar, 0.);
    vector <long double> totSelf (NPar, 0.);
    long double liktemp=0;
    long double pptemp;
    long double probtemp=0.5;
    vector <long double> ptemp (3, 0.);
    double totseuilLtemp;
    // On ne considère que la loi binomiable censurée en 0... pas de zero-deflated model
    vector <long double> totType0 (Ntype, 0.);
    vector <long double> totType1 (Ntype, 0.);
    vector <long double> totType2 (Ntype, 0.);
    vector <long double> totType3 (Ntype, 0.);
    long double moyDispDist, nep;
    vector <long double> propAutof0 (2, 0.);
    vector <long double> propAutof1 (2, 0.);
    vector <long double> propAutof2 (2, 0.);
    vector <long double> propAutof3 (2, 0.);
    
    
    
    for (int m=0; m<NPar; m++)
        if (SeedSetTot[m]>0)
        {
            if (posterior) {
                totType1= totType0; totType2= totType0; totType3= totType0;
                propAutof1=propAutof0; propAutof2=propAutof0; propAutof3=propAutof0;
                moyDispDist=0;nep=0;
            }
            
            //            cout << m << " ";cout.flush();
            
            if (polSatur == 0 || polSatur == 10 || polSatur == 20) // modèle de pollen non-saturant avec barrières pré-zygotique + effet longi/asta
            {
                for (int p=0; p<NPar; p++)
                {
                    if ((Type[p]==Type[m]) && (Clone[p]==Clone[m])) tot[m] += bSI * DISPPP[m][p] * PHENOPP[m][p] * Vec_Fec_All[p];
                    else tot[m] += DISPPP[m][p] * PHENOPP[m][p] * mat_Barr[Type[m]][Type[p]] * Vec_Fec_All[p];
                    if (isnan(tot[m])){tot[m]=0; pbm=1;}
                }
                if (tot[m]==0) pbm=1;
                if (pbm==1) { std::cout << " Warning: Overflow. One component of the pollen intensity vector was not defined or tot = 0 " << std::endl;}
                //        cout << " " << tot[m]; cout.flush();
                if (posterior) *PolLimFile << tot[m] << " ";
                
                if (FlowType[m]==0) probtemp = exp(Blimpol*(log(tot[m])-TotSeuilA))/(1+exp(Blimpol*(log(tot[m])-TotSeuilA)));
                else if (FlowType[m]==1) probtemp = exp(Blimpol*(log(tot[m])-TotSeuilL))/(1+exp(Blimpol*(log(tot[m])-TotSeuilL)));
                
            }
            
            else if (polSatur == 3 || polSatur == 13 || polSatur == 23) // modèle de pollen non-saturant avec barrières pré-zygotique
            {
                for (int p=0; p<NPar; p++)
                {
                    if ((Type[p]==Type[m]) && (Clone[p]==Clone[m])) tot[m] += bSI * DISPPP[m][p] * PHENOPP[m][p] * Vec_Fec_All[p];
                    else tot[m] += DISPPP[m][p] * PHENOPP[m][p] * mat_Barr[Type[m]][Type[p]] * Vec_Fec_All[p];
                    if (isnan(tot[m])){tot[m]=0; pbm=1;}
                }
                if (tot[m]==0) pbm=1;
                if (pbm==1) { std::cout << " Warning: Overflow. One component of the pollen intensity vector was not defined or tot = 0 " << std::endl;}
                //        cout << " " << tot[m]; cout.flush();
                if (posterior) *PolLimFile << tot[m] << " ";
                
                probtemp = pow(tot[m],Blimpol) / (exp(TotSeuilA) + pow(tot[m],Blimpol));
            }
            
            else if (polSatur == 1 || polSatur == 11 || polSatur == 21) // modèle de callose du stigmate avec barrières pré-zygotique
            {
                for (int p=0; p<NPar; p++)
                {
                    if ((Type[p]==Type[m]) && (Clone[p]==Clone[m])) tot[m] += bSI * DISPPP[m][p] * PHENOPP[m][p] * Vec_Fec_All[p];
                    else tot[m] += DISPPP[m][p] * PHENOPP[m][p] * mat_Barr[Type[m]][Type[p]] * Vec_Fec_All[p];
                    if (isnan(tot[m])){tot[m]=0; pbm=1;}
                    if ((Type[p]==Type[m]) && (Clone[p]==Clone[m])) totSelf[m] += DISPPP[m][p] * PHENOPP[m][p] * Vec_Fec_All[p];
                }
                if (tot[m]==0) pbm=1;
                if (pbm==1) { std::cout << " Warning: Overflow. One component of the pollen intensity vector was not defined or tot = 0 " << std::endl;}
                //        cout << " " << tot[m]; cout.flush();
                if (posterior) *PolLimFile << tot[m] << " ";
                
                
                //            totseuilLtemp = TotSeuilA + totSelf[m]*TotSeuilL;
                probtemp = exp(Blimpol*(log(totSelf[m])-TotSeuilA))/(1+exp(Blimpol*(log(totSelf[m])-TotSeuilA)));
                // Blimpol devrait être négatif: plus d'autopollen => plus de callose => moins de succès de fécondation
                // Pas de raison de faire de différence entrre les Asta et les Longi
            }
            
            else if (polSatur == 2 || polSatur == 12 || polSatur == 22) // modèle de compétition neutre avec SI post-zygotiques et barrières interspé pré-zygotique
            {
                for (int p=0; p<NPar; p++)
                {
                    if ((Type[p]==Type[m]) && (Clone[p]==Clone[m])) mat[m][p] = DISPPP[m][p] * PHENOPP[m][p] * Vec_Fec_All[p];
                    else mat[m][p] = DISPPP[m][p] * PHENOPP[m][p] * mat_Barr[Type[m]][Type[p]] * Vec_Fec_All[p];
                    if (isnan(mat[m][p])){mat[m][p]=0; pbm=1;}
                    if (posterior)
                    {
                        totType1[Type[p]] += DISPPP[m][p] * PHENOPP[m][p] * Vec_Fec_All[p];
                        totType2[Type[p]] += mat[m][p];
                        if ((Type[p]==Type[m]) && (Clone[p]==Clone[m])) totType3[Type[p]] += mat[m][p]*(1-bSI);
                        else totType3[Type[p]] += mat[m][p]*(1-Blimpol);
                        if ((Type[p]==Type[m]) && (Clone[p]==Clone[m]))
                        {
                            propAutof1[0] += mat[m][p];
                            propAutof2[0] += mat[m][p];
                            propAutof3[0] += mat[m][p]*(1-bSI);
                            moyDispDist += DistPP[m][p] * mat[m][p] *(1-bSI);
                            nep += mat[m][p]*mat[m][p]*(1-bSI)*(1-bSI);
                        }
                        else
                        {
                            propAutof1[1] += DISPPP[m][p] * PHENOPP[m][p] * Vec_Fec_All[p];
                            propAutof2[1] += mat[m][p];
                            propAutof3[1] += mat[m][p]*(1-Blimpol);
                            moyDispDist += DistPP[m][p] * mat[m][p]*(1-Blimpol);
                            nep += mat[m][p]*mat[m][p]*(1-Blimpol)*(1-Blimpol);
                        }
                    }
                    tot[m]+=mat[m][p];
                    //                    if (m==38) cout << p << " ";cout.flush();
                }
                if (tot[m]==0) pbm=1;
                if (posterior) *PolLimFile << tot[m] << " ";
                
                //                cout << "matOK ";cout.flush();
                
                probtemp = 0;
                for (int p=0; p<NPar; p++) {
                    if ((Type[p]==Type[m]) && (Clone[p]==Clone[m])) probtemp += mat[m][p]*(1-bSI);
                    else probtemp += mat[m][p]*(1-Blimpol);
                }
                probtemp /= tot[m];
                // Le taux d'avortement des autof est de bSI
                // Le taux d'avortement des allof est de Blimpol (pas de différence entre espèces)
                // Le taux de succès de féconation est Sum( pi * (1-b) )...
            }
            
            else if (polSatur == 4 || polSatur == 14 || polSatur == 24) // modèle1 avec limitation && SI post-zygotiques && barrières interspé pré-zygotique
            {
                for (int p=0; p<NPar; p++)
                {
                    if ((Type[p]==Type[m]) && (Clone[p]==Clone[m]))
                    { mat[m][p] = DISPPP[m][p] * PHENOPP[m][p] * Vec_Fec_All[p];
                        totSelf[m]+=mat[m][p];
                    }
                    else mat[m][p] = DISPPP[m][p] * PHENOPP[m][p] * mat_Barr[Type[m]][Type[p]] * Vec_Fec_All[p];
                    if (isnan(mat[m][p])){mat[m][p]=0; pbm=1;}
                    if (posterior)
                    {
                        totType1[Type[p]] += DISPPP[m][p] * PHENOPP[m][p] * Vec_Fec_All[p];
                        totType2[Type[p]] += mat[m][p];
                        if ((Type[p]==Type[m]) && (Clone[p]==Clone[m])) totType3[Type[p]] += mat[m][p]*(1-bSI);
                        else totType3[Type[p]] += mat[m][p];
                        if ((Type[p]==Type[m]) && (Clone[p]==Clone[m]))
                        {
                            propAutof1[0] += mat[m][p];
                            propAutof2[0] += mat[m][p];
                            propAutof3[0] += mat[m][p]*(1-bSI);
                            moyDispDist += DistPP[m][p] * mat[m][p] *(1-bSI);
                            nep += mat[m][p]*mat[m][p]*(1-bSI)*(1-bSI);
                        }
                        else
                        {
                            propAutof1[1] += DISPPP[m][p] * PHENOPP[m][p] * Vec_Fec_All[p];
                            propAutof2[1] += mat[m][p];
                            propAutof3[1] += mat[m][p];
                            moyDispDist += DistPP[m][p] * mat[m][p];
                            nep += mat[m][p]*mat[m][p];
                        }
                    }
                    tot[m]+=mat[m][p];
                    //                    if (m==38) cout << p << " ";cout.flush();
                }
                if (tot[m]==0) pbm=1;
                if (posterior) *PolLimFile << tot[m] << " ";
                
                //                cout << "matOK ";cout.flush();
                
                pptemp=exp(TotSeuilA + Vec_ODD[m] + Blimpol*tot[m]);
                if (isnan(pptemp))
                {probtemp=0.01;
                    //                        cout << " Warning: Overflow. pptemp was not defined. Seed-set likelihood undefined ( m= " << TotSeuilA << " " << Vec_ODD[m] << " " << Blimpol << " " << tot[m] << endl;
                }
                else probtemp = pptemp / (1 + pptemp) * (1 - totSelf[m]/tot[m]*bSI);
                
                //                if (isnan(probtemp)) {probtemp=1; cout << " Warning: Overflow. Prob(fecundation) was not defined. Seed-set likelihood undefined ( m= " << m << " pptemp= " << pptemp << " tot = " << tot[m] << " totself = " << totSelf[m] << endl;}
                
                // Le manque de pollen/pollinisateur est déterminé par les covariables mères (Vec_ODD) et la quantité de pollen reçue
                // Le taux d'avortement des autof est de bSI
                // Le taux d'avortement des allof est de 0
                // Le taux de succès (probtemp) est le produit P(pollinisation) x ( 1 - P(avortement))
            }
            else if (polSatur == 5 || polSatur == 15 || polSatur == 25) // modèle1 avec limitation qui dépende de l'allopollen slt && SI post-zygotiques && barrières interspé pré-zygotique
            {
                for (int p=0; p<NPar; p++)
                {
                    if ((Type[p]==Type[m]) && (Clone[p]==Clone[m]))
                    { mat[m][p] = DISPPP[m][p] * PHENOPP[m][p] * Vec_Fec_All[p];
                        totSelf[m]+=mat[m][p];
                    }
                    else mat[m][p] = DISPPP[m][p] * PHENOPP[m][p] * mat_Barr[Type[m]][Type[p]] * Vec_Fec_All[p];
                    if (isnan(mat[m][p])){mat[m][p]=0; pbm=1;}
                    if (posterior)
                    {
                        totType1[Type[p]] += DISPPP[m][p] * PHENOPP[m][p] * Vec_Fec_All[p];
                        totType2[Type[p]] += mat[m][p];
                        if ((Type[p]==Type[m]) && (Clone[p]==Clone[m])) totType3[Type[p]] += mat[m][p]*(1-bSI);
                        else totType3[Type[p]] += mat[m][p];
                        if ((Type[p]==Type[m]) && (Clone[p]==Clone[m]))
                        {
                            propAutof1[0] += mat[m][p];
                            propAutof2[0] += mat[m][p];
                            propAutof3[0] += mat[m][p]*(1-bSI);
                            moyDispDist += DistPP[m][p] * mat[m][p] *(1-bSI);
                            nep += mat[m][p]*mat[m][p]*(1-bSI)*(1-bSI);
                        }
                        else
                        {
                            propAutof1[1] += DISPPP[m][p] * PHENOPP[m][p] * Vec_Fec_All[p];
                            propAutof2[1] += mat[m][p];
                            propAutof3[1] += mat[m][p];
                            moyDispDist += DistPP[m][p] * mat[m][p];
                            nep += mat[m][p]*mat[m][p];
                        }
                    }
                    tot[m]+=mat[m][p];
                    //                    if (m==38) cout << p << " ";cout.flush();
                }
                if (tot[m]==0) pbm=1;
                if (posterior) *PolLimFile << tot[m] << " ";
                
                //                cout << "matOK ";cout.flush();
                
                pptemp=exp(TotSeuilA + Vec_ODD[m] + Blimpol*(tot[m]-totSelf[m]));
                if (isnan(pptemp))
                {probtemp=0.01;
                    //                        cout << " Warning: Overflow. pptemp was not defined. Seed-set likelihood undefined ( m= " << TotSeuilA << " " << Vec_ODD[m] << " " << Blimpol << " " << tot[m] << endl;
                }
                else probtemp = pptemp / (1 + pptemp) * (1 - totSelf[m]/tot[m]*bSI);
                
                //                if (isnan(probtemp)) {probtemp=1; cout << " Warning: Overflow. Prob(fecundation) was not defined. Seed-set likelihood undefined ( m= " << m << " pptemp= " << pptemp << " tot = " << tot[m] << " totself = " << totSelf[m] << endl;}
                
                // Le manque de pollen/pollinisateur est déterminé par les covariables mères (Vec_ODD) et la quantité de pollen reçue
                // Le taux d'avortement des autof est de bSI
                // Le taux d'avortement des allof est de TotSeuilL (ou =0)
                // Le taux de succès (probtemp) est le produit P(pollinisation) x ( 1 - P(avortement))
            }
            else if (polSatur == 6 || polSatur == 16 || polSatur == 26) // modèle1 avec limitation qui dépende de l'allopollen slt && SI pré && post-zygotiques && barrières interspé pré-zygotique
            {
                for (int p=0; p<NPar; p++)
                {
                    if ((Type[p]==Type[m]) && (Clone[p]==Clone[m]))
                    { mat[m][p] = Self * DISPPP[m][p] * PHENOPP[m][p] * Vec_Fec_All[p];
                        totSelf[m]+=mat[m][p];
                    }
                    else mat[m][p] = DISPPP[m][p] * PHENOPP[m][p] * mat_Barr[Type[m]][Type[p]] * Vec_Fec_All[p];
                    if (isnan(mat[m][p])){mat[m][p]=0; pbm=1;}
                    if (posterior)
                    {
                        totType1[Type[p]] += DISPPP[m][p] * PHENOPP[m][p] * Vec_Fec_All[p];
                        totType2[Type[p]] += mat[m][p];
                        if ((Type[p]==Type[m]) && (Clone[p]==Clone[m])) totType3[Type[p]] += mat[m][p]*(1-bSI);
                        else totType3[Type[p]] += mat[m][p]*(1-TotSeuilL);
                        if ((Type[p]==Type[m]) && (Clone[p]==Clone[m]))
                        {
                            propAutof1[0] += DISPPP[m][p] * PHENOPP[m][p] * Vec_Fec_All[p];
                            propAutof2[0] += mat[m][p];
                            propAutof3[0] += mat[m][p]*(1-bSI);
                            moyDispDist += DistPP[m][p] * mat[m][p] *(1-bSI);
                            nep += mat[m][p]*mat[m][p]*(1-bSI)*(1-bSI);
                        }
                        else
                        {
                            propAutof1[1] += DISPPP[m][p] * PHENOPP[m][p] * Vec_Fec_All[p];
                            propAutof2[1] += mat[m][p];
                            propAutof3[1] += mat[m][p]*(1-TotSeuilL);
                            moyDispDist += DistPP[m][p] * mat[m][p]*(1-TotSeuilL);
                            nep += mat[m][p]*mat[m][p]*(1-TotSeuilL)*(1-TotSeuilL);
                        }
                        
                    }
                    tot[m]+=mat[m][p];
                    //                    if (m==38) cout << p << " ";cout.flush();
                }
                if (tot[m]==0) pbm=1;
                if (posterior) *PolLimFile << tot[m] << " ";
                
                //                cout << "matOK ";cout.flush();
                
                pptemp=exp(TotSeuilA + Vec_ODD[m] + Blimpol*tot[m]);
                if (isnan(pptemp))
                {probtemp=0.01;
                    //                        cout << " Warning: Overflow. pptemp was not defined. Seed-set likelihood undefined ( m= " << TotSeuilA << " " << Vec_ODD[m] << " " << Blimpol << " " << tot[m] << endl;
                }
                else probtemp = pptemp / (1 + pptemp) * (1 - TotSeuilL + (TotSeuilL - bSI) * totSelf[m]/tot[m]);
                
                //                if (isnan(probtemp)) {probtemp=1; cout << " Warning: Overflow. Prob(fecundation) was not defined. Seed-set likelihood undefined ( m= " << m << " pptemp= " << pptemp << " tot = " << tot[m] << " totself = " << totSelf[m] << endl;}
                
                // Le manque de pollen/pollinisateur est déterminé par les covariables mères (Vec_ODD) et la quantité de pollen reçue
                // Le taux d'avortement des autof est de bSI
                // Le taux d'avortement des allof est de 0
                // Le taux de succès (probtemp) est le produit P(pollinisation) x ( 1 - P(avortement))
            }
            
            if (posterior)
            {
                *SummaryStatsAll << dynamic_cast<parent*>(allParents[m])->getName() << " " << moyDispDist/(propAutof3[0]+propAutof3[1]) << " ";
                *SummaryStatsAll << (propAutof3[0]+propAutof3[1])*(propAutof3[0]+propAutof3[1])/nep << " ";
                *SummaryStatsAll << propAutof1[0] << " " << propAutof1[1] << " ";
                *SummaryStatsAll << propAutof2[0] << " " << propAutof2[1] << " ";
                *SummaryStatsAll << propAutof3[0] << " " << propAutof3[1] << " ";
                for (int t=0; t<Ntype; t++) *SummaryStatsAll << totType1[t] << " ";
                for (int t=0; t<Ntype; t++) *SummaryStatsAll << totType2[t] << " ";
                for (int t=0; t<Ntype; t++) *SummaryStatsAll << totType3[t] << " ";
                *SummaryStatsAll << tot[m] << " " << probtemp;
                *SummaryStatsAll << endl;
                SummaryStatsAll->flush();
            }
            
            
            //            cout << probtemp << " ";cout.flush();
            if(tot[m]>0)
            {
                //            ptemp[0] = AlRed * pow( (1 - probtemp) , 3 );
                ptemp[0] = 3 * probtemp * pow( (1-probtemp) , 2 ) / (1 - pow( (1 - probtemp) , 3 ));
                ptemp[1] = ptemp[0] * probtemp / (1-probtemp);
                ptemp[2] = ptemp[1] * probtemp / (1-probtemp) / 3;
                // modèle pour écarter les bogues vides
                
                for (int b=0; b<3; b++)
                {
                    liktemp += SeedSet[m][b]*log(ptemp[b]);
                    liktemp += SeedSet[m][b+3]*log(ptemp[b]);
                }
                
                //                cout << liktemp << endl;cout.flush();
                
                if (posterior) *PolLimFile << probtemp << " ";
                
            }
            //        cout << " " << liktemp << endl; cout.flush();
        }
    
    if (posterior)
    {
        *PolLimFile << endl;
        PolLimFile->flush();
    }
    
    return liktemp;
}

void MEMM_logLik_multiSpe_covar::computeDISP()
{
    int pbm=0;
    long double K;
    double theta, ka, selfTemp;
    double crownRadius = 3. ;
    
    if ( fDisp <= 0 )
    {
        a=Scale*exp(gammln(2/Shape)-gammln(3/Shape));
        ab=-pow(a,-Shape);
        K=Shape/2/3.14/a/a/exp(gammln(2/Shape));
        theta = pow(a, Shape);
        ka = 2/Shape;
        selfTemp = boost::math::gamma_p(ka, pow(crownRadius,Shape)/theta)/3.14/crownRadius/crownRadius;
    }
    else
    {
        a=Scale;
        K=(Shape-1)/3.14/a/a;
        selfTemp = (1 - pow(1 + crownRadius*crownRadius/a/a , 1-Shape) ) /3.14/crownRadius/crownRadius;
    }
    
    
    for (int m=0; m<NPar; m++)
    {
        for (int p=0; p<NPar; p++)
        {
            if (p==m)
//            { DISPPP[m][p]=K*Self*Poids[p]; }
//            { DISPPP[m][p]= selfTemp*bSI*Poids[p]; }
            { DISPPP[m][p]= selfTemp*Poids[p]; }
//            else if ((Type[p]==Type[m]) && (Clone[p]==Clone[m]))
//            { if ( fDisp <= 0 ) DISPPP[m][p]=K*bSI*exp(ab*pow(DistPP[m][p],Shape))*Poids[p];
//                else DISPPP[m][p]=K*bSI*pow(1 + DistPP[m][p]*DistPP[m][p]/a/a, -Shape)*Poids[p];
//            }
            else
            { if ( fDisp <= 0 ) DISPPP[m][p]=K*exp(ab*pow(DistPP[m][p], Shape))*Poids[p];
                else DISPPP[m][p]=K*pow(1 + DistPP[m][p]*DistPP[m][p]/a/a, -Shape)*Poids[p]; }
            if (isnan(DISPPP[m][p])){DISPPP[m][p]=0; pbm=1;}
        }
    }
    if (pbm==1) { std::cout << " Warning: Overflow. One component of the dispersal matrix DISP was not defined. " << std::endl;}
    for (int m=0; m<Nm; m++)
    {
        DISP[m]=DISPPP[Meres[m]];
    }
}

void MEMM_logLik_multiSpe_covar::computePHENO()
{
    int pbm=0;
    
    for (int m=0; m<NPar; m++)
    {
        for (int p=0; p<NPar; p++)
        {
            PHENOPP[m][p]=(Weight12*exp(-pow((PhenoPP1[m][p]-Delta_opt1)/Sigma_pheno1,2))+(1-Weight12)*exp(-pow((PhenoPP2[m][p]-Delta_opt2)/Sigma_pheno2,2)));
            if (isnan(PHENOPP[m][p])){PHENOPP[m][p]=0; pbm=1;}
        }
        if (pbm==1) { cout << " Warning: Overflow. One component of the phenological matrix PHENO was not defined. " << endl;}
        for (int m=0; m<Nm; m++)
        {
            PHENO[m]=PHENOPP[Meres[m]];
        }
        
    }
}

void MEMM_logLik_multiSpe_covar::computeODD()
{
    int pbm=0;
    double temp=0;
    for (int p=0; p<NPar; p++)
    {
        temp=0;
        for (int v=0; v<Nvar; v++) temp += Vec_FixedEffSS[0][v] * Covar[p][v];
        Vec_ODD[p]=temp;
        if (isnan(Vec_ODD[p])){Vec_ODD[p]=0; pbm=1;}
    }
    if (pbm==1) { cout << " Warning: Overflow. One component of the seed-set vector Vec_ODD was not defined. " << endl;}
}


long double MEMM_logLik_multiSpe_covar::computeDyn()
{
    cout << "Dynamic mode not supported." << endl;
}
 
/************************ PROTECTED ****************************************/

/**************** INIT **********************/

void MEMM_logLik_multiSpe_covar::loadParentFile(Configuration * p)
{
    
    MEMM::loadParentFile(p);
    
    char * temp;
    
    // Ntype compte le nombre d'espèces-types présents dans le jeu de données
    // Ntype est instancié ici
    // mat_Barr est instancié ici aussi
    
    DistPP.resize(NPar,vector < double > (NPar , 0.));
    AzimPP.resize(NPar,vector < double > (NPar , 0.));
    DISPPP.resize(NPar,vector < long double > (NPar , 0.));
    PHENOPP.resize(NPar,vector < long double > (NPar , 0.));

    PhenoPP1.resize(NPar,vector < double > (NPar , 0.));
    PhenoPP2.resize(NPar,vector < double > (NPar , 0.));

    for (int m=0; m<NPar; m++)
        for (int p=0; p<NPar; p++)
        {
        DistPP[m][p]=(*allParents[m]).dist(*allParents[p]);
        AzimPP[m][p]=(*allParents[m]).azim(*allParents[p]);
        }

    Nvar = Nqt;
    Ntype = 1;
    
    temp = p->getValue(MEMM_COVAR_NUMBER);
    if(temp) Nvar = atoi(temp);
    if(temp) delete []temp;
    
    cout << Nvar << " covariates to be loaded..." << endl;
    
//    NBog.resize( NPar, 0);
    Covar.resize( NPar, vector<double>( Nvar , 0.) );
    Type.resize( NPar, 0);
    FlowType.resize( NPar, 0);
    Clone.resize( NPar, 0);
    NClone.resize(5,0);
    CloneList.resize(5);
    SeedSet.resize( NPar , vector<int>( 6 , 0 ) );
    
    cout <<endl << "Loading the species/types and covariates..."<<endl;
    for (int p=0; p<NPar; p++)
    {
//        if (p < 10 ) cout << " - Parent " << p;cout.flush();
        Type[p]=(allParents[p]->getCoVarQuali(0)) - 1;
        if (Type[p] + 1 > Ntype) Ntype = Type[p] + 1;
//        if (p < 10 ) cout << " : Type " << Type[p]; cout.flush();
    }
    cout << endl << endl;
 
    NClone.resize(Ntype,0);
    CloneList.resize(Ntype);
    SeedSet.resize( NPar , vector<int>( 6 , 0 ) );
    SeedSetTot.resize( NPar , 0 );
    
    for (int p=0; p<NPar; p++)
    {
        Clone[p]=allParents[p]->getCoVarQuali(1)-1;
        if (p < 20 ) cout << " - Parent " << p << " : Type = " << Type[p]; cout.flush();
        if (p < 20 ) cout << " | Clone(Type) " << Clone[p] << "(" << Type[p] << ")" ; cout.flush();
        if (Clone[p] + 1 > NClone[Type[p]] ) { NClone[Type[p]] = Clone[p] + 1; CloneList[Type[p]].resize(NClone[Type[p]]);}
        CloneList[Type[p]][Clone[p]].push_back(p);
        FlowType[p]=allParents[p]->getCoVarQuali(2) - 1;
        if (p < 20 ) cout << " | Flower Type " << FlowType[p];
        for (int b=0; b<6; b++)
        {
            SeedSet[p][b]=allParents[p]->getCoVarQuali(3+b);
            SeedSetTot[p]+=SeedSet[p][b];
        }

        for (int v=Nw; v<Nw+Nvar; v++)
            Covar[p][v]=allParents[p]->getCoVarQuanti(v);
        if (p < 20 ) cout << endl;
    }
    cout<< " ... " << endl;
    
    for(int m=0; m<NPar; m++)
        for(int p=0; p<NPar; p++)
        {   PhenoPP1[m][p] = Covar[p][Nvar-3]-Covar[m][Nvar-2];
            PhenoPP2[m][p] = Covar[p][Nvar-1]-Covar[m][Nvar-2];
        }
            
            
    Nvar = Nvar - 3;        // On n'utilise plus les 3 dernières variables comme covariables explicatives de la fécondité
    cout << endl << Nvar << " covariates used because the last ones are used as phenological indices " << endl;
    
    mat_Barr.resize(Ntype, vector < double > (Ntype, 1.));
    mBarr.resize(Ntype, vector < double > (Ntype, 1.));
    MBarr.resize(Ntype, vector < double > (Ntype, 1.));
    PriorIn_Barr.resize( Ntype, vector<double>( Ntype, 0.5) );
    In_Barr.resize( Ntype, vector<bool>( Ntype, true) );
    qMean_Barr.resize( Ntype, vector<double>( Ntype, 0.) );
    qSD_Barr.resize( Ntype, vector<double>( Ntype, 1.) );
    matbarrTune.resize(Ntype, vector < double > (Ntype, 0.1));
    
    // dim est le nombre de "paramètres" qui vont dépendre des covariables
    // ici seulement Var[Fecp]
    
    Vec_FixedEff.resize(1, vector<double>( Nvar, 0.) );
    mFixedEff.resize( 1, vector<double>( Nvar, -100.) );
    MFixedEff.resize(1, vector<double>( Nvar, 100.) );
    PriorIn_FixedEff.resize( 1, vector<double>( Nvar, 0.5) );
    In_FixedEff.resize( 1, vector<bool>( Nvar, true) );
    qMean_FixedEff.resize( 1, vector<double>( Nvar, 0.) );
    qSD_FixedEff.resize( 1, vector<double>( Nvar, 1.) );
    vecfixedeffTune.resize( 1, vector<double>( Nvar, 0.1) );

    Vec_FixedEffSS.resize(1, vector<double>( Nvar, 0.) );
    mFixedEffSS.resize( 1, vector<double>( Nvar, -100.) );
    MFixedEffSS.resize(1, vector<double>( Nvar, 100.) );
    PriorIn_FixedEffSS.resize( 1, vector<double>( Nvar, 0.5) );
    In_FixedEffSS.resize( 1, vector<bool>( Nvar, true) );
    qMean_FixedEffSS.resize( 1, vector<double>( Nvar, 0.) );
    qSD_FixedEffSS.resize( 1, vector<double>( Nvar, 1.) );
    vecfixedeffTuneSS.resize( 1, vector<double>( Nvar, 0.1) );
    
    temp = p->getValue(MEMM_PRIOR_COVAR_FILE);
    ifstream inputPriorCovar(temp);
    if(inputPriorCovar.good())
    {
        cout << endl<<"# Loading priors for covariate effects from file " <<temp<<" ..."<<endl;
        if(temp) delete []temp;
        
        int v;
        for(v=0; v<Nvar && inputPriorCovar.good() ; v++)
        {
            for (int dim=0; dim<1; dim++){
                inputPriorCovar >> PriorIn_FixedEff[dim][v];
                inputPriorCovar >> Vec_FixedEff[dim][v];
                inputPriorCovar >> mFixedEff[dim][v];
                inputPriorCovar >> MFixedEff[dim][v];
                inputPriorCovar >> qMean_FixedEff[dim][v];
                inputPriorCovar >> qSD_FixedEff[dim][v];
                inputPriorCovar >> vecfixedeffTune[dim][v];
            }
        }
//        if(v != Nvar && v>0)
//            for(v=1; v<Nvar ; v++)
//            {
//                for (int dim=0; dim<1; dim++){
//                    PriorIn_FixedEff[dim][v] = PriorIn_FixedEff[dim][0];
//                    Vec_FixedEff[dim][v] = Vec_FixedEff[dim][0];
//                    mFixedEff[dim][v] = mFixedEff[dim][0];
//                    MFixedEff[dim][v] = MFixedEff[dim][0];
//                    qMean_FixedEff[dim][v] = qMean_FixedEff[dim][0];
//                    qSD_FixedEff[dim][v] = qSD_FixedEff[dim][0];
//                }
//            }

        for(int t=0; t<Ntype && inputPriorCovar.good() ; t++)
        {
            for (int tt=0; tt<t; tt++){
                inputPriorCovar >> PriorIn_Barr[t][tt];
                inputPriorCovar >> mat_Barr[t][tt];
                mat_Barr[t][tt] = pow(10, mat_Barr[t][tt]);
                inputPriorCovar >> mBarr[t][tt];
                mBarr[t][tt] = pow(10, mBarr[t][tt]);
                inputPriorCovar >> MBarr[t][tt];
                MBarr[t][tt] = pow(10, MBarr[t][tt]);
                inputPriorCovar >> qMean_Barr[t][tt];
                inputPriorCovar >> qSD_Barr[t][tt];
                inputPriorCovar >> matbarrTune[t][tt];
            }
            for (int tt=t+1; tt<Ntype; tt++){
                inputPriorCovar >> PriorIn_Barr[t][tt];
                inputPriorCovar >> mat_Barr[t][tt];
                mat_Barr[t][tt] = pow(10, mat_Barr[t][tt]);
                inputPriorCovar >> mBarr[t][tt];
                mBarr[t][tt] = pow(10, mBarr[t][tt]);
                inputPriorCovar >> MBarr[t][tt];
                MBarr[t][tt] = pow(10, MBarr[t][tt]);
                inputPriorCovar >> qMean_Barr[t][tt];
                inputPriorCovar >> qSD_Barr[t][tt];
                inputPriorCovar >> matbarrTune[t][tt];
            }
        }
        
//        if (polSatur == 4 || polSatur == 14 || polSatur == 24)
//        {
            for(v=0; v<Nvar && inputPriorCovar.good() ; v++)
            {
                for (int dim=0; dim<1; dim++){
                    inputPriorCovar >> PriorIn_FixedEffSS[dim][v];
                    inputPriorCovar >> Vec_FixedEffSS[dim][v];
                    inputPriorCovar >> mFixedEffSS[dim][v];
                    inputPriorCovar >> MFixedEffSS[dim][v];
                    inputPriorCovar >> qMean_FixedEffSS[dim][v];
                    inputPriorCovar >> qSD_FixedEffSS[dim][v];
                    inputPriorCovar >> vecfixedeffTuneSS[dim][v];
                }
            }
//        }

        inputPriorCovar.close();
    }
    else
    {   cout << endl<<" File " <<temp<< " unavailable for loading priors of covariate effects: default values are used instead ..." << endl;}
    
    for (int dim=0; dim<1; dim++)
        for (int v=0; v<Nvar; v++)
        {
            if (PriorIn_FixedEff[dim][v]==0) In_FixedEff[dim][v]=false;
            cout << "V(" << v << ") initial = " << Vec_FixedEff[dim][v] << " | ";
            if ((polSatur == 4 || polSatur == 14 || polSatur == 24 || polSatur == 5 || polSatur == 15 || polSatur == 25 || polSatur == 6 || polSatur == 16 || polSatur == 26) && PriorIn_FixedEffSS[dim][v]==0) In_FixedEffSS[dim][v]=false;
        }
    cout <<endl << "   covariates loaded OK..."<<endl;
}



void MEMM_logLik_multiSpe_covar::calculDistances(Configuration *p)
{
    MEMM_logLik::calculDistances(p);
    
    PhenoMP1.resize(Nm, vector < double > (NPar , 0.));
    PhenoMP2.resize(Nm, vector < double > (NPar , 0.));
    PHENO.resize(Nm, vector < long double > (NPar , 0.));


    for(int m=0; m<Nm; m++)
    {   PhenoMP1[m]=PhenoPP1[Meres[m]];
        PhenoMP2[m]=PhenoPP2[Meres[m]];
    }
    
    cout << "Delta (Phenologies) of 2 mothers x 5 fathers: " << endl;
    for (int m=0; m<2; m++)
        for (int p=0; p<5; p++) cout << PhenoMP1[m][p] << " " << PhenoMP2[m][p] << " ";
    cout << endl;

}

void MEMM_logLik_multiSpe_covar::loadAllelicFreq(Configuration * p)
{

  bool readfile = false;
  char * temp;
  int ntemp;
    double floattemp;
    vector < double > stemp (Ntype, 0.);
  vector< vector<int> > AllSize (Nl);
  //vector< std::map<int,double> > SizeFreq (Nl);
    
    
  Na.clear();
  Na.assign(Nl, 0);

//  for(int i=AllFreq.size()-1; i>=0 ; i--)
//  {
//      for(int j=AllFreq[i].size()-1; j>=0 ; j--) (AllFreq[i][j]).clear();
//      (AllFreq[i]).clear();
//  }
//  AllFreq.clear();

    AllFreq.resize(Ntype, vector < vector < double > > (Nl, vector < double > (1, 0.)));

  for(int i=SizeAll.size()-1; i>=0 ; i--) SizeAll[i].clear();
  SizeAll.clear();
  SizeAll.resize(Nl);

  temp = p->getValue(MEMM_AF);
  if(temp && strcmp("",temp)!=0)
  {
    cout << endl << "# Use allelic Frequencies from file" << endl;
    delete []temp;
    temp = p->getValue(MEMM_AF_FILE_NAME);
    ifstream fall(temp);
    if(fall.good())
    {
        cout << "# Loading allelic frequencies from file " <<temp<<" ..."<<endl;cout.flush();
      if(temp) delete []temp;
      temp = NULL;
      for (int l=0; l<Nl; l++)
      {
        fall >> Na[l];
        for (int type=0; type<Ntype; type++)
        {
            stemp[type]=0;
            AllFreq[type][l].resize(Na[l],0.);
        }
        for (int a=0; a<Na[l]; a++)
        {
          fall >> ntemp >> floattemp;
          AllSize[l].push_back(ntemp);
          SizeAll[l][ntemp]=a;
            
          stemp[0]+=floattemp;
          AllFreq[0][l][a]=floattemp;
            for (int type=1; type<Ntype; type++)
            {
                fall >> floattemp;
                stemp[type]+=floattemp;
                AllFreq[type][l][a]=floattemp;
            }
        }
        cout << "Locus " << l << " : " << Na[l] << " alleles" <<endl;
        for (int a=0; a<Na[l]; a++)
        {
            cout << AllSize[l][a] << ":";
            for (int type=0; type<Ntype; type++)
            {
                AllFreq[type][l][a] /= stemp[type];
                cout << AllFreq[type][l][a] << "-";
            }
            cout << " | ";}
            cout << endl;
      }
          
      cout << "Loading of allelic frequencies : OK. " <<endl;
      fall.close();
      readfile = true;
        
        cout << endl << "# Check parent genotypes .... " <<endl;
        int all_check;
        
        for (int l=0; l<Nl; l++)
        {
            stemp[0]=1.;
            for (int p=0; p<NPar; p++)
            {
                all_check = (dynamic_cast<parent*>(allParents[p])->getGeno(l))->getAllele(0);
                if ( (SizeAll[l].find( all_check ) == SizeAll[l].end()) && (all_check >= 0) )
                {
                    cout << " WARNING ---- Uknown allele in parent " << dynamic_cast<parent*>(allParents[p])->getName() << " locus #" << l << " allele " << all_check << endl;
                    cout << " This allele has been added to the set of known alleles... " << endl;
                    Na[l]++;
                    AllSize[l].push_back(all_check);
                    AllFreq[0][l].push_back(1/NPar);
                    stemp[0]+=1/NPar;
                    SizeAll[l][all_check]=Na[l]-1;
                }
                
                all_check = (dynamic_cast<parent*>(allParents[p])->getGeno(l))->getAllele(1);
                if ( (SizeAll[l].find( all_check ) == SizeAll[l].end()) && (all_check >= 0) )
                {
                    cout << " WARNING ---- Uknown allele in parent " << dynamic_cast<parent*>(allParents[p])->getName() << " locus #" << l << " allele " << all_check << endl;
                    cout << " This allele has been added to the set of known alleles... " << endl;
                    Na[l]++;
                    AllSize[l].push_back(all_check);
                    AllFreq[0][l].push_back(1/NPar);
                    stemp[0]+=1/NPar;
                    SizeAll[l][all_check]=Na[l]-1;
                }
            }
            for (int a=0; a<Na[l]; a++) AllFreq[0][l][a] /= stemp[0];
        }
        cout << "... All parent genotypes checked." <<endl;
    }
    else{
      readfile = false;
      cerr<<"ERROR can't read allelic frequencies from "<<temp<<endl;
    }
  }
  
  if(!readfile)
  {
      cout << " Computing AF from adults is not implemented yet. Sorry! " << endl;
  }
  if(temp) delete []temp;

  loadMatrixError(p,AllSize);
}

void MEMM_logLik_multiSpe_covar::calculTransitionMatrix(Configuration *p)
{
    int NMistype=0;    // EK -> Inclus Mistyping
    int NMigrants=0;
    int NMigBest=0;
    long double best=0.;

        std::stringstream ss;
        ss.str("");
        ss << outputDirectory << "/Transition_Probabilities.txt";
        ofstream TransProbFile(ss.str(), ofstream::out);

    
    NbPeres.resize(Ns);
  ProbSelf.resize(Ns);
  ProbMig.resize (Ns);
  ProbPeres.resize(Ns);

  individu nul("nul", genotype(Nl) );
  double probtemp;
  PerePot perepottemp;

    
    PosteriorPeres.resize(Ns);
    PosteriorOut.resize(Ns, 0.0);
    PosteriorSelf.resize(Ns, 0.0);
    if(posterior==2) for(int s=0; s<Ns; s++) PosteriorPeres[s].resize(NPar+1,0.);
    
    
    
//  cout<< endl << "# Calcul fertilization probability"<<endl;
  cout << "Computing the mendelian likelihoods..." << std::endl;
  cout << "       With NMistypeMax = " << NMistypeMax << "  and  NullAll = " << NullAll << endl; 
  for (int s=0; s<Ns; s++)
  {
    best=0.;
      NbPeres[s]=0;
    if(!useMatError)
      ProbSelf[s]=dynamic_cast<graine*>(allSeeds[s])->mendel((*allParents[Meres[MerDesc[s]]]),(*allParents[Meres[MerDesc[s]]]),Nl,Na,SizeAll,AllFreq[0]);
    else
    {
        NMistype=0;
        if (NullAll<0)
            ProbSelf[s]=dynamic_cast<graine*>(allSeeds[s])->mendel((*allParents[Meres[MerDesc[s]]]), (*allParents[Meres[MerDesc[s]]]), Nl, Na, SizeAll, AllFreq[0], MatError, NMistype);
        else
            ProbSelf[s]=dynamic_cast<graine*>(allSeeds[s])->mendelNull((*allParents[Meres[MerDesc[s]]]), (*allParents[Meres[MerDesc[s]]]), Nl, Na, SizeAll, AllFreq[0], NullAll, MatError, NMistype);
    }
      
    if (ProbSelf[s]>0 && NMistype <= NMistypeMax)
    {
        std::cout << "Seed # " << s << " (" << dynamic_cast<graine*>(allSeeds[s])->getName() << ") is possibly issued from selfing. Likelihood = " << ProbSelf[s] << std::endl;
        TransProbFile << s << " " << dynamic_cast<graine*>(allSeeds[s])->getName() << " " << dynamic_cast<parent*>(allParents[Meres[MerDesc[s]]])->getName() << " " << dynamic_cast<parent*>(allParents[Meres[MerDesc[s]]])->getName() << " " << ProbSelf[s] << " " << NMistype << std::endl;
        
    }
    else ProbSelf[s]=0;

    if(!useMatError)
      ProbMig[s]=dynamic_cast<graine*>(allSeeds[s])->mendel((*allParents[Meres[MerDesc[s]]]),nul,Nl,Na,SizeAll,AllFreq[0]);
    else
    {
        NMistype=0;
        if (NullAll<0)
            ProbMig[s]=dynamic_cast<graine*>(allSeeds[s])->mendel((*allParents[Meres[MerDesc[s]]]),nul,Nl,Na,SizeAll,AllFreq[0], MatError,NMistype);
        else
            ProbMig[s]=dynamic_cast<graine*>(allSeeds[s])->mendelNull((*allParents[Meres[MerDesc[s]]]),nul,Nl,Na,SizeAll,AllFreq[0], NullAll, MatError,NMistype);
    }

      if (ProbMig[s]==0 || NMistype > NMistypeMax)
    {
        cout << "Seed # " << s << " (" << dynamic_cast<graine*>(allSeeds[s])->getName() << ") is certainly not issued from outside or incompatible with the mother. " << endl;
        ProbMig[s]=0;
        TransProbFile << s << " " << dynamic_cast<graine*>(allSeeds[s])->getName() << " " << dynamic_cast<parent*>(allParents[Meres[MerDesc[s]]])->getName() << " OUT " << ProbMig[s] << " " << NMistype << endl;
    }
      else
          TransProbFile << s << " " << dynamic_cast<graine*>(allSeeds[s])->getName() << " " << dynamic_cast<parent*>(allParents[Meres[MerDesc[s]]])->getName() << " OUT " << ProbMig[s] << " " << NMistype << endl;


    for (int p=0; p<NPar; p++)
    {
      //(*allParents[p]).afficher();
      if(!useMatError)
        probtemp = dynamic_cast<graine*>(allSeeds[s])->mendel((*allParents[Meres[MerDesc[s]]]),(*allParents[p]),Nl,Na,SizeAll,AllFreq[0]);
      else
      {
          NMistype=0;
          if (NullAll<0)
              probtemp = dynamic_cast<graine*>(allSeeds[s])->mendel((*allParents[Meres[MerDesc[s]]]),(*allParents[p]),Nl,Na,SizeAll,AllFreq[0],MatError,NMistype);
          else
              probtemp = dynamic_cast<graine*>(allSeeds[s])->mendelNull((*allParents[Meres[MerDesc[s]]]),(*allParents[p]),Nl,Na,SizeAll,AllFreq[0],NullAll, MatError,NMistype);
      }
      //std::cout << probtemp;
      if (probtemp>0 && NMistype <= NMistypeMax)
      {
          if (probtemp>best) best=probtemp;
          NbPeres[s]++;
        perepottemp.pere_pot=p;
        perepottemp.pere_prob=probtemp;
        ProbPeres[s].push_back(perepottemp);
          if(posterior == 1 || posterior == 4) PosteriorPeres[s].push_back(0);

          TransProbFile << s << " " << dynamic_cast<graine*>(allSeeds[s])->getName() << " " << dynamic_cast<parent*>(allParents[Meres[MerDesc[s]]])->getName() << " " << dynamic_cast<parent*>(allParents[p])->getName() << " " << probtemp << " " << NMistype << std::endl;

      }
    }

      if (NbPeres[s]==0) NMigrants++;
      else if (ProbMig[s]>best) NMigBest++;
      
    cout << "Seed #" << s << " " << dynamic_cast<graine*>(allSeeds[s])->getName() << " Probmig =" << ProbMig[s] << "; NbPeres =" << NbPeres[s] << ", Best-lik =" << best <<  endl;
  }
    cout << endl << "Computation of the mendelian likelihoods : OK. " <<endl;
    cout << NMigrants << " seeds with no compatible father inside ; " << NMigBest << " seeds with Pmig > Pbest_inside..." << endl << endl;

  //std::cout << "Seed #1: " << NbPeres[1] << " , " << MerDesc[1] << " , " << ProbMig[1] << " ," << ProbSelf[1] << " , " << ProbPeres[1][0].pere_pot << " , " << ProbPeres[1][0].pere_prob << std::endl;

  //for (int p=0; p<NPar; p++) {std::cout << DistMP[MerDesc[1]][p] << " ; ";}

}

void MEMM_logLik_multiSpe_covar::loadParameters(Configuration *p)
{
    MEMM_logLik::loadParameters(p);
    
    Parameter * param_temp;
    char * temp;
    
    Vec_Fec_All.resize(NPar, 1.0);
    Vec_ODD.resize(NPar, 1.0);
    
    Vec_Fec_Clone.resize(Ntype, vector < double > (1,1.));
    for (int t=0; t<Ntype; t++) (Vec_Fec_Clone[t]).resize(NClone[t], 1.);
    
    cout<< "Load more and more Parameters..."<<endl;
    
    // bSI
    param_temp = allParameters["b_Self_Incompat"];
    if(param_temp)
    {
        bSI = param_temp->INIT;
        mbSI = param_temp->MIN;
        MbSI = param_temp->MAX;
        cout << "b_Self_Incompat : "<< bSI << " [" << mbSI <<"|"<< MbSI << "] ... Tune : " << param_temp->TUNE <<endl;
    }
    
    
    // Delta_opt1
    param_temp = allParameters["Delta_opt1"];
    if(param_temp)
    {
        Delta_opt1 = param_temp->INIT;
        mDelta_opt1 = param_temp->MIN;
        MDelta_opt1 = param_temp->MAX;
        cout << "Delta_opt1 : "<< Delta_opt1 << " [" << mDelta_opt1 <<"|"<< MDelta_opt1 << "] ... Tune : " << param_temp->TUNE <<endl;
    }
    
    // Delta_opt1
    param_temp = allParameters["Delta_opt2"];
    if(param_temp)
    {
        Delta_opt2 = param_temp->INIT;
        mDelta_opt2 = param_temp->MIN;
        MDelta_opt2 = param_temp->MAX;
        cout << "Delta_opt2 : "<< Delta_opt2 << " [" << mDelta_opt2 <<"|"<< MDelta_opt2 << "] ... Tune : " << param_temp->TUNE <<endl;
    }
    
    // Sigma_pheno1
    param_temp = allParameters["Sigma_pheno1"];
    if(param_temp)
    {
        Sigma_pheno1 = param_temp->INIT;
        mSigma_pheno1 = param_temp->MIN;
        MSigma_pheno1 = param_temp->MAX;
        cout << "Sigma_pheno1 : "<< Sigma_pheno1 << " [" << mSigma_pheno1 <<"|"<< MSigma_pheno1 << "] ... Tune : " << param_temp->TUNE <<endl;
    }
    
    // Delta_opt1
    param_temp = allParameters["Sigma_pheno2"];
    if(param_temp)
    {
        Sigma_pheno2 = param_temp->INIT;
        mSigma_pheno2 = param_temp->MIN;
        MSigma_pheno2 = param_temp->MAX;
        cout << "Sigma_pheno2 : "<< Sigma_pheno2 << " [" << mSigma_pheno2 <<"|"<< MSigma_pheno2 << "] ... Tune : " << param_temp->TUNE <<endl;
    }
    
    // Weight12
    param_temp = allParameters["Weight12"];
    if(param_temp)
    {
        Weight12 = param_temp->INIT;
        mWeight12 = param_temp->MIN;
        MWeight12 = param_temp->MAX;
        cout << "Weight12 : "<< Weight12 << " [" << mWeight12 <<"|"<< MWeight12 << "] ... Tune : " << param_temp->TUNE <<endl;
    }
    
    
    // Blimpol
    param_temp = allParameters["B_lim_pol"];
    if(param_temp)
    {
        Blimpol = param_temp->INIT;
        mBlimpol = param_temp->MIN;
        MBlimpol = param_temp->MAX;
        cout << "Blimpol : "<< Blimpol << " [" << mBlimpol <<"|"<< MBlimpol << "] ... Tune : " << param_temp->TUNE <<endl;
    }
    
    char * limpolModel = "diffspecific";
    limpolModel = p->getValue(MEMM_PARAM_POLLIM);
    if (strcmp(limpolModel, "limitation")== 0 ) polSatur = 0;
    else if (strcmp(limpolModel, "limitationGonly")== 0 ) polSatur = 10;
    else if (strcmp(limpolModel, "limitationSSonly")== 0 ) polSatur = 20;
    else if (strcmp(limpolModel, "saturation")== 0 ) polSatur = 1;
    else if (strcmp(limpolModel, "saturationGonly")== 0 ) polSatur = 11;
    else if (strcmp(limpolModel, "saturationSSonly")== 0 ) polSatur = 21;
    else if (strcmp(limpolModel, "postzygotique")== 0 ) polSatur = 2;
    else if (strcmp(limpolModel, "postzygotiqueGonly")== 0 ) polSatur = 12;
    else if (strcmp(limpolModel, "postzygotiqueSSonly")== 0 ) polSatur = 22;
    else if (strcmp(limpolModel, "limitationBis")== 0 ) polSatur = 3;
    else if (strcmp(limpolModel, "limitationBisGonly")== 0 ) polSatur = 13;
    else if (strcmp(limpolModel, "limitationBisSSonly")== 0 ) polSatur = 23;
    else if (strcmp(limpolModel, "stackAll")== 0 ) polSatur = 4;
    else if (strcmp(limpolModel, "stackAllGonly")== 0 ) polSatur = 14;
    else if (strcmp(limpolModel, "stackAllSSonly")== 0 ) polSatur = 24;
    else if (strcmp(limpolModel, "stackAllAllo")== 0 ) polSatur = 5;
    else if (strcmp(limpolModel, "stackAllAlloGonly")== 0 ) polSatur = 15;
    else if (strcmp(limpolModel, "stackAllAlloSSonly")== 0 ) polSatur = 25;
    else if (strcmp(limpolModel, "prePostSI")== 0 ) polSatur = 6;
    else if (strcmp(limpolModel, "prePostSIGonly")== 0 ) polSatur = 16;
    else if (strcmp(limpolModel, "prePostSISSonly")== 0 ) polSatur = 26;
    else polSatur = 0;
    cout<<"Pollen limitation model :" << limpolModel << " " << polSatur << endl;
    if(limpolModel) delete []limpolModel;
    cout.flush();

    
    // TotSeuilA & TotSeuilL
    param_temp = allParameters["Tot_Seuil_Asta"];
    if(param_temp)
    {
        TotSeuilA = param_temp->INIT;
        mTotSeuilA = param_temp->MIN;
        MTotSeuilA = param_temp->MAX;
        cout << "TotSeuil_Asta : "<< TotSeuilA << " [" << mTotSeuilA <<"|"<< MTotSeuilA << "] ... Tune : " << param_temp->TUNE <<endl;
    }
    param_temp = allParameters["Tot_Seuil_Longi"];
    if(param_temp)
    {
        TotSeuilL = param_temp->INIT;
        mTotSeuilL = param_temp->MIN;
        MTotSeuilL = param_temp->MAX;
        cout << "TotSeuil_Longi : "<< TotSeuilL << " [" << mTotSeuilL <<"|"<< MTotSeuilL << "] ... Tune : " << param_temp->TUNE <<endl;
    }
    
    //    // AlRed
    //    param_temp = allParameters["Alpha_Zero_Deflated_Binomial"];
    //    if(param_temp)
    //    {
    //      AlRed = param_temp->INIT;
    //      mAlRed = param_temp->MIN;
    //      MAlRed = param_temp->MAX;
    //      cout << "AlRed : "<< AlRed << " [" << mAlRed <<"|"<< MAlRed << "]"<< endl;
    //    }
    
    
    // GamC
    param_temp = allParameters["Gam_Clone"];
    if(param_temp)
    {
        GamC = param_temp->INIT;
        mGamC = param_temp->MIN;
        MGamC = param_temp->MAX;
        cout << "GamC : " << GamC << " [" << mGamC << "|" << MGamC << "] ... Tune : " << param_temp->TUNE <<endl;
    }
    
    if (GamC >0) pLoiClone = loadDistribution(p, "individual_fecundities_clone");

    cout << " Covariates effects - Tune : ";
    for (int t=0; t<Nvar; t++) {cout << vecfixedeffTune[0][t] << " | ";}
    cout << endl;

    cout << " Barriers effects - Tune : ";
    for(int t=0; t<Ntype ; t++)
    {
        for (int tt=0; tt<t; tt++) cout << matbarrTune[t][tt] << " | ";
        for (int tt=t+1; tt<Ntype; tt++) cout << matbarrTune[t][tt] << " | ";
        cout << endl;
    }
    cout << endl;
    
    if (polSatur == 4 || polSatur == 14 || polSatur == 24 || polSatur == 5 || polSatur == 15 || polSatur == 25 || polSatur == 6 || polSatur == 16 || polSatur == 26)
    {
    cout << " Covariates effects on seedset - Tune : ";
    for (int t=0; t<Nvar; t++) {cout << vecfixedeffTuneSS[0][t] << " | ";}
    cout << endl;
    }

    
    cout << "... end reading Parameters" << endl << endl << endl;
    
}

void MEMM_logLik_multiSpe_covar::exportNEP(int iteration)
{
    vector < vector<long double> > mat (NPar, vector < long double > (NPar, 0.));
    vector <long double> tot (NPar, 0.);
    vector <long double> nep (NPar, 0.);
    int pbm=0;

    *NepFile << iteration << " ";
        for (int m=0; m<NPar; m++)
        {
            for (int p=0; p<NPar; p++)
            {
                mat[m][p] = DISPPP[m][p] * PHENOPP[m][p] * Vec_Fec_All[p];
                if (isnan(mat[m][p])){mat[m][p]=0; pbm=1;}
                tot[m]+=mat[m][p];
                nep[m]+=mat[m][p]*mat[m][p];
            }
            if (tot[m]==0) pbm=1;
            nep[m]/=tot[m]*tot[m];
            *NepFile <<  1/nep[m] << " ";
        }
        if (pbm==1) { std::cout << " Warning: Overflow. One component of the mating matrix was not defined or tot=0 " << std::endl;}
        
        *NepFile << endl;
        NepFile->flush();
}


void MEMM_logLik_multiSpe_covar::exportPosteriors()
{
    double tottemp;
    double probtemp, probtemp0;   // EK -> Inclus Mistyping
    string name;
    char * temp;
    int NMistype=0;    // EK -> Inclus Mistyping
    individu nul("nul", genotype(Nl));
   
    double best, scnd, nmisbest, dbest, azbest, dbest2, azbest2, dbest3, azbest3;
    int idbest, idscnd, idbest2, idscnd2;

    
    cout<<endl<<"# Exporting the posterior probabilities ... "<<endl;
    cout<<endl<<"Based on ... "<< Nposterior << "  sampled MCMC steps" << endl;
    
    if (posterior==1)
        *fposterior << "Seed#  SeedID  Mother Father Gen_Lik  NMistypings  PosteriorProb" << endl;
    else if (posterior==2)
    {   *fposterior << "Seed#  SeedID  ";
        for (int m=0; m<NPar; m++) *fposterior << dynamic_cast<parent*>(allParents[m])->getName() << " ";
        *fposterior << "OUT" << endl;
    }
    else if (posterior==4)
    {
        *fposterior << "#Indiv" << " " << "ID_Indiv" << " " << "Nb_Parents" ;
        *fposterior << "Prob_best" << " " << "Prob_scnd" << " " << "ID_best" << "ID_scnd";
        *fposterior << "NMistype_best" << " " << "Dist_best" << " " << "Dist_scnd" << " " << "Azim_best" << " " << "Azim_scnd" << endl;
    }

    for (int s=0; s<Ns; s++)
    {
        if (posterior==1) {
        tottemp=1;
        for (int m=0; m<NbPeres[s]; m++) {
            if(!useMatError)
                probtemp = dynamic_cast<graine*>(allSeeds[s])->mendel((*allParents[Meres[MerDesc[s]]]),(*allParents[ProbPeres[s][m].pere_pot]),Nl,Na,SizeAll,AllFreq[0]);
            else
            { NMistype=0;
                probtemp = dynamic_cast<graine*>(allSeeds[s])->mendel((*allParents[Meres[MerDesc[s]]]),(*allParents[ProbPeres[s][m].pere_pot]),Nl,Na,SizeAll,AllFreq[0],MatError,NMistype);
            }
            *fposterior << s << " " << dynamic_cast<graine*>(allSeeds[s])->getName() << " " << dynamic_cast<parent*>(allParents[Meres[MerDesc[s]]])->getName() << " " << dynamic_cast<parent*>(allParents[ProbPeres[s][m].pere_pot])->getName() << " ";
            *fposterior << ProbPeres[s][m].pere_prob << " " << NMistype << " " << PosteriorPeres[s][m]/Nposterior << endl;  // EK -> Inclus Mistyping
            tottemp -= PosteriorPeres[s][m]/Nposterior;
        }
            
        *fposterior << s << " " << dynamic_cast<graine*>(allSeeds[s])->getName() << " " << dynamic_cast<parent*>(allParents[Meres[MerDesc[s]]])->getName() << " OUT " << ProbMig[s] << " 0 " << PosteriorOut[s]/Nposterior << endl; // EK -> Inclus Mistyping
        }
        
        else if (posterior==2)
        {
            *fposterior << s << " " << dynamic_cast<graine*>(allSeeds[s])->getName() << " ";
            for (int m=0; m<NPar+1; m++) *fposterior << PosteriorPeres[s][m]/Nposterior << " ";
            *fposterior << endl;
            
        }
        
        else if (posterior==4)
        {
            *fposterior << s << " " << dynamic_cast<graine*>(allSeeds[s])->getName() << " ";
            
            best=0.0;
            scnd=0.0;
            idbest=-1;
            idscnd=-1;
            nmisbest=-1.;
            dbest=-1.;dbest2=-1.;
            azbest=0.;azbest2=0.;
            tottemp=1;

            if (s==3) cout << PosteriorPeres[s][0]/Nposterior << " ";
            
            for (int m=0; m<NbPeres[s]; m++) {
                probtemp=PosteriorPeres[s][m]/Nposterior;
                if (probtemp>best) {
                    scnd=best; idscnd=idbest;
                    best=probtemp; idbest=ProbPeres[s][m].pere_pot;
                    if(!useMatError) NMistype=0;
                    else
                    { NMistype=0;
                        probtemp0 = dynamic_cast<graine*>(allSeeds[s])->mendel((*allParents[Meres[MerDesc[s]]]),(*allParents[ProbPeres[s][m].pere_pot]),Nl,Na,SizeAll,AllFreq[0],MatError,NMistype);
                    }
                    nmisbest=NMistype;
                    dbest2=dbest; azbest2=azbest;
                    dbest=dynamic_cast<parent*>(allParents[Meres[MerDesc[s]]])->dist((*allParents[ProbPeres[s][m].pere_pot]));
                    azbest=dynamic_cast<parent*>(allParents[Meres[MerDesc[s]]])->azim((*allParents[ProbPeres[s][m].pere_pot]));
                }
                else if (probtemp>scnd)
                {scnd=probtemp; idscnd=m;
                    dbest2=dynamic_cast<parent*>(allParents[Meres[MerDesc[s]]])->dist((*allParents[ProbPeres[s][m].pere_pot]));
                    azbest2=dynamic_cast<parent*>(allParents[Meres[MerDesc[s]]])->azim((*allParents[ProbPeres[s][m].pere_pot]));
                }
                tottemp -= probtemp;
            }
            probtemp = PosteriorOut[s]/Nposterior;
            if (s==3) cout << tottemp << " =?= " << probtemp << endl;

            if (probtemp>best) {
                scnd=best; idscnd=idbest;
                best=probtemp; idbest=-1;
                nmisbest=0;
                dbest2=dbest;dbest=-1.;
                azbest2=azbest;azbest=0.;
            }
            else if (probtemp>scnd) {scnd=probtemp; idscnd=-1;  dbest2=-1.; azbest2=0.;}
            
            *fposterior << NbPeres[s] << " " << best << " " << scnd << " ";
            if (idbest<0) *fposterior << "-1 ";  else *fposterior << dynamic_cast<parent*>(allParents[idbest])->getName() << " ";
             if (idscnd<0) *fposterior << "-1 ";  else *fposterior << dynamic_cast<parent*>(allParents[idscnd])->getName() << " ";
            *fposterior  << nmisbest << " " << dbest << " " << dbest2 << " " << azbest << " " << azbest2 << endl;
        }
    }
    
    (*fposterior).close();
    if(temp) delete []temp;
}
