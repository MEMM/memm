/**************************************************************************************************************
 * memm is a bayesian estimation of pollen dispersal kernel and variance in male fecundity.                   *
 * copyright (c) 2009-2016 etienne klein <etienne.klein@paca.inra.fr>                                         *
 *                                                                                                            *
 * this file is part of memm.                                                                                 *
 *                                                                                                            *
 * memm is free software: you can redistribute it and/or modify                                               *
 * it under the terms of the gnu general public license as published by                                       *
 * the free software foundation, either version 3 of the license, or                                          *
 * (at your option) any later version.                                                                        *
 *                                                                                                            *
 * memm is distributed in the hope that it will be useful,                                                    *
 * but without any warranty; without even the implied warranty of                                             *
 * merchantability or fitness for a particular purpose.  see the                                              *
 * gnu general public license for more details.                                                               *
 *                                                                                                            *
 * you should have received a copy of the gnu general public license                                          *
 * along with memm.  if not, see <http://www.gnu.org/licenses/>.                                              *
 **************************************************************************************************************/

#include "MEMM_seedlings.hpp"

MEMM_seedlings::MEMM_seedlings()
{
  cout<< "MEMM : Seedlings mode"<<endl;
  pLois = NULL;
  Pi0s = mPi0s = MPi0s = 0;
    Nposterior = 0;
}

MEMM_seedlings::~MEMM_seedlings()
{
  if(pLois) delete pLois;
  Vec_Fecs.clear();
  Vec_Fecp.clear();
}

void MEMM_seedlings::mcmc(int nbIteration, int thinStep)
{
  
    ofstream cSuivi ("suiviPasAPas.txt");
    int suivi=1;

    double lastFecLogLiks, nextFecLogLiks;
    double lastFecLogLikp, nextFecLogLikp;
    double previousValue, nextValue;
    double previousValue2;
    long double previousLogLik, nextLogLik;
    
    accept_ratio gamasRatio = allParameters["gamas"]->accept_ratio_;
    accept_ratio scalesRatio = allParameters["scales"]->accept_ratio_;
    accept_ratio shapesRatio = allParameters["shapes"]->accept_ratio_;
    //     accept_ratio kappasRatio = allParameters["kappas"]->accept_ratio_;
    //     accept_ratio thetasRatio = allParameters["thetas"]->accept_ratio_;
    accept_ratio migsRatio = allParameters["migs"]->accept_ratio_;
    
    accept_ratio gamaRatio = allParameters["gama"]->accept_ratio_;
    accept_ratio scaleRatio = allParameters["scale"]->accept_ratio_;
    accept_ratio shapeRatio = allParameters["shape"]->accept_ratio_;
    accept_ratio migRatio = allParameters["mig"]->accept_ratio_;
    accept_ratio selfRatio = allParameters["self"]->accept_ratio_;
    
    
    int xpourCent = nbIteration/20;
    if(!xpourCent) xpourCent=1;
    
    lastFecLogLikp = pLoi->logLik(NPar, Vec_Fecp,GamA);
    lastFecLogLiks = pLois->logLik(NPar, Vec_Fecs,GamAs);
    previousLogLik = logLik(posterior);
    
    cout <<previousLogLik << endl;
    cout.flush();
    
    //output start value
    if(thinStep){
        
        *ParamFec << "iteration  LogLik(FecS)  Var(FecS)  LogLikp(FecP)  Var(FecP)" << endl;
        *IndivFec << "iteration " ;
        for (int p=0; p<NPar; p++) {*IndivFec << "Fs_" << dynamic_cast<parent*>(allParents[p])->getName() << " ";}
        for (int p=0; p<NPar; p++) {*IndivFec << "Fp_" << dynamic_cast<parent*>(allParents[p])->getName() << " ";}
        *IndivFec << endl;
        *ParamDisp << "iteration  LogLik  Scales  Shapes "
        //        << "Kappas  Thetas "
        << "MigS "
        //        << "Mig0S  bMigS  AgeThS  bFecS "
        << "ScaleP  ShapeP "
        //        << "KappaP  ThetaP "
        << "MigP "
        //      << " Mig0P  bMigP "
        //      << " AgeThP  bFecP "
        << " Self "
        <<endl;
        
        *ParamFec << "0 " << lastFecLogLiks << " " << GamAs << " " << lastFecLogLikp << " " << GamA << endl;
        ParamFec->flush();
        *IndivFec << "0 " ;
        for (int p=0; p<NPar; p++) {*IndivFec << Vec_Fecs[p] << " ";}
        for (int p=0; p<NPar; p++) {*IndivFec << Vec_Fecp[p] << " ";}
        *IndivFec << endl;
        IndivFec->flush();
        *ParamDisp << "0 " << previousLogLik
        << " " << Scales
        << " " << Shapes
        << " " << Migs
        << " " << Scale
        << " " << Shape
        << " " << Mig
        << " " << Self
        <<endl;
        ParamDisp->flush();
        
    }
    
    if(nbIteration) cout << "it=1..."<< std::endl;
    for(int ite=1; ite<=nbIteration; ite++)
    {
        if (ite % xpourCent == 0) {cout << "it="<<ite << "..."<< std::endl;}
        
        //GamAs- fecundities distribution
        //nextValue = pow(GamAs, exp(0.2*gauss()));
        if( gamasRatio(1.0,1.0) != 0 )
        {
            nextValue = GamAs*exp(0.2*gauss());
            if( nextValue>mGamAs && nextValue<MGamAs )
            {
                lastFecLogLiks = pLois->logLik(NPar, Vec_Fecs,GamAs);
                nextFecLogLiks = pLois->logLik(NPar, Vec_Fecs, nextValue);
                if (suivi>0) cSuivi << ite << " GamAs " << nextValue << " " << lastFecLogLiks << " " << nextFecLogLiks << " " << nextFecLogLiks-lastFecLogLiks << " ";
                if( accept() < exp(nextFecLogLiks-lastFecLogLiks) * gamasRatio(GamAs,nextValue) )
                {
                    GamAs = nextValue;
                    lastFecLogLiks = nextFecLogLiks;
                    pLois->setDParam(MEMM_LOI_GAMA,GamAs);
                    if (suivi>0) cSuivi << "Accept" << endl;
                }
                else if (suivi>0) cSuivi << "Reject" << endl;
            }
        }
        if(thinStep) *ParamFec << ite << " " << lastFecLogLiks << " " << GamAs << " ";
        
        //GamA (GamAp)- fecundities distribution
        //nextValue = pow(GamA, exp(0.2*gauss()));
        if( gamaRatio(1.0,1.0) != 0 )
        {
            nextValue = GamA*exp(0.2*gauss());
            if( nextValue>mGamA && nextValue<MGamA )
            {
                lastFecLogLikp = pLoi->logLik(NPar, Vec_Fecp,GamA);
                nextFecLogLikp = pLoi->logLik(NPar, Vec_Fecp, nextValue);
                if (suivi>0) cSuivi << ite << " Gamap " << nextValue << " "  << lastFecLogLikp << " " << nextFecLogLikp << " " << nextFecLogLikp-lastFecLogLikp << " ";
                if( accept() < exp(nextFecLogLikp-lastFecLogLikp) * gamaRatio(GamA,nextValue))
                {
                    GamA = nextValue;
                    lastFecLogLikp = nextFecLogLikp;
                    pLoi->setDParam(MEMM_LOI_GAMA,GamA);
                    if (suivi>0) cSuivi << "Accept" << endl;
                }
                else if (suivi>0) cSuivi << "Reject" << endl;
            }
        }
        if(thinStep) *ParamFec << lastFecLogLikp << " " << GamA << endl;
        
        // Individual fecundities
        if( (gamasRatio(1.0,1.0) != 0) || (gamaRatio(1.0,1.0) != 0) || (GamA != 0) || (GamAs != 0))
        {
            for(int p=0 ; p<NPar ; p++)
            {
                previousValue = Vec_Fecs[p];
                previousValue2 = Vec_Fecp[p];
                pLois->tirage(Vec_Fecs[p]);
                pLoi->tirage(Vec_Fecp[p]);
                nextLogLik = logLik();
                if (suivi>1) cSuivi << ite << " Fecundities " << p << " "  << previousValue << " "  << Vec_Fecs[p] << " " << previousValue2 << " "  << Vec_Fecp[p] << " "  << previousLogLik << " " << nextLogLik << " " << nextLogLik-previousLogLik << " ";
                if(accept()<exp(nextLogLik-previousLogLik))
                {
                    previousLogLik = nextLogLik;
                    if (suivi>1) cSuivi << "Accept" << endl;
                }
                else
                {
                    Vec_Fecs[p] = previousValue;
                    Vec_Fecp[p] = previousValue2;
                    if (suivi>1) cSuivi << "Reject" << endl;
                }
            }
        }
        
        if(thinStep && (ite%thinStep == 0))
        {
            *IndivFec << ite << " " ;
            for (int p=0; p<NPar; p++) {*IndivFec << Vec_Fecs[p] << " ";}
            for (int p=0; p<NPar; p++) {*IndivFec << Vec_Fecp[p] << " ";}
            *IndivFec << endl;
            IndivFec->flush();
        }
        
        // Scales - seed dispersal distance
        if( scalesRatio(1.0,1.0) != 0 )
        {
            previousValue = Scales;
            Scales = previousValue*exp(0.2*gauss());
            if( Scales>mScales && Scales<MScales)
            {
                as=Scales*exp(gammln(2/Shapes)-gammln(3/Shapes));
                abs=-pow(as,-Shapes);
                //        Kabs=Shapes/2/3.14/as/as/exp(gammln(2/Shapes));
                
                nextLogLik = logLik();
                if (suivi>0) cSuivi << ite << " FemaleParam1 " << Scales << " "  << previousLogLik << " " << nextLogLik << " " << nextLogLik-previousLogLik << " ";
                if( accept() < exp(nextLogLik-previousLogLik) * scalesRatio(previousValue, Scales)) {
                    previousLogLik = nextLogLik;
                    if (suivi>0) cSuivi << "Accept" << endl;
                }
                else
                {
                    Scales = previousValue;
                    as=Scales*exp(gammln(2/Shapes)-gammln(3/Shapes));
                    abs=-pow(as,-Shapes);
                    //          Kabs=Shapes/2/3.14/as/as/exp(gammln(2/Shapes));
                    
                    if (suivi>0) cSuivi << "Reject" << endl;
                }
            }
            else Scales = previousValue;
        }
        
        //change Shapes
        if( shapesRatio(1.0,1.0) != 0 )
        {
            previousValue = Shapes;
            Shapes = previousValue*exp(0.2*gauss());
            if( Shapes>mShapes && Shapes<MShapes)
            {
                as=Scales*exp(gammln(2/Shapes)-gammln(3/Shapes));
                abs=-pow(as,-Shapes);
                //        Kabs=Shapes/2/3.14/as/as/exp(gammln(2/Shapes));
                nextLogLik = logLik();
                if (suivi>0) cSuivi << ite << " FemaleParam2 " << Shapes << " "  << previousLogLik << " " << nextLogLik << " " << nextLogLik-previousLogLik << " ";
                if( accept() < exp(nextLogLik-previousLogLik) * shapesRatio(previousValue, Shapes)) {
                    previousLogLik = nextLogLik;
                    if (suivi>0) cSuivi << "Accept" << endl;
                }
                else{
                    Shapes = previousValue;
                    as=Scales*exp(gammln(2/Shapes)-gammln(3/Shapes));
                    abs=-pow(as,-Shapes);
                    //          Kabs=Shapes/2/3.14/as/as/exp(gammln(2/Shapes));
                    if (suivi>0) cSuivi << "Reject" << endl;
                }
            }
            else Shapes = previousValue;
        }
        
        //      //change Kappas
        //      if( kappasRatio(1.0,1.0) != 0 )
        //      {
        //          previousValue = Kappas;
        //          Kappas = previousValue*exp(Tune*gauss());
        //          if( Kappas>mKappas && Kappas<MKappas)
        //          {
        //              nextLogLik = logLik();
        //              if (suivi>0) cSuivi << ite << " FemaleParam3 " << Kappas << " "  << previousLogLik << " " << nextLogLik << " " << nextLogLik-previousLogLik << " ";
        //              if( accept() < exp(nextLogLik-previousLogLik)  * kappasRatio(previousValue, Kappas)) {
        //                  previousLogLik = nextLogLik;
        //                  if (suivi>0) cSuivi << "Accept" << endl;
        //              }
        //              else{
        //                  Kappas = previousValue;
        //                  if (suivi>0) cSuivi << "Reject" << endl;
        //              }
        //          }
        //          else Kappas = previousValue;
        //      }
        //
        //      //change Thetas
        //      if( thetasRatio(1.0,1.0) != 0 )
        //      {
        //      previousValue = Thetas;
        //      Thetas = fmod((previousValue + Tune*gauss()),3.14159);
        //      if( Thetas>mThetas && Thetas<MThetas)
        //      {
        //          nextLogLik = logLik();
        //          if (suivi>0) cSuivi << ite << " FemaleParam4 " << Thetas << " "  << previousLogLik << " " << nextLogLik << " " << nextLogLik-previousLogLik << " ";
        //          if( accept() < exp(nextLogLik-previousLogLik) * thetasRatio(previousValue, Thetas) ) {
        //              previousLogLik = nextLogLik;
        //              if (suivi>0) cSuivi << "Accept" << endl;
        //          }
        //          else{
        //              Thetas = previousValue;
        //              if (suivi>0) cSuivi << "Reject" << endl;
        //          }
        //      }
        //      else Thetas = previousValue;
        //      }
        
        // change Migs
        if( migsRatio(1.0,1.0) != 0 )
        {
            previousValue = Migs;
            Migs = previousValue*exp(0.2*gauss());
            if( Migs>mMigs && Migs<MMigs)
            {
                nextLogLik = logLik();
                if (suivi>0) cSuivi << ite << " Migration Seed " << Migs << " "  << previousLogLik << " " << nextLogLik << " " << nextLogLik-previousLogLik << " ";
                if( accept() < exp(nextLogLik-previousLogLik) * migsRatio(previousValue, Migs)) {
                    previousLogLik = nextLogLik;
                    if (suivi>0) cSuivi << "Accept" << endl;
                }
                else
                {
                    Migs = previousValue;
                    if (suivi>0) cSuivi << "Reject" << endl;
                }
            }
            else Migs = previousValue;
        }
        
        // change Scale
        if( scaleRatio(1.0,1.0) != 0 )
        {
            previousValue = Scale;
            Scale = previousValue*exp(0.2*gauss());
            if( Scale>mScale && Scale<MScale)
            {
                ap=Scale*exp(gammln(2/Shape)-gammln(3/Shape));
                abp=-pow(ap,-Shape);
                nextLogLik = logLik();
                if (suivi>0) cSuivi << ite << " MaleParam1 " << Scale << " "  << previousLogLik << " " << nextLogLik << " " << nextLogLik-previousLogLik << " ";
                if( accept() < exp(nextLogLik-previousLogLik) * scaleRatio(previousValue, Scale)) {
                    previousLogLik = nextLogLik;
                    if (suivi>0) cSuivi << "Accept" << endl;
                }
                else
                {
                    Scale = previousValue;
                    ap=Scale*exp(gammln(2/Shape)-gammln(3/Shape));
                    abp=-pow(ap,-Shape);
                    if (suivi>0) cSuivi << "Reject" << endl;
                }
            }
            else Scale = previousValue;
        }
        
        // change Shape
        if( shapeRatio(1.0,1.0) != 0 )
        {
            previousValue = Shape;
            Shape = previousValue*exp(0.2*gauss());
            if( Shape>mShape && Shape<MShape)
            {
                ap=Scale*exp(gammln(2/Shape)-gammln(3/Shape));
                abp=-pow(ap,-Shape);
                nextLogLik = logLik();
                if (suivi>0) cSuivi << ite << " MaleParam2 " << Shape << " "  << previousLogLik << " " << nextLogLik << " " << nextLogLik-previousLogLik << " ";
                if( accept() < exp(nextLogLik-previousLogLik) * shapeRatio(previousValue, Shape)) {
                    previousLogLik = nextLogLik;
                    if (suivi>0) cSuivi << "Accept" << endl;
                }
                else{
                    Shape = previousValue;
                    ap=Scale*exp(gammln(2/Shape)-gammln(3/Shape));
                    abp=-pow(ap,-Shape);
                    if (suivi>0) cSuivi << "Reject" << endl;
                }
            }
            else Shape = previousValue;
        }
        
        // change Mig
        if( migRatio(1.0,1.0) != 0 )
        {
            previousValue = Mig;
            Mig = previousValue*exp(0.2*gauss());
            if( Mig>mMig && Mig<MMig)
            {
                nextLogLik = logLik();
                if (suivi>0) cSuivi << ite << " Migration Pollen " << Mig << " "  << previousLogLik << " " << nextLogLik << " " << nextLogLik-previousLogLik << " ";
                if( accept() < exp(nextLogLik-previousLogLik) * migRatio(previousValue, Mig)) {
                    previousLogLik = nextLogLik;
                    if (suivi>0) cSuivi << "Accept" << endl;
                }
                else
                {
                    Mig = previousValue;
                    if (suivi>0) cSuivi << "Reject" << endl;
                }
            }
            else Mig = previousValue;
        }
        
        // change Self
        if( selfRatio(1.0,1.0) != 0 )
        {
            previousValue = Self;
            Self = previousValue*exp(0.2*gauss());
            if( Self>mSelf && Self<MSelf)
            {
                nextLogLik = logLik();
                if (suivi>0) cSuivi << ite << " SelfingRate " << Self << " "  << previousLogLik << " " << nextLogLik << " " << nextLogLik-previousLogLik << " ";
                if( accept() < exp(nextLogLik-previousLogLik) * selfRatio(previousValue, Self) ) {
                    previousLogLik = nextLogLik;
                    if (suivi>0) cSuivi << "Accept" << endl;
                }
                else {
                    Self = previousValue;
                    if (suivi>0) cSuivi << "Reject" << endl;
                }
            }
            else Self = previousValue;
        }
        
        *ParamDisp << ite<<" " << previousLogLik
        << " " << Scales
        << " " << Shapes
        //      << " " << Kappas
        //      << " " << Thetas
        << " " << Migs
        << " " << Scale
        //      << Kappa << " " << Theta
        << " " << Shape
        << " " << Mig
        << " " << Self
        <<endl;
        ParamDisp->flush();
    }
    if (thinStep) exportPosteriors();
}

void MEMM_seedlings::mcmc_dyn(int nbIteration, int thinStep)
{
  cerr<<"Dynamic seedlings not implemented yet"<<endl;
  exit(0);
}



/***************************** PROTECTED ********************************/

long double MEMM_seedlings::logLik(int posterior)
{
    
    long double liktemp = 0;
    long double pip = 0;
    long double pip2 = 0;
    long double piptot=0;
    int pbm = 0;
    
    vector < vector<long double> > matp (NPar, vector <long double> (NPar, 0.) );
    vector < vector<long double> > mats (Ns, vector <long double> (NPar, 0.) );
    vector <long double> totp (NPar);
    vector <long double> tots (Ns);
    
    for (int m=0; m<NPar; m++)
    {
        totp[m]=0;
        for (int p=0; p<m; p++)
        {
            matp[m][p]=exp(abp*pow(DistMP[m][p],Shape))*Vec_Fecp[p];
            if (isnan(matp[m][p])){matp[m][p]=0; pbm=1;}
            totp[m]+=matp[m][p];
        }
        for (int p=m+1; p<NPar; p++)
        {
            matp[m][p]=exp(abp*pow(DistMP[m][p],Shape))*Vec_Fecp[p];
            if (isnan(matp[m][p])){matp[m][p]=0; pbm=1;}
            totp[m]+=matp[m][p];
        }
        matp[m][m]=Self*totp[m]/(1-Mig-Self);
    }
    
    for (int s=0; s<Ns; s++)
    {
        tots[s]=0;
        for (int m=0; m<NPar; m++)
        {
            mats[s][m]=exp(abs*pow(DistSM[s][m],Shapes))*Vec_Fecs[m];
            if (isnan(mats[s][m])){mats[s][m]=0; pbm=1;}
            tots[s]+=mats[s][m];
        }
    }
    
    if (pbm==1) { cerr << " Warning: Overflow. One component of the dispersal matrix was not defined. " << endl;}
    
    if( posterior == 1 || posterior == 4) Nposterior++ ;
    if (posterior == 2) {
        Nposterior++ ;
        for (int s=0; s<Ns; s++)
        {
            PosteriorMeres[s][NPar] += Migs;
            for (int m=0; m<NPar; m++)
            {
                //cerr << s << " " << m << " " << tots[s] << " " << mats[s][m] << " " << PosteriorMeres[s][m] << " " << PosteriorMeres[s][NPar] << " ";
                PosteriorMeres[s][m] += (1-Migs)/tots[s]*mats[s][m]/2;
                PosteriorMeres[s][NPar] += (1-Migs)/tots[s]*mats[s][m]*Mig/2;
                for (int p=0; p<NPar; p++)
                {
                    PosteriorMeres[s][p] += (1-Migs)*(1-Mig-Self)/tots[s]*mats[s][m]/totp[m]*matp[s][p]/2;
                }
                //cerr << (1-Migs)/tots[s]*mats[s][m]/2 << " " << Migs + (1-Migs)/tots[s]*mats[s][m]*Mig/2 << " " << PosteriorMeres[s][1] << " " << PosteriorMeres[s][NPar] << endl;
            }
        }
    }
    
    for (int s=0; s<Ns; s++)
    {
        if( ProbMig[s]>0 && tots[s]>0)
        {
            pip=0;pip2=0;
            for (int m=0; m<NbMeres[s]; m++)
            {
                pip += (ProbMeres[s][m].pere_prob)*mats[s][ProbMeres[s][m].pere_pot];
            }
            for (int m=0; m<NbCouples[s]; m++)
            {
                pip2 += (ProbCouples[s][m].couple_prob)*mats[s][ProbCouples[s][m].mere_pot]*matp[ProbCouples[s][m].mere_pot][ProbCouples[s][m].pere_pot]/totp[ProbCouples[s][m].mere_pot];
            }
            
            piptot = ProbMig[s]*Migs+(1-Migs)/tots[s]*(pip*Mig+(1-Mig-Self)*pip2);
            liktemp += log(ProbMig[s]*Migs+(1-Migs)/tots[s]*(pip*Mig+(1-Mig-Self)*pip2));
            
            if(posterior == 1 || posterior == 4){
                PosteriorOut[s] += ProbMig[s]*Migs/piptot;
                for (int m=0; m<NbMeres[s]; m++)
                {
                    PosteriorMeres[s][m] += (ProbMeres[s][m].pere_prob)*mats[s][ProbMeres[s][m].pere_pot]*(1-Migs)/tots[s]*Mig/piptot;
                }
                for (int m=0; m<NbCouples[s]; m++)
                {
                    PosteriorCouples[s][m] += (ProbCouples[s][m].couple_prob)*mats[s][ProbCouples[s][m].mere_pot]*matp[s][ProbCouples[s][m].pere_pot]/totp[ProbCouples[s][m].mere_pot]*(1-Migs)*(1-Mig-Self)/tots[s]/piptot;
                }
            }
        }
    }
    
    return liktemp;
    
}


/*************** INIT ******************/
void MEMM_seedlings::loadSeeds(Configuration * p)
{
  int Nsl;
  string name;
  char * temp;

  if(p == NULL)return;

  cout<<endl<<"# Loading seeds... as parents"<<endl;
  allSeeds.clear();

  temp = p->getValue(MEMM_OFFSPRING_FILE_NAME);
  ifstream seeds(temp);
  if(temp) delete []temp;
  temp = p->getValue(MEMM_OFFSPRING_FILE_NUMBERS_OF_LOCUS);
  Nsl = atoi(temp);
  if(temp) delete []temp;

  Ns = 0;
  if(seeds.good())
  {
    while(seeds >> name)
    {
      cout<<name<<"...";
      allSeeds.push_back(new parent(name,seeds,0,0,Nsl));
      Ns++;
    }
    cout<<endl<<Ns<<" seeds loaded."<<endl;
    seeds.close();
  }
  else
  {
  cerr<<"#ERROR ! Can not read seeds file !"<<endl<<"Continue..."<<endl;
  }
}

void MEMM_seedlings::calculDistances(Configuration *p)
{
  MEMM::calculDistances(p);

  DistSM.resize(Ns,vector<double>(NPar,0.));
    AzimSM.resize(Ns,vector<double>(NPar,0.));
  
  cout<< "Calcul distance seeds - mothers"<<endl;
  for(int s=0; s<Ns; s++)
    for(int m=0; m<NPar; m++)
    {
      DistSM[s][m]=dynamic_cast<parent*>(allSeeds[s])->dist(*allParents[m]);
        AzimSM[s][m]=dynamic_cast<parent*>(allSeeds[s])->azim(*allParents[m]);
    }
}

void MEMM_seedlings::calculTransitionMatrix(Configuration *p)
{
    
    ///// First check of the existence of missing alleles
    
    cout << endl << endl << " Checking the alleles of all individuals ..." << endl;
    locus temp_allele;
    map<int,int>::iterator temp_check;
    for (int s=0; s<Ns; s++) {
        for (int l=0; l<Nl; l++) {
            temp_allele = *(allSeeds[s])->getGeno(l);
            temp_check = SizeAll[l].find(temp_allele.getAllele(0));
            if (temp_check == SizeAll[l].end() && temp_allele.getAllele(0)>0) cout << " Uknown allele found for offspring " << dynamic_cast<parent*>(allSeeds[s])->getName() << " : " << temp_allele.getAllele(0) << " at locus " << l << " and allele # 1 !" << endl;
            
            temp_allele = *(allSeeds[s])->getGeno(l);
            temp_check = SizeAll[l].find(temp_allele.getAllele(1));
            if (temp_check == SizeAll[l].end() && temp_allele.getAllele(1)>0) cout << " Uknown allele found for offspring " << dynamic_cast<parent*>(allSeeds[s])->getName() << " : " << temp_allele.getAllele(1) << " at locus " << l << " and allele # 2 !"<< endl;
        }
    }
    for (int s=0; s<NPar; s++) {
        for (int l=0; l<Nl; l++) {
            temp_allele = *(allParents[s])->getGeno(l);
            temp_check = SizeAll[l].find(temp_allele.getAllele(0));
            if (temp_check == SizeAll[l].end() && temp_allele.getAllele(0)>0) cout << " Uknown allele found for parent " << dynamic_cast<parent*>(allParents[s])->getName() << " : " << temp_allele.getAllele(0) << " at locus " << l << " and allele # 1 !" << endl;
            
            temp_allele = *(allParents[s])->getGeno(l);
            temp_check = SizeAll[l].find(temp_allele.getAllele(1));
            if (temp_check == SizeAll[l].end() && temp_allele.getAllele(1)>0) cout << " Uknown allele found for parent " << dynamic_cast<parent*>(allParents[s])->getName() << " : " << temp_allele.getAllele(1) << " at locus " << l << " and allele # 2 !"<< endl;
        }
    }
    cout << " ... end of checking. If any uknown alleles, this can cause problems..." << endl << endl;
    ///// End of checking of the existence of missing alleles
    
    
    NbMeres.clear();
    NbMeres.resize(Ns);
    NbCouples.clear();
    NbCouples.resize(Ns);
    ProbMig.clear();
    ProbMig.resize(Ns);
    ProbMeres.resize(Ns);
    ProbCouples.resize(Ns);
    
    PosteriorMeres.resize(Ns);
    PosteriorCouples.resize(Ns);
    PosteriorOut.resize(Ns, 0.0);
    if(posterior==2) for(int s=0; s<Ns; s++) PosteriorMeres[s].resize(NPar+1,0.);
    
    individu nul("nul", genotype(Nl));
    long double probtemp;
    PerePot perepottemp;
    CouplePot couplepottemp;
    int NMistype=0;  // NEW EK -> Considering a Maximum Number of Mistypings
    
    int writeProbTrans=1;
    ofstream exportProbTrans;
    stringstream ss;
    ss << outputDirectory << "/exportTransitionProb.txt";
    if (writeProbTrans>0) exportProbTrans.open(ss.str(), ofstream::out);
    
    long double best, scnd, nmisbest, dbest, azbest, dbest2, azbest2, dbest3, azbest3;
    int idbest, idscnd, idbest2, idscnd2;
    
    if (writeProbTrans>0) {
        exportProbTrans << "ID_Indiv" << "\t" << "log10T_out" << "\t";
        exportProbTrans << "Nb_Parents" << "\t" << "log10T_best" << "\t" << "log10T_scnd" << "\t" << "ID_best" << "\t" << "ID_scnd" << "\t";
        exportProbTrans << "NMistype_best" << "\t" << "Dist_best" << "\t" << "Azim_best" << "\t";
        exportProbTrans << "Nb_Couples" << "\t" << "log10T_best" << "\t" << "log10T_scnd" << "\t" << "ID_best1" << "\t" << "ID_best2" << "\t" << "ID_scnd1" << "\t" << "ID_scnd2" << "\t";
        exportProbTrans << "NMistype_best" << "\t" << "Dist_best1" << "\t" << "Dist_best2" << "\t" << "Dist_best12" << "\t" << "Azim_best1" << "\t" << "Azim_best2" << "\t" << "Azim_best12" << "\t" << endl;
        exportProbTrans.flush();
    }
    
    cout << endl << "Computing the mendelian likelihoods..."  << endl;
    for (int s=0; s<Ns; s++)
    {
        cout << s << "...";
        cout.flush();
        NbMeres[s]=0;
        
        if (writeProbTrans>0) {exportProbTrans << dynamic_cast<parent*>(allSeeds[s])->getName() << "\t";exportProbTrans.flush();}
        best=-1000.0;
        scnd=-1000.0;
        idbest=-1;
        idscnd=-1;
        nmisbest=-1.;
        dbest=-1.;
        azbest=0.;
        
        ProbMig[s]=dynamic_cast<parent*>(allSeeds[s])->mendel(nul,nul,Nl,Na,SizeAll,AllFreq);
        if (ProbMig[s]==0) {cout << "Seedling # " << s << " (" << dynamic_cast<parent*>(allSeeds[s])->getName() << ") carries an unknown allele " << endl;}
        
        for (int m=0; m<NPar; m++)
        {
            if(!useMatError)
                probtemp = dynamic_cast<parent*>(allSeeds[s])->mendel((*allParents[m]),nul,Nl,Na,SizeAll,AllFreq);
            else
            { NMistype=0;
                probtemp = dynamic_cast<parent*>(allSeeds[s])->mendel((*allParents[m]),nul,Nl,Na,SizeAll,AllFreq,MatError,NMistype);
            }
            
            if (probtemp>0 && NMistype <= NMistypeMax)
            {
                NbMeres[s]++;
                if (log10(probtemp)>best) {
                    scnd=best; idscnd=idbest;
                    best=log10(probtemp); idbest=m;
                    nmisbest=NMistype;
                    dbest=dynamic_cast<parent*>(allSeeds[s])->dist((*allParents[m]));
                    azbest=dynamic_cast<parent*>(allSeeds[s])->azim((*allParents[m]));
                }
                else if (log10(probtemp)>scnd) {scnd=log10(probtemp); idscnd=m; }
                perepottemp.pere_pot=m;
                perepottemp.pere_prob=probtemp;
                ProbMeres[s].push_back(perepottemp);
                if(posterior == 1 || posterior == 4) PosteriorMeres[s].push_back(0); // NEW EK -> Computing posterior probabilities of parents
            }
        }
        
        cout << "Tree # " << s << " (" << dynamic_cast<parent*>(allSeeds[s])->getName() << ") has " << NbMeres[s] << " compatible parents : ";
        for (int m=0; m < NbMeres[s]; m++) cout << dynamic_cast<parent*>(allParents[ProbMeres[s][m].pere_pot])->getName() << " | ";
        cout << endl;
        
        if (writeProbTrans>0) {
            exportProbTrans << log10(ProbMig[s]) << "\t" << NbMeres[s] << "\t" << best << "\t" << scnd << "\t";
            if (idbest<0) exportProbTrans << "-1\t";  else exportProbTrans << dynamic_cast<parent*>(allParents[idbest])->getName() << "\t";
            if (idscnd<0) exportProbTrans << "-1\t";  else exportProbTrans << dynamic_cast<parent*>(allParents[idscnd])->getName() << "\t";
            exportProbTrans << nmisbest << "\t" << dbest << "\t" << azbest << "\t";
            exportProbTrans.flush();
        }
        
        best=-1000.0;
        scnd=-1000.0;
        idbest=-1;idbest2=-1;
        idscnd=-1;idscnd2=-1;
        nmisbest=-1.;
        dbest=-1.;dbest2=-1.;dbest3=-1.;
        azbest=0.;azbest2=0.;azbest3=0.;
        
        
        for (int m=0; m<NbMeres[s]; m++)
        {
            for (int p=0; p<NbMeres[s]; p++)
            {
                
                if(!useMatError)
                    probtemp = dynamic_cast<parent*>(allSeeds[s])->mendel((*allParents[ProbMeres[s][m].pere_pot]),(*allParents[ProbMeres[s][p].pere_pot]),Nl,Na,SizeAll,AllFreq);
                else
                { NMistype=0;
                    probtemp = dynamic_cast<parent*>(allSeeds[s])->mendel((*allParents[ProbMeres[s][m].pere_pot]),(*allParents[ProbMeres[s][p].pere_pot]),Nl,Na,SizeAll,AllFreq,MatError,NMistype);
                }
                
                if (probtemp>0 && NMistype <= NMistypeMax)
                {
                    NbCouples[s]++;
                    if (log10(probtemp)>best && !(ProbMeres[s][m].pere_pot==idbest2 && ProbMeres[s][p].pere_pot==idbest)) {
                        scnd=best; idscnd=idbest; idscnd2=idbest2;
                        best=log10(probtemp); idbest=ProbMeres[s][m].pere_pot; idbest2=ProbMeres[s][p].pere_pot;
                        nmisbest=NMistype;
                        dbest=dynamic_cast<parent*>(allSeeds[s])->dist((*allParents[ProbMeres[s][m].pere_pot]));
                        dbest2=dynamic_cast<parent*>(allSeeds[s])->dist((*allParents[ProbMeres[s][p].pere_pot]));
                        dbest3=dynamic_cast<parent*>(allParents[ProbMeres[s][m].pere_pot])->dist((*allParents[ProbMeres[s][p].pere_pot]));
                        azbest=dynamic_cast<parent*>(allSeeds[s])->azim((*allParents[ProbMeres[s][m].pere_pot]));
                        azbest2=dynamic_cast<parent*>(allSeeds[s])->azim((*allParents[ProbMeres[s][p].pere_pot]));
                        azbest3=dynamic_cast<parent*>(allParents[ProbMeres[s][m].pere_pot])->azim((*allParents[ProbMeres[s][p].pere_pot]));
                    }
                    else if (log10(probtemp)>scnd) {scnd=log10(probtemp); idscnd=ProbMeres[s][m].pere_pot; idscnd2=ProbMeres[s][p].pere_pot;}
                    couplepottemp.mere_pot=ProbMeres[s][m].pere_pot;
                    couplepottemp.pere_pot=ProbMeres[s][p].pere_pot;
                    couplepottemp.couple_prob=probtemp;
                    ProbCouples[s].push_back(couplepottemp);
                    if(posterior == 1 || posterior == 4) PosteriorCouples[s].push_back(0); // NEW EK -> Computing posterior probabilities of parents
                }
            }
        }
        if (writeProbTrans>0)
        {
            exportProbTrans << NbCouples[s] << "\t" << best << "\t" << scnd << "\t";
            if (idbest<0) exportProbTrans << "-1\t-1\t";
            else exportProbTrans << dynamic_cast<parent*>(allParents[idbest])->getName() << "\t" << dynamic_cast<parent*>(allParents[idbest2])->getName() << "\t" ;
            if (idscnd<0) exportProbTrans << "-1\t-1\t";
            else exportProbTrans << dynamic_cast<parent*>(allParents[idscnd])->getName() << "\t" << dynamic_cast<parent*>(allParents[idscnd2])->getName() << "\t";
            exportProbTrans << nmisbest << "\t" << dbest << "\t" << dbest2 << "\t" << dbest3 << "\t" << azbest << "\t" << azbest2 << "\t" << azbest3 << "\t" << endl;
        }
        
        cout << "Tree # " << s << " (" << dynamic_cast<parent*>(allSeeds[s])->getName() << ") has " << NbCouples[s] << " compatible couples : ";
        for (int m=0; m < NbCouples[s]; m++) cout << dynamic_cast<parent*>(allParents[ProbCouples[s][m].mere_pot])->getName() << " x " << dynamic_cast<parent*>(allParents[ProbCouples[s][m].pere_pot])->getName() << " | ";
        cout << endl << "--------------------" << endl;
        
    }
    if (writeProbTrans>0) exportProbTrans << endl;
    cout << endl << "Computation of the mendelian likelihoods : OK. " <<endl << endl;
}

void MEMM_seedlings::loadParameters(Configuration *p)
{
  MEMM::loadParameters(p);

  Parameter * param_temp;

  cout<< "Load more Parameters..."<<endl;

  // gama
  param_temp = allParameters["gamas"];
  if(param_temp)
  {
    GamAs = param_temp->INIT;
    mGamAs = param_temp->MIN;
    MGamAs = param_temp->MAX;
    cout << "GamAs : "<< GamAs << " [" <<mGamAs <<"|"<< MGamAs << "]"<<endl;
  }

  // Pi0s
  param_temp = allParameters["Pi0s"];
  if(param_temp)
  {
    Pi0s = param_temp->INIT;
    mPi0s = param_temp->MIN;
    MPi0s = param_temp->MAX;
    cout << "Pi0s : "<< Pi0s << " [" <<mPi0s <<"|"<< MPi0s << "]"<<endl;
  }

  // Scales
  param_temp = allParameters["scales"];
  if(param_temp)
  {
    Scales = param_temp->INIT;
    mScales = param_temp->MIN;
    MScales = param_temp->MAX;
    cout << "Scales : "<< Scales << " [" <<mScales <<"|"<< MScales << "]"<<endl;
  }
   
  // Shapes
  param_temp = allParameters["shapes"];
  if(param_temp)
  {
    Shapes = param_temp->INIT;
    mShapes = param_temp->MIN;
    MShapes = param_temp->MAX;
    cout << "Shapes : "<< Shapes << " [" <<mShapes <<"|"<< MShapes << "]"<<endl;
  }

  // Migs
  param_temp = allParameters["migs"];
  if(param_temp)
  {
    Migs = param_temp->INIT;
    mMigs = param_temp->MIN;
    MMigs = param_temp->MAX;
    cout << "Migs : "<< Migs << " [" <<mMigs <<"|"<< MMigs << "]"<<endl;
  }

  as=Scales*exp(gammln(2/Shapes)-gammln(3/Shapes));
//  cout <<"Deltas : "<<as<<endl;
  abs=-pow(as,-Shapes);
//    Kabs=Shapes/2/3.14/as/as/exp(gammln(2/Shapes));

  ap=Scale*exp(gammln(2/Shape)-gammln(3/Shape));
//  cout <<"Deltap : "<<ap<<endl;
  abp=-pow(ap,-Shape);

  Vec_Fecs.resize(NPar, 1.0 );
  Vec_Fecp.resize(NPar, 1.0 );

  pLois = loadDistribution(p, "individual_fecundities_seeds");

  cout<<"end loading Parameters"<<endl;

}

void MEMM_seedlings::loadOptions(Configuration *p)
{
  MEMM::loadOptions(p);
}

void MEMM_seedlings::exportPosteriors()
{
    long double tottemp;
    long double probtemp, probtemp0;   // EK -> Inclus Mistyping
    string name;
    char * temp;
    int NMistype=0;    // EK -> Inclus Mistyping
    individu nul("nul", genotype(Nl));
   
    long double best, scnd, nmisbest, dbest, azbest, dbest2, azbest2, dbest3, azbest3;
    int idbest, idscnd, idbest2, idscnd2;

    
    cout<<endl<<"# Exporting the posterior probabilities ... "<<endl;
    cout<<endl<<"Based on ... "<< Nposterior << "  sampled MCMC steps" << endl;
    
    if (posterior==1)
        *fposterior << "Seed#  SeedID  Parent#1  Parent#2  Gen_Lik  NMistypings  PosteriorProb" << endl;
    else if (posterior==2)
    {   *fposterior << "Seed#  SeedID  ";
        for (int m=0; m<NPar; m++) *fposterior << dynamic_cast<parent*>(allParents[m])->getName() << " ";
        *fposterior << "OUT" << endl;
    }
    else if (posterior==4)
    {
        *fposterior << "#Indiv" << " " << "ID_Indiv" << " " << "Nb_Parents" << " " << "Nb_Couples" << " " ;
        *fposterior << "Prob_best" << " " << "Prob_scnd" << " " << "ID_best1" << " " << "ID_best2" << " " << "ID_scnd1" << " " << "ID_scnd2" << " ";
        *fposterior << "NMistype_best" << " " << "Dist_best1" << " " << "Dist_best2" << " " << "Dist_best12" << " " << "Azim_best1" << " " << "Azim_best2" << " " << "Azim_best12" << " " << endl;
    }

    for (int s=0; s<Ns; s++)
    {
        
        if (posterior==1) {
        
        tottemp=1;
        for (int m=0; m<NbMeres[s]; m++) {
            if(!useMatError)
                probtemp = dynamic_cast<parent*>(allParents[s])->mendel((*allParents[ProbMeres[s][m].pere_pot]),nul,Nl,Na,SizeAll,AllFreq);
            else
            { NMistype=0;
                probtemp = dynamic_cast<parent*>(allParents[s])->mendel((*allParents[ProbMeres[s][m].pere_pot]),nul,Nl,Na,SizeAll,AllFreq,MatError,NMistype);
            }
            *fposterior << s << " " << dynamic_cast<parent*>(allParents[s])->getName() << " " << dynamic_cast<parent*>(allParents[ProbMeres[s][m].pere_pot])->getName() << " OUT ";
            *fposterior << ProbMeres[s][m].pere_prob << " " << NMistype << " " << PosteriorMeres[s][m]/Nposterior << endl;  // EK -> Inclus Mistyping
            tottemp -= PosteriorMeres[s][m]/Nposterior;
        }
            
        for (int m=0; m<NbCouples[s]; m++) {
            if(!useMatError)
                probtemp = dynamic_cast<parent*>(allParents[s])->mendel((*allParents[ProbCouples[s][m].mere_pot]),(*allParents[ProbCouples[s][m].pere_pot]),Nl,Na,SizeAll,AllFreq);
            else
            { NMistype=0;
                probtemp = dynamic_cast<parent*>(allParents[s])->mendel((*allParents[ProbCouples[s][m].mere_pot]),(*allParents[ProbCouples[s][m].pere_pot]),Nl,Na,SizeAll,AllFreq,MatError,NMistype);
            }
            *fposterior << s << " " << dynamic_cast<parent*>(allParents[s])->getName() << " " << dynamic_cast<parent*>(allParents[ProbCouples[s][m].mere_pot])->getName() << " ";
            *fposterior << dynamic_cast<parent*>(allParents[ProbCouples[s][m].pere_pot])->getName() << " " << ProbCouples[s][m].couple_prob << " " << NMistype << " "  <<  PosteriorCouples[s][m]/Nposterior << endl;  // EK -> Inclus Mistyping
            tottemp -= PosteriorCouples[s][m]/Nposterior;
        }
        *fposterior << s << " " << dynamic_cast<parent*>(allParents[s])->getName() << " OUT OUT " << ProbMig[s] << " 0 " << PosteriorOut[s]/Nposterior << endl; // EK -> Inclus Mistyping

        }
        
        else if (posterior==2)
        {
            *fposterior << s << " " << dynamic_cast<parent*>(allParents[s])->getName() << " ";
            for (int m=0; m<NPar+1; m++) *fposterior << PosteriorMeres[s][m]/Nposterior << " ";
            *fposterior << endl;
            
        }
        
        else if (posterior==4)
        {
            *fposterior << s << " " << (allParents[s])->getName() << " ";
            
            best=0.0;
            scnd=0.0;
            idbest=-1;idbest2=-1;
            idscnd=-1;idscnd2=-1;
            nmisbest=-1.;
            dbest=-1.;dbest2=-1.;dbest3=-1.;
            azbest=0.;azbest2=0.;azbest3=0.;
            tottemp=1;

            if (s==3) cout << PosteriorMeres[s][0]/Nposterior << " ";
            
            for (int m=0; m<NbMeres[s]; m++) {
                probtemp=PosteriorMeres[s][m]/Nposterior;
                if (probtemp>best) {
                    scnd=best; idscnd=idbest;
                    best=probtemp; idbest=ProbMeres[s][m].pere_pot;
                    if(!useMatError) NMistype=0;
                    else
                    { NMistype=0;
                        probtemp0 = dynamic_cast<parent*>(allParents[s])->mendel((*allParents[ProbMeres[s][m].pere_pot]),nul,Nl,Na,SizeAll,AllFreq,MatError,NMistype);
                    }
                    nmisbest=NMistype;
                    dbest=dynamic_cast<parent*>(allParents[s])->dist((*allParents[m]));
                    azbest=dynamic_cast<parent*>(allParents[s])->azim((*allParents[m]));
                }
                else if (probtemp>scnd) {scnd=probtemp; idscnd=m; }
                tottemp -= probtemp;
            }
            for (int m=0; m<NbCouples[s]; m++) {
                probtemp = PosteriorCouples[s][m]/Nposterior;
                if (probtemp>best) {
                    scnd=best; idscnd=idbest;idscnd2=idbest2;
                    best=probtemp; idbest=ProbCouples[s][m].mere_pot; idbest2=ProbCouples[s][m].pere_pot;
                    if(!useMatError) NMistype=0;
                    else
                    { NMistype=0;
                        probtemp0 = dynamic_cast<parent*>(allParents[s])->mendel((*allParents[ProbCouples[s][m].mere_pot]),(*allParents[ProbCouples[s][m].pere_pot]),Nl,Na,SizeAll,AllFreq,MatError,NMistype);
                    }
                    nmisbest=NMistype;
                    dbest=dynamic_cast<parent*>(allParents[s])->dist((*allParents[ProbCouples[s][m].mere_pot]));
                    dbest2=dynamic_cast<parent*>(allParents[s])->dist((*allParents[ProbCouples[s][m].pere_pot]));
                    dbest3=dynamic_cast<parent*>(allParents[ProbCouples[s][m].mere_pot])->dist((*allParents[ProbCouples[s][m].pere_pot]));
                    azbest=dynamic_cast<parent*>(allParents[s])->azim((*allParents[ProbCouples[s][m].mere_pot]));
                    azbest2=dynamic_cast<parent*>(allParents[s])->azim((*allParents[ProbCouples[s][m].pere_pot]));
                    azbest3=dynamic_cast<parent*>(allParents[ProbCouples[s][m].mere_pot])->azim((*allParents[ProbCouples[s][m].pere_pot]));
                }
                else if (probtemp>scnd) {scnd=probtemp; idscnd=ProbCouples[s][m].mere_pot; idscnd2=ProbCouples[s][m].pere_pot; }
                tottemp -= probtemp;
            }
            probtemp = PosteriorOut[s]/Nposterior;
            if (s==3) cout << tottemp << " =?= " << probtemp << endl;

            if (probtemp>best) {
                scnd=best; idscnd=idbest;idscnd2=idbest2;
                best=probtemp; idbest=-1; idbest2=-1;
                nmisbest=0;
                dbest=-1.;dbest2=-1.;dbest3=-1.;
                azbest=0.;azbest2=0.;azbest3=0.;
            }
            else if (probtemp>scnd) {scnd=probtemp; idscnd=-1; idscnd2=-1; }
            
            *fposterior << NbMeres[s] << " " << NbCouples[s] << " " << best << " " << scnd << " ";
            if (idbest<0) *fposterior << "-1 ";  else *fposterior << dynamic_cast<parent*>(allParents[idbest])->getName() << " ";
            if (idbest2<0) *fposterior << "-1 ";  else *fposterior << dynamic_cast<parent*>(allParents[idbest2])->getName() << " ";
            if (idscnd<0) *fposterior << "-1 ";  else *fposterior << dynamic_cast<parent*>(allParents[idscnd])->getName() << " ";
            if (idscnd2<0) *fposterior << "-1 ";  else *fposterior << dynamic_cast<parent*>(allParents[idscnd2])->getName() << " ";
            *fposterior  << nmisbest << " " << dbest << " " << dbest2 << " " << dbest3 << " " << azbest << " " << azbest2 << " " << azbest3 << endl;
        }
    }
    
    (*fposterior).close();
    if(temp) delete []temp;
}

