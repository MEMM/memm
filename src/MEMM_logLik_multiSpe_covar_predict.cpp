/**************************************************************************************************************
 * memm is a bayesian estimation of pollen dispersal kernel and variance in male fecundity.                   *
 * copyright (c) 2009-2016 etienne klein <etienne.klein@paca.inra.fr>                                         *
 *                                                                                                            *
 * this file is part of memm.                                                                                 *
 *                                                                                                            *
 * memm is free software: you can redistribute it and/or modify                                               *
 * it under the terms of the gnu general public license as published by                                       *
 * the free software foundation, either version 3 of the license, or                                          *
 * (at your option) any later version.                                                                        *
 *                                                                                                            *
 * memm is distributed in the hope that it will be useful,                                                    *
 * but without any warranty; without even the implied warranty of                                             *
 * merchantability or fitness for a particular purpose.  see the                                              *
 * gnu general public license for more details.                                                               *
 *                                                                                                            *
 * you should have received a copy of the gnu general public license                                          *
 * along with memm.  if not, see <http://www.gnu.org/licenses/>.                                              *
 **************************************************************************************************************/

/*! \file MEMM_lokLik.cpp
 * \brief MEMM_logLik class implementation
 * \author Jean-François REY
 * \vers//ion 1.0
 * \date 23 Sept 2014
 */

#include "MEMM_logLik_multiSpe_covar_predict.hpp"
#include <ctime>
#include <algorithm>
#include <cstdlib>
#include <boost/math/special_functions/gamma.hpp>

MEMM_logLik_multiSpe_covar_predict::MEMM_logLik_multiSpe_covar_predict() : MEMM_logLik()
{
  cout<< "MEMM : multi-species mode : Prediction ONLY"<<endl;
  Nm = 0;
  Nposterior = 0;
  pLoiClone = NULL;
}

MEMM_logLik_multiSpe_covar_predict::~MEMM_logLik_multiSpe_covar_predict()
{
  if(IDmere.size() != 0) IDmere.clear();
  if(Meres.size() != 0) Meres.clear();
  if(MerDesc.size() != 0) MerDesc.clear();
  if(pLoiClone) delete pLoiClone;
}

void MEMM_logLik_multiSpe_covar_predict::mcmc(int nbIteration, int thinStep)
{

    double lastFecLogLik, nextFecLogLik;
    double lastFecCLogLik, nextFecCLogLik;
    double previousValue, nextValue, previousValue2;
    long double previousLogLik, nextLogLik;
    long double previousLogLik_SeedSet, nextLogLik_SeedSet;
    double temp;
        
    pLoi->setDParam(MEMM_LOI_GAMA,GamA);
    for (int p=0; p<NPar; p++)
        pLoi->tirage(Vec_Fec[p]);
    lastFecLogLik = pLoi->logLik(NPar, Vec_Fec,GamA);
    cout << lastFecLogLik << " " ;cout.flush();
    
    for (int p=0; p<NPar; p++)
    {Vec_Fec_All[p] = Vec_Fec[p];
        for (int v=0; v<Nvar; v++)
            Vec_Fec_All[p] *= exp(Vec_FixedEff[0][v] * Covar[p][v]);
    }
    
    computeDISP(); cout << "Compute DISP OK " ;cout.flush();
    computePHENO(); cout << "Compute PHENO OK " ;cout.flush();
    previousLogLik = compute(); cout << "Compute likelihood OK " ;cout.flush();
    cout << previousLogLik ; cout.flush();
    computeODD(); cout << "Compute ODD OK " ;cout.flush();
    
    // output start value
    if(thinStep){

            std::stringstream ss;
        //    ss.str("");
        //    ss << outputDirectory << "/Pollen_Limitation.txt";
        //    PolLimFile = new std::ofstream(ss.str(), ofstream::out);
            ss.str("");
            ss << outputDirectory << "/Summary_Statistics_PerMother.txt";
            SummaryStats = new std::ofstream(ss.str(), ofstream::out);
            ss.str("");
            ss << outputDirectory << "/Summary_Statistics_PerFather.txt";
            SummaryStatsAll = new std::ofstream(ss.str(), ofstream::out);
            *SummaryStatsAll << "Test"<< endl; SummaryStatsAll->flush();
            ss.str("");
        //    ss << outputDirectory << "/Effective_Numbers_PollenDonors.txt";
        //    NepFile = new std::ofstream(ss.str(), ofstream::out);

        *IndivFec << "iteration " ;
        for (int p=0; p<NPar; p++) {*IndivFec << "Ep_" << dynamic_cast<parent*>(allParents[p])->getName() << " ";}
        for (int p=0; p<NPar; p++) {*IndivFec << "Fp_" << dynamic_cast<parent*>(allParents[p])->getName() << " ";}
//        for (int p=0; p<NPar; p++) {*IndivFec << "Gp_" << dynamic_cast<parent*>(allParents[p])->getName() << " ";}
        *IndivFec << endl;
                
        *ParamDisp << "Scale  Shape "
        //      << "KappaP  ThetaP "
        << "Mig " << "Self " << "bSI "
        << "Delta_opt1 Delta_opt2 Sigma_pheno1 Sigma_pheno2 Weight12 "
        << "TotSeuilA TotSeuilL Blimpol ";
        for (int v=0; v<Nvar; v++) *ParamDisp  << "V" << v+1 << " ";
        *ParamDisp  << "SxCS SxC SxM SxO ";
        *ParamDisp  << "CSxS CSxC CSxM CSxO ";
        *ParamDisp  << "CxS CxCS CxM CxO ";
        *ParamDisp  << "MxS MxCS MxC MxO ";
        *ParamDisp  << "OxS OxCS OxC OxM";
        if (polSatur == 4 || polSatur == 14 || polSatur == 24 || polSatur == 5 || polSatur == 15 || polSatur == 25 || polSatur == 6 || polSatur == 16 || polSatur == 26) for (int v=0; v<Nvar; v++) *ParamDisp  << " W" << v+1;
        *ParamDisp << endl;
        
        *SummaryStats << "ID MeanDist Nep QAutof_pool QAllof_pool QAutof_tubes QAllof_tubes QAutof_seeds QAllof_seeds ";
        for (int t=0; t<Ntype; t++) *SummaryStats << "Q" << t << "_pool ";
        for (int t=0; t<Ntype; t++) *SummaryStats << "Q" << t << "_tubes ";
        for (int t=0; t<Ntype; t++) *SummaryStats << "Q" << t << "_seeds ";
        *SummaryStats << "Q_Asta Q_Semi Q_Longi";
        *SummaryStats << endl;
        SummaryStats->flush();

        *SummaryStatsAll << "ID MeanDist Nep QAutof_pool QAllof_pool QAutof_tubes QAllof_tubes QAutof_seeds QAllof_seeds ";
        for (int t=0; t<Ntype; t++) *SummaryStatsAll << "Q" << t << "_pool ";
        for (int t=0; t<Ntype; t++) *SummaryStatsAll << "Q" << t << "_tubes ";
        for (int t=0; t<Ntype; t++) *SummaryStatsAll << "Q" << t << "_seeds ";
        *SummaryStatsAll << "P_avort P_success";
        *SummaryStatsAll << endl;
        SummaryStatsAll->flush();

        *IndivFec << "0 " ;
        for (int p=0; p<NPar; p++) {*IndivFec << Vec_Fec[p] << " ";}
        for (int p=0; p<NPar; p++) {*IndivFec << Vec_Fec_All[p]  << " ";}
//        for (int p=0; p<NPar; p++) {*IndivFec << Vec_Fec_Clone[Type[p]][Clone[p]] << " ";}
        *IndivFec << endl;
                
        *ParamDisp << Scale
        << " " << Shape
        //        << " " << Kappa << " " << ThetaP
        << " " << Mig << " " << Self << " " << bSI
        << " " << Delta_opt1 << " " << Delta_opt2 << " " << Sigma_pheno1 << " " << Sigma_pheno2 << " " << Weight12
        << " " << TotSeuilA
        << " " << TotSeuilL
        << " " << Blimpol;
        for ( int v=0; v<Nvar; v++) *ParamDisp << " " << Vec_FixedEff[0][v];
        for ( int t=0; t<Ntype; t++)
            for ( int tt=0; tt<Ntype; tt++)
                if (t!=tt)
                *ParamDisp << " " << mat_Barr[t][tt];
        if (polSatur == 4 || polSatur == 14 || polSatur == 24 || polSatur == 5 || polSatur == 15 || polSatur == 25 || polSatur == 6 || polSatur == 16 || polSatur == 26) for (int v=0; v<Nvar; v++) *ParamDisp  << " " << Vec_FixedEffSS[0][v];
        *ParamDisp << endl;
        ParamDisp->flush();
    
        compute(posterior);
        
        if(SummaryStats)
        {
            SummaryStats->close();
            delete SummaryStats;
        }
        if(SummaryStatsAll)
        {
            SummaryStatsAll->close();
            delete SummaryStatsAll;
        }

    }
   

}

void MEMM_logLik_multiSpe_covar_predict::mcmc_dyn(int nbIteration, int thinStep)
{
    cout << "Dynamic mode not supported." << endl;
}


/************************ PRIVATE ************************************/
long double MEMM_logLik_multiSpe_covar_predict::compute(int posterior)
{
    long double liktemp=0;
    
    if (polSatur < 20) liktemp += logLik_Genet(posterior); //cout << "loglik_Genet OK ..." << liktemp; cout.flush();
    if (polSatur < 10 || polSatur > 19) {cout << endl << "loglik_SeedSet launched" << endl; liktemp += logLik_SeedSet(posterior); } //cout << "... loglik_SeedSet OK ..." << liktemp  << endl; cout.flush();
    if (polSatur >9 && polSatur < 20 && posterior) logLik_SeedSet(posterior);
    if (polSatur > 20 && posterior) logLik_Genet(posterior);
        
    return liktemp;
}

long double MEMM_logLik_multiSpe_covar_predict::logLik_Genet(int posterior)
{
    vector < vector<long double> > mat (Nm, vector < long double > (NPar, 0.));
    vector <long double> tot (Nm, 0.);
    long double bb, liktemp=0, pip=0;
    long double piptot=0;
    int pbm=0;
    vector <long double> totType0 (Ntype, 0.);
    vector <long double> totType1 (Ntype, 0.);
    vector <long double> totType2 (Ntype, 0.);
    vector <long double> totType3 (Ntype, 0.);
    vector <long double> totFlowType (3, 0.);
    long double moyDispDist, nep;
    vector <long double> propAutof0 (2, 0.);
    vector <long double> propAutof1 (2, 0.);
    vector <long double> propAutof2 (2, 0.);
    vector <long double> propAutof3 (2, 0.);
 
    cout << endl << "loglik_Genet entered in... posterior = " << posterior << endl;

    if(posterior == 1 || posterior == 4) Nposterior++;
    
    //cout << "entering in loglik_Genet..."; cout.flush();
    for (int m=0; m<Nm; m++)
    {
        if (posterior) {
            totType1= totType0; totType2= totType0; totType3= totType0;
            totFlowType[0]=0; totFlowType[1]=0; totFlowType[2]=0;
            propAutof1=propAutof0; propAutof2=propAutof0; propAutof3=propAutof0;
            moyDispDist=0;nep=0;
        }
        if (polSatur == 2 || polSatur == 12 || polSatur == 22 ) // modèle de compétition neutre avec SI post-zygotiques et barrières interspé pré-zygotique
        {
            for (int p=0; p<NPar; p++)
            {
                if ((Type[p]==Type[Meres[m]]) && (Clone[p]==Clone[Meres[m]])) mat[m][p] = (1-bSI)* DISP[m][p] * PHENO[m][p] * Vec_Fec_All[p];
                else mat[m][p] = (1-Blimpol) * DISP[m][p] * PHENO[m][p] * mat_Barr[Type[Meres[m]]][Type[p]] * Vec_Fec_All[p];
                if (isnan(mat[m][p])){mat[m][p]=0; pbm=1;}
                if (posterior)
                {
                    totType1[Type[p]] += DISP[m][p] * PHENO[m][p] * Vec_Fec_All[p];
                    if ((Type[p]==Type[Meres[m]]) && (Clone[p]==Clone[Meres[m]])) totType2[Type[p]] += DISP[m][p] * PHENO[m][p] * Vec_Fec_All[p];
                    else totType2[Type[p]] += DISP[m][p] * PHENO[m][p] * mat_Barr[Type[Meres[m]]][Type[p]] * Vec_Fec_All[p];
                    totType3[Type[p]] += mat[m][p];
                    if ((Type[p]==Type[Meres[m]]) && (Clone[p]==Clone[Meres[m]]))
                    {
                        propAutof1[0] += DISP[m][p] * PHENO[m][p] * Vec_Fec_All[p];
                        propAutof2[0] += DISP[m][p] * PHENO[m][p] * Vec_Fec_All[p];
                        propAutof3[0] += mat[m][p];
                    }
                    else
                    {
                        propAutof1[1] += DISP[m][p] * PHENO[m][p] * Vec_Fec_All[p];
                        propAutof2[1] += DISP[m][p] * PHENO[m][p] * mat_Barr[Type[Meres[m]]][Type[p]] * Vec_Fec_All[p];
                        propAutof3[1] += mat[m][p];
                    }
                    moyDispDist += DistMP[m][p] * mat[m][p];
                    nep += mat[m][p]*mat[m][p];
                }
                tot[m]+=mat[m][p];
                totFlowType[FlowType[p]]+=mat[m][p];
                //                    if (m==38) cout << p << " ";cout.flush();
            }
        }
        
        else if (polSatur == 4 || polSatur == 14 || polSatur == 24
                 || polSatur == 5 || polSatur == 15 || polSatur == 25) // modèle1 avec limitation && SI post-zygotiques && barrières interspé pré-zygotique
        {
            for (int p=0; p<NPar; p++)
            {
                if ((Type[p]==Type[Meres[m]]) && (Clone[p]==Clone[Meres[m]])) mat[m][p] = (1-bSI)* DISP[m][p] * PHENO[m][p] * Vec_Fec_All[p];
                else mat[m][p] = DISP[m][p] * PHENO[m][p] * mat_Barr[Type[Meres[m]]][Type[p]] * Vec_Fec_All[p];
                if (isnan(mat[m][p])){mat[m][p]=0; pbm=1;}
                if (posterior)
                {
                    totType1[Type[p]] += DISP[m][p] * PHENO[m][p] * Vec_Fec_All[p];
                    if ((Type[p]==Type[Meres[m]]) && (Clone[p]==Clone[Meres[m]])) totType2[Type[p]] += DISP[m][p] * PHENO[m][p] * Vec_Fec_All[p];
                    else totType2[Type[p]] += mat[m][p];
                    totType3[Type[p]] += mat[m][p];
                    if ((Type[p]==Type[Meres[m]]) && (Clone[p]==Clone[Meres[m]]))
                    {
                        propAutof1[0] += DISP[m][p] * PHENO[m][p] * Vec_Fec_All[p];
                        propAutof2[0] += DISP[m][p] * PHENO[m][p] * Vec_Fec_All[p];
                        propAutof3[0] += mat[m][p];
                    }
                    else
                    {
                        propAutof1[1] += DISP[m][p] * PHENO[m][p] * Vec_Fec_All[p];
                        propAutof2[1] += mat[m][p];
                        propAutof3[1] += mat[m][p];
                    }
                    moyDispDist += DistMP[m][p] * mat[m][p];
                    nep += mat[m][p]*mat[m][p];
                }
                tot[m]+=mat[m][p];
                totFlowType[FlowType[p]]+=mat[m][p];
                //                    if (m==38) cout << p << " ";cout.flush();
            }
        }

        else if (polSatur == 6 || polSatur == 16 || polSatur == 26) // modèle avec limitation && SI pré && post-zygotiques && barrières interspé pré-zygotique
        {
            for (int p=0; p<NPar; p++)
            {
                if ((Type[p]==Type[Meres[m]]) && (Clone[p]==Clone[Meres[m]])) mat[m][p] = (1-bSI) * Self * DISP[m][p] * PHENO[m][p] * Vec_Fec_All[p];
                else mat[m][p] = (1-TotSeuilL) * DISP[m][p] * PHENO[m][p] * mat_Barr[Type[Meres[m]]][Type[p]] * Vec_Fec_All[p];
                if (isnan(mat[m][p])){mat[m][p]=0; pbm=1;}
                if (posterior)
                {
                    totType1[Type[p]] += DISP[m][p] * PHENO[m][p] * Vec_Fec_All[p];
                    if ((Type[p]==Type[Meres[m]]) && (Clone[p]==Clone[Meres[m]])) totType2[Type[p]] += Self * DISP[m][p] * PHENO[m][p] * Vec_Fec_All[p];
                    else totType2[Type[p]] += mat[m][p];
                    totType3[Type[p]] += mat[m][p];
                    if ((Type[p]==Type[Meres[m]]) && (Clone[p]==Clone[Meres[m]]))
                    {
                        propAutof1[0] += DISP[m][p] * PHENO[m][p] * Vec_Fec_All[p];
                        propAutof2[0] += Self * DISP[m][p] * PHENO[m][p] * Vec_Fec_All[p];
                        propAutof3[0] += mat[m][p];
                    }
                    else
                    {
                        propAutof1[1] += DISP[m][p] * PHENO[m][p] * Vec_Fec_All[p];
                        propAutof2[1] += mat[m][p];
                        propAutof3[1] += mat[m][p];
                    }
                    moyDispDist += DistMP[m][p] * mat[m][p];
                    nep += mat[m][p]*mat[m][p];
                }
                tot[m]+=mat[m][p];
                totFlowType[FlowType[p]]+=mat[m][p];
                //                    if (m==38) cout << p << " ";cout.flush();
            }
        }

        
        else
        {
            for (int p=0; p<NPar; p++)
            {
                if ((Type[p]==Type[Meres[m]]) && (Clone[p]==Clone[Meres[m]])) mat[m][p] = bSI * DISP[m][p] * PHENO[m][p] * Vec_Fec_All[p];
                else mat[m][p] = DISP[m][p] * PHENO[m][p] * mat_Barr[Type[Meres[m]]][Type[p]] * Vec_Fec_All[p];
                if (isnan(mat[m][p])){mat[m][p]=0; pbm=1;}
                if (posterior)
                {
                    totType1[Type[p]] += DISP[m][p] * PHENO[m][p] * Vec_Fec_All[p];
                    if ((Type[p]==Type[Meres[m]]) && (Clone[p]==Clone[Meres[m]])) totType2[Type[p]] += DISP[m][p] * PHENO[m][p] * Vec_Fec_All[p];
                    else totType2[Type[p]] += DISP[m][p] * PHENO[m][p] * mat_Barr[Type[Meres[m]]][Type[p]] * Vec_Fec_All[p];
                    totType3[Type[p]] += mat[m][p];
                    if ((Type[p]==Type[Meres[m]]) && (Clone[p]==Clone[Meres[m]]))
                    {
                        propAutof1[0] += DISP[m][p] * PHENO[m][p] * Vec_Fec_All[p];
                        propAutof2[0] += DISP[m][p] * PHENO[m][p] * Vec_Fec_All[p];
                        propAutof3[0] += mat[m][p];
                    }
                    else
                    {
                        propAutof1[1] += DISP[m][p] * PHENO[m][p] * Vec_Fec_All[p];
                        propAutof2[1] += DISP[m][p] * PHENO[m][p] * mat_Barr[Type[Meres[m]]][Type[p]] * Vec_Fec_All[p];
                        propAutof3[1] += mat[m][p];
                    }
                    moyDispDist += DistMP[m][p] * mat[m][p];
                    nep += mat[m][p] * mat[m][p];
                }
                tot[m]+=mat[m][p];
                totFlowType[FlowType[p]]+=mat[m][p];
            }
        }
        if (tot[m]==0) pbm=1;
        if (posterior)
        {
            *SummaryStats << dynamic_cast<parent*>(allParents[Meres[m]])->getName() << " " << moyDispDist/tot[m] << " ";
            *SummaryStats << tot[m]*tot[m]/nep << " ";
            *SummaryStats << propAutof1[0] << " " << propAutof1[1] << " ";
            *SummaryStats << propAutof2[0] << " " << propAutof2[1] << " ";
            *SummaryStats << propAutof3[0] << " " << propAutof3[1] << " ";
            for (int t=0; t<Ntype; t++) *SummaryStats << totType1[t] << " ";
            for (int t=0; t<Ntype; t++) *SummaryStats << totType2[t] << " ";
            for (int t=0; t<Ntype; t++) *SummaryStats << totType3[t] << " ";
            for (int t=0; t<3; t++) *SummaryStats << totFlowType[t] << " ";
            *SummaryStats << endl;
            SummaryStats->flush();
        }
    }
    if (pbm==1) { std::cout << " Warning: Overflow. One component of the mating matrix was not defined or tot=0 " << std::endl;}
    
    return 1.0;
}


long double MEMM_logLik_multiSpe_covar_predict::logLik_SeedSet(int posterior)
{
    int pbm=0;
    vector < vector<long double> > mat (NPar, vector < long double > (NPar, 0.));
    vector <long double> tot (NPar, 0.);
    vector <long double> totSelf (NPar, 0.);
    long double liktemp=0;
    long double pptemp;
    long double probtemp=0.5;
    vector <long double> ptemp (3, 0.);
    double totseuilLtemp;
    // On ne considère que la loi binomiable censurée en 0... pas de zero-deflated model
    vector <long double> totType0 (Ntype, 0.);
    vector <long double> totType1 (Ntype, 0.);
    vector <long double> totType2 (Ntype, 0.);
    vector <long double> totType3 (Ntype, 0.);
    long double moyDispDist, nep;
    vector <long double> propAutof0 (2, 0.);
    vector <long double> propAutof1 (2, 0.);
    vector <long double> propAutof2 (2, 0.);
    vector <long double> propAutof3 (2, 0.);
    
    cout << endl << "loglik_SeedSet entered in... posterior = " << posterior << "... modele polSatur = " << polSatur << endl;
    
    
    for (int m=0; m<NPar; m++)
        {
            if (posterior>0) {
                totType1= totType0; totType2= totType0; totType3= totType0;
                propAutof1=propAutof0; propAutof2=propAutof0; propAutof3=propAutof0;
                moyDispDist=0;nep=0;
            }
            
            //            cout << m << " ";cout.flush();
            
            if (polSatur == 0 || polSatur == 10 || polSatur == 20) // modèle de pollen non-saturant avec barrières pré-zygotique + effet longi/asta
            {
                for (int p=0; p<NPar; p++)
                {
                    if ((Type[p]==Type[m]) && (Clone[p]==Clone[m])) tot[m] += bSI * DISPPP[m][p] * PHENOPP[m][p] * Vec_Fec_All[p];
                    else tot[m] += DISPPP[m][p] * PHENOPP[m][p] * mat_Barr[Type[m]][Type[p]] * Vec_Fec_All[p];
                    if (isnan(tot[m])){tot[m]=0; pbm=1;}
                }
                if (tot[m]==0) pbm=1;
                if (pbm==1) { std::cout << " Warning: Overflow. One component of the pollen intensity vector was not defined or tot = 0 " << std::endl;}
                //        cout << " " << tot[m]; cout.flush();
                if (posterior) *PolLimFile << tot[m] << " ";
                
                if (FlowType[m]==0) probtemp = exp(Blimpol*(log(tot[m])-TotSeuilA))/(1+exp(Blimpol*(log(tot[m])-TotSeuilA)));
                else if (FlowType[m]==1) probtemp = exp(Blimpol*(log(tot[m])-TotSeuilL))/(1+exp(Blimpol*(log(tot[m])-TotSeuilL)));
                
            }
            
            else if (polSatur == 3 || polSatur == 13 || polSatur == 23) // modèle de pollen non-saturant avec barrières pré-zygotique
            {
                for (int p=0; p<NPar; p++)
                {
                    if ((Type[p]==Type[m]) && (Clone[p]==Clone[m])) tot[m] += bSI * DISPPP[m][p] * PHENOPP[m][p] * Vec_Fec_All[p];
                    else tot[m] += DISPPP[m][p] * PHENOPP[m][p] * mat_Barr[Type[m]][Type[p]] * Vec_Fec_All[p];
                    if (isnan(tot[m])){tot[m]=0; pbm=1;}
                }
                if (tot[m]==0) pbm=1;
                if (pbm==1) { std::cout << " Warning: Overflow. One component of the pollen intensity vector was not defined or tot = 0 " << std::endl;}
                //        cout << " " << tot[m]; cout.flush();
                if (posterior) *PolLimFile << tot[m] << " ";
                
                probtemp = pow(tot[m],Blimpol) / (exp(TotSeuilA) + pow(tot[m],Blimpol));
            }
            
            else if (polSatur == 1 || polSatur == 11 || polSatur == 21) // modèle de callose du stigmate avec barrières pré-zygotique
            {
                for (int p=0; p<NPar; p++)
                {
                    if ((Type[p]==Type[m]) && (Clone[p]==Clone[m])) tot[m] += bSI * DISPPP[m][p] * PHENOPP[m][p] * Vec_Fec_All[p];
                    else tot[m] += DISPPP[m][p] * PHENOPP[m][p] * mat_Barr[Type[m]][Type[p]] * Vec_Fec_All[p];
                    if (isnan(tot[m])){tot[m]=0; pbm=1;}
                    if ((Type[p]==Type[m]) && (Clone[p]==Clone[m])) totSelf[m] += DISPPP[m][p] * PHENOPP[m][p] * Vec_Fec_All[p];
                }
                if (tot[m]==0) pbm=1;
                if (pbm==1) { std::cout << " Warning: Overflow. One component of the pollen intensity vector was not defined or tot = 0 " << std::endl;}
                //        cout << " " << tot[m]; cout.flush();
                if (posterior) *PolLimFile << tot[m] << " ";
                
                
                //            totseuilLtemp = TotSeuilA + totSelf[m]*TotSeuilL;
                probtemp = exp(Blimpol*(log(totSelf[m])-TotSeuilA))/(1+exp(Blimpol*(log(totSelf[m])-TotSeuilA)));
                // Blimpol devrait être négatif: plus d'autopollen => plus de callose => moins de succès de fécondation
                // Pas de raison de faire de différence entrre les Asta et les Longi
            }
            
            else if (polSatur == 2 || polSatur == 12 || polSatur == 22) // modèle de compétition neutre avec SI post-zygotiques et barrières interspé pré-zygotique
            {
                for (int p=0; p<NPar; p++)
                {
                    if ((Type[p]==Type[m]) && (Clone[p]==Clone[m])) mat[m][p] = DISPPP[m][p] * PHENOPP[m][p] * Vec_Fec_All[p];
                    else mat[m][p] = DISPPP[m][p] * PHENOPP[m][p] * mat_Barr[Type[m]][Type[p]] * Vec_Fec_All[p];
                    if (isnan(mat[m][p])){mat[m][p]=0; pbm=1;}
                    if (posterior)
                    {
                        totType1[Type[p]] += DISPPP[m][p] * PHENOPP[m][p] * Vec_Fec_All[p];
                        totType2[Type[p]] += mat[m][p];
                        if ((Type[p]==Type[m]) && (Clone[p]==Clone[m])) totType3[Type[p]] += mat[m][p]*(1-bSI);
                        else totType3[Type[p]] += mat[m][p]*(1-Blimpol);
                        if ((Type[p]==Type[m]) && (Clone[p]==Clone[m]))
                        {
                            propAutof1[0] += mat[m][p];
                            propAutof2[0] += mat[m][p];
                            propAutof3[0] += mat[m][p]*(1-bSI);
                            moyDispDist += DistPP[m][p] * mat[m][p] *(1-bSI);
                            nep += mat[m][p]*mat[m][p]*(1-bSI)*(1-bSI);
                        }
                        else
                        {
                            propAutof1[1] += DISPPP[m][p] * PHENOPP[m][p] * Vec_Fec_All[p];
                            propAutof2[1] += mat[m][p];
                            propAutof3[1] += mat[m][p]*(1-Blimpol);
                            moyDispDist += DistPP[m][p] * mat[m][p]*(1-Blimpol);
                            nep += mat[m][p]*mat[m][p]*(1-Blimpol)*(1-Blimpol);
                        }
                    }
                    tot[m]+=mat[m][p];
                    //                    if (m==38) cout << p << " ";cout.flush();
                }
                if (tot[m]==0) pbm=1;
                if (posterior) *PolLimFile << tot[m] << " ";
                
                //                cout << "matOK ";cout.flush();
                
                probtemp = 0;
                for (int p=0; p<NPar; p++) {
                    if ((Type[p]==Type[m]) && (Clone[p]==Clone[m])) probtemp += mat[m][p]*(1-bSI);
                    else probtemp += mat[m][p]*(1-Blimpol);
                }
                probtemp /= tot[m];
                // Le taux d'avortement des autof est de bSI
                // Le taux d'avortement des allof est de Blimpol (pas de différence entre espèces)
                // Le taux de succès de féconation est Sum( pi * (1-b) )...
            }
            
            else if (polSatur == 4 || polSatur == 14 || polSatur == 24) // modèle1 avec limitation && SI post-zygotiques && barrières interspé pré-zygotique
            {
                for (int p=0; p<NPar; p++)
                {
                    if ((Type[p]==Type[m]) && (Clone[p]==Clone[m]))
                    { mat[m][p] = DISPPP[m][p] * PHENOPP[m][p] * Vec_Fec_All[p];
                        totSelf[m]+=mat[m][p];
                    }
                    else mat[m][p] = DISPPP[m][p] * PHENOPP[m][p] * mat_Barr[Type[m]][Type[p]] * Vec_Fec_All[p];
                    if (isnan(mat[m][p])){mat[m][p]=0; pbm=1;}
                    if (posterior)
                    {
                        totType1[Type[p]] += DISPPP[m][p] * PHENOPP[m][p] * Vec_Fec_All[p];
                        totType2[Type[p]] += mat[m][p];
                        if ((Type[p]==Type[m]) && (Clone[p]==Clone[m])) totType3[Type[p]] += mat[m][p]*(1-bSI);
                        else totType3[Type[p]] += mat[m][p];
                        if ((Type[p]==Type[m]) && (Clone[p]==Clone[m]))
                        {
                            propAutof1[0] += mat[m][p];
                            propAutof2[0] += mat[m][p];
                            propAutof3[0] += mat[m][p]*(1-bSI);
                            moyDispDist += DistPP[m][p] * mat[m][p] *(1-bSI);
                            nep += mat[m][p]*mat[m][p]*(1-bSI)*(1-bSI);
                        }
                        else
                        {
                            propAutof1[1] += DISPPP[m][p] * PHENOPP[m][p] * Vec_Fec_All[p];
                            propAutof2[1] += mat[m][p];
                            propAutof3[1] += mat[m][p];
                            moyDispDist += DistPP[m][p] * mat[m][p];
                            nep += mat[m][p]*mat[m][p];
                        }
                    }
                    tot[m]+=mat[m][p];
                    //                    if (m==38) cout << p << " ";cout.flush();
                }
                if (tot[m]==0) pbm=1;
                if (posterior) *PolLimFile << tot[m] << " ";
                
                //                cout << "matOK ";cout.flush();
                
                pptemp=exp(TotSeuilA + Vec_ODD[m] + Blimpol*tot[m]);
                if (isnan(pptemp))
                {probtemp=0.01;
                    //                        cout << " Warning: Overflow. pptemp was not defined. Seed-set likelihood undefined ( m= " << TotSeuilA << " " << Vec_ODD[m] << " " << Blimpol << " " << tot[m] << endl;
                }
                else probtemp = pptemp / (1 + pptemp) * (1 - totSelf[m]/tot[m]*bSI);
                
                //                if (isnan(probtemp)) {probtemp=1; cout << " Warning: Overflow. Prob(fecundation) was not defined. Seed-set likelihood undefined ( m= " << m << " pptemp= " << pptemp << " tot = " << tot[m] << " totself = " << totSelf[m] << endl;}
                
                // Le manque de pollen/pollinisateur est déterminé par les covariables mères (Vec_ODD) et la quantité de pollen reçue
                // Le taux d'avortement des autof est de bSI
                // Le taux d'avortement des allof est de 0
                // Le taux de succès (probtemp) est le produit P(pollinisation) x ( 1 - P(avortement))
            }
            else if (polSatur == 5 || polSatur == 15 || polSatur == 25) // modèle1 avec limitation qui dépende de l'allopollen slt && SI post-zygotiques && barrières interspé pré-zygotique
            {
                for (int p=0; p<NPar; p++)
                {
                    if ((Type[p]==Type[m]) && (Clone[p]==Clone[m]))
                    { mat[m][p] = DISPPP[m][p] * PHENOPP[m][p] * Vec_Fec_All[p];
                        totSelf[m]+=mat[m][p];
                    }
                    else mat[m][p] = DISPPP[m][p] * PHENOPP[m][p] * mat_Barr[Type[m]][Type[p]] * Vec_Fec_All[p];
                    if (isnan(mat[m][p])){mat[m][p]=0; pbm=1;}
                    if (posterior)
                    {
                        totType1[Type[p]] += DISPPP[m][p] * PHENOPP[m][p] * Vec_Fec_All[p];
                        totType2[Type[p]] += mat[m][p];
                        if ((Type[p]==Type[m]) && (Clone[p]==Clone[m])) totType3[Type[p]] += mat[m][p]*(1-bSI);
                        else totType3[Type[p]] += mat[m][p];
                        if ((Type[p]==Type[m]) && (Clone[p]==Clone[m]))
                        {
                            propAutof1[0] += mat[m][p];
                            propAutof2[0] += mat[m][p];
                            propAutof3[0] += mat[m][p]*(1-bSI);
                            moyDispDist += DistPP[m][p] * mat[m][p] *(1-bSI);
                            nep += mat[m][p]*mat[m][p]*(1-bSI)*(1-bSI);
                        }
                        else
                        {
                            propAutof1[1] += DISPPP[m][p] * PHENOPP[m][p] * Vec_Fec_All[p];
                            propAutof2[1] += mat[m][p];
                            propAutof3[1] += mat[m][p];
                            moyDispDist += DistPP[m][p] * mat[m][p];
                            nep += mat[m][p]*mat[m][p];
                        }
                    }
                    tot[m]+=mat[m][p];
                    //                    if (m==38) cout << p << " ";cout.flush();
                }
                if (tot[m]==0) pbm=1;
                if (posterior) *PolLimFile << tot[m] << " ";
                
                //                cout << "matOK ";cout.flush();
                
                pptemp=exp(TotSeuilA + Vec_ODD[m] + Blimpol*(tot[m]-totSelf[m]));
                if (isnan(pptemp))
                {probtemp=0.01;
                    //                        cout << " Warning: Overflow. pptemp was not defined. Seed-set likelihood undefined ( m= " << TotSeuilA << " " << Vec_ODD[m] << " " << Blimpol << " " << tot[m] << endl;
                }
                else probtemp = pptemp / (1 + pptemp) * (1 - totSelf[m]/tot[m]*bSI);
                
                //                if (isnan(probtemp)) {probtemp=1; cout << " Warning: Overflow. Prob(fecundation) was not defined. Seed-set likelihood undefined ( m= " << m << " pptemp= " << pptemp << " tot = " << tot[m] << " totself = " << totSelf[m] << endl;}
                
                // Le manque de pollen/pollinisateur est déterminé par les covariables mères (Vec_ODD) et la quantité de pollen reçue
                // Le taux d'avortement des autof est de bSI
                // Le taux d'avortement des allof est de TotSeuilL (ou =0)
                // Le taux de succès (probtemp) est le produit P(pollinisation) x ( 1 - P(avortement))
            }
            else if (polSatur == 6 || polSatur == 16 || polSatur == 26) // modèle1 avec limitation qui dépende de l'allopollen slt && SI pré && post-zygotiques && barrières interspé pré-zygotique
            {
                for (int p=0; p<NPar; p++)
                {
                    if ((Type[p]==Type[m]) && (Clone[p]==Clone[m]))
                    { mat[m][p] = Self * DISPPP[m][p] * PHENOPP[m][p] * Vec_Fec_All[p];
                        totSelf[m]+=mat[m][p];
                    }
                    else mat[m][p] = DISPPP[m][p] * PHENOPP[m][p] * mat_Barr[Type[m]][Type[p]] * Vec_Fec_All[p];
                    if (isnan(mat[m][p])){mat[m][p]=0; pbm=1;}
                    if (posterior>0)
                    {
                        totType1[Type[p]] += DISPPP[m][p] * PHENOPP[m][p] * Vec_Fec_All[p];
                        totType2[Type[p]] += mat[m][p];
                        if ((Type[p]==Type[m]) && (Clone[p]==Clone[m])) totType3[Type[p]] += mat[m][p]*(1-bSI);
                        else totType3[Type[p]] += mat[m][p]*(1-TotSeuilL);
                        if ((Type[p]==Type[m]) && (Clone[p]==Clone[m]))
                        {
                            propAutof1[0] += DISPPP[m][p] * PHENOPP[m][p] * Vec_Fec_All[p];
                            propAutof2[0] += mat[m][p];
                            propAutof3[0] += mat[m][p]*(1-bSI);
                            moyDispDist += DistPP[m][p] * mat[m][p] *(1-bSI);
                            nep += mat[m][p]*mat[m][p]*(1-bSI)*(1-bSI);
                        }
                        else
                        {
                            propAutof1[1] += DISPPP[m][p] * PHENOPP[m][p] * Vec_Fec_All[p];
                            propAutof2[1] += mat[m][p];
                            propAutof3[1] += mat[m][p]*(1-TotSeuilL);
                            moyDispDist += DistPP[m][p] * mat[m][p]*(1-TotSeuilL);
                            nep += mat[m][p]*mat[m][p]*(1-TotSeuilL)*(1-TotSeuilL);
                        }
                    }
                    tot[m]+=mat[m][p];
                    //                    if (m==38) cout << p << " ";cout.flush();
                }
                if (tot[m]==0) pbm=1;
//                if (posterior) *PolLimFile << tot[m] << " ";
                
                //                cout << "matOK ";cout.flush();
                
                pptemp=exp(TotSeuilA + Vec_ODD[m] + Blimpol*tot[m]);
                if (isnan(pptemp))
                {probtemp=0.01;
                    //                        cout << " Warning: Overflow. pptemp was not defined. Seed-set likelihood undefined ( m= " << TotSeuilA << " " << Vec_ODD[m] << " " << Blimpol << " " << tot[m] << endl;
                }
                else probtemp = pptemp / (1 + pptemp) * (1 - TotSeuilL + (TotSeuilL - bSI) * totSelf[m]/tot[m]);
                
                //                if (isnan(probtemp)) {probtemp=1; cout << " Warning: Overflow. Prob(fecundation) was not defined. Seed-set likelihood undefined ( m= " << m << " pptemp= " << pptemp << " tot = " << tot[m] << " totself = " << totSelf[m] << endl;}
                
                // Le manque de pollen/pollinisateur est déterminé par les covariables mères (Vec_ODD) et la quantité de pollen reçue
                // Le taux d'avortement des autof est de bSI
                // Le taux d'avortement des allof est de 0
                // Le taux de succès (probtemp) est le produit P(pollinisation) x ( 1 - P(avortement))
            }
  
            
            if (posterior>0)
            {
                *SummaryStatsAll << dynamic_cast<parent*>(allParents[m])->getName() << " " << moyDispDist/(propAutof3[0]+propAutof3[1]) << " ";
                *SummaryStatsAll << (propAutof3[0]+propAutof3[1])*(propAutof3[0]+propAutof3[1])/nep << " ";
                *SummaryStatsAll << propAutof1[0] << " " << propAutof1[1] << " ";
                *SummaryStatsAll << propAutof2[0] << " " << propAutof2[1] << " ";
                *SummaryStatsAll << propAutof3[0] << " " << propAutof3[1] << " ";
                for (int t=0; t<Ntype; t++) *SummaryStatsAll << totType1[t] << " ";
                for (int t=0; t<Ntype; t++) *SummaryStatsAll << totType2[t] << " ";
                for (int t=0; t<Ntype; t++) *SummaryStatsAll << totType3[t] << " ";
                *SummaryStatsAll << 1 - probtemp << " " << probtemp;
                *SummaryStatsAll << endl;
                SummaryStatsAll->flush();
            }
            
            
//            if(tot[m]>0)
//            {
//                if (posterior) *PolLimFile << probtemp << " ";
//            }
        }
//    if (posterior)
//    {
//        *PolLimFile << endl;
//        PolLimFile->flush();
//    }
    return 1.0;
}

void MEMM_logLik_multiSpe_covar_predict::computeDISP()
{
    int pbm=0;
    long double K;
    double theta, ka, selfTemp;
    double crownRadius = 3. ;
    
    if ( fDisp <= 0 )
    {
        a=Scale*exp(gammln(2/Shape)-gammln(3/Shape));
        ab=-pow(a,-Shape);
        K=Shape/2/3.14/a/a/exp(gammln(2/Shape));
        theta = pow(a, Shape);
        ka = 2/Shape;
        selfTemp = boost::math::gamma_p(ka, pow(crownRadius,Shape)/theta)/3.14/crownRadius/crownRadius;
    }
    else
    {
        a=Scale;
        K=(Shape-1)/3.14/a/a;
        selfTemp = (1 - pow(1 + crownRadius*crownRadius/a/a , 1-Shape) ) /3.14/crownRadius/crownRadius;
    }
    
    
    for (int m=0; m<NPar; m++)
    {
        for (int p=0; p<NPar; p++)
        {
            if (p==m)
//            { DISPPP[m][p]=K*Self*Poids[p]; }
//            { DISPPP[m][p]= selfTemp*bSI*Poids[p]; }
            { DISPPP[m][p]= selfTemp*Poids[p]; }
//            else if ((Type[p]==Type[m]) && (Clone[p]==Clone[m]))
//            { if ( fDisp <= 0 ) DISPPP[m][p]=K*bSI*exp(ab*pow(DistPP[m][p],Shape))*Poids[p];
//                else DISPPP[m][p]=K*bSI*pow(1 + DistPP[m][p]*DistPP[m][p]/a/a, -Shape)*Poids[p];
//            }
            else
            { if ( fDisp <= 0 ) DISPPP[m][p]=K*exp(ab*pow(DistPP[m][p], Shape))*Poids[p];
                else DISPPP[m][p]=K*pow(1 + DistPP[m][p]*DistPP[m][p]/a/a, -Shape)*Poids[p]; }
            if (isnan(DISPPP[m][p])){DISPPP[m][p]=0; pbm=1;}
        }
    }
    if (pbm==1) { std::cout << " Warning: Overflow. One component of the dispersal matrix DISP was not defined. " << std::endl;}
    for (int m=0; m<Nm; m++)
    {
        DISP[m]=DISPPP[Meres[m]];
    }
}

void MEMM_logLik_multiSpe_covar_predict::computePHENO()
{
    int pbm=0;
    
    for (int m=0; m<NPar; m++)
    {
        for (int p=0; p<NPar; p++)
        {
            PHENOPP[m][p]=(Weight12*exp(-pow((PhenoPP1[m][p]-Delta_opt1)/Sigma_pheno1,2))+(1-Weight12)*exp(-pow((PhenoPP2[m][p]-Delta_opt2)/Sigma_pheno2,2)));
            if (isnan(PHENOPP[m][p])){PHENOPP[m][p]=0; pbm=1;}
        }
        if (pbm==1) { cout << " Warning: Overflow. One component of the phenological matrix PHENO was not defined. " << endl;}
        for (int m=0; m<Nm; m++)
        {
            PHENO[m]=PHENOPP[Meres[m]];
        }
        
    }
}

void MEMM_logLik_multiSpe_covar_predict::computeODD()
{
    int pbm=0;
    double temp=0;
    for (int p=0; p<NPar; p++)
    {
        temp=0;
        for (int v=0; v<Nvar; v++) temp += Vec_FixedEffSS[0][v] * Covar[p][v];
        Vec_ODD[p]=temp;
        if (isnan(Vec_ODD[p])){Vec_ODD[p]=0; pbm=1;}
    }
    if (pbm==1) { cout << " Warning: Overflow. One component of the seed-set vector Vec_ODD was not defined. " << endl;}
}


long double MEMM_logLik_multiSpe_covar_predict::computeDyn()
{
    cout << "Dynamic mode not supported." << endl;
}
 
/************************ PROTECTED ****************************************/

/**************** INIT **********************/

void MEMM_logLik_multiSpe_covar_predict::loadSeeds(Configuration *p)
{
//    cout << "No need to load seeds for prediction" << endl;
    
    if(p == NULL) return;

//    if(IDmere.size() != 0) IDmere.clear();
    if(Meres.size() != 0) Meres.clear();
//    if(MerDesc.size() != 0) MerDesc.clear();

//    string name;
    
    cout<<endl<<"# Defining mothers..."<<endl;
    Nm = NPar;
    Meres.resize(NPar, 0);
      for (int k=0; k<NPar; k++)
      {
//          IDmere = fatherID ;
          Meres[k]=k;
//          std::cout << name << "=" << IDmere[name] << "...";
      }
      cout<<endl<<"Defining mothers : OK. "<<Nm<<" mother sampled. "<<endl;
}

void MEMM_logLik_multiSpe_covar_predict::loadParentFile(Configuration * p)
{
    
    MEMM::loadParentFile(p);
    
    char * temp;
    
    // Ntype compte le nombre d'espèces-types présents dans le jeu de données
    // Ntype est instancié ici
    // mat_Barr est instancié ici aussi
    
    DistPP.resize(NPar,vector < double > (NPar , 0.));
    AzimPP.resize(NPar,vector < double > (NPar , 0.));
    DISPPP.resize(NPar,vector < long double > (NPar , 0.));
    PHENOPP.resize(NPar,vector < long double > (NPar , 0.));

    PhenoPP1.resize(NPar,vector < double > (NPar , 0.));
    PhenoPP2.resize(NPar,vector < double > (NPar , 0.));

    for (int m=0; m<NPar; m++)
        for (int p=0; p<NPar; p++)
        {
        DistPP[m][p]=(*allParents[m]).dist(*allParents[p]);
        AzimPP[m][p]=(*allParents[m]).azim(*allParents[p]);
        }

    Nvar = Nqt;
    Ntype = 5;
    
    temp = p->getValue(MEMM_COVAR_NUMBER);
    if(temp) Nvar = atoi(temp);
    if(temp) delete []temp;
    
    cout << Nvar << " covariates to be loaded..." << endl;
    
//    NBog.resize( NPar, 0);
    Covar.resize( NPar, vector<double>( Nvar , 0.) );
    Type.resize( NPar, 0);
    FlowType.resize( NPar, 0);
    Clone.resize( NPar, 0);
    NClone.resize(5,0);
    CloneList.resize(5);
    SeedSet.resize( NPar , vector<int>( 6 , 0 ) );
    
    cout <<endl << "Loading the species/types and covariates..."<<endl;
//    for (int p=0; p<NPar; p++)
//    {
//        if (p < 10 ) cout << " - Parent " << p;cout.flush();
//        Type[p]=(allParents[p]->getCoVarQuali(0)) - 1;
//        if (Type[p] + 1 > Ntype) Ntype = Type[p] + 1;
//        if (p < 10 ) cout << " : Type " << Type[p]; cout.flush();
//    }
//    cout << endl << endl;
 
    NClone.resize(Ntype,0);
    CloneList.resize(Ntype);
    SeedSet.resize( NPar , vector<int>( 6 , 0 ) );
    SeedSetTot.resize( NPar , 0 );
    
    for (int p=0; p<NPar; p++)
    {
        Clone[p]=allParents[p]->getCoVarQuali(1)-1;
        if (p < 20 ) cout << " - Parent " << p << " : Type = " << Type[p]; cout.flush();
        if (p < 20 ) cout << " | Clone(Type) " << Clone[p] << "(" << Type[p] << ")" ; cout.flush();
        if (Clone[p] + 1 > NClone[Type[p]] ) { NClone[Type[p]] = Clone[p] + 1; CloneList[Type[p]].resize(NClone[Type[p]]);}
        CloneList[Type[p]][Clone[p]].push_back(p);
        FlowType[p]= ->getCoVarQuali(2) - 1;
        if (p < 20 ) cout << " | Flower Type " << FlowType[p];
        for (int b=0; b<6; b++)
        {
            SeedSet[p][b]=allParents[p]->getCoVarQuali(3+b);
            SeedSetTot[p]+=SeedSet[p][b];
        }

        for (int v=Nw; v<Nw+Nvar; v++)
            Covar[p][v]=allParents[p]->getCoVarQuanti(v);
        if (p < 20 ) cout << endl;
    }
    cout<< " ... " << endl;
    
    for(int m=0; m<NPar; m++)
        for(int p=0; p<NPar; p++)
        {   PhenoPP1[m][p] = Covar[p][Nvar-3]-Covar[m][Nvar-2];
            PhenoPP2[m][p] = Covar[p][Nvar-1]-Covar[m][Nvar-2];
        }
            
            
    Nvar = Nvar - 3;        // On n'utilise plus les 3 dernières variables comme covariables explicatives de la fécondité
    cout << endl << Nvar << " covariates used because the last ones are used as phenological indices " << endl;
    
    mat_Barr.resize(Ntype, vector < double > (Ntype, 1.));
    Vec_FixedEff.resize(1, vector<double>( Nvar, 0.) );
    Vec_FixedEffSS.resize(1, vector<double>( Nvar, 0.) );
    
    temp = p->getValue(MEMM_PRIOR_COVAR_FILE);
    ifstream inputPriorCovar(temp);
    if(inputPriorCovar.good())
    {
        cout << endl<<"# Loading priors for covariate effects from file " <<temp<<" ..."<<endl;
        if(temp) delete []temp;
        
        int v;
        for(v=0; v<Nvar && inputPriorCovar.good() ; v++)
                inputPriorCovar >> Vec_FixedEff[0][v];
        for(int t=0; t<Ntype && inputPriorCovar.good() ; t++)
        {
            for (int tt=0; tt<t; tt++){
                inputPriorCovar >> mat_Barr[t][tt];
                //mat_Barr[t][tt] = pow(10, mat_Barr[t][tt]);
            }
            for (int tt=t+1; tt<Ntype; tt++){
                inputPriorCovar >> mat_Barr[t][tt];
                //mat_Barr[t][tt] = pow(10, mat_Barr[t][tt]);
            }
        }
        for(v=0; v<Nvar && inputPriorCovar.good() ; v++)
                    inputPriorCovar >> Vec_FixedEffSS[0][v];

        inputPriorCovar.close();
    }
    else
    {   cout << endl<<" File " <<temp<< " unavailable for loading covariate effects..." << endl;}
    
        for (int v=0; v<Nvar; v++)
            cout << "V(" << v << ") initial = " << Vec_FixedEff[0][v] << " | ";
    cout <<endl << "   covariates loaded OK..."<<endl;
}



void MEMM_logLik_multiSpe_covar_predict::calculDistances(Configuration *p)
{
    MEMM_logLik::calculDistances(p);
    
    PhenoMP1.resize(Nm, vector < double > (NPar , 0.));
    PhenoMP2.resize(Nm, vector < double > (NPar , 0.));
    PHENO.resize(Nm, vector < long double > (NPar , 0.));


    for(int m=0; m<Nm; m++)
    {   PhenoMP1[m]=PhenoPP1[Meres[m]];
        PhenoMP2[m]=PhenoPP2[Meres[m]];
    }
    
    cout << "Delta (Phenologies) of 2 mothers x 5 fathers: " << endl;
    for (int m=0; m<2; m++)
        for (int p=0; p<5; p++) cout << PhenoMP1[m][p] << " " << PhenoMP2[m][p] << " ";
    cout << endl;

}

void MEMM_logLik_multiSpe_covar_predict::loadAllelicFreq(Configuration * p)
{
    cout << "No need of allelic frequencies for prediction" << endl;
}

void MEMM_logLik_multiSpe_covar_predict::calculTransitionMatrix(Configuration *p)
{
    cout << "No need of transition probabilities for prediction" << endl;
}

void MEMM_logLik_multiSpe_covar_predict::loadParameters(Configuration *p)
{
    MEMM_logLik::loadParameters(p);
    
    Parameter * param_temp;
    char * temp;
    
    Vec_Fec_All.resize(NPar, 1.0);
    Vec_ODD.resize(NPar, 1.0);
    
    Vec_Fec_Clone.resize(Ntype, vector < double > (1,1.));
    for (int t=0; t<Ntype; t++) (Vec_Fec_Clone[t]).resize(NClone[t], 1.);
    
    cout<< "Load more and more Parameters..."<<endl;
    
    // bSI
    param_temp = allParameters["b_Self_Incompat"];
    if(param_temp)
    {
        bSI = param_temp->INIT;
        mbSI = param_temp->MIN;
        MbSI = param_temp->MAX;
        cout << "b_Self_Incompat : "<< bSI << " [" << mbSI <<"|"<< MbSI << "] ... Tune : " << param_temp->TUNE <<endl;
    }
    
    
    // Delta_opt1
    param_temp = allParameters["Delta_opt1"];
    if(param_temp)
    {
        Delta_opt1 = param_temp->INIT;
        mDelta_opt1 = param_temp->MIN;
        MDelta_opt1 = param_temp->MAX;
        cout << "Delta_opt1 : "<< Delta_opt1 << " [" << mDelta_opt1 <<"|"<< MDelta_opt1 << "] ... Tune : " << param_temp->TUNE <<endl;
    }
    
    // Delta_opt1
    param_temp = allParameters["Delta_opt2"];
    if(param_temp)
    {
        Delta_opt2 = param_temp->INIT;
        mDelta_opt2 = param_temp->MIN;
        MDelta_opt2 = param_temp->MAX;
        cout << "Delta_opt2 : "<< Delta_opt2 << " [" << mDelta_opt2 <<"|"<< MDelta_opt2 << "] ... Tune : " << param_temp->TUNE <<endl;
    }
    
    // Sigma_pheno1
    param_temp = allParameters["Sigma_pheno1"];
    if(param_temp)
    {
        Sigma_pheno1 = param_temp->INIT;
        mSigma_pheno1 = param_temp->MIN;
        MSigma_pheno1 = param_temp->MAX;
        cout << "Sigma_pheno1 : "<< Sigma_pheno1 << " [" << mSigma_pheno1 <<"|"<< MSigma_pheno1 << "] ... Tune : " << param_temp->TUNE <<endl;
    }
    
    // Delta_opt1
    param_temp = allParameters["Sigma_pheno2"];
    if(param_temp)
    {
        Sigma_pheno2 = param_temp->INIT;
        mSigma_pheno2 = param_temp->MIN;
        MSigma_pheno2 = param_temp->MAX;
        cout << "Sigma_pheno2 : "<< Sigma_pheno2 << " [" << mSigma_pheno2 <<"|"<< MSigma_pheno2 << "] ... Tune : " << param_temp->TUNE <<endl;
    }
    
    // Weight12
    param_temp = allParameters["Weight12"];
    if(param_temp)
    {
        Weight12 = param_temp->INIT;
        mWeight12 = param_temp->MIN;
        MWeight12 = param_temp->MAX;
        cout << "Weight12 : "<< Weight12 << " [" << mWeight12 <<"|"<< MWeight12 << "] ... Tune : " << param_temp->TUNE <<endl;
    }
    
    
    // Blimpol
    param_temp = allParameters["B_lim_pol"];
    if(param_temp)
    {
        Blimpol = param_temp->INIT;
        mBlimpol = param_temp->MIN;
        MBlimpol = param_temp->MAX;
        cout << "Blimpol : "<< Blimpol << " [" << mBlimpol <<"|"<< MBlimpol << "] ... Tune : " << param_temp->TUNE <<endl;
    }
    
    char * limpolModel = "diffspecific";
    limpolModel = p->getValue(MEMM_PARAM_POLLIM);
    if (strcmp(limpolModel, "limitation")== 0 ) polSatur = 0;
    else if (strcmp(limpolModel, "limitationGonly")== 0 ) polSatur = 10;
    else if (strcmp(limpolModel, "limitationSSonly")== 0 ) polSatur = 20;
    else if (strcmp(limpolModel, "saturation")== 0 ) polSatur = 1;
    else if (strcmp(limpolModel, "saturationGonly")== 0 ) polSatur = 11;
    else if (strcmp(limpolModel, "saturationSSonly")== 0 ) polSatur = 21;
    else if (strcmp(limpolModel, "postzygotique")== 0 ) polSatur = 2;
    else if (strcmp(limpolModel, "postzygotiqueGonly")== 0 ) polSatur = 12;
    else if (strcmp(limpolModel, "postzygotiqueSSonly")== 0 ) polSatur = 22;
    else if (strcmp(limpolModel, "limitationBis")== 0 ) polSatur = 3;
    else if (strcmp(limpolModel, "limitationBisGonly")== 0 ) polSatur = 13;
    else if (strcmp(limpolModel, "limitationBisSSonly")== 0 ) polSatur = 23;
    else if (strcmp(limpolModel, "stackAll")== 0 ) polSatur = 4;
    else if (strcmp(limpolModel, "stackAllGonly")== 0 ) polSatur = 14;
    else if (strcmp(limpolModel, "stackAllSSonly")== 0 ) polSatur = 24;
    else if (strcmp(limpolModel, "stackAllAllo")== 0 ) polSatur = 5;
    else if (strcmp(limpolModel, "stackAllAlloGonly")== 0 ) polSatur = 15;
    else if (strcmp(limpolModel, "stackAllAlloSSonly")== 0 ) polSatur = 25;
    else if (strcmp(limpolModel, "prePostSI")== 0 ) polSatur = 6;
    else if (strcmp(limpolModel, "prePostSIGonly")== 0 ) polSatur = 16;
    else if (strcmp(limpolModel, "prePostSISSonly")== 0 ) polSatur = 26;
    else polSatur = 0;
    cout<<"Pollen limitation model :" << limpolModel << " " << polSatur << endl;
    if(limpolModel) delete []limpolModel;
    cout.flush();

    
    // TotSeuilA & TotSeuilL
    param_temp = allParameters["Tot_Seuil_Asta"];
    if(param_temp)
    {
        TotSeuilA = param_temp->INIT;
        mTotSeuilA = param_temp->MIN;
        MTotSeuilA = param_temp->MAX;
        cout << "TotSeuil_Asta : "<< TotSeuilA << " [" << mTotSeuilA <<"|"<< MTotSeuilA << "] ... Tune : " << param_temp->TUNE <<endl;
    }
    param_temp = allParameters["Tot_Seuil_Longi"];
    if(param_temp)
    {
        TotSeuilL = param_temp->INIT;
        mTotSeuilL = param_temp->MIN;
        MTotSeuilL = param_temp->MAX;
        cout << "TotSeuil_Longi : "<< TotSeuilL << " [" << mTotSeuilL <<"|"<< MTotSeuilL << "] ... Tune : " << param_temp->TUNE <<endl;
    }
        
    // GamC
    param_temp = allParameters["Gam_Clone"];
    if(param_temp)
    {
        GamC = param_temp->INIT;
        mGamC = param_temp->MIN;
        MGamC = param_temp->MAX;
        cout << "GamC : " << GamC << " [" << mGamC << "|" << MGamC << "] ... Tune : " << param_temp->TUNE <<endl;
    }
    
    if (GamC >0) pLoiClone = loadDistribution(p, "individual_fecundities_clone");
    
    cout << "... end reading Parameters" << endl << endl << endl;
    
}

void MEMM_logLik_multiSpe_covar_predict::exportNEP(int iteration)
{
    vector < vector<long double> > mat (NPar, vector < long double > (NPar, 0.));
    vector <long double> tot (NPar, 0.);
    vector <long double> nep (NPar, 0.);
    int pbm=0;

    *NepFile << iteration << " ";
        for (int m=0; m<NPar; m++)
        {
            for (int p=0; p<NPar; p++)
            {
                mat[m][p] = DISPPP[m][p] * PHENOPP[m][p] * Vec_Fec_All[p];
                if (isnan(mat[m][p])){mat[m][p]=0; pbm=1;}
                tot[m]+=mat[m][p];
                nep[m]+=mat[m][p]*mat[m][p];
            }
            if (tot[m]==0) pbm=1;
            nep[m]/=tot[m]*tot[m];
            *NepFile <<  1/nep[m] << " ";
        }
        if (pbm==1) { std::cout << " Warning: Overflow. One component of the mating matrix was not defined or tot=0 " << std::endl;}
        
        *NepFile << endl;
        NepFile->flush();
}


void MEMM_logLik_multiSpe_covar_predict::exportPosteriors()
{
    double tottemp;
    double probtemp, probtemp0;   // EK -> Inclus Mistyping
    string name;
    char * temp;
    int NMistype=0;    // EK -> Inclus Mistyping
    individu nul("nul", genotype(Nl));
   
    double best, scnd, nmisbest, dbest, azbest, dbest2, azbest2, dbest3, azbest3;
    int idbest, idscnd, idbest2, idscnd2;

    
    cout<<endl<<"# Exporting the posterior probabilities ... "<<endl;
    cout<<endl<<"Based on ... "<< Nposterior << "  sampled MCMC steps" << endl;
    
    if (posterior==1)
        *fposterior << "Seed#  SeedID  Mother Father Gen_Lik  NMistypings  PosteriorProb" << endl;
    else if (posterior==2)
    {   *fposterior << "Seed#  SeedID  ";
        for (int m=0; m<NPar; m++) *fposterior << dynamic_cast<parent*>(allParents[m])->getName() << " ";
        *fposterior << "OUT" << endl;
    }
    else if (posterior==4)
    {
        *fposterior << "#Indiv" << " " << "ID_Indiv" << " " << "Nb_Parents" ;
        *fposterior << "Prob_best" << " " << "Prob_scnd" << " " << "ID_best" << "ID_scnd";
        *fposterior << "NMistype_best" << " " << "Dist_best" << " " << "Dist_scnd" << " " << "Azim_best" << " " << "Azim_scnd" << endl;
    }

    for (int s=0; s<Ns; s++)
    {
        if (posterior==1) {
        tottemp=1;
        for (int m=0; m<NbPeres[s]; m++) {
            if(!useMatError)
                probtemp = dynamic_cast<graine*>(allSeeds[s])->mendel((*allParents[Meres[MerDesc[s]]]),(*allParents[ProbPeres[s][m].pere_pot]),Nl,Na,SizeAll,AllFreq[0]);
            else
            { NMistype=0;
                probtemp = dynamic_cast<graine*>(allSeeds[s])->mendel((*allParents[Meres[MerDesc[s]]]),(*allParents[ProbPeres[s][m].pere_pot]),Nl,Na,SizeAll,AllFreq[0],MatError,NMistype);
            }
            *fposterior << s << " " << dynamic_cast<graine*>(allSeeds[s])->getName() << " " << dynamic_cast<parent*>(allParents[Meres[MerDesc[s]]])->getName() << " " << dynamic_cast<parent*>(allParents[ProbPeres[s][m].pere_pot])->getName() << " ";
            *fposterior << ProbPeres[s][m].pere_prob << " " << NMistype << " " << PosteriorPeres[s][m]/Nposterior << endl;  // EK -> Inclus Mistyping
            tottemp -= PosteriorPeres[s][m]/Nposterior;
        }
            
        *fposterior << s << " " << dynamic_cast<graine*>(allSeeds[s])->getName() << " " << dynamic_cast<parent*>(allParents[Meres[MerDesc[s]]])->getName() << " OUT " << ProbMig[s] << " 0 " << PosteriorOut[s]/Nposterior << endl; // EK -> Inclus Mistyping
        }
        
        else if (posterior==2)
        {
            *fposterior << s << " " << dynamic_cast<graine*>(allSeeds[s])->getName() << " ";
            for (int m=0; m<NPar+1; m++) *fposterior << PosteriorPeres[s][m]/Nposterior << " ";
            *fposterior << endl;
            
        }
        
        else if (posterior==4)
        {
            *fposterior << s << " " << dynamic_cast<graine*>(allSeeds[s])->getName() << " ";
            
            best=0.0;
            scnd=0.0;
            idbest=-1;
            idscnd=-1;
            nmisbest=-1.;
            dbest=-1.;dbest2=-1.;
            azbest=0.;azbest2=0.;
            tottemp=1;

            if (s==3) cout << PosteriorPeres[s][0]/Nposterior << " ";
            
            for (int m=0; m<NbPeres[s]; m++) {
                probtemp=PosteriorPeres[s][m]/Nposterior;
                if (probtemp>best) {
                    scnd=best; idscnd=idbest;
                    best=probtemp; idbest=ProbPeres[s][m].pere_pot;
                    if(!useMatError) NMistype=0;
                    else
                    { NMistype=0;
                        probtemp0 = dynamic_cast<graine*>(allSeeds[s])->mendel((*allParents[Meres[MerDesc[s]]]),(*allParents[ProbPeres[s][m].pere_pot]),Nl,Na,SizeAll,AllFreq[0],MatError,NMistype);
                    }
                    nmisbest=NMistype;
                    dbest2=dbest; azbest2=azbest;
                    dbest=dynamic_cast<parent*>(allParents[Meres[MerDesc[s]]])->dist((*allParents[ProbPeres[s][m].pere_pot]));
                    azbest=dynamic_cast<parent*>(allParents[Meres[MerDesc[s]]])->azim((*allParents[ProbPeres[s][m].pere_pot]));
                }
                else if (probtemp>scnd)
                {scnd=probtemp; idscnd=m;
                    dbest2=dynamic_cast<parent*>(allParents[Meres[MerDesc[s]]])->dist((*allParents[ProbPeres[s][m].pere_pot]));
                    azbest2=dynamic_cast<parent*>(allParents[Meres[MerDesc[s]]])->azim((*allParents[ProbPeres[s][m].pere_pot]));
                }
                tottemp -= probtemp;
            }
            probtemp = PosteriorOut[s]/Nposterior;
            if (s==3) cout << tottemp << " =?= " << probtemp << endl;

            if (probtemp>best) {
                scnd=best; idscnd=idbest;
                best=probtemp; idbest=-1;
                nmisbest=0;
                dbest2=dbest;dbest=-1.;
                azbest2=azbest;azbest=0.;
            }
            else if (probtemp>scnd) {scnd=probtemp; idscnd=-1;  dbest2=-1.; azbest2=0.;}
            
            *fposterior << NbPeres[s] << " " << best << " " << scnd << " ";
            if (idbest<0) *fposterior << "-1 ";  else *fposterior << dynamic_cast<parent*>(allParents[idbest])->getName() << " ";
             if (idscnd<0) *fposterior << "-1 ";  else *fposterior << dynamic_cast<parent*>(allParents[idscnd])->getName() << " ";
            *fposterior  << nmisbest << " " << dbest << " " << dbest2 << " " << azbest << " " << azbest2 << endl;
        }
    }
    
    (*fposterior).close();
    if(temp) delete []temp;
}
