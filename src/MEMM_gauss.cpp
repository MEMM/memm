/**************************************************************************************************************
 * memm is a bayesian estimation of pollen dispersal kernel and variance in male fecundity.                   *
 * copyright (c) 2009-2016 etienne klein <etienne.klein@paca.inra.fr>                                         *
 *                                                                                                            *
 * this file is part of memm.                                                                                 *
 *                                                                                                            *
 * memm is free software: you can redistribute it and/or modify                                               *
 * it under the terms of the gnu general public license as published by                                       *
 * the free software foundation, either version 3 of the license, or                                          *
 * (at your option) any later version.                                                                        *
 *                                                                                                            *
 * memm is distributed in the hope that it will be useful,                                                    *
 * but without any warranty; without even the implied warranty of                                             *
 * merchantability or fitness for a particular purpose.  see the                                              *
 * gnu general public license for more details.                                                               *
 *                                                                                                            *
 * you should have received a copy of the gnu general public license                                          *
 * along with memm.  if not, see <http://www.gnu.org/licenses/>.                                              *
 **************************************************************************************************************/

#include <iostream>
#include <string>
#include "MEMM_util.h"
#include "MEMM_gauss.h"


MEMM_gauss::MEMM_gauss(double& pmu, double& pvar,
		        boost::variate_generator<boost::lagged_fibonacci19937&, boost::uniform_real<> >& accept):
  _pmu(pmu),_pvar(pvar),_accept(accept),_dgauss(pmu,pvar)
{
  _IDLoi=MEMM_IDLOI_GAUSS;
    std::cout<<"Using gaussian distribution"<<std::endl;
}



MEMM_gauss::~MEMM_gauss()
{
    //dtor
}

void MEMM_gauss::tirage(double& v){
  v= quantile(_dgauss,_accept());
}
double MEMM_gauss::logLik(int npar, const std::vector<double> & y, double& pmu, double& pvar) {

  double liktemp=0, a1;
    boost::math::normal_distribution<> tempGauss(pmu,pow(pvar,0.5));
    
  for (int p=0; p<npar; p++) {
    liktemp += log(pdf(tempGauss,y[p]));
  }
  return liktemp;
}

double MEMM_gauss::logLik(int npar, const std::vector<double> & prop, double& palpha) {}

//double MEMM_gauss::logLik(int npar, const std::vector<double> & y, double& pmu) {}

double MEMM_gauss::logLik(const std::vector< std::vector<double> > & fec, double& palpha) {}

void MEMM_gauss::setDParam(unsigned int indiceParam, double & value){
  switch (indiceParam)
    {
    case MEMM_LOI_MU:
            _pmu = value;
            _dgauss = boost::math::normal_distribution<>(_pmu,pow(_pvar,0.5));
      break;
    case MEMM_LOI_VAR:
            _pvar = value;
            _dgauss = boost::math::normal_distribution<>(_pmu,pow(_pvar,0.5));
    break;
    default:
      printf("Error: MEMM_gauss indiceParam out of range \n");
    }
}
