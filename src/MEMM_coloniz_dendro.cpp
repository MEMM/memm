#include "MEMM_coloniz_dendro.hpp"

MEMM_coloniz_dendro::MEMM_coloniz_dendro()
{
    cout<< "MEMM : Coloniz_dendro mode"<<endl;
    Nposterior = 0;
}

MEMM_coloniz_dendro::~MEMM_coloniz_dendro()
{
    
    Vec_Fecs.clear();
    Vec_Fecp.clear();
    if(pLois) delete pLois;
    
}

void MEMM_coloniz_dendro::mcmc(int nbIteration, int thinStep)
{
    
    
    ofstream cSuivi ("suiviPasAPas.txt");
    int suivi=1;
    
    double lastFecLogLiks, nextFecLogLiks;
    double lastFecLogLikp, nextFecLogLikp;
    double previousValue, nextValue;
    double previousValue2;
    long double previousLogLik, nextLogLik;
    
    double Tune=0.2;
    double fineTune=0.05;
    
    accept_ratio gamasRatio = allParameters["gamas"]->accept_ratio_;
    accept_ratio scalesRatio = allParameters["scales"]->accept_ratio_;
    accept_ratio shapesRatio = allParameters["shapes"]->accept_ratio_;
    accept_ratio kappasRatio = allParameters["kappas"]->accept_ratio_;
    accept_ratio thetasRatio = allParameters["thetas"]->accept_ratio_;
    // accept_ratio migsRatio = allParameters["migs"]->accept_ratio_;
    accept_ratio mig0sRatio = allParameters["mig0s"]->accept_ratio_;
    accept_ratio bmigsRatio = allParameters["bmigs"]->accept_ratio_;
    accept_ratio agethsRatio = allParameters["ageths"]->accept_ratio_;
    accept_ratio bfecsRatio = allParameters["bfecs"]->accept_ratio_;
    
    accept_ratio gamaRatio = allParameters["gama"]->accept_ratio_;
    //    accept_ratio scaleRatio = allParameters["scale"]->accept_ratio_;
    //    accept_ratio shapeRatio = allParameters["shape"]->accept_ratio_;
    //    accept_ratio migRatio = allParameters["mig"]->accept_ratio_;
    //    accept_ratio selfRatio = allParameters["self"]->accept_ratio_;
    accept_ratio mig0pRatio = allParameters["mig0p"]->accept_ratio_;
    accept_ratio bmigpRatio = allParameters["bmigp"]->accept_ratio_;
    accept_ratio agethpRatio = allParameters["agethp"]->accept_ratio_;
    accept_ratio bfecpRatio = allParameters["bfecp"]->accept_ratio_;
    
    
    cout << posterior << " " << Tune << " " << fineTune << endl;
    cout.flush();
    
    int xpourCent = nbIteration/20;
    if(!xpourCent) xpourCent=1;
    
    lastFecLogLikp = pLoi->logLik(NPar, Vec_Fecp, GamA);
    lastFecLogLiks = pLois->logLik(NPar, Vec_Fecs, GamAs);
    
    previousLogLik = logLik(posterior);
    
    cout <<previousLogLik << endl;
    cout.flush();
    //cerr << PosteriorMeres[1][1] << " " << PosteriorMeres[5][NPar] << endl;
    
    //previousLogLik = logLik();
    
    //output start value
    if(thinStep){
        
        previousLogLik = logLik(posterior);  // EK -> Computing posterior probabilities of parents
        cout <<previousLogLik << " "<<endl; cout.flush();
        
        *ParamFec << "iteration LogLik(FecS) Var(FecS) LogLikp(FecP) Var(FecP)" << endl;
        *IndivFec << "iteration " ;
        for (int p=0; p<NPar; p++) {*IndivFec << "Fs_" << dynamic_cast<parent*>(allParents[p])->getName() << " ";}
        for (int p=0; p<NPar; p++) {*IndivFec << "Fp_" << dynamic_cast<parent*>(allParents[p])->getName() << " ";}
        *IndivFec << endl;
        *ParamDisp << "iteration LogLik Scales Shapes Kappas Thetas Mig0S bMigS AgeThS bFecS"
        //      << " ScaleP ShapeP KappaP ThetaP"
        << " Mig0P bMigP"
        << " AgeThP bFecP"
        //      << " Self"
        <<endl;
        
        *ParamFec << "0 " << lastFecLogLiks << " " << GamAs << " " << lastFecLogLikp << " " << GamA << endl;
        ParamFec->flush();
        *IndivFec << "0 " ;
        for (int p=0; p<NPar; p++) {*IndivFec << Vec_Fecs[p] << " ";}
        for (int p=0; p<NPar; p++) {*IndivFec << Vec_Fecp[p] << " ";}
        *IndivFec << endl;
        IndivFec->flush();
        *ParamDisp << "0 " << previousLogLik
        << " " << Scales
        << " " << Shapes
        << " " << Kappas
        << " " << Thetas
        << " " << Mig0S << " " << bMigS
        << " " << AgeThS << " " << bFecS
        //      << " " << Scale << " " << Shape << " " << Kappa << " " << Theta
        << " " << Mig0P << " " << bMigP
        << " " << AgeThP << " " << bFecP
        //      << " " << Self
        <<endl;
        ParamDisp->flush();
        
    }
    
    else {previousLogLik = logLik();       cout <<previousLogLik << " " << endl;   }  // EK -> Computing posterior probabilities of parents
    
    if(nbIteration) cout << "it=1..."<< std::endl;
    for(int ite=1; ite<=nbIteration; ite++)
    {
        if (ite % xpourCent == 0) {cout << "it="<<ite << "..."<< std::endl;}
        
        if(thinStep && (ite%thinStep == 0)) {logLik(posterior);
            //cout << PosteriorMeres[1][1] << " " << PosteriorMeres[5][NPar] << endl;
        }
        
        //GamAs- fecundities distribution
        //nextValue = pow(GamAs, exp(0.2*gauss()));
        if( gamasRatio(1.0,1.0) != 0 )
        {
            nextValue = GamAs*exp(Tune*gauss());
            if( nextValue>mGamAs && nextValue<MGamAs )
            {
                lastFecLogLiks = pLois->logLik(NPar, Vec_Fecs,GamAs);
                nextFecLogLiks = pLois->logLik(NPar, Vec_Fecs, nextValue);
                if (suivi>0) cSuivi << ite << " GamAs " << nextValue << " " << lastFecLogLiks << " " << nextFecLogLiks << " " << nextFecLogLiks-lastFecLogLiks << " ";
                if( accept() < exp(nextFecLogLiks-lastFecLogLiks) * gamasRatio(GamAs,nextValue) )
                {
                    GamAs = nextValue;
                    lastFecLogLiks = nextFecLogLiks;
                    pLois->setDParam(MEMM_LOI_GAMA,GamAs);
                    if (suivi>0) cSuivi << "Accept" << endl;
                }
                else if (suivi>0) cSuivi << "Reject" << endl;
            }
        }
        if(thinStep)*ParamFec << ite << " " << lastFecLogLiks << " " << GamAs << " ";
        
        //GamA (GamAp)- fecundities distribution
        //nextValue = pow(GamA, exp(tune*gauss()));
        if( gamaRatio(1.0,1.0) != 0 )
        {
            nextValue = GamA*exp(Tune*gauss());
            if( nextValue>mGamA && nextValue<MGamA )
            {
                lastFecLogLikp = pLoi->logLik(NPar, Vec_Fecp,GamA);
                nextFecLogLikp = pLoi->logLik(NPar, Vec_Fecp, nextValue);
                if (suivi>0) cSuivi << ite << " Gamap " << nextValue << " "  << lastFecLogLikp << " " << nextFecLogLikp << " " << nextFecLogLikp-lastFecLogLikp << " ";
                if( accept() < exp(nextFecLogLikp-lastFecLogLikp) * gamaRatio(GamA,nextValue)  )
                {
                    GamA = nextValue;
                    lastFecLogLikp = nextFecLogLikp;
                    pLoi->setDParam(MEMM_LOI_GAMA,GamA);
                    if (suivi>0) cSuivi << "Accept" << endl;
                }
                else if (suivi>0) cSuivi << "Reject" << endl;
            }
        }
        if(thinStep)*ParamFec << lastFecLogLikp << " " << GamA << endl;
        
        
        // Individual fecundities
        if( (gamasRatio(1.0,1.0) != 0) || (gamaRatio(1.0,1.0) != 0) || (GamA != 0) || (GamAs != 0))
        {
            for(int p=0 ; p<NPar ; p++)
            {
                previousValue = Vec_Fecs[p];
                previousValue2 = Vec_Fecp[p];
                pLois->tirage(Vec_Fecs[p]);
                pLoi->tirage(Vec_Fecp[p]);
                nextLogLik = logLik();
                if (suivi>1) cSuivi << ite << " Fecundities " << p << " "  << previousValue << " "  << Vec_Fecs[p] << " " << previousValue2 << " "  << Vec_Fecp[p] << " "  << previousLogLik << " " << nextLogLik << " " << nextLogLik-previousLogLik << " ";
                if(accept()<exp(nextLogLik-previousLogLik)) {
                    previousLogLik = nextLogLik;
                    if (suivi>1) cSuivi << "Accept" << endl;
                }
                else
                {
                    Vec_Fecs[p] = previousValue;
                    Vec_Fecp[p] = previousValue2;
                    if (suivi>1) cSuivi << "Reject" << endl;
                }
            }
        }
        if(thinStep && (ite%thinStep == 0))
        {
            *IndivFec << ite << " " ;
            for (int p=0; p<NPar; p++) {*IndivFec << Vec_Fecs[p] << " ";}
            for (int p=0; p<NPar; p++) {*IndivFec << Vec_Fecp[p] << " ";}
            *IndivFec << endl;
            IndivFec->flush();
        }
        
        // Scales - seed dispersal distance
        if( scalesRatio(1.0,1.0) != 0 )
        {
            
            previousValue = Scales;
            Scales = previousValue*exp(Tune*gauss());
            if( Scales>mScales && Scales<MScales)
            {
                as=Scales*exp(gammln(2/Shapes)-gammln(3/Shapes));
                abs=-pow(as,-Shapes);
                Kabs=Shapes/2/3.14/as/as/exp(gammln(2/Shapes));
                
                nextLogLik = logLik();
                if (suivi>0) cSuivi << ite << " FemaleParam1 " << Scales << " "  << previousLogLik << " " << nextLogLik << " " << nextLogLik-previousLogLik << " ";
                if( accept() < exp(nextLogLik-previousLogLik) * scalesRatio(previousValue, Scales)) {
                    previousLogLik = nextLogLik;
                    if (suivi>0) cSuivi << "Accept" << endl;
                }
                else
                {
                    Scales = previousValue;
                    as=Scales*exp(gammln(2/Shapes)-gammln(3/Shapes));
                    abs=-pow(as,-Shapes);
                    Kabs=Shapes/2/3.14/as/as/exp(gammln(2/Shapes));
                    
                    if (suivi>0) cSuivi << "Reject" << endl;
                }
            }
            else Scales = previousValue;
        }
        
        //change Shapes
        if( shapesRatio(1.0,1.0) != 0 )
        {
            previousValue = Shapes;
            Shapes = previousValue*exp(Tune*gauss());
            if( Shapes>mShapes && Shapes<MShapes)
            {
                as=Scales*exp(gammln(2/Shapes)-gammln(3/Shapes));
                abs=-pow(as,-Shapes);
                Kabs=Shapes/2/3.14/as/as/exp(gammln(2/Shapes));
                nextLogLik = logLik();
                if (suivi>0) cSuivi << ite << " FemaleParam2 " << Shapes << " "  << previousLogLik << " " << nextLogLik << " " << nextLogLik-previousLogLik << " ";
                if( accept() < exp(nextLogLik-previousLogLik)  * shapesRatio(previousValue, Shapes)) {
                    previousLogLik = nextLogLik;
                    if (suivi>0) cSuivi << "Accept" << endl;
                }
                else{
                    Shapes = previousValue;
                    as=Scales*exp(gammln(2/Shapes)-gammln(3/Shapes));
                    abs=-pow(as,-Shapes);
                    Kabs=Shapes/2/3.14/as/as/exp(gammln(2/Shapes));
                    if (suivi>0) cSuivi << "Reject" << endl;
                }
            }
            else Shapes = previousValue;
        }
        
        //change Kappas
        if( kappasRatio(1.0,1.0) != 0 )
        {
            previousValue = Kappas;
            Kappas = previousValue*exp(Tune*gauss());
            if( Kappas>mKappas && Kappas<MKappas)
            {
                nextLogLik = logLik();
                if (suivi>0) cSuivi << ite << " FemaleParam3 " << Kappas << " "  << previousLogLik << " " << nextLogLik << " " << nextLogLik-previousLogLik << " ";
                if( accept() < exp(nextLogLik-previousLogLik)  * kappasRatio(previousValue, Kappas)) {
                    previousLogLik = nextLogLik;
                    if (suivi>0) cSuivi << "Accept" << endl;
                }
                else{
                    Kappas = previousValue;
                    if (suivi>0) cSuivi << "Reject" << endl;
                }
            }
            else Kappas = previousValue;
        }
        
        //change Thetas
        if( thetasRatio(1.0,1.0) != 0 )
        {
            previousValue = Thetas;
            Thetas = fmod((previousValue + Tune*gauss()),3.14159);
            if( Thetas>mThetas && Thetas<MThetas)
            {
                nextLogLik = logLik();
                if (suivi>0) cSuivi << ite << " FemaleParam4 " << Thetas << " "  << previousLogLik << " " << nextLogLik << " " << nextLogLik-previousLogLik << " ";
                if( accept() < exp(nextLogLik-previousLogLik) * thetasRatio(previousValue, Thetas) ) {
                    previousLogLik = nextLogLik;
                    if (suivi>0) cSuivi << "Accept" << endl;
                }
                else{
                    Thetas = previousValue;
                    if (suivi>0) cSuivi << "Reject" << endl;
                }
            }
            else Thetas = previousValue;
        }
        
        //change Mig0S
        if (mig0sRatio(1.0,1.0) != 0)
        {
            previousValue = Mig0S;
            Mig0S = previousValue + fineTune*gauss();
            
            if( Mig0S>mMig0S && Mig0S<MMig0S )
            {
                nextLogLik = logLik();
                if (suivi>0) cSuivi << ite << " Migration Seed " << Mig0S << " "  << previousLogLik << " " << nextLogLik << " " << nextLogLik-previousLogLik << " ";
                if( accept() < exp(nextLogLik-previousLogLik) * mig0sRatio(previousValue, Mig0S) ) {
                    previousLogLik = nextLogLik;
                    if (suivi>0) cSuivi << "Accept" << endl;
                }
                else{
                    Mig0S = previousValue;
                    if (suivi>0) cSuivi << "Reject" << endl;
                }
            }
            else {
                Mig0S = previousValue;
            }
        }
        
        //change bMigS
        if( bmigsRatio(1.0,1.0) != 0)
        {
            previousValue2 = bMigS;
            bMigS = previousValue2 + fineTune*gauss();
            
            if( bMigS>mbMigS && bMigS<MbMigS)
            {
                nextLogLik = logLik();
                if (suivi>0) cSuivi << ite << " Migration Seed " << bMigS << " "  << previousLogLik << " " << nextLogLik << " " << nextLogLik-previousLogLik << " ";
                if( accept() < exp(nextLogLik-previousLogLik) * bmigsRatio(previousValue2, bMigS)  ) {
                    previousLogLik = nextLogLik;
                    if (suivi>0) cSuivi << "Accept" << endl;
                }
                else{
                    bMigS = previousValue2;
                    if (suivi>0) cSuivi << "Reject" << endl;
                }
            }
            else {
                bMigS = previousValue2;
            }
        }
        
        //change Age Threshold AgeThS //For COLONIZ_DENDRO, Age Threshold is actually a DBH_Threshold
        if (agethsRatio(1.0,1.0) != 0)
        {
            previousValue = AgeThS;
            //      if (accept() < 0.5) AgeThS = previousValue + 1; else AgeThS = previousValue - 1;
            AgeThS = previousValue*exp(fineTune*gauss());
            
            if( AgeThS>mAgeThS && AgeThS<MAgeThS )
            {
                nextLogLik = logLik();
                if (suivi>0) cSuivi << ite << " FecxAge " << AgeThS << " "  << previousLogLik << " " << nextLogLik << " " << nextLogLik-previousLogLik << " ";
                if( accept() < exp(nextLogLik-previousLogLik) * agethsRatio(previousValue, AgeThS) ) {
                    previousLogLik = nextLogLik;
                    if (suivi>0) cSuivi << "Accept" << endl;
                }
                else{
                    AgeThS = previousValue;
                    if (suivi>0) cSuivi << "Reject" << endl;
                }
            }
            else {
                AgeThS = previousValue;
            }
        }
        
        //change  Slope bFecS //For COLONIZ_DENDRO, bFecS is actually a power over DBH
        if( bfecsRatio(1.0,1.0) != 0)
        {
            previousValue2 = bFecS;
            bFecS = previousValue2*exp(fineTune*gauss());
            
            if( bFecS>mbFecS && bFecS<MbFecS )
            {
                nextLogLik = logLik();
                if (suivi>0) cSuivi << ite << " FecxAge " << bFecS << " "  << previousLogLik << " " << nextLogLik << " " << nextLogLik-previousLogLik << " ";
                if( accept() < exp(nextLogLik-previousLogLik) * bfecsRatio(previousValue2, bFecS)) {
                    previousLogLik = nextLogLik;
                    if (suivi>0) cSuivi << "Accept" << endl;
                }
                else{
                    bFecS = previousValue2;
                    if (suivi>0) cSuivi << "Reject" << endl;
                }
            }
            else {
                bFecS = previousValue2;
            }
        }
        
        //    // change Scale
        //      if( scaleRatio(1.0,1.0) != 0 )
        //      {
        //    previousValue = Scale;
        //    Scale = previousValue*exp(0.2*gauss());
        //    if( Scale>mScale && Scale<MScale)
        //    {
        //      ap=Scale*exp(gammln(2/Shape)-gammln(3/Shape));
        //      abp=-pow(ap,-Shape);
        //      nextLogLik = logLik();
        //        if (suivi>0) cSuivi << ite << " MaleParam1 " << Scale << " "  << previousLogLik << " " << nextLogLik << " " << nextLogLik-previousLogLik << " ";
        //        if( accept() < exp(nextLogLik-previousLogLik) * scaleRatio(previousValue, Scale)) {
        //            previousLogLik = nextLogLik;
        //            if (suivi>0) cSuivi << "Accept" << endl;
        //        }
        //      else
        //      {
        //        Scale = previousValue;
        //        ap=Scale*exp(gammln(2/Shape)-gammln(3/Shape));
        //        abp=-pow(ap,-Shape);
        //           if (suivi>0) cSuivi << "Reject" << endl;
        //      }
        //    }
        //    else Scale = previousValue;
        //}
        //
        //    // change Shape
        //if( shapeRatio(1.0,1.0) != 0 )
        //{
        //    previousValue = Shape;
        //    Shape = previousValue*exp(0.2*gauss());
        //    if( Shape>mShape && Shape<MShape)
        //    {
        //      ap=Scale*exp(gammln(2/Shape)-gammln(3/Shape));
        //      abp=-pow(ap,-Shape);
        //      nextLogLik = logLik();
        //        if (suivi>0) cSuivi << ite << " MaleParam2 " << Shape << " "  << previousLogLik << " " << nextLogLik << " " << nextLogLik-previousLogLik << " ";
        //        if( accept() < exp(nextLogLik-previousLogLik) * shapeRatio(previousValue, Shape)) {
        //            previousLogLik = nextLogLik;
        //            if (suivi>0) cSuivi << "Accept" << endl;
        //        }
        //      else{
        //        Shape = previousValue;
        //        ap=Scale*exp(gammln(2/Shape)-gammln(3/Shape));
        //        abp=-pow(ap,-Shape);
        //          if (suivi>0) cSuivi << "Reject" << endl;
        //      }
        //    }
        //    else Shape = previousValue;
        //}
        //
        //      //change Kappa
        //if( kappaRatio(1.0,1.0) != 0 )
        //{
        //      previousValue = Kappa;
        //      Kappa = previousValue*exp(0.2*gauss());
        //      if( Kappa>mKappa && Kappa<MKappa)
        //      {
        //          nextLogLik = logLik();
        //          if (suivi>0) cSuivi << ite << " MaleParam3 " << Kappa << " "  << previousLogLik << " " << nextLogLik << " " << nextLogLik-previousLogLik << " ";
        //          if( accept() < exp(nextLogLik-previousLogLik) * kappaRatio(previousValue, Kappa)) {
        //              previousLogLik = nextLogLik;
        //              if (suivi>0) cSuivi << "Accept" << endl;
        //          }
        //          else{
        //              Kappa = previousValue;
        //              if (suivi>0) cSuivi << "Reject" << endl;
        //          }
        //      }
        //      else Kappa = previousValue;
        //}
        //
        //      //change Theta
        //if( thetaRatio(1.0,1.0) != 0 )
        //{
        //      previousValue = Theta;
        //      Theta = fmod((previousValue + 0.2*gauss()),3.14159);
        //      if( Theta>mTheta && Theta<MTheta)
        //      {
        //          nextLogLik = logLik();
        //          if (suivi>0) cSuivi << ite << " MaleParam4 " << Theta << " "  << previousLogLik << " " << nextLogLik << " " << nextLogLik-previousLogLik << " ";
        //          if( accept() < exp(nextLogLik-previousLogLik) * thetaRatio(previousValue, Theta)) {
        //              previousLogLik = nextLogLik;
        //              if (suivi>0) cSuivi << "Accept" << endl;
        //          }
        //          else{
        //              Theta = previousValue;
        //              if (suivi>0) cSuivi << "Reject" << endl;
        //          }
        //      }
        //      else Theta = previousValue;
        //}
        
        //    // change Mig
        //if( migRatio(1.0,1.0) != 0 )
        //{
        //    previousValue = Mig;
        //    Mig = previousValue*exp(0.2*gauss());
        //    if( Mig>mMig && Mig<MMig)
        //    {
        //      nextLogLik = logLik();
        //        if (suivi>0) cSuivi << ite << " MigrationRate " << Mig << " "  << previousLogLik << " " << nextLogLik << " " << nextLogLik-previousLogLik << " ";
        //        if( accept() < exp(nextLogLik-previousLogLik) * migRatio(previousValue, Mig) ) {
        //            previousLogLik = nextLogLik;
        //            if (suivi>0) cSuivi << "Accept" << endl;
        //        }
        //        else {
        //            Mig = previousValue;
        //            if (suivi>0) cSuivi << "Reject" << endl;
        //        }
        //    }
        //    else Mig = previousValue;
        //}
        //    // change Self
        //if( selfRatio(1.0,1.0) != 0 )
        //{
        //    previousValue = Self;
        //    Self = previousValue*exp(0.2*gauss());
        //    if( Self>mSelf && Self<MSelf)
        //    {
        //      nextLogLik = logLik();
        //        if (suivi>0) cSuivi << ite << " SelfingRate " << Self << " "  << previousLogLik << " " << nextLogLik << " " << nextLogLik-previousLogLik << " ";
        //        if( accept() < exp(nextLogLik-previousLogLik) * selfRatio(previousValue, Self) ) {
        //            previousLogLik = nextLogLik;
        //            if (suivi>0) cSuivi << "Accept" << endl;
        //        }
        //        else {
        //            Self = previousValue;
        //            if (suivi>0) cSuivi << "Reject" << endl;
        //        }
        //    }
        //    else Self = previousValue;
        //}
        
        //change Mig0P
        if (mig0pRatio(1.0,1.0) != 0)
        {
            previousValue = Mig0P;
            Mig0P = previousValue + fineTune*gauss();
            
            if( Mig0P>mMig0P && Mig0P<MMig0P )
            {
                nextLogLik = logLik();
                if (suivi>0) cSuivi << ite << " Migration Pollen " << Mig0P << " "  << previousLogLik << " " << nextLogLik << " " << nextLogLik-previousLogLik << " ";
                if( accept() < exp(nextLogLik-previousLogLik) * mig0pRatio(previousValue, Mig0P) ) {
                    previousLogLik = nextLogLik;
                    if (suivi>0) cSuivi << "Accept" << endl;
                }
                else{
                    Mig0P = previousValue;
                    if (suivi>0) cSuivi << "Reject" << endl;
                }
            }
            else {
                Mig0P = previousValue;
            }
        }
        
        //change bMigP
        if( bmigpRatio(1.0,1.0) != 0)
        {
            previousValue2 = bMigP;
            bMigP = previousValue2 + fineTune*gauss();
            
            if(bMigP>mbMigP && bMigP<MbMigP)
            {
                nextLogLik = logLik();
                if (suivi>0) cSuivi << ite << " Migration Pollen " << bMigP << " "  << previousLogLik << " " << nextLogLik << " " << nextLogLik-previousLogLik << " ";
                if( accept() < exp(nextLogLik-previousLogLik) * bmigpRatio(previousValue2, bMigP)) {
                    previousLogLik = nextLogLik;
                    if (suivi>0) cSuivi << "Accept" << endl;
                }
                else{
                    bMigP = previousValue2;
                    if (suivi>0) cSuivi << "Reject" << endl;
                }
            }
            else {
                bMigP = previousValue2;
            }
        }
        
        //change Age Threshold AgeThP //For COLONIZ_DENDRO, Age Threshold is actually a DBH_Threshold
        if( agethpRatio(1.0,1.0) != 0)
        {
            previousValue = AgeThP;
            //      if (accept() < 0.5) AgeThP = previousValue + 1; else AgeThP = previousValue - 1;
            AgeThP = previousValue*exp(fineTune*gauss());
            
            if( AgeThP>mAgeThP && AgeThP<MAgeThP )
            {
                nextLogLik = logLik();
                if (suivi>0) cSuivi << ite << " FecxAge " << AgeThP << " "  << previousLogLik << " " << nextLogLik << " " << nextLogLik-previousLogLik << " ";
                if( accept() < exp(nextLogLik-previousLogLik) * agethpRatio(previousValue, AgeThP) ) {
                    previousLogLik = nextLogLik;
                    if (suivi>0) cSuivi << "Accept" << endl;
                }
                else{
                    AgeThP = previousValue;
                    if (suivi>0) cSuivi << "Reject" << endl;
                }
            }
            else {
                AgeThP = previousValue;
            }
        }
        
        //change Slope bFecP //For COLONIZ_DENDRO, bFecP is actually a power over DBH
        if( bfecpRatio(1.0,1.0) != 0)
        {
            previousValue2 = bFecP;
            bFecP = previousValue2*exp(fineTune*gauss());
            
            if( bFecP>mbFecP && bFecP<MbFecP )
            {
                nextLogLik = logLik();
                if (suivi>0) cSuivi << ite << " FecxAge " << bFecP << " "  << previousLogLik << " " << nextLogLik << " " << nextLogLik-previousLogLik << " ";
                if( accept() < exp(nextLogLik-previousLogLik) * bfecpRatio(previousValue2, bFecP)) {
                    previousLogLik = nextLogLik;
                    if (suivi>0) cSuivi << "Accept" << endl;
                }
                else{
                    bFecP = previousValue2;
                    if (suivi>0) cSuivi << "Reject" << endl;
                }
            }
            else {
                bFecP = previousValue2;
            }
        }
        
        
        if (thinStep){
            *ParamDisp << ite<<" " << previousLogLik
            << " " << Scales
            << " " << Shapes
            << " " << Kappas
            << " " << Thetas
            << " " << Mig0S << " " << bMigS
            << " " << AgeThS << " " << bFecS
            //      << " " << Scale << " " << Shape << " " << Kappa << " " << Theta
            << " " << Mig0P << " " << bMigP
            << " " << AgeThP << " " << bFecP
            //      << " " << Self
            <<endl;
            ParamDisp->flush();
        }
        
        //      if (ite==100) logLik(3);
        
    }
    
    if (thinStep) exportPosteriors(posterior, *fposterior);
    (*fposterior).close();
}

void MEMM_coloniz_dendro::mcmc_dyn(int nbIteration, int thinStep)
{
    cerr<<"Dynamic seedlings not implemented yet"<<endl;
    exit(0);
}



/***************************** PROTECTED ********************************/

long double MEMM_coloniz_dendro::logLik(int posterior)
{
    
    long double liktemp = 0;
    long double pip = 0;
    long double pip2 = 0;
    long double piptot=0;
    int pbm = 0;
    int p, mm;
    
    vector < vector<long double> > matp (NPar, vector <long double> (NPar, 0.) );
    vector < vector<long double> > mats (Ns, vector <long double> (NPar, 0.) );
    vector <long double> totp (NPar);
    vector <long double> tots (Ns);
    
    // Under the hypothesis that pollen dispersal is not limited by distance, mat[s][p] is the proportion of father p in the pollen cloud of all mothers at the year of birth of individual s... matp is not the traditional matrix of pollen pool composition over the different mothers!...
    for (int s=0; s<NPar; s++)
    {
        totp[s]= exp(Mig0P + bMigP * (NYear - Age[s]));
        //for (int p=0; p<m; p++)
        p=0;
        //        while (Age[p] - Age[m] >= AgeThP)
        while (Age[p] > Age[s] )
        {
            if(DBH[p][NYear - Age[s]]>=AgeThP) matp[s][p]=Vec_Fecp[p]*pow(DBH[p][NYear - Age[s]],bFecP);
            // matp[m][p]=Vec_Fecp[p]*bFecP*(Age[p] - Age[m] - AgeThP);
            if (isnan(matp[s][p])){matp[s][p]=0; pbm=1;}
            totp[s]+=matp[s][p];
            p++;
        }
    }
    
//        cout << "Matp OK" << endl; cout.flush();
    
    for (int s=0; s<Ns; s++)
    {
        tots[s]= exp(Mig0S + bMigS * (NYear - Age[s]));
        if (posterior==3) cout << s << " : " << tots[s] << " : " ;
        
        //for (int m=0; m<NPar; m++)
        mm=0;
        //        while (Age[mm] - Age[s] >= AgeThS )
        while (Age[mm] > Age[s] )
        {
            if(DBH[mm][NYear - Age[s]]>=AgeThS) mats[s][mm]=Kabs*exp(abs*pow(DistMP[s][mm],Shapes))*exp(Kappas*cos(AzimMP[s][mm]-Thetas))*Vec_Fecs[mm]*pow(DBH[mm][NYear - Age[s]],bFecS);
            if (posterior==3) cout << mm << " -> " << mats[s][mm] << " : " ;
            if (isnan(mats[s][mm])){mats[s][mm]=0; pbm=1;}
            tots[s]+=mats[s][mm];
            mm++;
        }
        if (posterior==3) cout << endl;
    }
    
//        cout << "MatS OK" << endl; cout.flush();
    
    if (pbm==1) { cerr << " Warning: Overflow. One component of the dispersal matrix was not defined. " << endl;}
    
    // if (posterior == 1) => Old version of this step is still present in MEMM_coloniz...
    if( posterior == 1 || posterior == 4) Nposterior++ ;
    if (posterior == 2) {
        Nposterior++ ;
        for (int s=0; s<Ns; s++)
        {
            PosteriorMeres[s][NPar] += exp(Mig0S + bMigS * (NYear - Age[s]))/tots[s];
            for (int m=0; m<NPar; m++)
            {
                //cerr << s << " " << m << " " << tots[s] << " " << mats[s][m] << " " << PosteriorMeres[s][m] << " " << PosteriorMeres[s][NPar] << " ";
                PosteriorMeres[s][m] += 1/tots[s]*mats[s][m]/2;
                PosteriorMeres[s][NPar] += 1/tots[s]*mats[s][m]*exp(Mig0P + bMigP * (NYear - Age[s]))/totp[s]/2;
                for (int p=0; p<NPar; p++)
                {
                    PosteriorMeres[s][p] += 1/tots[s]*mats[s][m]/totp[m]*matp[s][p]/2;
                }
                //cerr << (1-Migs)/tots[s]*mats[s][m]/2 << " " << Migs + (1-Migs)/tots[s]*mats[s][m]*Mig/2 << " " << PosteriorMeres[s][1] << " " << PosteriorMeres[s][NPar] << endl;
            }
        }
    }

//    cout << "Posterior increment OK" << endl; cout.flush();

    for (int s=0; s<Ns; s++){
        if( ProbMig[s]>0 && tots[s]>0)
        {
            pip=0;pip2=0;
            for (int m=0; m<NbMeres[s]; m++)
            {
                pip += (ProbMeres[s][m].pere_prob)*mats[s][ProbMeres[s][m].pere_pot];
            }
            for (int m=0; m<NbCouples[s]; m++)
            {
                pip2 += (ProbCouples[s][m].couple_prob)*mats[s][ProbCouples[s][m].mere_pot]*matp[s][ProbCouples[s][m].pere_pot];
            }
            
            piptot = (ProbMig[s]*exp(Mig0S + bMigS * (NYear - Age[s]))+(pip*exp(Mig0P + bMigP * (NYear - Age[s]))+pip2)/totp[s])/tots[s];
            liktemp += log(piptot);
            
            if(posterior == 1 || posterior == 4){
                PosteriorOut[s] += ProbMig[s]*exp(Mig0S + bMigS * (NYear - Age[s]))/tots[s]/piptot;
                for (int m=0; m<NbMeres[s]; m++)
                {
                    PosteriorMeres[s][m] += (ProbMeres[s][m].pere_prob)*mats[s][ProbMeres[s][m].pere_pot]*exp(Mig0P + bMigP * (NYear - Age[s]))/totp[s]/tots[s]/piptot;
                }
                for (int m=0; m<NbCouples[s]; m++)
                {
                    PosteriorCouples[s][m] += (ProbCouples[s][m].couple_prob)*mats[s][ProbCouples[s][m].mere_pot]*matp[s][ProbCouples[s][m].pere_pot]/totp[s]/tots[s]/piptot;
                }
            }
 
//            cout << " posterior+ " ; cout.flush();

            //liktemp += log((ProbMig[s]*exp(Mig0S + bMigS * (Age[s] - 100.))+(pip*exp(Mig0P + bMigP * (Age[s] - 100.))+pip2)/totp[s])/tots[s]);
            if (posterior==3)
            {
                cout << s << " : " << tots[s] << " : " << totp[s] << " : ";
                for (int m=0; m<NbMeres[s]; m++)
                { cout << (ProbMeres[s][m].pere_prob) << " x " << mats[s][ProbMeres[s][m].pere_pot] << " : ";}
                cout << endl;
            }
            
            if (posterior==3) cout << s << " OUTxOUT " << ProbMig[s] << " * " << exp(Mig0S + bMigS *(NYear - Age[s]))/tots[s] << " + MxOUT " << pip/tots[s] << " * " << exp(Mig0P + bMigP * (NYear - Age[s]))/totp[s] << " + MxP " << pip2/totp[s]/tots[s] << endl;
            
        }
    }
    
//        cout << "Compute lik OK" << endl; cout.flush();
    
    return liktemp;
    
}


/*************** INIT ******************/


void MEMM_coloniz_dendro::loadParentFile(Configuration *p)
{
     char * temp;

    MEMM::loadParentFile(p);
    
       temp = p->getValue(MEMM_COLONIZ_Y0);
       Y0 = atof(temp);
       temp = p->getValue(MEMM_COLONIZ_YOBS);
       Yobs = atof(temp);
       NYear = Yobs - Y0 + 1;
       
       cout <<endl << "Observation range : " << Y0 << " to " << Yobs << " = " << NYear << " years. ";
       if (NYear > Nqt - 1 ) cout << " WARNING : Observation range longer than the available number of DBHs ...";
       cout <<endl;
    
}

void MEMM_coloniz_dendro::loadPopulation(Population * Pop)
{

    MEMM::loadPopulation(Pop);
    
    Y0=(Pop->y0_);
    Yobs=(Pop->yobs_);
    NYear = Yobs - Y0 + 1;
    cout << "Loading also Y0 and Yobs : " << Y0 << " | " << Yobs << endl;
    Ns=NPar;

    
}

void MEMM_coloniz_dendro::loadSeeds(Configuration *p)
{
    Ns=NPar;
    cout << "No seeds, only trees in Coloniz_dendro... Ns = " << Ns << " = NPar = " << NPar << " ? " << endl;
}

void MEMM_coloniz_dendro::calculDistances(Configuration *p)
{
    MEMM::calculDistances(p);
    cout << "No aditional distance computed in Coloniz_dendro" << endl;
}


void MEMM_coloniz_dendro::calculTransitionMatrix(Configuration *p)
{
    
    outputDirectory = string(p->getValue(MEMM_DIRECTORY_PATH));
    DIR * dir;
    if( (dir = opendir(outputDirectory.c_str())) != NULL )
    {
        closedir(dir);
    }
    else {
        cout << "#ERROR opening directory " << outputDirectory <<endl;
        exit(1);
    }

    //    cerr << " essai posterior : " << posterior;
    
    ///// First check of the existence of missing alleles
    
    cout << endl << endl << " Checking the alleles of all individuals ..." << endl;
    locus temp_allele;
    map<int,int>::iterator temp_check;
    
    for (int s=0; s<Ns; s++) {
        for (int l=0; l<Nl; l++) {
            temp_allele = *(allParents[s])->getGeno(l);
            temp_check = SizeAll[l].find(temp_allele.getAllele(0));
            if (temp_check == SizeAll[l].end() && temp_allele.getAllele(0)>0) cout << " Uknown allele found for individual " << dynamic_cast<parent*>(allParents[s])->getName() << " : " << temp_allele.getAllele(0) << " at locus " << l << " and allele # 1 !" << endl;
            
            temp_allele = *(allParents[s])->getGeno(l);
            temp_check = SizeAll[l].find(temp_allele.getAllele(1));
            if (temp_check == SizeAll[l].end() && temp_allele.getAllele(1)>0) cout << " Uknown allele found for individual " << dynamic_cast<parent*>(allParents[s])->getName() << " : " << temp_allele.getAllele(1) << " at locus " << l << " and allele # 2 !"<< endl;
            
        }
    }
    cout << " ... end of checking. If any uknown alleles, this can cause problems..." << endl << endl;
    ///// End of checking of the existence of missing alleles
    
    
    NbMeres.clear();
    NbMeres.resize(Ns);
    NbCouples.clear();
    NbCouples.resize(Ns);
    ProbMig.clear();
    ProbMig.resize(Ns);
    ProbMeres.resize(Ns);
    ProbCouples.resize(Ns);
    
    PosteriorMeres.resize(Ns);
    PosteriorCouples.resize(Ns);
    PosteriorOut.resize(Ns, 0.0);
    
    if(posterior==2) for(int s=0; s<Ns; s++) PosteriorMeres[s].resize(NPar+1,0.);
    
    
    individu nul("nul", genotype(Nl));
    double probtemp;
    PerePot perepottemp;
    CouplePot couplepottemp;
    int NMistype=0;  // NEW EK -> Considering a Maximum Number of Mistypings
    
    int writeProbTrans=1;
    ofstream exportProbTrans;
    stringstream ss;
    ss << outputDirectory << "/exportTransitionProb.txt";
    if (writeProbTrans>0) exportProbTrans.open(ss.str(), ofstream::out | ofstream::app);
    
    double best, scnd, nmisbest, dbest, azbest, dbest2, azbest2, dbest3, azbest3;
    int idbest, idscnd, idbest2, idscnd2;
    
    if (writeProbTrans>0) {
        exportProbTrans << "ID_Indiv" << "\t" << "log10T_out" << "\t";
        exportProbTrans << "Nb_Parents" << "\t" << "log10T_best" << "\t" << "log10T_scnd" << "\t" << "ID_best" << "\t" << "ID_scnd" << "\t";
        exportProbTrans << "NMistype_best" << "\t" << "Dist_best" << "\t" << "Azim_best" << "\t";
        exportProbTrans << "Nb_Couples" << "\t" << "log10T_best" << "\t" << "log10T_scnd" << "\t" << "ID_best1" << "\t" << "ID_best2" << "\t" << "ID_scnd1" << "\t" << "ID_scnd2" << "\t";
        exportProbTrans << "NMistype_best" << "\t" << "Dist_best1" << "\t" << "Dist_best2" << "\t" << "Dist_best12" << "\t" << "Azim_best1" << "\t" << "Azim_best2" << "\t" << "Azim_best12" << "\t" << endl;
        exportProbTrans.flush();
    }
    
    cout << endl << "Computing the mendelian likelihoods..."  << endl;
    
    for (int s=0; s<Ns; s++)   // Rappel : Ns = NPar
    {
              cout << s << "...";
              cout.flush();
              cout << (allParents[s])->getName()  << " " ; cout.flush();//<< endl;
        
        if (writeProbTrans>0) {exportProbTrans << (allParents[s])->getName() << "\t";exportProbTrans.flush();}
        
        
        NbMeres[s]=0;
        best=-1000.0;
        scnd=-1000.0;
        idbest=-1;
        idscnd=-1;
        nmisbest=-1.;
        dbest=-1.;
        azbest=0.;
        
        ProbMig[s]=(allParents[s])->mendel(nul,nul,Nl,Na,SizeAll,AllFreq);
        //      cout << ProbMig[s]  << " " ; cout.flush();//<< endl;
        if (ProbMig[s]==0) {cout << "Tree # " << s << " (" << (allParents[s])->getName() << ") carries an unknown allele " << endl;}
        
        for (int m=0; m<s; m++) if (Age[m]>Age[s])
        {
            //        cout << m << " " << Age[m] << " " << Age[s] << " "; cout.flush();
            
            if(!useMatError)
                probtemp = (allParents[s])->mendel((*allParents[m]),nul,Nl,Na,SizeAll,AllFreq);
            else
            { NMistype=0;
                probtemp = dynamic_cast<parent*>(allParents[s])->mendel((*allParents[m]),nul,Nl,Na,SizeAll,AllFreq,MatError,NMistype);
                //if(s<=5 && m<=5) cout<< NMistype << endl;
            }
            
            //        cout << probtemp << " " << NMistype << " "; cout.flush();
            
            if (probtemp>0 && NMistype <= NMistypeMax)  // NEW EK -> Considering a Maximum Number of Mistypings
            {
                NbMeres[s]++;
                if (log10(probtemp)>best) {
                    scnd=best; idscnd=idbest;
                    best=log10(probtemp); idbest=m;
                    nmisbest=NMistype;
                    dbest=dynamic_cast<parent*>(allParents[s])->dist((*allParents[m]));
                    azbest=dynamic_cast<parent*>(allParents[s])->azim((*allParents[m]));
                }
                else if (log10(probtemp)>scnd) {scnd=log10(probtemp); idscnd=m; }
                perepottemp.pere_pot=m;
                perepottemp.pere_prob=probtemp;
                ProbMeres[s].push_back(perepottemp);
                if(posterior == 1 || posterior == 4) PosteriorMeres[s].push_back(0); // NEW EK -> Computing posterior probabilities of parents
            }
        }
        cout << "Tree # " << s << " (" << dynamic_cast<parent*>(allParents[s])->getName() << ") has " << NbMeres[s] << " compatible parents : ";
        for (int m=0; m < NbMeres[s]; m++) cout << dynamic_cast<parent*>(allParents[ProbMeres[s][m].pere_pot])->getName() << " | ";
        cout << endl;
        
        if (writeProbTrans>0) {
            exportProbTrans << log10(ProbMig[s]) << "\t" << NbMeres[s] << "\t" << best << "\t" << scnd << "\t";
            if (idbest<0) exportProbTrans << "-1\t";  else exportProbTrans << dynamic_cast<parent*>(allParents[idbest])->getName() << "\t";
            if (idscnd<0) exportProbTrans << "-1\t";  else exportProbTrans << dynamic_cast<parent*>(allParents[idscnd])->getName() << "\t";
            exportProbTrans << nmisbest << "\t" << dbest << "\t" << azbest << "\t";
            exportProbTrans.flush();
        }
        
        
        best=-1000.0;
        scnd=-1000.0;
        idbest=-1;idbest2=-1;
        idscnd=-1;idscnd2=-1;
        nmisbest=-1.;
        dbest=-1.;dbest2=-1.;dbest3=-1.;
        azbest=0.;azbest2=0.;azbest3=0.;
        
        for (int m=0; m<NbMeres[s]; m++)
        {
            for (int p=0; p<NbMeres[s]; p++)
            {
                
                if(!useMatError)
                    probtemp = dynamic_cast<parent*>(allParents[s])->mendel((*allParents[ProbMeres[s][m].pere_pot]),(*allParents[ProbMeres[s][p].pere_pot]),Nl,Na,SizeAll,AllFreq);
                else
                { NMistype=0;
                    probtemp = dynamic_cast<parent*>(allParents[s])->mendel((*allParents[ProbMeres[s][m].pere_pot]),(*allParents[ProbMeres[s][p].pere_pot]),Nl,Na,SizeAll,AllFreq,MatError,NMistype);
                    //if(s<=5 && m<=5) cout<< NMistype << endl;
                }
                
                if (probtemp>0 && NMistype <= NMistypeMax)  // NEW EK -> Considering a Maximum Number of Mistypings
                {
                    NbCouples[s]++;
                    if (log10(probtemp)>best && !(ProbMeres[s][m].pere_pot==idbest2 && ProbMeres[s][p].pere_pot==idbest)) {
                        scnd=best; idscnd=idbest; idscnd2=idbest2;
                        best=log10(probtemp); idbest=ProbMeres[s][m].pere_pot; idbest2=ProbMeres[s][p].pere_pot;
                        nmisbest=NMistype;
                        dbest=dynamic_cast<parent*>(allParents[s])->dist((*allParents[ProbMeres[s][m].pere_pot]));
                        dbest2=dynamic_cast<parent*>(allParents[s])->dist((*allParents[ProbMeres[s][p].pere_pot]));
                        dbest3=dynamic_cast<parent*>(allParents[ProbMeres[s][m].pere_pot])->dist((*allParents[ProbMeres[s][p].pere_pot]));
                        azbest=dynamic_cast<parent*>(allParents[s])->azim((*allParents[ProbMeres[s][m].pere_pot]));
                        azbest2=dynamic_cast<parent*>(allParents[s])->azim((*allParents[ProbMeres[s][p].pere_pot]));
                        azbest3=dynamic_cast<parent*>(allParents[ProbMeres[s][m].pere_pot])->azim((*allParents[ProbMeres[s][p].pere_pot]));
                    }
                    else if (log10(probtemp)>scnd) {scnd=log10(probtemp); idscnd=ProbMeres[s][m].pere_pot; idscnd2=ProbMeres[s][p].pere_pot;}
                    couplepottemp.mere_pot=ProbMeres[s][m].pere_pot;
                    couplepottemp.pere_pot=ProbMeres[s][p].pere_pot;
                    couplepottemp.couple_prob=probtemp;
                    ProbCouples[s].push_back(couplepottemp);
                    if(posterior == 1 || posterior == 4) PosteriorCouples[s].push_back(0); // NEW EK -> Computing posterior probabilities of parents
                    
                }
            }
        }
        
        if (writeProbTrans>0)
        {
            exportProbTrans << NbCouples[s] << "\t" << best << "\t" << scnd << "\t";
            if (idbest<0) exportProbTrans << "-1\t-1\t";
            else exportProbTrans << dynamic_cast<parent*>(allParents[idbest])->getName() << "\t" << dynamic_cast<parent*>(allParents[idbest2])->getName() << "\t" ;
            if (idscnd<0) exportProbTrans << "-1\t-1\t";
            else exportProbTrans << dynamic_cast<parent*>(allParents[idscnd])->getName() << "\t" << dynamic_cast<parent*>(allParents[idscnd2])->getName() << "\t";
            exportProbTrans << nmisbest << "\t" << dbest << "\t" << dbest2 << "\t" << dbest3 << "\t" << azbest << "\t" << azbest2 << "\t" << azbest3 << "\t" << endl;
        }
        
        cout << "Tree # " << s << " (" << dynamic_cast<parent*>(allParents[s])->getName() << ") has " << NbCouples[s] << " compatible couples : ";
        for (int m=0; m < NbCouples[s]; m++) cout << dynamic_cast<parent*>(allParents[ProbCouples[s][m].mere_pot])->getName() << " x " << dynamic_cast<parent*>(allParents[ProbCouples[s][m].pere_pot])->getName() << " | ";
        cout << endl << "--------------------" << endl;
        
    }
    
    if (writeProbTrans>0) {exportProbTrans << endl; exportProbTrans.close();}
    //    cout << "Test PosteriorMeres " << PosteriorMeres[3][0] << endl;
    cout << endl << "Computation of the mendelian likelihoods : OK. " <<endl << endl;
    
}


void MEMM_coloniz_dendro::loadParameters(Configuration *p)
{
    char * temp;
    MEMM::loadParameters(p);
    
    Parameter * param_temp;
    
    cout<< "Load more Parameters..."<<endl;
    
    // Mig0P
    param_temp = allParameters["mig0p"];
    if(param_temp)
    {
        Mig0P = param_temp->INIT;
        mMig0P = param_temp->MIN;
        MMig0P = param_temp->MAX;
        cout << "Mig0P : "<< Mig0P << " [" <<mMig0P <<"|"<< MMig0P << "]"<<endl;
    }
    
    // bMigP
    param_temp = allParameters["bmigp"];
    if(param_temp)
    {
        bMigP = param_temp->INIT;
        mbMigP = param_temp->MIN;
        MbMigP = param_temp->MAX;
        cout << "bMigP : "<< bMigP << " [" <<mbMigP <<"|"<< MbMigP << "]"<<endl;
    }
    
    // AgeThP
    param_temp = allParameters["agethp"];
    if(param_temp)
    {
        AgeThP = param_temp->INIT;
        mAgeThP = param_temp->MIN;
        MAgeThP = param_temp->MAX;
        cout << "AgeThP : "<< AgeThP << " [" <<mAgeThP <<"|"<< MAgeThP << "]"<<endl;
    }
    
    // bFecP
    param_temp = allParameters["bfecp"];
    if(param_temp)
    {
        bFecP = param_temp->INIT;
        mbFecP = param_temp->MIN;
        MbFecP = param_temp->MAX;
        cout << "bFecP : "<< bFecP << " [" <<mbFecP <<"|"<< MbFecP << "]"<<endl;
    }
    
    // gamas
    param_temp = allParameters["gamas"];
    if(param_temp)
    {
        GamAs = param_temp->INIT;
        mGamAs = param_temp->MIN;
        MGamAs = param_temp->MAX;
        cout << "GamAs : "<< GamAs << " [" <<mGamAs <<"|"<< MGamAs << "]"<<endl;
    }
    
    // Scales
    param_temp = allParameters["scales"];
    if(param_temp)
    {
        Scales = param_temp->INIT;
        mScales = param_temp->MIN;
        MScales = param_temp->MAX;
        cout << "Scales : "<< Scales << " [" <<mScales <<"|"<< MScales << "]"<<endl;
    }
    
    // Shapes
    param_temp = allParameters["shapes"];
    if(param_temp)
    {
        Shapes = param_temp->INIT;
        mShapes = param_temp->MIN;
        MShapes = param_temp->MAX;
        cout << "Shapes : "<< Shapes << " [" <<mShapes <<"|"<< MShapes << "]"<<endl;
    }
    
    // Thetas
    param_temp = allParameters["thetas"];
    if(param_temp)
    {
        Thetas = param_temp->INIT;
        mThetas = param_temp->MIN;
        MThetas = param_temp->MAX;
        cout << "Thetas : "<< Thetas << " [" <<mThetas <<"|"<< MThetas << "]"<<endl;
    }
    
    // Kappas
    param_temp = allParameters["kappas"];
    if(param_temp)
    {
        Kappas = param_temp->INIT;
        mKappas = param_temp->MIN;
        MKappas = param_temp->MAX;
        cout << "Kappas : "<< Kappas << " [" <<mKappas <<"|"<< MKappas << "]"<<endl;
    }
    
    // Mig0S
    param_temp = allParameters["mig0s"];
    if(param_temp)
    {
        Mig0S = param_temp->INIT;
        mMig0S = param_temp->MIN;
        MMig0S = param_temp->MAX;
        cout << "Mig0S : "<< Mig0S << " [" <<mMig0S <<"|"<< MMig0S << "]"<<endl;
    }
    
    // bMigS
    param_temp = allParameters["bmigs"];
    if(param_temp)
    {
        bMigS = param_temp->INIT;
        mbMigS = param_temp->MIN;
        MbMigS = param_temp->MAX;
        cout << "bMigS : "<< bMigS << " [" <<mbMigS <<"|"<< MbMigS << "]"<<endl;
    }
    
    // AgeThS
    param_temp = allParameters["ageths"];
    if(param_temp)
    {
        AgeThS = param_temp->INIT;
        mAgeThS = param_temp->MIN;
        MAgeThS = param_temp->MAX;
        cout << "AgeThS : "<< AgeThS << " [" <<mAgeThS <<"|"<< MAgeThS << "]"<<endl;
    }
    
    // bFecS
    param_temp = allParameters["bfecs"];
    if(param_temp)
    {
        bFecS = param_temp->INIT;
        mbFecS = param_temp->MIN;
        MbFecS = param_temp->MAX;
        cout << "bFecS : "<< bFecS << " [" <<mbFecS <<"|"<< MbFecS << "]"<<endl;
    }
    
    
    as=Scales*exp(gammln(2/Shapes)-gammln(3/Shapes));
    //    cout <<"Deltas : "<<as<<endl;
    abs=-pow(as,-Shapes);
    Kabs=Shapes/2/3.14/as/as/exp(gammln(2/Shapes));
    
    
    Vec_Fecs.resize(NPar, 1.0 );
    Vec_Fecp.resize(NPar, 1.0 );
    
    pLois = loadDistribution(p, "individual_fecundities_seeds");
    
    cout<<"end loading Parameters"<<endl;
    
}
void MEMM_coloniz_dendro::calculWeightPollenDonor(Configuration *p)
{
       
    Age.resize( NPar, 0.0);
    DBH.resize( NPar, vector <double> (NYear, 0.0));
    
    cout <<endl << "Loading the DBH of all trees at all years..." <<endl;
    cout.flush();
    for (int p=0; p<NPar; p++)
    {
        Age[p]=((*allParents[p]).getCoVarQuanti(0));
        cout << "Indiv " << p << " : Age = " << Age[p];
        if (Age[p]>NYear) cout << " WARNING: this tree is older than the observation range ... ";
        cout << " : DBH = ";
        for (int y=0; y<NYear; y++)
        {
            DBH[p][y]=((*allParents[p]).getCoVarQuanti(1+y));
            if (DBH[p][y]>0) cout << DBH[p][y] << " : ";
        }
        cout<<endl;
    }
}


// NEW EK -> Computing posterior probabilities of parents


void MEMM_coloniz_dendro::exportPosteriors(int posteriortype, ofstream & fileposterior)
{
    double tottemp;
    double probtemp, probtemp0;   // EK -> Inclus Mistyping
    string name;
    char * temp;
    int NMistype=0;    // EK -> Inclus Mistyping
    individu nul("nul", genotype(Nl));
    
    double best, scnd, nmisbest, dbest, azbest, dbest2, azbest2, dbest3, azbest3;
    int idbest, idscnd, idbest2, idscnd2;
    
    
    cout<<endl<<"# Exporting the posterior probabilities ... "<<endl;
    cout<<"Based on ... "<< Nposterior << "  sampled MCMC steps" << endl;
    
    if (posteriortype==1)
        fileposterior << "Seed#  SeedID  Parent#1  Parent#2  Gen_Lik  NMistypings  PosteriorProb" << endl;
    else if (posteriortype==2)
    {   fileposterior << "Seed#  SeedID  ";
        for (int m=0; m<NPar; m++) fileposterior << dynamic_cast<parent*>(allParents[m])->getName() << " ";
        fileposterior << "OUT" << endl;
    }
    else if (posteriortype==4)
    {
        fileposterior << "#Indiv" << " " << "ID_Indiv" << " " << "Nb_Parents" << " " << "Nb_Couples" << " " ;
        fileposterior << "Prob_best" << " " << "Prob_scnd" << " " << "ID_best1" << " " << "ID_best2" << " " << "ID_scnd1" << " " << "ID_scnd2" << " ";
        fileposterior << "NMistype_best" << " " << "Dist_best1" << " " << "Dist_best2" << " " << "Dist_best12" << " " << "Azim_best1" << " " << "Azim_best2" << " " << "Azim_best12" << " " << endl;
    }
    
    for (int s=0; s<Ns; s++)
    {
        
        if (posteriortype==1) {
            
            tottemp=1;
            for (int m=0; m<NbMeres[s]; m++) {
                if(!useMatError)
                    probtemp = dynamic_cast<parent*>(allParents[s])->mendel((*allParents[ProbMeres[s][m].pere_pot]),nul,Nl,Na,SizeAll,AllFreq);
                else
                { NMistype=0;
                    probtemp = dynamic_cast<parent*>(allParents[s])->mendel((*allParents[ProbMeres[s][m].pere_pot]),nul,Nl,Na,SizeAll,AllFreq,MatError,NMistype);
                }
                fileposterior << s << " " << dynamic_cast<parent*>(allParents[s])->getName() << " " << dynamic_cast<parent*>(allParents[ProbMeres[s][m].pere_pot])->getName() << " OUT ";
                fileposterior << ProbMeres[s][m].pere_prob << " " << NMistype << " " << PosteriorMeres[s][m]/Nposterior << endl;  // EK -> Inclus Mistyping
                tottemp -= PosteriorMeres[s][m]/Nposterior;
            }
            
            for (int m=0; m<NbCouples[s]; m++) {
                if(!useMatError)
                    probtemp = dynamic_cast<parent*>(allParents[s])->mendel((*allParents[ProbCouples[s][m].mere_pot]),(*allParents[ProbCouples[s][m].pere_pot]),Nl,Na,SizeAll,AllFreq);
                else
                { NMistype=0;
                    probtemp = dynamic_cast<parent*>(allParents[s])->mendel((*allParents[ProbCouples[s][m].mere_pot]),(*allParents[ProbCouples[s][m].pere_pot]),Nl,Na,SizeAll,AllFreq,MatError,NMistype);
                }
                fileposterior << s << " " << dynamic_cast<parent*>(allParents[s])->getName() << " " << dynamic_cast<parent*>(allParents[ProbCouples[s][m].mere_pot])->getName() << " ";
                fileposterior << dynamic_cast<parent*>(allParents[ProbCouples[s][m].pere_pot])->getName() << " " << ProbCouples[s][m].couple_prob << " " << NMistype << " "  <<  PosteriorCouples[s][m]/Nposterior << endl;  // EK -> Inclus Mistyping
                tottemp -= PosteriorCouples[s][m]/Nposterior;
            }
            fileposterior << s << " " << dynamic_cast<parent*>(allParents[s])->getName() << " OUT OUT " << ProbMig[s] << " 0 " << PosteriorOut[s]/Nposterior << endl; // EK -> Inclus Mistyping
            
        }
        
        else if (posteriortype==2)
        {
            fileposterior << s << " " << dynamic_cast<parent*>(allParents[s])->getName() << " ";
            for (int m=0; m<NPar+1; m++) fileposterior << PosteriorMeres[s][m]/Nposterior << " ";
            fileposterior << endl;
            
        }
        
        else if (posteriortype==4)
        {
            fileposterior << s << " " << (allParents[s])->getName() << " ";
            
            best=0.0;
            scnd=0.0;
            idbest=-1;idbest2=-1;
            idscnd=-1;idscnd2=-1;
            nmisbest=-1.;
            dbest=-1.;dbest2=-1.;dbest3=-1.;
            azbest=0.;azbest2=0.;azbest3=0.;
            tottemp=1;
            
            if (s==3) cout << PosteriorMeres[s][0]/Nposterior << " ";
            
            for (int m=0; m<NbMeres[s]; m++) {
                probtemp=PosteriorMeres[s][m]/Nposterior;
                if (probtemp>best) {
                    scnd=best; idscnd=idbest;
                    best=probtemp; idbest=ProbMeres[s][m].pere_pot;
                    if(!useMatError) NMistype=0;
                    else
                    { NMistype=0;
                        probtemp0 = dynamic_cast<parent*>(allParents[s])->mendel((*allParents[ProbMeres[s][m].pere_pot]),nul,Nl,Na,SizeAll,AllFreq,MatError,NMistype);
                    }
                    nmisbest=NMistype;
                    dbest=dynamic_cast<parent*>(allParents[s])->dist((*allParents[m]));
                    azbest=dynamic_cast<parent*>(allParents[s])->azim((*allParents[m]));
                }
                else if (probtemp>scnd) {scnd=probtemp; idscnd=m; }
                tottemp -= probtemp;
            }
            for (int m=0; m<NbCouples[s]; m++) {
                probtemp = PosteriorCouples[s][m]/Nposterior;
                if (probtemp>best) {
                    scnd=best; idscnd=idbest;idscnd2=idbest2;
                    best=probtemp; idbest=ProbCouples[s][m].mere_pot; idbest2=ProbCouples[s][m].pere_pot;
                    if(!useMatError) NMistype=0;
                    else
                    { NMistype=0;
                        probtemp0 = dynamic_cast<parent*>(allParents[s])->mendel((*allParents[ProbCouples[s][m].mere_pot]),(*allParents[ProbCouples[s][m].pere_pot]),Nl,Na,SizeAll,AllFreq,MatError,NMistype);
                    }
                    nmisbest=NMistype;
                    dbest=dynamic_cast<parent*>(allParents[s])->dist((*allParents[ProbCouples[s][m].mere_pot]));
                    dbest2=dynamic_cast<parent*>(allParents[s])->dist((*allParents[ProbCouples[s][m].pere_pot]));
                    dbest3=dynamic_cast<parent*>(allParents[ProbCouples[s][m].mere_pot])->dist((*allParents[ProbCouples[s][m].pere_pot]));
                    azbest=dynamic_cast<parent*>(allParents[s])->azim((*allParents[ProbCouples[s][m].mere_pot]));
                    azbest2=dynamic_cast<parent*>(allParents[s])->azim((*allParents[ProbCouples[s][m].pere_pot]));
                    azbest3=dynamic_cast<parent*>(allParents[ProbCouples[s][m].mere_pot])->azim((*allParents[ProbCouples[s][m].pere_pot]));
                }
                else if (probtemp>scnd) {scnd=probtemp; idscnd=ProbCouples[s][m].mere_pot; idscnd2=ProbCouples[s][m].pere_pot; }
                tottemp -= probtemp;
            }
            probtemp = PosteriorOut[s]/Nposterior;
            if (s==3) cout << tottemp << " =?= " << probtemp << endl;
            
            if (probtemp>best) {
                scnd=best; idscnd=idbest;idscnd2=idbest2;
                best=probtemp; idbest=-1; idbest2=-1;
                nmisbest=0;
                dbest=-1.;dbest2=-1.;dbest3=-1.;
                azbest=0.;azbest2=0.;azbest3=0.;
            }
            else if (probtemp>scnd) {scnd=probtemp; idscnd=-1; idscnd2=-1; }
            
            fileposterior << NbMeres[s] << " " << NbCouples[s] << " " << best << " " << scnd << " ";
            if (idbest<0) fileposterior << "-1 ";  else fileposterior << dynamic_cast<parent*>(allParents[idbest])->getName() << " ";
            if (idbest2<0) fileposterior << "-1 ";  else fileposterior << dynamic_cast<parent*>(allParents[idbest2])->getName() << " ";
            if (idscnd<0) fileposterior << "-1 ";  else fileposterior << dynamic_cast<parent*>(allParents[idscnd])->getName() << " ";
            if (idscnd2<0) fileposterior << "-1 ";  else fileposterior << dynamic_cast<parent*>(allParents[idscnd2])->getName() << " ";
            fileposterior  << nmisbest << " " << dbest << " " << dbest2 << " " << dbest3 << " " << azbest << " " << azbest2 << " " << azbest3 << endl;
        }
    }
    
//    (fileposterior).close();
//    if(temp) delete []temp;
}


