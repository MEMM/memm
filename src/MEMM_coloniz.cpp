#include "MEMM_coloniz.hpp"

MEMM_coloniz::MEMM_coloniz()
{
  cout<< "MEMM : Coloniz mode"<<endl;
  Nposterior = 0;
}

MEMM_coloniz::~MEMM_coloniz()
{
  
  Vec_Fecs.clear();
  Vec_Fecp.clear();
  if(pLois) delete pLois;
  
}

void MEMM_coloniz::mcmc(int nbIteration, int thinStep)
{


   ofstream cSuivi ("suiviPasAPas.txt");
   int suivi=1;
    
  double lastFecLogLiks, nextFecLogLiks;
  double lastFecLogLikp, nextFecLogLikp;
  double previousValue, nextValue;
  double previousValue2;
  long double previousLogLik, nextLogLik;
    
    double Tune=0.2;
    double fineTune=0.05;

  int xpourCent = nbIteration/20;
  if(!xpourCent) xpourCent=1;

  lastFecLogLikp = pLoi->logLik(NPar, Vec_Fecp,GamA);
  lastFecLogLiks = pLois->logLik(NPar, Vec_Fecs,GamAs);
    
    previousLogLik = logLik(posterior);

    cout << Tune << " " << fineTune << endl;
    cout.flush();
    

    cout <<previousLogLik << endl;
    cout.flush();
    //cerr << PosteriorMeres[1][1] << " " << PosteriorMeres[5][NPar] << endl;

    //previousLogLik = logLik();

  //output start value
  if(thinStep){

      previousLogLik = logLik(posterior);  // EK -> Computing posterior probabilities of parents
      cout <<previousLogLik << " "; cout.flush();

    *ParamFec << "0 " << lastFecLogLiks << " " << GamAs << " " << lastFecLogLikp << " " << GamA << endl;
    ParamFec->flush();
    *IndivFec << "0 " ;
    for (int p=0; p<NPar; p++) {*IndivFec << Vec_Fecs[p] << " ";}
    for (int p=0; p<NPar; p++) {*IndivFec << Vec_Fecp[p] << " ";}
    *IndivFec << endl;
    IndivFec->flush();
    *ParamDisp << "0 " << previousLogLik
      << " " << Scales
      << " " << Shapes
      << " " << Kappas
      << " " << Thetas
      << " " << Mig0S << " " << bMigS
      << " " << AgeThS << " " << bFecS
//      << " " << Scale << " " << Shape << " " << Kappa << " " << Theta
      << " " << Mig0P << " " << bMigP
      << " " << AgeThP << " " << bFecP
//      << " " << Self
      <<endl;
    ParamDisp->flush();

  }

  else {previousLogLik = logLik();       cout <<previousLogLik << " ";   }  // EK -> Computing posterior probabilities of parents

  if(nbIteration) cout << "it=1..."<< std::endl;
  for(int ite=1; ite<=nbIteration; ite++)
  {
    if (ite % xpourCent == 0) {cout << "it="<<ite << "..."<< std::endl;}

      if(thinStep && (ite%thinStep == 0)) {logLik(posterior);
          //cout << PosteriorMeres[1][1] << " " << PosteriorMeres[5][NPar] << endl;
      }

    //GamAs- fecundities distribution
    //nextValue = pow(GamAs, exp(0.2*gauss()));
      nextValue = GamAs*exp(Tune*gauss());
    if( nextValue>mGamAs && nextValue<MGamAs )
    {
      lastFecLogLiks = pLois->logLik(NPar, Vec_Fecs,GamAs);
      nextFecLogLiks = pLois->logLik(NPar, Vec_Fecs, nextValue);
        if (suivi>0) cSuivi << ite << " GamAs " << nextValue << " " << lastFecLogLiks << " " << nextFecLogLiks << " " << nextFecLogLiks-lastFecLogLiks << " ";
      if( accept() < exp(nextFecLogLiks-lastFecLogLiks) )
      {
        GamAs = nextValue;
        lastFecLogLiks = nextFecLogLiks;
        pLois->setDParam(MEMM_LOI_GAMA,GamAs);
          if (suivi>0) cSuivi << "Accept" << endl;
      }
        else if (suivi>0) cSuivi << "Reject" << endl;
    }
    if(thinStep) *ParamFec << ite << " " << lastFecLogLiks << " " << GamAs << " ";

    //GamA (GamAp)- fecundities distribution
      //nextValue = pow(GamA, exp(tune*gauss()));
      nextValue = GamA*exp(Tune*gauss());
    if( nextValue>mGamA && nextValue<MGamA )
    {
      lastFecLogLikp = pLoi->logLik(NPar, Vec_Fecp,GamA);
      nextFecLogLikp = pLoi->logLik(NPar, Vec_Fecp, nextValue);
        if (suivi>0) cSuivi << ite << " Gamap " << nextValue << " "  << lastFecLogLikp << " " << nextFecLogLikp << " " << nextFecLogLikp-lastFecLogLikp << " ";
      if( accept() < exp(nextFecLogLikp-lastFecLogLikp) )
      {
        GamA = nextValue;
        lastFecLogLikp = nextFecLogLikp;
        pLoi->setDParam(MEMM_LOI_GAMA,GamA);
          if (suivi>0) cSuivi << "Accept" << endl;
      }
        else if (suivi>0) cSuivi << "Reject" << endl;
    }
    if(thinStep) *ParamFec << lastFecLogLikp << " " << GamA << endl;

    for(int p=0 ; p<NPar ; p++)
    {
      previousValue = Vec_Fecs[p];
      previousValue2 = Vec_Fecp[p];
      pLois->tirage(Vec_Fecs[p]);
      pLoi->tirage(Vec_Fecp[p]);
      nextLogLik = logLik();
        if (suivi>1) cSuivi << ite << " Fecundities " << p << " "  << previousValue << " "  << Vec_Fecs[p] << " " << previousValue2 << " "  << Vec_Fecp[p] << " "  << previousLogLik << " " << nextLogLik << " " << nextLogLik-previousLogLik << " ";
        if(accept()<exp(nextLogLik-previousLogLik)) {
            previousLogLik = nextLogLik;
            if (suivi>1) cSuivi << "Accept" << endl;
        }
      else
      {
        Vec_Fecs[p] = previousValue;
        Vec_Fecp[p] = previousValue2;
          if (suivi>1) cSuivi << "Reject" << endl;
      }
    }

    if(thinStep && (ite%thinStep == 0))
    {
      *IndivFec << ite << " " ;
      for (int p=0; p<NPar; p++) {*IndivFec << Vec_Fecs[p] << " ";}
      for (int p=0; p<NPar; p++) {*IndivFec << Vec_Fecp[p] << " ";}
      *IndivFec << endl;
      IndivFec->flush();
    }

    // change Scales
    previousValue = Scales;
    Scales = previousValue*exp(Tune*gauss());
    if( Scales>mScales && Scales<MScales)
    {
      as=Scales*exp(gammln(2/Shapes)-gammln(3/Shapes));
      abs=-pow(as,-Shapes);
      nextLogLik = logLik();
        if (suivi>0) cSuivi << ite << " FemaleParam1 " << Scales << " "  << previousLogLik << " " << nextLogLik << " " << nextLogLik-previousLogLik << " ";
        if( accept() < exp(nextLogLik-previousLogLik) ) {
            previousLogLik = nextLogLik;
            if (suivi>0) cSuivi << "Accept" << endl;
        }
      else
      {
        Scales = previousValue;
        as=Scales*exp(gammln(2/Shapes)-gammln(3/Shapes));
        abs=-pow(as,-Shapes);
          if (suivi>0) cSuivi << "Reject" << endl;
      }
    }
    else Scales = previousValue;

    //change Shapes
    previousValue = Shapes;
    Shapes = previousValue*exp(Tune*gauss());
    if( Shapes>mShapes && Shapes<MShapes)
    {
      as=Scales*exp(gammln(2/Shapes)-gammln(3/Shapes));
      abs=-pow(as,-Shapes);
      nextLogLik = logLik();
        if (suivi>0) cSuivi << ite << " FemaleParam2 " << Shapes << " "  << previousLogLik << " " << nextLogLik << " " << nextLogLik-previousLogLik << " ";
        if( accept() < exp(nextLogLik-previousLogLik) ) {
            previousLogLik = nextLogLik;
            if (suivi>0) cSuivi << "Accept" << endl;
        }
      else{
        Shapes = previousValue;
        as=Scales*exp(gammln(2/Shapes)-gammln(3/Shapes));
        abs=-pow(as,-Shapes);
          if (suivi>0) cSuivi << "Reject" << endl;
      }
    }
    else Shapes = previousValue;

      
      //change Kappas
      previousValue = Kappas;
      Kappas = previousValue*exp(Tune*gauss());
      if( Kappas>mKappas && Kappas<MKappas)
      {
          nextLogLik = logLik();
          if (suivi>0) cSuivi << ite << " FemaleParam3 " << Kappas << " "  << previousLogLik << " " << nextLogLik << " " << nextLogLik-previousLogLik << " ";
          if( accept() < exp(nextLogLik-previousLogLik) ) {
              previousLogLik = nextLogLik;
              if (suivi>0) cSuivi << "Accept" << endl;
          }
          else{
              Kappas = previousValue;
              if (suivi>0) cSuivi << "Reject" << endl;
          }
      }
      else Kappas = previousValue;

      //change Thetas
      previousValue = Thetas;
      Thetas = fmod((previousValue + Tune*gauss()),3.14159);
      if( Thetas>mThetas && Thetas<MThetas)
      {
          nextLogLik = logLik();
          if (suivi>0) cSuivi << ite << " FemaleParam4 " << Thetas << " "  << previousLogLik << " " << nextLogLik << " " << nextLogLik-previousLogLik << " ";
          if( accept() < exp(nextLogLik-previousLogLik) ) {
              previousLogLik = nextLogLik;
              if (suivi>0) cSuivi << "Accept" << endl;
          }
          else{
              Thetas = previousValue;
              if (suivi>0) cSuivi << "Reject" << endl;
          }
      }
      else Thetas = previousValue;
      
      //change Mig0S & bMigS simultaneously
      previousValue = Mig0S;
      previousValue2 = bMigS;
      Mig0S = previousValue*exp(fineTune*gauss());
      bMigS = (previousValue2 + fineTune*gauss());
      
      if( Mig0S>mMig0S && Mig0S<MMig0S && bMigS>mbMigS && bMigS<MbMigS)
      {
          nextLogLik = logLik();
          if (suivi>0) cSuivi << ite << " Migration Seed " << bMigS << " "  << previousLogLik << " " << nextLogLik << " " << nextLogLik-previousLogLik << " ";
          if( accept() < exp(nextLogLik-previousLogLik) ) {
              previousLogLik = nextLogLik;
              if (suivi>0) cSuivi << "Accept" << endl;
          }
          else{
              Mig0S = previousValue;
              bMigS = previousValue2;
              if (suivi>0) cSuivi << "Reject" << endl;
          }
      }
      else {
          Mig0S = previousValue;
          bMigS = previousValue2;
      }

      //change Age Threshold AgeThS and Slope bFecS
      previousValue = AgeThS;
      previousValue2 = bFecS;
      if (accept() < 0.5) AgeThS = previousValue + 1; else AgeThS = previousValue - 1;
      bFecS = previousValue2*exp(fineTune*gauss());

      if( AgeThS>mAgeThS && AgeThS<MAgeThS && bFecS>mbFecS && bFecS<MbFecS )
      {
          nextLogLik = logLik();
          if (suivi>0) cSuivi << ite << " FecxAge " << bFecS << " "  << previousLogLik << " " << nextLogLik << " " << nextLogLik-previousLogLik << " ";
          if( accept() < exp(nextLogLik-previousLogLik) ) {
              previousLogLik = nextLogLik;
              if (suivi>0) cSuivi << "Accept" << endl;
          }
          else{
              AgeThS = previousValue;
              bFecS = previousValue2;
              if (suivi>0) cSuivi << "Reject" << endl;
          }
      }
      else {
          AgeThS = previousValue;
          bFecS = previousValue2;
      }
      
      
//    // change Scale
//    previousValue = Scale;
//    Scale = previousValue*exp(0.2*gauss());
//    if( Scale>mScale && Scale<MScale)
//    {
//      ap=Scale*exp(gammln(2/Shape)-gammln(3/Shape));
//      abp=-pow(ap,-Shape);
//      nextLogLik = logLik();
//        if (suivi>0) cSuivi << ite << " MaleParam1 " << Scale << " "  << previousLogLik << " " << nextLogLik << " " << nextLogLik-previousLogLik << " ";
//        if( accept() < exp(nextLogLik-previousLogLik) ) {
//            previousLogLik = nextLogLik;
//            if (suivi>0) cSuivi << "Accept" << endl;
//        }
//      else
//      {
//        Scale = previousValue;
//        ap=Scale*exp(gammln(2/Shape)-gammln(3/Shape));
//        abp=-pow(ap,-Shape);
//           if (suivi>0) cSuivi << "Reject" << endl;
//      }
//    }
//    else Scale = previousValue;
//
//    // change Shape
//    previousValue = Shape;
//    Shape = previousValue*exp(0.2*gauss());
//    if( Shape>mShape && Shape<MShape)
//    {
//      ap=Scale*exp(gammln(2/Shape)-gammln(3/Shape));
//      abp=-pow(ap,-Shape);
//      nextLogLik = logLik();
//        if (suivi>0) cSuivi << ite << " MaleParam2 " << Shape << " "  << previousLogLik << " " << nextLogLik << " " << nextLogLik-previousLogLik << " ";
//        if( accept() < exp(nextLogLik-previousLogLik) ) {
//            previousLogLik = nextLogLik;
//            if (suivi>0) cSuivi << "Accept" << endl;
//        }
//      else{
//        Shape = previousValue;
//        ap=Scale*exp(gammln(2/Shape)-gammln(3/Shape));
//        abp=-pow(ap,-Shape);
//          if (suivi>0) cSuivi << "Reject" << endl;
//      }
//    }
//    else Shape = previousValue;
//
//      //change Kappa
//      previousValue = Kappa;
//      Kappa = previousValue*exp(0.2*gauss());
//      if( Kappa>mKappa && Kappa<MKappa)
//      {
//          nextLogLik = logLik();
//          if (suivi>0) cSuivi << ite << " MaleParam3 " << Kappa << " "  << previousLogLik << " " << nextLogLik << " " << nextLogLik-previousLogLik << " ";
//          if( accept() < exp(nextLogLik-previousLogLik) ) {
//              previousLogLik = nextLogLik;
//              if (suivi>0) cSuivi << "Accept" << endl;
//          }
//          else{
//              Kappa = previousValue;
//              if (suivi>0) cSuivi << "Reject" << endl;
//          }
//      }
//      else Kappa = previousValue;
//
//      //change Theta
//      previousValue = Theta;
//      Theta = fmod((previousValue + 0.2*gauss()),3.14159);
//      if( Theta>mTheta && Theta<MTheta)
//      {
//          nextLogLik = logLik();
//          if (suivi>0) cSuivi << ite << " MaleParam4 " << Theta << " "  << previousLogLik << " " << nextLogLik << " " << nextLogLik-previousLogLik << " ";
//          if( accept() < exp(nextLogLik-previousLogLik) ) {
//              previousLogLik = nextLogLik;
//              if (suivi>0) cSuivi << "Accept" << endl;
//          }
//          else{
//              Theta = previousValue;
//              if (suivi>0) cSuivi << "Reject" << endl;
//          }
//      }
//      else Theta = previousValue;
//
//    // change Self
//    previousValue = Self;
//    Self = previousValue*exp(0.2*gauss());
//    if( Self>mSelf && Self<MSelf)
//    {
//      nextLogLik = logLik();
//        if (suivi>0) cSuivi << ite << " SelfingRate " << Self << " "  << previousLogLik << " " << nextLogLik << " " << nextLogLik-previousLogLik << " ";
//        if( accept() < exp(nextLogLik-previousLogLik) ) {
//            previousLogLik = nextLogLik;
//            if (suivi>0) cSuivi << "Accept" << endl;
//        }
//        else {
//            Self = previousValue;
//            if (suivi>0) cSuivi << "Reject" << endl;
//        }
//    }
//    else Self = previousValue;

      
      //change Mig0P & bMigP simultaneously
      previousValue = Mig0P;
      previousValue2 = bMigP;
      Mig0P = previousValue*exp(fineTune*gauss());
      bMigP = (previousValue2 + fineTune*gauss());
      
      if( Mig0P>mMig0P && Mig0P<MMig0P && bMigP>mbMigP && bMigP<MbMigP)
      {
          nextLogLik = logLik();
          if (suivi>0) cSuivi << ite << " Migration Pollen " << bMigP << " "  << previousLogLik << " " << nextLogLik << " " << nextLogLik-previousLogLik << " ";
          if( accept() < exp(nextLogLik-previousLogLik) ) {
              previousLogLik = nextLogLik;
              if (suivi>0) cSuivi << "Accept" << endl;
          }
          else{
              Mig0P = previousValue;
              bMigP = previousValue2;
              if (suivi>0) cSuivi << "Reject" << endl;
          }
      }
      else {
          Mig0P = previousValue;
          bMigP = previousValue2;
      }
      
      //change Age Threshold AgeThP and Slope bFecP
      previousValue = AgeThP;
      previousValue2 = bFecP;
      if (accept() < 0.5) AgeThP = previousValue + 1; else AgeThP = previousValue - 1;
      bFecP = previousValue2*exp(fineTune*gauss());
      
      if( AgeThP>mAgeThP && AgeThP<MAgeThP && bFecP>mbFecP && bFecP<MbFecP )
      {
          nextLogLik = logLik();
          if (suivi>0) cSuivi << ite << " FecxAge " << bFecP << " "  << previousLogLik << " " << nextLogLik << " " << nextLogLik-previousLogLik << " ";
          if( accept() < exp(nextLogLik-previousLogLik) ) {
              previousLogLik = nextLogLik;
              if (suivi>0) cSuivi << "Accept" << endl;
          }
          else{
              AgeThP = previousValue;
              bFecP = previousValue2;
              if (suivi>0) cSuivi << "Reject" << endl;
          }
      }
      else {
          AgeThP = previousValue;
          bFecP = previousValue2;
      }
      
    *ParamDisp << ite<<" " << previousLogLik
      << " " << Scales
      << " " << Shapes
      << " " << Kappas
      << " " << Thetas
      << " " << Mig0S << " " << bMigS
      << " " << AgeThS << " " << bFecS
      //      << " " << Scale << " " << Shape << " " << Kappa << " " << Theta
      << " " << Mig0P << " " << bMigP
      << " " << AgeThP << " " << bFecP
      //      << " " << Self
      <<endl;
    ParamDisp->flush();

  }
}

void MEMM_coloniz::mcmc_dyn(int nbIteration, int thinStep)
{
  cerr<<"Dynamic seedlings not implemented yet"<<endl;
  exit(0);
}





/***************************** PROTECTED ********************************/

long double MEMM_coloniz::logLik(int posterior)
{
    
    long double liktemp = 0;
    long double pip = 0;
    long double pip2 = 0;
    long double piptot=0;
    int pbm = 0;
    int p, mm;
    
    vector < vector<long double> > matp (NPar, vector <long double> (NPar, 0.) );
    vector < vector<long double> > mats (Ns, vector <long double> (NPar, 0.) );
    vector <long double> totp (NPar);
    vector <long double> tots (Ns);
    
//    cout << Mig0P << " " << bMigP << " " << AgeThP << " " << bFecP << endl;
//    cout << Mig0S << " " << bMigS << " " << AgeThS << " " << bFecS << endl; cout.flush();

    for (int m=0; m<NPar; m++)
    {
        totp[m]= exp(Mig0P + bMigP * (Age[m] - 100.));
        //for (int p=0; p<m; p++)
        p=0;
        while (Age[p] - Age[m] >= AgeThP)
        {
            matp[m][p]=Vec_Fecp[p]*bFecP*(Age[p] - Age[m] - AgeThP);
            if (isnan(matp[m][p])){matp[m][p]=0; pbm=1;}
            totp[m]+=matp[m][p];
            p++;
        }
    }
    
//    cout << "Matp OK" << endl; cout.flush();
    
    for (int s=0; s<Ns; s++)
    {
        tots[s]= exp(Mig0S + bMigS * (Age[s] - 100.));
        //for (int m=0; m<NPar; m++)
        mm=0;
        while (Age[mm] - Age[s] >= AgeThS)
        {
            mats[s][mm]=exp(abs*pow(DistMP[s][mm],Shapes))*exp(Kappas*cos(AzimMP[s][mm]-Thetas))*Vec_Fecs[mm]*bFecS*(Age[mm] - Age[s] - AgeThS);
            if (isnan(mats[s][mm])){mats[s][mm]=0; pbm=1;}
            tots[s]+=mats[s][mm];
            mm++;
        }
        //cout << s << " " << abs << " " << Shapes << " " << tots[s] << endl;
    }
    
//    cout << "MatS OK" << endl; cout.flush();

    if (pbm==1) { cerr << " Warning: Overflow. One component of the dispersal matrix was not defined. " << endl;}
    
//    if(posterior == 1){
//        Nposterior++ ;
//        for (int s=0; s<Ns; s++)
//        {
//            if( ProbMig[s]>0 && tots[s]>0)
//            {
//                pip=0;pip2=0;
//                for (int m=0; m<NbMeres[s]; m++)
//                {
//                    pip += (ProbMeres[s][m].pere_prob)*mats[s][ProbMeres[s][m].pere_pot];
//                }
//                for (int m=0; m<NbCouples[s]; m++)
//                {
//                    pip2 += (ProbCouples[s][m].couple_prob)*mats[s][ProbCouples[s][m].mere_pot]*matp[s][ProbCouples[s][m].pere_pot];
//                }
//                piptot = (ProbMig[s]*(Mig0S + bMigS * (Age[s] - 100.))+(pip*(Mig0P + bMigP * (Age[s] - 100.))+pip2)/totp[s])/tots[s];
//                liktemp += piptot;
//
//                for (int m=0; m<NbMeres[s]; m++)
//                {
//                    PosteriorMeres[s][m] += (Mig0P + bMigP * (Age[s] - 100.))/totp[s]/tots[s]*(ProbMeres[s][m].pere_prob)*mats[s][ProbMeres[s][m].pere_pot]/piptot;
//                }
//                for (int m=0; m<NbCouples[s]; m++)
//                {
//                    PosteriorCouples[s][m] += (ProbCouples[s][m].couple_prob)*mats[s][ProbCouples[s][m].mere_pot]*matp[s][ProbCouples[s][m].pere_pot]/totp[s]/tots[s];
//                }
//
//            }
//        }
////        cout << "PosteriorProb OK" << endl; cout.flush();
//    }
    
    if (posterior == 2) {
        Nposterior++ ;
        for (int s=0; s<Ns; s++)
        {
            PosteriorMeres[s][NPar] += exp(Mig0S + bMigS * (Age[s] - 100.))/tots[s];
            for (int m=0; m<NPar; m++)
            {
                //cerr << s << " " << m << " " << tots[s] << " " << mats[s][m] << " " << PosteriorMeres[s][m] << " " << PosteriorMeres[s][NPar] << " ";
                PosteriorMeres[s][m] += 1/tots[s]*mats[s][m]/2;
                PosteriorMeres[s][NPar] += 1/tots[s]*mats[s][m]*exp(Mig0P + bMigP * (Age[s] - 100.))/totp[s]/2;
                for (int p=0; p<NPar; p++)
                {
                    PosteriorMeres[s][p] += 1/tots[s]*mats[s][m]/totp[m]*matp[s][p]/2;
                }
                //cerr << (1-Migs)/tots[s]*mats[s][m]/2 << " " << Migs + (1-Migs)/tots[s]*mats[s][m]*Mig/2 << " " << PosteriorMeres[s][1] << " " << PosteriorMeres[s][NPar] << endl;
            }
        }
    }
    
//    if(posterior == 1){
//        Nposterior++ ;
//        for (int s=0; s<Ns; s++)
//        {
//            if( ProbMig[s]>0 && tots[s]>0)
//            {
//                pip=0;pip2=0;
//                for (int m=0; m<NbMeres[s]; m++)
//                {
//                    pip += (ProbMeres[s][m].pere_prob)*mats[s][ProbMeres[s][m].pere_pot];
//                }
//                for (int m=0; m<NbCouples[s]; m++)
//                {
//                    pip2 += (ProbCouples[s][m].couple_prob)*mats[s][ProbCouples[s][m].mere_pot]*matp[s][ProbCouples[s][m].pere_pot];
//                }
//                piptot = (ProbMig[s]*(Mig0S + bMigS * (Age[s] - 100.))+(pip*(Mig0P + bMigP * (Age[s] - 100.))+pip2)/totp[s])/tots[s];
//                liktemp += piptot;
//
//                for (int m=0; m<NbMeres[s]; m++)
//                {
//                    PosteriorMeres[s][m] += (Mig0P + bMigP * (Age[s] - 100.))/totp[s]/tots[s]*(ProbMeres[s][m].pere_prob)*mats[s][ProbMeres[s][m].pere_pot]/piptot;
//                }
//                for (int m=0; m<NbCouples[s]; m++)
//                {
//                    PosteriorCouples[s][m] += (ProbCouples[s][m].couple_prob)*mats[s][ProbCouples[s][m].mere_pot]*matp[s][ProbCouples[s][m].pere_pot]/totp[s]/tots[s];
//                }
//
//            }
//        }
//        //        cout << "PosteriorProb OK" << endl; cout.flush();
//    }
//
    
    
    for (int s=0; s<Ns; s++){
        if( ProbMig[s]>0 && tots[s]>0)
        {
            pip=0;pip2=0;
            for (int m=0; m<NbMeres[s]; m++)
            {
                pip += (ProbMeres[s][m].pere_prob)*mats[s][ProbMeres[s][m].pere_pot];
            }
            for (int m=0; m<NbCouples[s]; m++)
            {
                pip2 += (ProbCouples[s][m].couple_prob)*mats[s][ProbCouples[s][m].mere_pot]*matp[s][ProbCouples[s][m].pere_pot];
            }
            
            piptot = (ProbMig[s]*exp(Mig0S + bMigS * (Age[s] - 100.))+(pip*exp(Mig0P + bMigP * (Age[s] - 100.))+pip2)/totp[s])/tots[s];
            liktemp += log(piptot);

            if(posterior == 1){
                Nposterior++ ;
                for (int m=0; m<NbMeres[s]; m++)
                {
                    PosteriorMeres[s][m] += exp(Mig0P + bMigP * (Age[s] - 100.))/totp[s]/tots[s]*(ProbMeres[s][m].pere_prob)*mats[s][ProbMeres[s][m].pere_pot]/piptot;
                }
                for (int m=0; m<NbCouples[s]; m++)
                {
                    PosteriorCouples[s][m] += (ProbCouples[s][m].couple_prob)*mats[s][ProbCouples[s][m].mere_pot]*matp[s][ProbCouples[s][m].pere_pot]/totp[s]/tots[s];
                }
            }

            //liktemp += log((ProbMig[s]*exp(Mig0S + bMigS * (Age[s] - 100.))+(pip*exp(Mig0P + bMigP * (Age[s] - 100.))+pip2)/totp[s])/tots[s]);
            if (posterior==3) cout << s << " " << log((ProbMig[s]*exp(Mig0S + bMigS * (Age[s] - 100.))+(pip*exp(Mig0P + bMigP * (Age[s] - 100.))+pip2)/totp[s])/tots[s]) << endl;
            
        }
    }
    
//    cout << "Compute lik OK" << endl; cout.flush();

    return liktemp;
    
}


/*************** INIT ******************/



void MEMM_coloniz::loadSeeds(Configuration *p)
{
    Ns=NPar;
    cout << "Nos seeds, only trees in Coloniz... Ns = " << Ns << " = NPar = " << NPar << " ? " << endl;
}

void MEMM_coloniz::calculDistances(Configuration *p)
{
  MEMM::calculDistances(p);
    Ns=NPar;
    cout << "No aditional distance computed in Coloniz" << endl;
}

void MEMM_coloniz::calculTransitionMatrix(Configuration *p)
{

    cerr << " essai posterior : " << posterior;
   
///// First check of the existence of missing alleles

    cout << endl << endl << " Checking the alleles of all individuals ..." << endl;
    locus temp_allele;
    map<int,int>::iterator temp_check;
    
    for (int s=0; s<Ns; s++) {
        for (int l=0; l<Nl; l++) {
            temp_allele = *(allParents[s])->getGeno(l);
            temp_check = SizeAll[l].find(temp_allele.getAllele(0));
            if (temp_check == SizeAll[l].end() && temp_allele.getAllele(0)>0) cout << " Uknown allele found for individual " << dynamic_cast<parent*>(allParents[s])->getName() << " : " << temp_allele.getAllele(0) << " at locus " << l << " and allele # 1 !" << endl;

            temp_allele = *(allParents[s])->getGeno(l);
            temp_check = SizeAll[l].find(temp_allele.getAllele(1));
            if (temp_check == SizeAll[l].end() && temp_allele.getAllele(1)>0) cout << " Uknown allele found for individual " << dynamic_cast<parent*>(allParents[s])->getName() << " : " << temp_allele.getAllele(1) << " at locus " << l << " and allele # 2 !"<< endl;

        }
    }
    cout << " ... end of checking. If any uknown alleles, this can cause problems..." << endl << endl;
///// End of checking of the existence of missing alleles

    
  NbMeres.clear();
  NbMeres.resize(Ns);
  NbCouples.clear();
  NbCouples.resize(Ns);
  ProbMig.clear();
  ProbMig.resize(Ns);
  ProbMeres.resize(Ns);
  ProbCouples.resize(Ns);

    PosteriorMeres.resize(Ns);
    PosteriorCouples.resize(Ns);

    if(posterior==2) for(int s=0; s<Ns; s++) PosteriorMeres[s].resize(NPar+1,0.);
    
    
  individu nul("nul", genotype(Nl));
  double probtemp;
  PerePot perepottemp;
  CouplePot couplepottemp;
    
  int NMistype=0;  // NEW EK -> Considering a Maximum Number of Mistypings

    int writeProbTrans=1;
    ofstream exportProbTrans;
    if (writeProbTrans>0) exportProbTrans.open("exportProbTrans.txt");
    
    double best, scnd, nmisbest, dbest, azbest, dbest2, azbest2, dbest3, azbest3;
    int idbest, idscnd, idbest2, idscnd2;
    

    cout << endl << "Computing the mendelian likelihoods..."  << endl;

    for (int s=0; s<Ns; s++)   // Rappel : Ns = NPar
  {
//      cout << s << "...";
//      cout.flush();
//      cout << dynamic_cast<parent*>(allParents[s])->getName()  << " " ; //<< endl;
      
       if (writeProbTrans>0) exportProbTrans << dynamic_cast<parent*>(allParents[s])->getName() << "\t";
          
    NbMeres[s]=0;
      best=-100.0;
      scnd=-100.0;
      idbest=-1;
      idscnd=-1;
      nmisbest=-1.;
      dbest=-1.;
      azbest=0.;

    ProbMig[s]=dynamic_cast<parent*>(allParents[s])->mendel(nul,nul,Nl,Na,SizeAll,AllFreq);
     if (ProbMig[s]==0) {cout << "Tree # " << s << " (" << dynamic_cast<parent*>(allParents[s])->getName() << ") carries an unknown allele " << endl;}

    for (int m=0; m<s; m++) if (Age[m]>Age[s])
    {
//        cout << m << " " << Age[m] << " " << Age[s] << " "; cout.flush();
        
        if(!useMatError)
            probtemp = dynamic_cast<parent*>(allParents[s])->mendel((*allParents[m]),nul,Nl,Na,SizeAll,AllFreq);
        else
        { NMistype=0;
            probtemp = dynamic_cast<parent*>(allParents[s])->mendel((*allParents[m]),nul,Nl,Na,SizeAll,AllFreq,MatError,NMistype);
            //if(s<=5 && m<=5) cout<< NMistype << endl;
        }

//        cout << probtemp << " " << NMistype << " "; cout.flush();
        
        if (probtemp>0 && NMistype <= NMistypeMax)  // NEW EK -> Considering a Maximum Number of Mistypings
      {
        NbMeres[s]++;
          if (log10(probtemp)>best) {
              scnd=best; idscnd=idbest;
              best=log10(probtemp); idbest=m;
              nmisbest=NMistype;
              dbest=dynamic_cast<parent*>(allParents[s])->dist((*allParents[m]));
              azbest=dynamic_cast<parent*>(allParents[s])->azim((*allParents[m]));
          }
          else if (log10(probtemp)>scnd) {scnd=log10(probtemp); idscnd=m; }
        perepottemp.pere_pot=m;
        perepottemp.pere_prob=probtemp;
        ProbMeres[s].push_back(perepottemp);
          if(posterior==1) PosteriorMeres[s].push_back(0); // NEW EK -> Computing posterior probabilities of parents
      }
    }
      cout << "Seedling # " << s << " has " << NbMeres[s] << " compatible parents : ";
      for (int m=0; m < NbMeres[s]; m++) cout << dynamic_cast<parent*>(allParents[ProbMeres[s][m].pere_pot])->getName() << " | ";
      cout << endl;
      
     if (writeProbTrans>0) exportProbTrans << log10(ProbMig[s]) << "\t" << NbMeres[s] << "\t" << best << "\t" << scnd << "\t" << idbest << "\t" << idscnd << "\t";
       if (writeProbTrans>0) exportProbTrans << nmisbest << "\t" << dbest << "\t" << azbest << "\t";
      
      
      best=-100.0;
      scnd=-100.0;
      idbest=-1;idbest2=-1;
      idscnd=-1;idscnd2=-1;
      nmisbest=-1.;
      dbest=-1.;dbest2=-1.;dbest3=-1.;
      azbest=0.;azbest2=0.;azbest=0.;

    for (int m=0; m<NbMeres[s]; m++)
    {
      for (int p=0; p<NbMeres[s]; p++)
      {
        
          if(!useMatError)
              probtemp = dynamic_cast<parent*>(allParents[s])->mendel((*allParents[ProbMeres[s][m].pere_pot]),(*allParents[ProbMeres[s][p].pere_pot]),Nl,Na,SizeAll,AllFreq);
          else
              { NMistype=0;
                  probtemp = dynamic_cast<parent*>(allParents[s])->mendel((*allParents[ProbMeres[s][m].pere_pot]),(*allParents[ProbMeres[s][p].pere_pot]),Nl,Na,SizeAll,AllFreq,MatError,NMistype);
              //if(s<=5 && m<=5) cout<< NMistype << endl;
              }
          
        if (probtemp>0 && NMistype <= NMistypeMax)  // NEW EK -> Considering a Maximum Number of Mistypings
        {
          NbCouples[s]++;
            if (log10(probtemp)>best && !(ProbMeres[s][m].pere_pot==idbest2 && ProbMeres[s][p].pere_pot==idbest)) {
                scnd=best; idscnd=idbest; idscnd2=idbest2;
                best=log10(probtemp); idbest=ProbMeres[s][m].pere_pot; idbest2=ProbMeres[s][p].pere_pot;
                nmisbest=NMistype;
                dbest=dynamic_cast<parent*>(allParents[s])->dist((*allParents[ProbMeres[s][m].pere_pot]));
                dbest2=dynamic_cast<parent*>(allParents[s])->dist((*allParents[ProbMeres[s][p].pere_pot]));
                dbest3=dynamic_cast<parent*>(allParents[ProbMeres[s][m].pere_pot])->dist((*allParents[ProbMeres[s][p].pere_pot]));
                azbest=dynamic_cast<parent*>(allParents[s])->azim((*allParents[ProbMeres[s][m].pere_pot]));
                azbest2=dynamic_cast<parent*>(allParents[s])->azim((*allParents[ProbMeres[s][p].pere_pot]));
                azbest3=dynamic_cast<parent*>(allParents[ProbMeres[s][m].pere_pot])->azim((*allParents[ProbMeres[s][p].pere_pot]));
            }
            else if (log10(probtemp)>scnd) {scnd=log10(probtemp); idscnd=ProbMeres[s][m].pere_pot; idscnd2=ProbMeres[s][p].pere_pot;}
          couplepottemp.mere_pot=ProbMeres[s][m].pere_pot;
          couplepottemp.pere_pot=ProbMeres[s][p].pere_pot;
          couplepottemp.couple_prob=probtemp;
          ProbCouples[s].push_back(couplepottemp);
          if(posterior==1) PosteriorCouples[s].push_back(0); // NEW EK -> Computing posterior probabilities of parents

        }
      }
    }
      
      if (writeProbTrans>0) exportProbTrans << NbCouples[s] << "\t" << best << "\t" << scnd << "\t" << idbest << "\t" << idbest2 << "\t" << idscnd << "\t" << idscnd2 << "\t";
      if (writeProbTrans>0) exportProbTrans << nmisbest << "\t" << dbest << "\t" << dbest2 << "\t" << dbest3 << "\t" << azbest << "\t" << azbest2 << "\t" << azbest3 << "\t" << endl;

      cout << "Tree # " << s << " has " << NbCouples[s] << " compatible couples : ";
      for (int m=0; m < NbCouples[s]; m++) cout << dynamic_cast<parent*>(allParents[ProbCouples[s][m].mere_pot])->getName() << " x " << dynamic_cast<parent*>(allParents[ProbCouples[s][m].pere_pot])->getName() << " | ";
      cout << endl << "--------------------" << endl;

  }
    
    if (writeProbTrans>0) exportProbTrans << endl;
    //cout << PosteriorMeres[1][1] << " " << PosteriorMeres[5][NPar] << endl;
  cout << endl << "Computation of the mendelian likelihoods : OK. " <<endl << endl;

}

void MEMM_coloniz::loadParameters(Configuration *p)
{
  char * temp;
  MEMM::loadParameters(p);

    Parameter * param_temp;

    cout<< "Load more Parameters..."<<endl;

    // gamas
    param_temp = allParameters["gamas"];
    if(param_temp)
    {
      GamAs = param_temp->INIT;
      mGamAs = param_temp->MIN;
      MGamAs = param_temp->MAX;
      cout << "GamAs : "<< GamAs << " [" <<mGamAs <<"|"<< MGamAs << "]"<<endl;
    }

    // Scales
    param_temp = allParameters["scales"];
    if(param_temp)
    {
      Scales = param_temp->INIT;
      mScales = param_temp->MIN;
      MScales = param_temp->MAX;
      cout << "Scales : "<< Scales << " [" <<mScales <<"|"<< MScales << "]"<<endl;
    }
     
    // Shapes
    param_temp = allParameters["shapes"];
    if(param_temp)
    {
      Shapes = param_temp->INIT;
      mShapes = param_temp->MIN;
      MShapes = param_temp->MAX;
      cout << "Shapes : "<< Shapes << " [" <<mShapes <<"|"<< MShapes << "]"<<endl;
    }

    // Thetas
    param_temp = allParameters["thetas"];
    if(param_temp)
    {
      Thetas = param_temp->INIT;
      mThetas = param_temp->MIN;
      MThetas = param_temp->MAX;
      cout << "Thetas : "<< Thetas << " [" <<mThetas <<"|"<< MThetas << "]"<<endl;
    }

    // Kappas
    param_temp = allParameters["kappas"];
    if(param_temp)
    {
      Kappas = param_temp->INIT;
      mKappas = param_temp->MIN;
      MKappas = param_temp->MAX;
      cout << "Kappas : "<< Kappas << " [" <<mKappas <<"|"<< MKappas << "]"<<endl;
    }

    // Mig0S
    param_temp = allParameters["mig0s"];
    if(param_temp)
    {
      Mig0S = param_temp->INIT;
      mMig0S = param_temp->MIN;
      MMig0S = param_temp->MAX;
      cout << "Mig0S : "<< Mig0S << " [" <<mMig0S <<"|"<< MMig0S << "]"<<endl;
    }

    // bMigS
    param_temp = allParameters["bmigs"];
    if(param_temp)
    {
      bMigS = param_temp->INIT;
      mbMigS = param_temp->MIN;
      MbMigS = param_temp->MAX;
      cout << "bMigS : "<< bMigS << " [" <<mbMigS <<"|"<< MbMigS << "]"<<endl;
    }

    // AgeThS
    param_temp = allParameters["ageths"];
    if(param_temp)
    {
      AgeThS = param_temp->INIT;
      mAgeThS = param_temp->MIN;
      MAgeThS = param_temp->MAX;
      cout << "AgeThS : "<< AgeThS << " [" <<mAgeThS <<"|"<< MAgeThS << "]"<<endl;
    }
    
    // bFecS
    param_temp = allParameters["bfecs"];
    if(param_temp)
    {
      bFecS = param_temp->INIT;
      mbFecS = param_temp->MIN;
      MbFecS = param_temp->MAX;
      cout << "bFecS : "<< bFecS << " [" <<mbFecS <<"|"<< MbFecS << "]"<<endl;
    }
    

    as=Scales*exp(gammln(2/Shapes)-gammln(3/Shapes));
    cout <<"Deltas : "<<as<<endl;
    abs=-pow(as,-Shapes);
//    ap=Scale*exp(gammln(2/Shape)-gammln(3/Shape));
//    cout <<"Deltap : "<<ap<<endl;
//    abp=-pow(ap,-Shape);

    Vec_Fecs.resize(NPar, 1.0 );
    Vec_Fecp.resize(NPar, 1.0 );

    pLois = loadDistribution(p, "individual_fecundities_seeds");

    cout<<"end loading Parameters"<<endl;

}

void MEMM_coloniz::calculWeightPollenDonor(Configuration * p)
{
    Age.resize( NPar, 1.0 );
    
    cout <<endl << "Loading the ages of all trees..."<<endl;
    cout.flush();
    for (int p=0; p<NPar; p++)
    {
        Age[p]=((*allParents[p]).getCoVarQuanti(0));
        cout << Age[p] << " | ";
    }
    cout<<endl;
}

// NEW EK -> Computing posterior probabilities of parents

void MEMM_coloniz::exportPosteriors(Configuration * p)
{
    double tottemp;
    double probtemp;   // EK -> Inclus Mistyping
    string name;
    char * temp;
    int NMistype=0;    // EK -> Inclus Mistyping
    individu nul("nul", genotype(Nl));
    
    if(p == NULL)return;
    
    cout<<endl<<"# Exporting the posterior probabilities ... "<<endl;
    cout<<endl<<"Based on ... "<< Nposterior << "  sampled MCMC steps" << endl;
    
    temp = p->getValue(MEMM_POSTERIOR_FILE_NAME);
    ofstream fposterior(temp,std::ofstream::out);

    if (posterior==2)
    {   fposterior << "Seed#  SeedID  ";
        for (int m=0; m<NPar; m++) fposterior << dynamic_cast<parent*>(allParents[m])->getName() << " ";
        fposterior << "OUT" << endl;
    }
    
    for (int s=0; s<Ns && fposterior.good(); s++)
    {
        
        if (posterior==1) {
        
        tottemp=1;
        //fposterior << "-----------------------------------" << endl;
        for (int m=0; m<NbMeres[s]; m++) {
            if(!useMatError)
                probtemp = dynamic_cast<parent*>(allParents[s])->mendel((*allParents[ProbMeres[s][m].pere_pot]),nul,Nl,Na,SizeAll,AllFreq);
            else
            { NMistype=0;
                probtemp = dynamic_cast<parent*>(allParents[s])->mendel((*allParents[ProbMeres[s][m].pere_pot]),nul,Nl,Na,SizeAll,AllFreq,MatError,NMistype);
            }
            fposterior << s << " " << dynamic_cast<parent*>(allParents[s])->getName() << " " << dynamic_cast<parent*>(allParents[ProbMeres[s][m].pere_pot])->getName() << " OUT ";
            fposterior << ProbMeres[s][m].pere_prob << " " << NMistype << " " << PosteriorMeres[s][m]/Nposterior << endl;  // EK -> Inclus Mistyping
            tottemp -= PosteriorMeres[s][m]/Nposterior;
        }
            
        for (int m=0; m<NbCouples[s]; m++) {
            if(!useMatError)
                probtemp = dynamic_cast<parent*>(allParents[s])->mendel((*allParents[ProbCouples[s][m].mere_pot]),(*allParents[ProbCouples[s][m].pere_pot]),Nl,Na,SizeAll,AllFreq);
            else
            { NMistype=0;
                probtemp = dynamic_cast<parent*>(allParents[s])->mendel((*allParents[ProbCouples[s][m].mere_pot]),(*allParents[ProbCouples[s][m].pere_pot]),Nl,Na,SizeAll,AllFreq,MatError,NMistype);
            }
            fposterior << s << " " << dynamic_cast<parent*>(allParents[s])->getName() << " " << dynamic_cast<parent*>(allParents[ProbCouples[s][m].mere_pot])->getName() << " ";
            fposterior << dynamic_cast<parent*>(allParents[ProbCouples[s][m].pere_pot])->getName() << " " << ProbCouples[s][m].couple_prob << " " << NMistype << " "  <<  PosteriorCouples[s][m]/Nposterior << endl;  // EK -> Inclus Mistyping
            tottemp -= PosteriorCouples[s][m]/Nposterior;
        }
        fposterior << s << " " << dynamic_cast<parent*>(allParents[s])->getName() << " OUT OUT " << ProbMig[s] << " 0 " << tottemp << endl; // EK -> Inclus Mistyping
            
        }
        
        else if (posterior==2)
            
        {
            fposterior << s << " " << dynamic_cast<parent*>(allParents[s])->getName() << " ";
            for (int m=0; m<NPar+1; m++) fposterior << PosteriorMeres[s][m]/Nposterior << " ";
            fposterior << endl;
            
        }
    }
    
    fposterior.close();
    if(temp) delete []temp;
}


