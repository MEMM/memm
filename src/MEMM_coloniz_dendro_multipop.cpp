#include "MEMM_coloniz_dendro_multipop.hpp"

MEMM_coloniz_dendro_multipop::MEMM_coloniz_dendro_multipop()
{
    cout<< "MEMM : Coloniz_dendro_multipop mode"<<endl;
}

MEMM_coloniz_dendro_multipop::~MEMM_coloniz_dendro_multipop()
{
    
    //    Destruction des différents objets coloniz_dendro qui ont été créés
    
}

void MEMM_coloniz_dendro_multipop::mcmc(int nbIteration, int thinStep)
{
//    cout << " mcmc starts ... "; cout.flush();
    
    ofstream cSuivi ("suiviPasAPas.txt");
    int suivi=1;
    
    double lastFecLogLiks, nextFecLogLiks;
    double lastFecLogLikp, nextFecLogLikp;
    double previousValue, nextValue;
    double previousValue2, nextvalue2;
    long double previousLogLik, nextLogLik;
    double astemp, abstemp, Kabstemp;
    
    double Tune=0.2;
    double fineTune=0.05;
    
    bool gamasFixed;
    bool scalesFixed, shapesFixed;
    bool kappasFixed, thetasFixed;
    bool mig0sFixed, bmigsFixed;
    bool agethsFixed, bfecsFixed;
    bool gamaFixed;
    //    bool scaleFixed, shapeFixed;
    //    bool migFixed, selfFixed;
    bool mig0pFixed , bmigpFixed;
    bool agethpFixed , bfecpFixed;
        
    accept_ratio gamasRatio = allParameters["gamas"]->accept_ratio_;
    accept_ratio scalesRatio = allParameters["scales"]->accept_ratio_;
    accept_ratio shapesRatio = allParameters["shapes"]->accept_ratio_;
    accept_ratio kappasRatio = allParameters["kappas"]->accept_ratio_;
    accept_ratio thetasRatio = allParameters["thetas"]->accept_ratio_;
    // accept_ratio migsRatio = allParameters["migs"]->accept_ratio_;
    accept_ratio mig0sRatio = allParameters["mig0s"]->accept_ratio_;
    accept_ratio bmigsRatio = allParameters["bmigs"]->accept_ratio_;
    accept_ratio agethsRatio = allParameters["ageths"]->accept_ratio_;
    accept_ratio bfecsRatio = allParameters["bfecs"]->accept_ratio_;
    accept_ratio gamaRatio = allParameters["gama"]->accept_ratio_;
    //    accept_ratio scaleRatio = allParameters["scale"]->accept_ratio_;
    //    accept_ratio shapeRatio = allParameters["shape"]->accept_ratio_;
    //    accept_ratio migRatio = allParameters["mig"]->accept_ratio_;
    //    accept_ratio selfRatio = allParameters["self"]->accept_ratio_;
    accept_ratio mig0pRatio = allParameters["mig0p"]->accept_ratio_;
    accept_ratio bmigpRatio = allParameters["bmigp"]->accept_ratio_;
    accept_ratio agethpRatio = allParameters["agethp"]->accept_ratio_;
    accept_ratio bfecpRatio = allParameters["bfecp"]->accept_ratio_;
    
    gamasFixed = ( gamasRatio(1.0,1.0) == 0 );
    scalesFixed = ( scalesRatio(1.0,1.0) == 0 );
    shapesFixed = ( shapesRatio(1.0,1.0) == 0 );
    kappasFixed = ( kappasRatio(1.0,1.0) == 0 );
    thetasFixed = ( thetasRatio(1.0,1.0) == 0 );
    mig0sFixed = ( mig0sRatio(1.0,1.0) == 0 );
    bmigsFixed = ( bmigsRatio(1.0,1.0) == 0 );
    agethsFixed = ( agethsRatio(1.0,1.0) == 0 );
    bfecsFixed = ( bfecsRatio(1.0,1.0) == 0 );
    gamaFixed = ( gamaRatio(1.0,1.0) == 0 );
    //    scaleFixed = ( scaleRatio(1.0,1.0) == 0 );
    //    shapeFixed = ( shapeRatio(1.0,1.0) == 0 );
    //    selfFixed = ( selfRatio(1.0,1.0) == 0 );
    //    migFixed = ( migRatio(1.0,1.0) == 0 );
    mig0pFixed = ( mig0pRatio(1.0,1.0) == 0 );
    bmigpFixed = ( bmigpRatio(1.0,1.0) == 0 );
    agethpFixed = ( agethpRatio(1.0,1.0) == 0 );
    bfecpFixed = ( bfecpRatio(1.0,1.0) == 0 );

//    cout << " prior ratios ok ... ";cout.flush();

    bool gamasLevel = allParameters["gamas"]->level_;
    bool scalesLevel = allParameters["scales"]->level_;
    bool shapesLevel = allParameters["shapes"]->level_;
    bool kappasLevel = allParameters["kappas"]->level_;
    bool thetasLevel = allParameters["thetas"]->level_;
    // bool migsLevel = allParameters["migs"]->level_;
    bool mig0sLevel = allParameters["mig0s"]->level_;
    bool bmigsLevel = allParameters["bmigs"]->level_;
    bool agethsLevel = allParameters["ageths"]->level_;
    bool bfecsLevel = allParameters["bfecs"]->level_;
    bool gamaLevel = allParameters["gama"]->level_;
    //    bool scaleLevel = allParameters["scale"]->level_;
    //    bool shapeLevel = allParameters["shape"]->level_;
    //    bool migLevel = allParameters["mig"]->level_;
    //    bool selfLevel = allParameters["self"]->level_;
    bool mig0pLevel = allParameters["mig0p"]->level_;
    bool bmigpLevel = allParameters["bmigp"]->level_;
    bool agethpLevel = allParameters["agethp"]->level_;
    bool bfecpLevel = allParameters["bfecp"]->level_;
    
    int xpourCent = nbIteration/20;
    if(!xpourCent) xpourCent=1;
 
//    cout << " param levels ok ... posterior = " << posterior << " ... " ;cout.flush();

    previousLogLik = logLik(posterior);
    cout <<previousLogLik << endl;
    cout.flush();
    
    //output start value
    if(thinStep){
        
        previousLogLik = logLik(posterior);  // EK -> Computing posterior probabilities of parents
        cout <<previousLogLik << " "<<endl; cout.flush();
        
        *ParamFec << "iteration";
        if (!gamasFixed)
        {
            if (!gamasLevel) *ParamFec << " Var(FecS)0";
            else for (int pop=0; pop<Npop; pop++) *ParamFec << " Var(FecS)_" << (pop+1);
        }
        if (!gamaFixed)
        {
            if (!gamaLevel) *ParamFec << " Var(FecP)0";
            else for (int pop=0; pop<Npop; pop++) *ParamFec << " Var(FecP)_" << (pop+1);
        }
        *ParamFec << endl;
        
        *IndivFec << "iteration" ;
        for (int pop=0; pop<Npop; pop++)
        {
            if( (!gamasFixed) || (*GamAs[0] != 0))
                for(int p=0 ; p<MEMM_pops[pop].NPar ; p++)
                    *IndivFec << " Fs_" << pop << "_" << p;
            if( (!gamaFixed) || (*GamA[0] != 0))
                for(int p=0 ; p<MEMM_pops[pop].NPar ; p++)
                    *IndivFec << " Fp_" << pop << "_" << p;
        }
        *IndivFec << endl;
        
        *ParamDisp << "iteration";
        if (!scalesFixed){
            if (!scalesLevel) *ParamDisp << " Scales0";
            else for (int pop=0; pop<Npop; pop++) *ParamDisp << " Scales_" << (pop+1);
        }
        if (!shapesFixed){
            if (!shapesLevel) *ParamDisp << " Shapes0";
            else for (int pop=0; pop<Npop; pop++) *ParamDisp << " Shapes_" << (pop+1);
        }
        if (!kappasFixed){
            if (!kappasLevel) *ParamDisp << " Kappas0";
            else for (int pop=0; pop<Npop; pop++) *ParamDisp << " Kappas_" << (pop+1);
        }
        if (!thetasFixed){
            if (!thetasLevel) *ParamDisp << " Thetas0";
            else for (int pop=0; pop<Npop; pop++) *ParamDisp << " Thetas_" << (pop+1);
        }
        if (!mig0sFixed){
            if (!mig0sLevel) *ParamDisp << " Mig0s0";
            else for (int pop=0; pop<Npop; pop++) *ParamDisp << " Mig0s_" << (pop+1);
        }
        if (!bmigsFixed){
            if (!bmigsLevel) *ParamDisp << " bMigS0";
            else for (int pop=0; pop<Npop; pop++) *ParamDisp << " bMigS_" << (pop+1);
        }
        if (!agethsFixed){
            if (!agethsLevel) *ParamDisp << " AgeThS0";
            else for (int pop=0; pop<Npop; pop++) *ParamDisp << " AgeThs_" << (pop+1);
        }
        if (!bfecsFixed){
            if (!bfecsLevel) *ParamDisp << " bFecS0";
            else for (int pop=0; pop<Npop; pop++) *ParamDisp << " bFecS_" << (pop+1);
        }
        if (!mig0pFixed)
        {
            if (!mig0pLevel) *ParamDisp << " Mig0P0";
            else for (int pop=0; pop<Npop; pop++) *ParamDisp << " Mig0P_" << (pop+1);
        }
        if (!bmigpFixed)
        {
            if (!bmigpLevel) *ParamDisp << " bMigP0";
            else for (int pop=0; pop<Npop; pop++) *ParamDisp << " bMigP_" << (pop+1);
        }
        if (!agethpFixed)
        {
            if (!agethpLevel) *ParamDisp << " AgeThP0";
            else for (int pop=0; pop<Npop; pop++) *ParamDisp << " AgeThP_" << (pop+1);
        }
        if (!bfecpFixed)
        {
            if (!bfecpLevel) *ParamDisp << " bFecP0";
            else for (int pop=0; pop<Npop; pop++) *ParamDisp << " bFecP_" << (pop+1);
        }
        *ParamDisp << " LogLik" <<endl;
        
        *ParamFec << "0";
        if (!gamasFixed)
        {
            if (!gamasLevel) *ParamFec << " " << *GamAs[0];
            else for (int pop=0; pop<Npop; pop++) *ParamFec << " " << *GamAs[pop];
        }
        if (!gamaFixed)
        {
            if (!gamaLevel) *ParamFec << " " << *GamA[0];
            else for (int pop=0; pop<Npop; pop++) *ParamFec << " " << *GamA[pop];
        }
        *ParamFec << endl;ParamFec->flush();
        
        
        *IndivFec << "0 " ;
        for (int pop=0; pop<Npop; pop++)
        {
            if( (!gamasFixed) || (*GamAs[0] != 0))
                for(int p=0 ; p<MEMM_pops[pop].NPar ; p++)
                    *IndivFec << " " << MEMM_pops[pop].Vec_Fecs[p] ;
            if( (!gamaFixed) || (*GamA[0] != 0))
                for(int p=0 ; p<MEMM_pops[pop].NPar ; p++)
                    *IndivFec << " " << MEMM_pops[pop].Vec_Fecp[p] ;
        }
        *IndivFec << endl;IndivFec->flush();
        
        *ParamDisp << "0";
        if (!scalesFixed) {
            if (!scalesLevel) *ParamDisp << " " << *Scales[0];
            else for (int pop=0; pop<Npop; pop++) *ParamDisp << " " << *Scales[pop];}
        if (!shapesFixed) {
            if (!shapesLevel) *ParamDisp << " " << *Shapes[0];
            else for (int pop=0; pop<Npop; pop++) *ParamDisp << " " << *Shapes[pop];}
        if (!kappasFixed) {
            if (!kappasLevel) *ParamDisp << " " << *Kappas[0];
            else for (int pop=0; pop<Npop; pop++) *ParamDisp << " " << *Kappas[pop];}
        if (!thetasFixed) {
            if (!thetasLevel) *ParamDisp << " " << *Thetas[0];
            else for (int pop=0; pop<Npop; pop++) *ParamDisp << " " << *Thetas[pop];}
        if (!mig0sFixed) {
            if (!mig0sLevel) *ParamDisp << " " << *Mig0S[0];
            else for (int pop=0; pop<Npop; pop++) *ParamDisp << " " << *Mig0S[pop];}
        if (!bmigsFixed) {
            if (!bmigsLevel) *ParamDisp << " " << *bMigS[0];
            else for (int pop=0; pop<Npop; pop++) *ParamDisp << " " << *bMigS[pop];}
        if (!agethsFixed) {
            if (!agethsLevel) *ParamDisp << " " << *AgeThS[0];
            else for (int pop=0; pop<Npop; pop++) *ParamDisp << " " << *AgeThS[pop];}
        if (!bfecsFixed) {
            if (!bfecsLevel) *ParamDisp << " " << *bFecS[0];
            else for (int pop=0; pop<Npop; pop++) *ParamDisp << " " << *bFecS[pop];}
        if (!mig0pFixed) {
            if (!mig0pLevel) *ParamDisp << " " << *Mig0P[0];
            else for (int pop=0; pop<Npop; pop++) *ParamDisp << " " << *Mig0P[pop];}
        if (!bmigpFixed){
            if (!bmigpLevel) *ParamDisp << " " << *bMigP[0];
            else for (int pop=0; pop<Npop; pop++) *ParamDisp << " " << *bMigP[pop];}
        if (!agethpFixed){
            if (!agethpLevel) *ParamDisp << " " << *AgeThP[0];
            else for (int pop=0; pop<Npop; pop++) *ParamDisp << " " << *AgeThP[pop];}
        if (!bfecpFixed){
            if (!bfecpLevel) *ParamDisp << " " << *bFecP[0];
            else for (int pop=0; pop<Npop; pop++) *ParamDisp << " " << *bFecP[pop];}
        *ParamDisp << " " << previousLogLik  << endl;
        ParamDisp->flush();
    }
    else {previousLogLik = logLik();       cout <<previousLogLik << " " << endl;   }  // EK -> Computing posterior probabilities of parents
    
    if(nbIteration) cout << "it=1..."<< std::endl;
    for(int ite=1; ite<=nbIteration; ite++)
    {
        if (ite % xpourCent == 0) {cout << "it="<<ite << "..."<< std::endl;}
        
        if(thinStep && (ite%thinStep == 0)) {
            logLik(posterior);
            //cout << PosteriorMeres[1][1] << " " << PosteriorMeres[5][NPar] << endl;
        }
        
        //GamAs- fecundities distribution
        //nextValue = pow(GamAs, exp(0.2*gauss()));
        if( !gamasFixed )
            if (gamasLevel) {
                for (int pop=0; pop<Npop; pop++) {
                    nextValue = *GamAs[pop] * exp(Tune*gauss());
                    if( nextValue>mGamAs && nextValue<MGamAs )
                    {
                        lastFecLogLiks = (MEMM_pops[pop].pLois)->logLik(MEMM_pops[pop].NPar, MEMM_pops[pop].Vec_Fecs, *GamAs[pop]);
                        nextFecLogLiks = (MEMM_pops[pop].pLois)->logLik(MEMM_pops[pop].NPar, MEMM_pops[pop].Vec_Fecs, nextValue);
                        if( accept() < exp(nextFecLogLiks-lastFecLogLiks) * gamasRatio(*GamAs[pop],nextValue) )
                        {
                            *GamAs[pop] = nextValue;
                            (MEMM_pops[pop].pLois)->setDParam(MEMM_LOI_GAMA, *GamAs[pop]);
                        }
                    }
                }
            }
            else
            {
                nextValue = *GamAs[0] * exp(Tune*gauss());
                if( nextValue>mGamAs && nextValue<MGamAs )
                {
                    lastFecLogLiks = 0;
                    for (int pop=0; pop<Npop; pop++) lastFecLogLiks += (MEMM_pops[pop].pLois)->logLik(MEMM_pops[pop].NPar, MEMM_pops[pop].Vec_Fecs, *GamAs[0]);
                    nextFecLogLiks = 0;
                    for (int pop=0; pop<Npop; pop++) nextFecLogLiks += (MEMM_pops[pop].pLois)->logLik(MEMM_pops[pop].NPar, MEMM_pops[pop].Vec_Fecs, nextValue);
                    if( accept() < exp(nextFecLogLiks-lastFecLogLiks) * gamasRatio(*GamAs[0],nextValue) )
                    {
                        for (int pop=0; pop<Npop; pop++) {
                            *GamAs[pop] = nextValue;
                            (MEMM_pops[pop].pLois)->setDParam(MEMM_LOI_GAMA, *GamAs[pop]);
                        }
                    }
                }
            }
        if(thinStep && (ite%thinStep == 0)){
            *ParamFec << ite;
            if (!gamasLevel) *ParamFec << " " << *GamAs[0];
            else for (int pop=0; pop<Npop; pop++) *ParamFec << " " << *GamAs[pop];
        }
        
        //GamA (GamAp)- fecundities distribution
        //nextValue = pow(GamA, exp(tune*gauss()));
        if( !gamaFixed )
            if (gamaLevel) {
                for (int pop=0; pop<Npop; pop++) {
                    nextValue = *GamA[pop] * exp(Tune*gauss());
                    if( nextValue>mGamA && nextValue<MGamA )
                    {
                        lastFecLogLikp = (MEMM_pops[pop].pLoi)->logLik(MEMM_pops[pop].NPar, MEMM_pops[pop].Vec_Fecp, *GamA[pop]);
                        nextFecLogLikp = (MEMM_pops[pop].pLoi)->logLik(MEMM_pops[pop].NPar, MEMM_pops[pop].Vec_Fecp, nextValue);
                        if( accept() < exp(nextFecLogLikp-lastFecLogLikp) * gamaRatio(*GamA[pop],nextValue) )
                        {
                            *GamA[pop] = nextValue;
                            (MEMM_pops[pop].pLoi)->setDParam(MEMM_LOI_GAMA, *GamA[pop]);
                        }
                    }
                }
            }
            else
            {
                nextValue = *GamA[0] * exp(Tune*gauss());
                if( nextValue>mGamA && nextValue<MGamA )
                {
                    lastFecLogLikp = 0;
                    for (int pop=0; pop<Npop; pop++) lastFecLogLikp += (MEMM_pops[pop].pLoi)->logLik(MEMM_pops[pop].NPar, MEMM_pops[pop].Vec_Fecp, *GamA[0]);
                    nextFecLogLikp = 0;
                    for (int pop=0; pop<Npop; pop++) nextFecLogLikp += (MEMM_pops[pop].pLoi)->logLik(MEMM_pops[pop].NPar, MEMM_pops[pop].Vec_Fecp, nextValue);
                    if( accept() < exp(nextFecLogLikp-lastFecLogLikp) * gamaRatio(*GamA[0],nextValue) )
                    {
                        for (int pop=0; pop<Npop; pop++) {
                            *GamA[pop] = nextValue;
                            (MEMM_pops[pop].pLoi)->setDParam(MEMM_LOI_GAMA, *GamA[pop]);
                        }
                    }
                }
            }
        if(thinStep && (ite%thinStep == 0)){
            if (!gamaLevel) *ParamFec << " " << *GamA[0];
            else for (int pop=0; pop<Npop; pop++) *ParamFec << " " << *GamA[pop];
            *ParamFec << endl;
            ParamFec->flush();
        }
        
        
        // Individual fecundities
        if( !gamasFixed || !gamaFixed || (*GamA[0] != 0) || (*GamAs[0] != 0))
        {
            for (int pop=0; pop<Npop; pop++) {
                for(int p=0 ; p<MEMM_pops[pop].NPar ; p++)
                {
                    previousValue = MEMM_pops[pop].Vec_Fecs[p];
                    previousValue2 = MEMM_pops[pop].Vec_Fecp[p];
                    (MEMM_pops[pop].pLois)->tirage(MEMM_pops[pop].Vec_Fecs[p]);
                    (MEMM_pops[pop].pLoi)->tirage(MEMM_pops[pop].Vec_Fecp[p]);
                    nextLogLik = logLik();
                    if(accept()<exp(nextLogLik-previousLogLik)) {
                        previousLogLik = nextLogLik;
                        if (suivi>1) cSuivi << "Accept" << endl;
                    }
                    else {
                        MEMM_pops[pop].Vec_Fecs[p]=previousValue;
                        MEMM_pops[pop].Vec_Fecp[p]=previousValue2;
                    }
                }
                
                if(thinStep && (ite%thinStep == 0))
                {
                    *IndivFec << ite << " " ;
                    if( (!gamasFixed) || (*GamAs[0] != 0)) for (int p=0; p<MEMM_pops[pop].NPar; p++) {*IndivFec << MEMM_pops[pop].Vec_Fecs[p] << " ";}
                    if( (!gamaFixed) || (*GamA[0] != 0)) for (int p=0; p<MEMM_pops[pop].NPar; p++) {*IndivFec << MEMM_pops[pop].Vec_Fecp[p] << " ";}
                    *IndivFec << endl;
                    IndivFec->flush();
                }
            }
        }
        
        // Scales - seed dispersal distance
        if( !scalesFixed )
            if (scalesLevel) {
                for (int pop=0; pop<Npop; pop++) {
                    previousLogLik = MEMM_pops[pop].logLik();
                    previousValue = *Scales[pop];
                    *Scales[pop] *= exp(Tune*gauss());
                    if( *Scales[pop]>mScales && *Scales[pop]<MScales )
                    {
                        astemp = *as[pop]; abstemp = *abs[pop]; Kabstemp = *Kabs[pop];
                        *as[pop] = *Scales[pop]*exp(gammln(2/(*Shapes[pop]))-gammln(3/(*Shapes[pop])));
                        *abs[pop]=-pow(*as[pop],-(*Shapes[pop]));
                        *Kabs[pop]=(*Shapes[pop])/2/3.14/(*as[pop])/(*as[pop])/exp(gammln(2/(*Shapes[pop])));
                        nextLogLik = MEMM_pops[pop].logLik();
                        if( accept() < exp(nextLogLik-previousLogLik) * scalesRatio(previousValue,*Scales[pop]) )
                        {
                        }
                        else
                        {
                            *Scales[pop] = previousValue;
                            *as[pop] = astemp;
                            *abs[pop]= abstemp;
                            *Kabs[pop]= Kabstemp;
                        }
                    }
                    else *Scales[pop] = previousValue;
                }
            }
            else
            {
                previousLogLik = logLik();
                previousValue = *Scales[0];
                *Scales[0] *= exp(Tune*gauss());
                if( *Scales[0]>mScales && *Scales[0]<MScales )
                {
                    astemp = *as[0]; abstemp = *abs[0]; Kabstemp = *Kabs[0];
                    *as[0] = *Scales[0]*exp(gammln(2/(*Shapes[0]))-gammln(3/(*Shapes[0])));
                    *abs[0]=-pow(*as[0],-(*Shapes[0]));
                    *Kabs[0]=(*Shapes[0])/2/3.14/(*as[0])/(*as[0])/exp(gammln(2/(*Shapes[0])));
                    for (int pop=1; pop<Npop; pop++) {
                        *Scales[pop] = *Scales[0];
                        *as[pop] = *as[0];
                        *abs[pop] = *abs[0];
                        *Kabs[pop] = *Kabs[0];
                    }
                    nextLogLik = logLik();
                    if( accept() < exp(nextLogLik-previousLogLik) * scalesRatio(previousValue,*Scales[0]) )
                    {
                    }
                    else {
                        for (int pop=0; pop<Npop; pop++) {
                            *Scales[pop] = previousValue;
                            *as[pop] = astemp;
                            *abs[pop] = abstemp;
                            *Kabs[pop] = Kabstemp;
                        }
                    }
                }
                else *Scales[0]=previousValue;
            }
        if(thinStep && (ite%thinStep == 0)){
            *ParamDisp << ite ;
            if (!scalesLevel) *ParamDisp << " " << *Scales[0];
            else for (int pop=0; pop<Npop; pop++) *ParamDisp << " " << *Scales[pop];
        }
        
        
        //change Shapes
        if( !shapesFixed )
            if (shapesLevel) {
                for (int pop=0; pop<Npop; pop++) {
                    previousLogLik = MEMM_pops[pop].logLik();
                    previousValue = *Shapes[pop];
                    *Shapes[pop] *= exp(Tune*gauss());
                    if( *Shapes[pop]>mShapes && *Shapes[pop]<MShapes )
                    {
                        astemp = *as[pop]; abstemp = *abs[pop]; Kabstemp = *Kabs[pop];
                        *as[pop] = *Scales[pop]*exp(gammln(2/(*Shapes[pop]))-gammln(3/(*Shapes[pop])));
                        *abs[pop]=-pow(*as[pop],-(*Shapes[pop]));
                        *Kabs[pop]=(*Shapes[pop])/2/3.14/(*as[pop])/(*as[pop])/exp(gammln(2/(*Shapes[pop])));
                        nextLogLik = MEMM_pops[pop].logLik();
                        if( accept() < exp(nextLogLik-previousLogLik) * shapesRatio(previousValue,*Shapes[pop]) )
                        {
                        }
                        else
                        {
                            *Shapes[pop] = previousValue;
                            *as[pop] = astemp;
                            *abs[pop]= abstemp;
                            *Kabs[pop]= Kabstemp;
                        }
                    }
                    else *Shapes[pop] = previousValue;
                }
            }
            else
            {
                previousLogLik = logLik();
                previousValue = *Shapes[0];
                *Shapes[0] *= exp(Tune*gauss());
                if( *Shapes[0]>mShapes && *Shapes[0]<MShapes )
                {
                    astemp = *as[0]; abstemp = *abs[0]; Kabstemp = *Kabs[0];
                    *as[0] = *Scales[0]*exp(gammln(2/(*Shapes[0]))-gammln(3/(*Shapes[0])));
                    *abs[0]=-pow(*as[0],-(*Shapes[0]));
                    *Kabs[0]=(*Shapes[0])/2/3.14/(*as[0])/(*as[0])/exp(gammln(2/(*Shapes[0])));
                    for (int pop=1; pop<Npop; pop++) {
                        *Shapes[pop] = *Shapes[0];
                        *as[pop] = *as[0];
                        *abs[pop] = *abs[0];
                        *Kabs[pop] = *Kabs[0];
                    }
                    nextLogLik = logLik();
                    if( accept() < exp(nextLogLik-previousLogLik) * shapesRatio(previousValue,*Shapes[0]) )
                    {
                    }
                    else {
                        for (int pop=0; pop<Npop; pop++) {
                            *Shapes[pop] = previousValue;
                            *as[pop] = astemp;
                            *abs[pop] = abstemp;
                            *Kabs[pop] = Kabstemp;
                        }
                    }
                }
                else *Shapes[0]=previousValue;
            }
        if(thinStep && (ite%thinStep == 0)){
            if (!shapesLevel) *ParamDisp << " " << *Shapes[0];
            else for (int pop=0; pop<Npop; pop++) *ParamDisp << " " << *Shapes[pop];
        }
        
        
        //change Kappas
        if( !kappasFixed )
            if (kappasLevel) {
                for (int pop=0; pop<Npop; pop++) {
                    previousLogLik = MEMM_pops[pop].logLik();
                    previousValue = *Kappas[pop];
                    *Kappas[pop] *= exp(Tune*gauss());
                    if( *Kappas[pop]>mKappas && *Kappas[pop]<MKappas )
                    {
                        nextLogLik = MEMM_pops[pop].logLik();
                        if( accept() < exp(nextLogLik-previousLogLik) * kappasRatio(previousValue,*Kappas[pop]) )
                        {
                        }
                        else
                        {
                            *Kappas[pop] = previousValue;
                        }
                    }
                    else *Kappas[pop] = previousValue;
                }
            }
            else
            {
                previousLogLik = logLik();
                previousValue = *Kappas[0];
                *Kappas[0] *= exp(Tune*gauss());
                if( *Kappas[0]>mKappas && *Kappas[0]<MKappas )
                {
                    for (int pop=1; pop<Npop; pop++) *Kappas[pop] = *Kappas[0];
                    nextLogLik = logLik();
                    if( accept() < exp(nextLogLik-previousLogLik) * kappasRatio(previousValue,*Kappas[0]) )
                    {
                    }
                    else {
                        *Kappas[0] = previousValue;
                        for (int pop=0; pop<Npop; pop++) *Kappas[pop] = *Kappas[0];
                    }
                }
                else *Kappas[0]=previousValue;
            }
        if(thinStep && (ite%thinStep == 0)){
            if (!kappasLevel) *ParamDisp << " " << *Kappas[0];
            else for (int pop=0; pop<Npop; pop++) *ParamDisp << " " << *Kappas[pop];
        }
        
        
        //change Thetas
        if( !thetasFixed )
            if (thetasLevel) {
                for (int pop=0; pop<Npop; pop++) {
                    previousLogLik = MEMM_pops[pop].logLik();
                    previousValue = *Thetas[pop];
                    *Thetas[pop] = fmod((previousValue + Tune*gauss()),3.14159);
                    if( *Thetas[pop]>mThetas && *Thetas[pop]<MThetas )
                    {
                        nextLogLik = MEMM_pops[pop].logLik();
                        if( accept() < exp(nextLogLik-previousLogLik) * thetasRatio(previousValue,*Thetas[pop]) )
                        {
                        }
                        else
                        {
                            *Thetas[pop] = previousValue;
                        }
                    }
                    else *Thetas[pop] = previousValue;
                }
            }
            else
            {
                previousLogLik = logLik();
                previousValue = *Thetas[0];
                *Thetas[0] = fmod((previousValue + Tune*gauss()),3.14159);
                if( *Thetas[0]>mThetas && *Thetas[0]<MThetas )
                {
                    for (int pop=1; pop<Npop; pop++) *Thetas[pop] = *Thetas[0];
                    nextLogLik = logLik();
                    if( accept() < exp(nextLogLik-previousLogLik) * thetasRatio(previousValue,*Thetas[0]) )
                    {
                    }
                    else {
                        *Thetas[0] = previousValue;
                        for (int pop=0; pop<Npop; pop++) *Thetas[pop] = *Thetas[0];
                    }
                }
                else *Thetas[0]=previousValue;
            }
        if(thinStep && (ite%thinStep == 0)){
            if (!thetasLevel) *ParamDisp << " " << *Thetas[0];
            else for (int pop=0; pop<Npop; pop++) *ParamDisp << " " << *Thetas[pop];
        }
        
        
        //change Mig0S
        if ( !mig0sFixed )
            if (mig0sLevel) {
                for (int pop=0; pop<Npop; pop++) {
                    previousLogLik = MEMM_pops[pop].logLik();
                    previousValue = *Mig0S[pop];
                    *Mig0S[pop] = previousValue + fineTune*gauss();
                    if( *Mig0S[pop]>mMig0S && *Mig0S[pop]<MMig0S )
                    {
                        nextLogLik = MEMM_pops[pop].logLik();
                        if( accept() < exp(nextLogLik-previousLogLik) * mig0sRatio(previousValue,*Mig0S[pop]) )
                        {
                        }
                        else
                        {
                            *Mig0S[pop] = previousValue;
                        }
                    }
                    else *Mig0S[pop] = previousValue;
                }
            }
            else
            {
                previousLogLik = logLik();
                previousValue = *Mig0S[0];
                *Mig0S[0] = previousValue + fineTune*gauss();
                if( *Mig0S[0]>mMig0S && *Mig0S[0]<MMig0S )
                {
                    for (int pop=1; pop<Npop; pop++) *Mig0S[pop] = *Mig0S[0];
                    nextLogLik = logLik();
                    if( accept() < exp(nextLogLik-previousLogLik) * mig0sRatio(previousValue,*Mig0S[0]) )
                    {
                    }
                    else {
                        *Mig0S[0] = previousValue;
                        for (int pop=0; pop<Npop; pop++) *Mig0S[pop] = *Mig0S[0];
                    }
                }
                else *Mig0S[0]=previousValue;
            }
        if(thinStep && (ite%thinStep == 0)){
            if (!mig0sLevel) *ParamDisp << " " << *Mig0S[0];
            else for (int pop=0; pop<Npop; pop++) *ParamDisp << " " << *Mig0S[pop];
        }
        
        //change bMigs
        if ( !bmigsFixed )
            if (bmigsLevel) {
                for (int pop=0; pop<Npop; pop++) {
                    previousLogLik = MEMM_pops[pop].logLik();
                    previousValue = *bMigS[pop];
                    *bMigS[pop] = previousValue + fineTune*gauss();
                    if( *bMigS[pop]>mbMigS && *bMigS[pop]<MbMigS )
                    {
                        nextLogLik = MEMM_pops[pop].logLik();
                        if( accept() < exp(nextLogLik-previousLogLik) * mig0sRatio(previousValue,*bMigS[pop]) )
                        {
                        }
                        else
                        {
                            *bMigS[pop] = previousValue;
                        }
                    }
                    else *bMigS[pop] = previousValue;
                }
            }
            else
            {
                previousLogLik = logLik();
                previousValue = *bMigS[0];
                *bMigS[0] = previousValue + fineTune*gauss();
                if( *bMigS[0]>mbMigS && *bMigS[0]<MbMigS )
                {
                    for (int pop=1; pop<Npop; pop++) *bMigS[pop] = *bMigS[0];
                    nextLogLik = logLik();
                    if( accept() < exp(nextLogLik-previousLogLik) * bmigsRatio(previousValue,*bMigS[0]) )
                    {
                    }
                    else {
                        *bMigS[0] = previousValue;
                        for (int pop=0; pop<Npop; pop++) *bMigS[pop] = *bMigS[0];
                    }
                }
                else *bMigS[0]=previousValue;
            }
        if(thinStep && (ite%thinStep == 0)){
            if (!bmigsLevel) *ParamDisp << " " << *bMigS[0];
            else for (int pop=0; pop<Npop; pop++) *ParamDisp << " " << *bMigS[pop];
        }
        
        
        //change Age Threshold AgeThS //For COLONIZ_DENDRO, Age Threshold is actually a DBH_Threshold
        if ( !agethsFixed)
            if (agethsLevel) {
                for (int pop=0; pop<Npop; pop++) {
                    previousLogLik = MEMM_pops[pop].logLik();
                    previousValue = *AgeThS[pop];
                    *AgeThS[pop] = previousValue * exp(Tune*gauss());
                    if( *AgeThS[pop]>mAgeThS && *AgeThS[pop]<MAgeThS )
                    {
                        nextLogLik = MEMM_pops[pop].logLik();
                        if( accept() < exp(nextLogLik-previousLogLik) * agethsRatio(previousValue,*AgeThS[pop]) )
                        {
                        }
                        else
                        {
                            *AgeThS[pop] = previousValue;
                        }
                    }
                    else *AgeThS[pop] = previousValue;
                }
            }
            else
            {
                previousLogLik = logLik();
                previousValue = *AgeThS[0];
                *AgeThS[0] = previousValue * exp(Tune*gauss());
                if( *AgeThS[0]>mAgeThS && *AgeThS[0]<MAgeThS )
                {
                    for (int pop=1; pop<Npop; pop++) *AgeThS[pop] = *AgeThS[0];
                    nextLogLik = logLik();
                    if( accept() < exp(nextLogLik-previousLogLik) * agethsRatio(previousValue,*AgeThS[0]) )
                    {
                    }
                    else {
                        *AgeThS[0] = previousValue;
                        for (int pop=0; pop<Npop; pop++) *AgeThS[pop] = *AgeThS[0];
                    }
                }
                else *AgeThS[0]=previousValue;
            }
        if(thinStep && (ite%thinStep == 0)){
            if (!agethsLevel) *ParamDisp << " " << *AgeThS[0];
            else for (int pop=0; pop<Npop; pop++) *ParamDisp << " " << *AgeThS[pop];
        }
        
        //change Slope bFecS //For COLONIZ_DENDRO, bFec is actually a power over DBH
        if ( !bfecsFixed )
            if (bfecsLevel) {
                for (int pop=0; pop<Npop; pop++) {
                    previousLogLik = MEMM_pops[pop].logLik();
                    previousValue = *bFecS[pop];
                    *bFecS[pop] = previousValue * exp(Tune*gauss());
                    if( *bFecS[pop]>mbFecS && *bFecS[pop]<MbFecS )
                    {
                        nextLogLik = MEMM_pops[pop].logLik();
                        if( accept() < exp(nextLogLik-previousLogLik) * bfecsRatio(previousValue,*bFecS[pop]) )
                        {
                        }
                        else
                        {
                            *bFecS[pop] = previousValue;
                        }
                    }
                    else *bFecS[pop] = previousValue;
                }
            }
            else
            {
                previousLogLik = logLik();
                previousValue = *bFecS[0];
                *bFecS[0] = previousValue * exp(Tune*gauss());
                if( *bFecS[0]>mbFecS && *bFecS[0]<MbFecS )
                {
                    for (int pop=1; pop<Npop; pop++) *bFecS[pop] = *bFecS[0];
                    nextLogLik = logLik();
                    if( accept() < exp(nextLogLik-previousLogLik) * bfecsRatio(previousValue,*bFecS[0]) )
                    {
                    }
                    else {
                        *bFecS[0] = previousValue;
                        for (int pop=0; pop<Npop; pop++) *bFecS[pop] = *bFecS[0];
                    }
                }
                else *bFecS[0]=previousValue;
            }
        if(thinStep && (ite%thinStep == 0)){
            if (!bfecsLevel) *ParamDisp << " " << *bFecS[0];
            else for (int pop=0; pop<Npop; pop++) *ParamDisp << " " << *bFecS[pop];
        }
        
        
        //change Mig0P
        if ( !mig0pFixed )
            if (mig0pLevel) {
                for (int pop=0; pop<Npop; pop++) {
                    previousLogLik = MEMM_pops[pop].logLik();
                    previousValue = *Mig0P[pop];
                    *Mig0P[pop] = previousValue + fineTune*gauss();
                    if( *Mig0P[pop]>mMig0P && *Mig0P[pop]<MMig0P )
                    {
                        nextLogLik = MEMM_pops[pop].logLik();
                        if( accept() < exp(nextLogLik-previousLogLik) * mig0pRatio(previousValue,*Mig0P[pop]) )
                        {
                        }
                        else
                        {
                            *Mig0P[pop] = previousValue;
                        }
                    }
                    else *Mig0P[pop] = previousValue;
                }
            }
            else
            {
                previousLogLik = logLik();
                previousValue = *Mig0P[0];
                *Mig0P[0] = previousValue + fineTune*gauss();
                if( *Mig0P[0]>mMig0P && *Mig0P[0]<MMig0P )
                {
                    for (int pop=1; pop<Npop; pop++) *Mig0P[pop] = *Mig0P[0];
                    nextLogLik = logLik();
                    if( accept() < exp(nextLogLik-previousLogLik) * mig0pRatio(previousValue,*Mig0P[0]) )
                    {
                    }
                    else {
                        for (int pop=0; pop<Npop; pop++) *Mig0P[pop] = previousValue;
                    }
                }
                else *Mig0P[0]=previousValue;
            }
        if(thinStep && (ite%thinStep == 0)){
            if (!mig0pLevel) *ParamDisp << " " << *Mig0P[0];
            else for (int pop=0; pop<Npop; pop++) *ParamDisp << " " << *Mig0P[pop];
        }
        
        //change bMigp
        if ( !bmigpFixed )
            if (bmigpLevel) {
                for (int pop=0; pop<Npop; pop++) {
                    previousLogLik = MEMM_pops[pop].logLik();
                    previousValue = *bMigP[pop];
                    *bMigP[pop] = previousValue + fineTune*gauss();
                    if( *bMigP[pop]>mbMigP && *bMigP[pop]<MbMigP )
                    {
                        nextLogLik = MEMM_pops[pop].logLik();
                        if( accept() < exp(nextLogLik-previousLogLik) * mig0pRatio(previousValue,*bMigP[pop]) )
                        {
                        }
                        else
                        {
                            *bMigP[pop] = previousValue;
                        }
                    }
                    else *bMigP[pop] = previousValue;
                }
            }
            else
            {
                previousLogLik = logLik();
                previousValue = *bMigP[0];
                *bMigP[0] = previousValue + fineTune*gauss();
                if( *bMigP[0]>mbMigP && *bMigP[0]<MbMigP )
                {
                    for (int pop=1; pop<Npop; pop++) *bMigP[pop] = *bMigP[0];
                    nextLogLik = logLik();
                    if( accept() < exp(nextLogLik-previousLogLik) * bmigpRatio(previousValue,*bMigP[0]) )
                    {
                    }
                    else {
                        *bMigP[0] = previousValue;
                        for (int pop=0; pop<Npop; pop++) *bMigP[pop] = *bMigP[0];
                    }
                }
                else *bMigP[0]=previousValue;
            }
        if(thinStep && (ite%thinStep == 0)){
            if (!bmigpLevel) *ParamDisp << " " << *bMigP[0];
            else for (int pop=0; pop<Npop; pop++) *ParamDisp << " " << *bMigP[pop];
        }
        
        //change Age Threshold AgeThP //For COLONIZ_DENDRO, Age Threshold is actually a DBH_Threshold
        if ( !agethpFixed )
            if (agethpLevel) {
                for (int pop=0; pop<Npop; pop++) {
                    previousLogLik = MEMM_pops[pop].logLik();
                    previousValue = *AgeThP[pop];
                    *AgeThP[pop] = previousValue * exp(Tune*gauss());
                    if( *AgeThP[pop]>mAgeThP && *AgeThP[pop]<MAgeThP )
                    {
                        nextLogLik = MEMM_pops[pop].logLik();
                        if( accept() < exp(nextLogLik-previousLogLik) * agethpRatio(previousValue,*AgeThP[pop]) )
                        {
                        }
                        else
                        {
                            *AgeThP[pop] = previousValue;
                        }
                    }
                    else *AgeThP[pop] = previousValue;
                }
            }
            else
            {
                previousLogLik = logLik();
                previousValue = *AgeThP[0];
                *AgeThP[0] = previousValue * exp(Tune*gauss());
                if( *AgeThP[0]>mAgeThP && *AgeThP[0]<MAgeThP )
                {
                    for (int pop=1; pop<Npop; pop++) *AgeThP[pop] = *AgeThP[0];
                    nextLogLik = logLik();
                    if( accept() < exp(nextLogLik-previousLogLik) * agethpRatio(previousValue,*AgeThP[0]) )
                    {
                    }
                    else {
                        *AgeThP[0] = previousValue;
                        for (int pop=0; pop<Npop; pop++) *AgeThP[pop] = *AgeThP[0];
                    }
                }
                else *AgeThP[0]=previousValue;
            }
        if(thinStep && (ite%thinStep == 0)){
            if (!agethpLevel) *ParamDisp << " " << *AgeThP[0];
            else for (int pop=0; pop<Npop; pop++) *ParamDisp << " " << *AgeThP[pop];
        }
        
        //change Slope bFecS //For COLONIZ_DENDRO, bFec is actually a power over DBH
        if ( !bfecpFixed )
            if (bfecpLevel) {
                for (int pop=0; pop<Npop; pop++) {
                    previousLogLik = MEMM_pops[pop].logLik();
                    previousValue = *bFecP[pop];
                    *bFecP[pop] = previousValue * exp(Tune*gauss());
                    if( *bFecP[pop]>mbFecP && *bFecP[pop]<MbFecP )
                    {
                        nextLogLik = MEMM_pops[pop].logLik();
                        if( accept() < exp(nextLogLik-previousLogLik) * bfecpRatio(previousValue,*bFecP[pop]) )
                        {
                        }
                        else
                        {
                            *bFecP[pop] = previousValue;
                        }
                    }
                    else *bFecP[pop] = previousValue;
                }
            }
            else
            {
                previousLogLik = logLik();
                previousValue = *bFecP[0];
                *bFecP[0] = previousValue * exp(Tune*gauss());
                if( *bFecP[0]>mbFecP && *bFecP[0]<MbFecP )
                {
                    for (int pop=1; pop<Npop; pop++) *bFecP[pop] = *bFecP[0];
                    nextLogLik = logLik();
                    if( accept() < exp(nextLogLik-previousLogLik) * bfecpRatio(previousValue,*bFecP[0]) )
                    {
                    }
                    else {
                        *bFecP[0] = previousValue;
                        for (int pop=0; pop<Npop; pop++) *bFecP[pop] = *bFecP[0];
                    }
                }
                else *bFecP[0]=previousValue;
            }
        if(thinStep && (ite%thinStep == 0)){
            if (!bfecpLevel) *ParamDisp << " " << *bFecP[0];
            else for (int pop=0; pop<Npop; pop++) *ParamDisp << " " << *bFecP[pop];
            *ParamDisp << " " << logLik() << endl;
            ParamDisp->flush();
        }
        
    }
    if(thinStep) exportPosteriors();
}

void MEMM_coloniz_dendro_multipop::mcmc_dyn(int nbIteration, int thinStep)
{
    cerr<<"Dynamic seedlings not implemented yet"<<endl;
    exit(0);
}

/***************************** PROTECTED ********************************/

long double MEMM_coloniz_dendro_multipop::logLik(int posterior)
{
    long double liktemp = 0;
    
    //    vector < *MEMM_coloniz_dendro >    et memm_Pop[pop] -> logLik(posterior)
    //    ou bien vector < MEMM_coloniz_dendro >   et memm_Pop[pop].logLik(posterior)
    //    memm_Pop[pop].Shpaes += sio les paramètres sont passé publics
    
    for (int pop=0; pop<Npop; pop++){
//        cout << "pop" << pop << " ready ... ";
        liktemp += MEMM_pops[pop].logLik(posterior);
        
    }
    
    //    cout << "Compute lik OK" << endl; cout.flush();
    
    return liktemp;
}


/*************** INIT ******************/

void MEMM_coloniz_dendro_multipop::init(Configuration * p)
{
    if(p == NULL) return;
    
    cout << endl << endl << "########## Launching initialization multipop... " << endl;
    // load main general options: Npop (I/O), seed,...
    loadOptions(p);
    cout << "     ... load Options multipop OK" << endl << endl;
    cout << "- Loading Genetic system multipop..." << endl;
    
    // load allelic frequencies
    loadAllelicFreq(p);
//    cout << "     ... load Allelic Frequencies multipop OK" << endl << endl;
    cout << endl << "- Loading the different pop files multipop..." << endl;
    
    // Load parents from file and fatherID
    loadParentFile(p);
    cout << "     ... load the different pop files multipop OK" << endl << endl;
    cout << "- Loading the Parameters multipop..." << endl;

    //  load parameters, priors ...;
    loadParameters(p);
    cout << endl << "########## All initializations cvompleted OK #############" << endl;

}

void MEMM_coloniz_dendro_multipop::loadAllelicFreq(Configuration * p)
{
    bool readfile = false;
    char * temp;
    int ntemp;
    double floattemp, stemp;
    
    temp = p->getValue(MEMM_NUMBER_OF_LOCUS);
    Nl = atoi(temp);
    if(temp) delete []temp;
    
    vector< std::vector<int> > AllSize (Nl);
    
    Na.clear();
    Na.assign(Nl, 0);
    
    for(int i=AllFreq.size()-1; i>=0 ; i--) AllFreq[i].clear();
    AllFreq.clear();
    AllFreq.resize(Nl);
    
    for(int i=SizeAll.size()-1; i>=0 ; i--) SizeAll[i].clear();
    SizeAll.clear();
    SizeAll.resize(Nl);
    
    temp = p->getValue(MEMM_AF);
    if(temp && strcmp("",temp)!=0)
    {
        cout << "# Use allelic Frequencies from file" << endl;
        delete []temp;
        temp = p->getValue(MEMM_AF_FILE_NAME);
        ifstream fall(temp);
        if(fall.good())
        {
            cout << "     .... loading allelic frequencies from file " <<temp<<" ..."<<endl;
            if(temp) delete []temp;
            temp = NULL;
            for (int l=0; l<Nl; l++)
            {
                fall >> Na[l];
                stemp=0;
                for (int a=0; a<Na[l]; a++)
                {
                    fall >> ntemp >> floattemp;
                    stemp+=floattemp;
                    AllSize[l].push_back(ntemp);
                    AllFreq[l].push_back(floattemp);
                    SizeAll[l][ntemp]=a;
                }
                cout << "Locus " << l << " : " << Na[l] << " alleles" <<endl;
                for (int a=0; a<Na[l]; a++)
                {
                    AllFreq[l][a] /= stemp;
                    //SizeFreq[l][AllSize[l][a]]=AllFreq[l][a];
                    std::cout << AllSize[l][a] << ":" << AllFreq[l][a] << " | ";
                }
                cout << endl;
            }
            cout << "    ... load of allelic frequencies : OK. " <<endl;
            fall.close();
            readfile = true;
        }
        else{
            readfile = false;
            cerr<<"ERROR can't read allelic frequencies from "<<temp<<endl;
        }
    }
    
    if(!readfile)
    {
        std::cout << "Computing allelic frequencies from adults is not available in the multipop mode, sorry. " <<std::endl << std::endl;
        exit(1);
    }
    
    if(temp) delete []temp;
    
    loadMatrixError(p,AllSize);
}

void MEMM_coloniz_dendro_multipop::loadParentFile(Configuration *p)
{
    int pop=0;
    allPopulations = p->loadPopulations();
    Npop = allPopulations.size();
    MEMM_pops.resize(Npop);
    
    for(unordered_map<string, Population *>::iterator it = allPopulations.begin() ; it != allPopulations.end(); ++it)
    {
        
        (MEMM_pops[pop]).loadGeneticSystem(Nl, Na, SizeAll, AllFreq, posterior, useMatError, NMistypeMax, MatError);
        
        //        dynamic_cast<MEMM_coloniz_dendro*>(MEMM_pops[pop]).loadParentFile(p); // .loadParentFile(p_pops[pop]);
        //        dynamic_cast<MEMM_coloniz_dendro*>(MEMM_pops[pop]).calculWeightPollenDonor(p);
        //        dynamic_cast<MEMM_coloniz_dendro*>(MEMM_pops[pop]).calculDistances(p);
        //        dynamic_cast<MEMM_coloniz_dendro*>(MEMM_pops[pop]).calculTransitionMatrix(p);
        
        //        (MEMM_pops[pop]).loadParentFile(p); // .loadParentFile(p_pops[pop]);
        
        cout << endl << "Loading Population #" << pop << " from file " << ((*it).second)->filename_;
        (MEMM_pops[pop]).loadPopulation( ((*it).second) );
        (MEMM_pops[pop]).calculWeightPollenDonor(p);
        (MEMM_pops[pop]).calculDistances(p);
        (MEMM_pops[pop]).calculTransitionMatrix(p);
        pop++;
    }
    
    GamA.resize(Npop); for (int pop=0; pop<Npop; pop++) GamA[pop] = &((MEMM_pops[pop]).GamA);
    //    Scale.resize(Npop); for (int pop=0; pop<Npop; pop++) Scale[pop] = &((MEMM_pops[pop]).Scale);
    //    Shape.resize(Npop);for (int pop=0; pop<Npop; pop++) Shape[pop] = &((MEMM_pops[pop]).Shape);
    //    Mig.resize(Npop); for (int pop=0; pop<Npop; pop++) Mig[pop] = &((MEMM_pops[pop]).Mig);
    //    Self.resize(Npop); for (int pop=0; pop<Npop; pop++) Self[pop] = &((MEMM_pops[pop]).Self);
    Mig0P.resize(Npop); for (int pop=0; pop<Npop; pop++) Mig0P[pop] = &((MEMM_pops[pop]).Mig0P);
    bMigP.resize(Npop); for (int pop=0; pop<Npop; pop++) bMigP[pop] = &((MEMM_pops[pop]).bMigP);
    AgeThP.resize(Npop); for (int pop=0; pop<Npop; pop++) AgeThP[pop] = &((MEMM_pops[pop]).AgeThP);
    bFecP.resize(Npop); for (int pop=0; pop<Npop; pop++) bFecP[pop] = &((MEMM_pops[pop]).bFecP);
    GamAs.resize(Npop); for (int pop=0; pop<Npop; pop++) GamAs[pop] = &((MEMM_pops[pop]).GamAs);
    Scales.resize(Npop); for (int pop=0; pop<Npop; pop++) Scales[pop] = &((MEMM_pops[pop]).Scales);
    Shapes.resize(Npop);for (int pop=0; pop<Npop; pop++) Shapes[pop] = &((MEMM_pops[pop]).Shapes);
    Kappas.resize(Npop); for (int pop=0; pop<Npop; pop++) Kappas[pop] = &((MEMM_pops[pop]).Kappas);
    Thetas.resize(Npop); for (int pop=0; pop<Npop; pop++) Thetas[pop] = &((MEMM_pops[pop]).Thetas);
    Mig0S.resize(Npop); for (int pop=0; pop<Npop; pop++) Mig0S[pop] = &((MEMM_pops[pop]).Mig0S);
    bMigS.resize(Npop); for (int pop=0; pop<Npop; pop++) bMigS[pop] = &((MEMM_pops[pop]).bMigS);
    AgeThS.resize(Npop); for (int pop=0; pop<Npop; pop++) AgeThS[pop] = &((MEMM_pops[pop]).AgeThS);
    bFecS.resize(Npop); for (int pop=0; pop<Npop; pop++) bFecS[pop] = &((MEMM_pops[pop]).bFecS);
    as.resize(Npop); for (int pop=0; pop<Npop; pop++) as[pop] = &((MEMM_pops[pop]).as);
    abs.resize(Npop); for (int pop=0; pop<Npop; pop++) abs[pop] = &((MEMM_pops[pop]).abs);
    Kabs.resize(Npop);for (int pop=0; pop<Npop; pop++) Kabs[pop] = &((MEMM_pops[pop]).Kabs);
    
}


//void MEMM_coloniz_dendro_multipop::loadSeeds(Parameters *p)
//{
//cout << endl << "Call of MEMM_coloniz_dendro_multipop::loadSeeds " <<endl << endl;
//}

//void MEMM_coloniz_dendro_multipop::calculDistances(Parameters *p)
//{
//cout << endl << "Call of MEMM_coloniz_dendro_multipop::calculDistances " <<endl << endl;
//}

//void MEMM_coloniz_dendro_multipop::calculTransitionMatrix(Parameters *p)
//{
//  cout << endl << "Call of MEMM_coloniz_dendro_multipop::calculTransitionMatrix " <<endl << endl;
//}

//void MEMM_coloniz_dendro_multipop::calculWeightPollenDonor(Parameters *p)
//{
//  cout << endl << "Call of MEMM_coloniz_dendro_multipop::calculWeightPollenDonor " <<endl << endl;
//}

void MEMM_coloniz_dendro_multipop::loadParameters(Configuration *p)
{
    char * temp;
    //  MEMM::loadParameters(p);
    
    Parameter * param_temp;
    accept_ratio tempRatio;
    string tempPrior;
    bool tempBool;
    
    cout << "Reading Parameters ..."<<endl;
    
    // free loadParameters() return after ?
    allParameters = (p->loadParameters());
    cout << "Laoding parameters from XML OK..."<<endl; cout.flush();
    
    // gama
    param_temp = allParameters["gama"];
    if(param_temp)
    {
        mGamA = param_temp->MIN;
        MGamA = param_temp->MAX;
        tempRatio = param_temp->accept_ratio_;
        tempBool = param_temp->level_;
        tempPrior= param_temp->prior_name_;
        
        for (int pop=0; pop<Npop; pop++) *GamA[pop] = param_temp->INIT;
        cout << "GamA : "<< *GamA[0];
        if (tempRatio(1.0,1.0) == 0 ) cout << " Fixed " << endl ; cout.flush();
        if (tempRatio(1.0,1.0) != 0 ) {
            cout << " [" <<mGamA <<"|"<< MGamA << "] " << tempPrior;
            if (tempBool) cout << " at Pop level " << endl ;
            else cout << " at Universe level " << endl ; cout.flush();
        }
    }
    
    //     // Scale
    //      param_temp = allParameters["scale"];
    //      if(param_temp)
    //      {
    //        mScale = param_temp->MIN;
    //        MScale = param_temp->MAX;
    //        for (int pop=0; pop<Npop; pop++) *Scale[pop] = param_temp->INIT;
    //          cout << "Scale : "<< Scale << " [" <<mScale <<"|"<< MScale << "]"<<endl;cout.flush();
    //      }
    
    //      // Shape
    //      param_temp = allParameters["shape"];
    //      if(param_temp)
    //      {
    //        mShape = param_temp->MIN;
    //        MShape = param_temp->MAX;
    //        for (int pop=0; pop<Npop; pop++) *Shape[pop] = param_temp->INIT;
    //        cout << "Shape : "<< Shape << " [" <<mShape <<"|"<< MShape << "]"<<endl;cout.flush();
    //      }
    
    //      // Mig
    //      param_temp = allParameters["mig"];
    //      if(param_temp)
    //      {
    //        mMig = param_temp->MIN;
    //        MMig = param_temp->MAX;
    //        for (int pop=0; pop<Npop; pop++) *Mig[pop] = param_temp->INIT;
    //        cout << "Mig : "<< Mig << " [" <<mMig <<"|"<< MMig << "]"<<endl;cout.flush();
    //      }
    
    //      // Self
    //      param_temp = allParameters["self"];
    //      if(param_temp)
    //      {
    //        mSelf = param_temp->MIN;
    //        MSelf = param_temp->MAX;
    //        for (int pop=0; pop<Npop; pop++) *Self[pop] = param_temp->INIT;
    //        cout << "Self : "<< Self << " [" <<mSelf <<"|"<< MSelf << "]"<<endl;cout.flush();
    //      }
    
    // Mig0P
    param_temp = allParameters["mig0p"];
    if(param_temp)
    {
        mMig0P = param_temp->MIN;
        MMig0P = param_temp->MAX;
        tempRatio = param_temp->accept_ratio_;
        tempBool = param_temp->level_;
        tempPrior= param_temp->prior_name_;
        for (int pop=0; pop<Npop; pop++) *Mig0P[pop] = param_temp->INIT;
        cout << "Mig0P : "<< *Mig0P[0];
        if (tempRatio(1.0,1.0) == 0 ) cout << " Fixed " << endl ; cout.flush();
        if (tempRatio(1.0,1.0) != 0 ) {
            cout << " [" <<mMig0P <<"|"<< MMig0P << "] "<< tempPrior;
            if (tempBool) cout << " at Pop level " << endl ;
            else cout << " at Universe level " << endl ; cout.flush();
        }
    }
    
    // bMigP
    param_temp = allParameters["bmigp"];
    if(param_temp)
    {
        mbMigP = param_temp->MIN;
        MbMigP = param_temp->MAX;
        tempRatio = param_temp->accept_ratio_;
        tempBool = param_temp->level_;
        tempPrior= param_temp->prior_name_;
        for (int pop=0; pop<Npop; pop++) *bMigP[pop] = param_temp->INIT;
        cout << "bMigP : "<< *bMigP[0];
        if (tempRatio(1.0,1.0) == 0 ) cout << " Fixed " << endl ; cout.flush();
        if (tempRatio(1.0,1.0) != 0 ) {
            cout << " [" <<mbMigP <<"|"<< MbMigP << "] "<< tempPrior;
            if (tempBool) cout << " at Pop level " << endl ;
            else cout << " at Universe level " << endl ; cout.flush();
        }
    }
    
    // AgeThP
    param_temp = allParameters["agethp"];
    if(param_temp)
    {
        mAgeThP = param_temp->MIN;
        MAgeThP = param_temp->MAX;
        tempRatio = param_temp->accept_ratio_;
        tempBool = param_temp->level_;
        tempPrior= param_temp->prior_name_;
        for (int pop=0; pop<Npop; pop++) *AgeThP[pop] = param_temp->INIT;
        cout << "AgeThP : "<< *AgeThP[0];
        if (tempRatio(1.0,1.0) == 0 ) cout << " Fixed " << endl ; cout.flush();
        if (tempRatio(1.0,1.0) != 0 ) {
            cout << " [" <<mAgeThP <<"|"<< MAgeThP << "] "<< tempPrior;
            if (tempBool) cout << " at Pop level " << endl ;
            else cout << " at Universe level " << endl ; cout.flush();
        }
    }
    
    // bFecP
    param_temp = allParameters["bfecp"];
    if(param_temp)
    {
        mbFecP = param_temp->MIN;
        MbFecP = param_temp->MAX;
        tempRatio = param_temp->accept_ratio_;
        tempBool = param_temp->level_;
        tempPrior= param_temp->prior_name_;
        for (int pop=0; pop<Npop; pop++) *bFecP[pop] = param_temp->INIT;
        cout << "bFecP : "<< *bFecP[0];
        if (tempRatio(1.0,1.0) == 0 ) cout << " Fixed " << endl ; cout.flush();
        if (tempRatio(1.0,1.0) != 0 ) {
            cout << " [" <<mbFecP <<"|"<< MbFecP << "] "<< tempPrior;
            if (tempBool) cout << " at Pop level " << endl ;
            else cout << " at Universe level " << endl ; cout.flush();
        }
    }
    
    // gamas
    param_temp = allParameters["gamas"];
    if(param_temp)
    {
        mGamAs = param_temp->MIN;
        MGamAs = param_temp->MAX;
        tempRatio = param_temp->accept_ratio_;
        tempBool = param_temp->level_;
        tempPrior= param_temp->prior_name_;
        for (int pop=0; pop<Npop; pop++) *GamAs[pop] = param_temp->INIT;
        cout << "GamAs : "<< *GamAs[0];
        if (tempRatio(1.0,1.0) == 0 ) cout << " Fixed " << endl ; cout.flush();
        if (tempRatio(1.0,1.0) != 0 ) {
            cout << " [" <<mGamAs <<"|"<< MGamAs << "] "<< tempPrior;
            if (tempBool) cout << " at Pop level " << endl ;
            else cout << " at Universe level " << endl ; cout.flush();
        }
    }
    
    // Scales
    param_temp = allParameters["scales"];
    if(param_temp)
    {
        mScales = param_temp->MIN;
        MScales = param_temp->MAX;
        tempRatio = param_temp->accept_ratio_;
        tempBool = param_temp->level_;
        tempPrior= param_temp->prior_name_;
        for (int pop=0; pop<Npop; pop++) *Scales[pop] = param_temp->INIT;
        cout << "Scales : "<< *Scales[0];
        if (tempRatio(1.0,1.0) == 0 ) cout << " Fixed " << endl ; cout.flush();
        if (tempRatio(1.0,1.0) != 0 ) {
            cout << " [" <<mScales <<"|"<< MScales << "] "<< tempPrior;
            if (tempBool) cout << " at Pop level " << endl ;
            else cout << " at Universe level " << endl ; cout.flush();
        }
    }
    
    // Shapes
    param_temp = allParameters["shapes"];
    if(param_temp)
    {
        mShapes = param_temp->MIN;
        MShapes = param_temp->MAX;
        tempRatio = param_temp->accept_ratio_;
        tempBool = param_temp->level_;
        tempPrior= param_temp->prior_name_;
        for (int pop=0; pop<Npop; pop++) *Shapes[pop] = param_temp->INIT;
        cout << "Shapes : "<< *Shapes[0];
        if (tempRatio(1.0,1.0) == 0 ) cout << " Fixed " << endl ; cout.flush();
        if (tempRatio(1.0,1.0) != 0 ) {
            cout << " [" <<mShapes <<"|"<< MShapes << "] "<< tempPrior;
            if (tempBool) cout << " at Pop level " << endl ;
            else cout << " at Universe level " << endl ; cout.flush();
        }
    }
    
    // Thetas
    param_temp = allParameters["thetas"];
    if(param_temp)
    {
        mThetas = param_temp->MIN;
        MThetas = param_temp->MAX;
        tempRatio = param_temp->accept_ratio_;
        tempBool = param_temp->level_;
        tempPrior= param_temp->prior_name_;
        for (int pop=0; pop<Npop; pop++) *Thetas[pop] = param_temp->INIT;
        cout << "Thetas : "<< *Thetas[0];
        if (tempRatio(1.0,1.0) == 0 ) cout << " Fixed " << endl ; cout.flush();
        if (tempRatio(1.0,1.0) != 0 ) {
            cout << " [" <<mThetas <<"|"<< MThetas << "] "<< tempPrior;
            if (tempBool) cout << " at Pop level " << endl ;
            else cout << " at Universe level " << endl ; cout.flush();
        }
    }
    
    // Kappas
    param_temp = allParameters["kappas"];
    if(param_temp)
    {
        mKappas = param_temp->MIN;
        MKappas = param_temp->MAX;
        tempRatio = param_temp->accept_ratio_;
        tempBool = param_temp->level_;
        tempPrior= param_temp->prior_name_;
        for (int pop=0; pop<Npop; pop++) *Kappas[pop] = param_temp->INIT;
        cout << "Kappas : "<< *Kappas[0];
        if (tempRatio(1.0,1.0) == 0 ) cout << " Fixed " << endl ; cout.flush();
        if (tempRatio(1.0,1.0) != 0 ) {
            cout << " [" <<mKappas <<"|"<< MKappas << "] "<< tempPrior;
            if (tempBool) cout << " at Pop level " << endl ;
            else cout << " at Universe level " << endl ; cout.flush();
        }
    }
    
    // Mig0S
    param_temp = allParameters["mig0s"];
    if(param_temp)
    {
        mMig0S = param_temp->MIN;
        MMig0S = param_temp->MAX;
        tempRatio = param_temp->accept_ratio_;
        tempBool = param_temp->level_;
        tempPrior= param_temp->prior_name_;
        for (int pop=0; pop<Npop; pop++) *Mig0S[pop] = param_temp->INIT;
        cout << "Mig0S : "<< *Mig0S[0];
        if (tempRatio(1.0,1.0) == 0 ) cout << " Fixed " << endl ; cout.flush();
        if (tempRatio(1.0,1.0) != 0 ) {
            cout << " [" <<mMig0S <<"|"<< MMig0S << "] "<< tempPrior;
            if (tempBool) cout << " at Pop level " << endl ;
            else cout << " at Universe level " << endl ; cout.flush();
        }
    }
    
    // bMigS
    param_temp = allParameters["bmigs"];
    if(param_temp)
    {
        mbMigS = param_temp->MIN;
        MbMigS = param_temp->MAX;
        tempRatio = param_temp->accept_ratio_;
        tempBool = param_temp->level_;
        tempPrior= param_temp->prior_name_;
        for (int pop=0; pop<Npop; pop++) *bMigS[pop] = param_temp->INIT;
        cout << "bMigS : "<< *bMigS[0];
        if (tempRatio(1.0,1.0) == 0 ) cout << " Fixed " << endl ; cout.flush();
        if (tempRatio(1.0,1.0) != 0 ) {
            cout << " [" <<mbMigS <<"|"<< MbMigS << "] "<< tempPrior;
            if (tempBool) cout << " at Pop level " << endl ;
            else cout << " at Universe level " << endl ; cout.flush();
        }
    }
    
    // AgeThS
    param_temp = allParameters["ageths"];
    if(param_temp)
    {
        mAgeThS = param_temp->MIN;
        MAgeThS = param_temp->MAX;
        tempRatio = param_temp->accept_ratio_;
        tempBool = param_temp->level_;
        tempPrior= param_temp->prior_name_;
        for (int pop=0; pop<Npop; pop++) *AgeThS[pop] = param_temp->INIT;
        cout << "AgeThS : "<< *AgeThS[0];
        if (tempRatio(1.0,1.0) == 0 ) cout << " Fixed " << endl ; cout.flush();
        if (tempRatio(1.0,1.0) != 0 ) {
            cout << " [" <<mAgeThS <<"|"<< MAgeThS << "] "<< tempPrior;
            if (tempBool) cout << " at Pop level " << endl ;
            else cout << " at Universe level " << endl ; cout.flush();
        }
    }
    
    // bFecS
    param_temp = allParameters["bfecs"];
    if(param_temp)
    {
        mbFecS = param_temp->MIN;
        MbFecS = param_temp->MAX;
        tempRatio = param_temp->accept_ratio_;
        tempBool = param_temp->level_;
        tempPrior= param_temp->prior_name_;
        for (int pop=0; pop<Npop; pop++) *bFecS[pop] = param_temp->INIT;
        cout << "bFecS : "<< *bFecS[0];
        if (tempRatio(1.0,1.0) == 0 ) cout << " Fixed " << endl ; cout.flush();
        if (tempRatio(1.0,1.0) != 0 ) {
            cout << " [" <<mbFecS <<"|"<< MbFecS << "] "<< tempPrior;
            if (tempBool) cout << " at Pop level " << endl ;
            else cout << " at Universe level " << endl ; cout.flush();
        }
    }
    
    
    *as[0] = *Scales[0]*exp(gammln(2/(*Shapes[0]))-gammln(3/(*Shapes[0])));
    *abs[0]=-pow(*as[0],-(*Shapes[0]));
    *Kabs[0]=(*Shapes[0])/2/3.14/(*as[0])/(*as[0])/exp(gammln(2/(*Shapes[0])));
    
    (MEMM_pops[0].Vec_Fecs).resize(MEMM_pops[0].NPar, 1.0 );
    (MEMM_pops[0].Vec_Fecp).resize(MEMM_pops[0].NPar, 1.0 );

    
    cout << endl << " Loading distributions of latent variables..." << endl;

    (MEMM_pops[0].pLois) = loadDistribution(p, "individual_fecundities_seeds");
    (MEMM_pops[0].pLoi) = loadDistribution(p, "individual_fecundities");
    
    for (int pop=1; pop<Npop; pop++) {
        *Shapes[pop] = *Shapes[0];
        *as[pop] = *as[0];
        *abs[pop] = *abs[0];
        *Kabs[pop] = *Kabs[0];
        
        (MEMM_pops[pop].Vec_Fecs).resize(MEMM_pops[pop].NPar, 1.0 );
        (MEMM_pops[pop].Vec_Fecp).resize(MEMM_pops[pop].NPar, 1.0 );
        
        (MEMM_pops[pop].pLois) = loadDistribution(p, "individual_fecundities_seeds");
        (MEMM_pops[pop].pLoi) = loadDistribution(p, "individual_fecundities");
        
    }
    
    
    cout<<"end loading Parameters"<<endl;
    
}


void MEMM_coloniz_dendro_multipop::exportPosteriors()
{
    for (int pop=0; pop<Npop; pop++)
    {
        cout << endl << "Exporting the posteriors of population #" << pop << endl;
        *fposterior << " Poplation #" << pop << endl;
        (MEMM_pops[pop]).exportPosteriors(posterior, *fposterior);
    }

    fposterior->close();
}


void MEMM_coloniz_dendro_multipop::loadOptions(Configuration *p)
{
    char * temp;
    cout << "Load options..."<<endl << endl;
    
//    temp = p->getValue(MEMM_NUMBER_POPS);
//    if(atoi(temp) >= 2)
//    {
//        cout << "Number of pops analyzed jointly : " <<temp << endl;
//        Npop = atoi(temp);
//    }
//    else {
//        cout << "multipop mode necessitates >1 pops !!!" << endl;
//        exit(1);
//    }
    
    temp = p->getValue(MEMM_SEED);
    if(atoi(temp) >= 0)
    {
        cout << "New seed : "<<temp<<endl;
        generator.seed(static_cast<long unsigned int>(atoi(temp)));
    }
    else {
        cout << "New seed : generated from time "<<endl;
        // by default
        //generator.seed(static_cast<long unsigned int>(time(NULL)));
    }
    
    if(temp) delete []temp;
    
    temp = p->getValue(MEMM_BURNIN);
    Nburn = atoi(temp);
    if(temp) delete []temp;
    temp = p->getValue(MEMM_ITE);
    Nstep = atoi(temp);
    if(temp) delete []temp;
    temp = p->getValue(MEMM_THIN);
    Nthin = atoi(temp);
    if(temp) delete []temp;
    cout<< "Burnin " << Nburn << " | Iteration " << Nstep<< " | Thin " << Nthin << endl;
    
    cout << "Output directory :" << endl;
    outputDirectory = string(p->getValue(MEMM_DIRECTORY_PATH));
    DIR * dir;
    if( (dir = opendir(outputDirectory.c_str())) != NULL )
    {
        cout << "\t-" << outputDirectory <<endl;
        closedir(dir);
    }
    else {
        cout << "#ERROR opening directory " << outputDirectory <<endl;
        exit(1);
    }
    
    std::stringstream ss;
//    ss << outputDirectory << "/";
    
    ofstream exportProbTrans;
    ss << outputDirectory << "/exportTransitionProb.txt";
    exportProbTrans.open(ss.str(), ofstream::out);
    exportProbTrans.close();

    
    cout<<"Ouput files :"<<endl;
    temp = p->getValue(MEMM_GAMA_FILE_NAME);
    ss.str("");
    ss << outputDirectory << "/" << temp;
    ParamFec = new std::ofstream(ss.str(), ofstream::out);
    if(ParamFec->good()) cout<<"\t-"<<temp<<endl;
    else cerr<<"#ERROR openning "<<temp<<" file"<<endl;
    if(temp) delete []temp;
    
    temp = p->getValue(MEMM_IND_FEC_FILE_NAME);
    ss.str("");
    ss << outputDirectory << "/" << temp;
    IndivFec = new std::ofstream(ss.str(), ofstream::out);
    if(IndivFec->good()) cout<<"\t-"<<temp<<endl;
    else cerr<<"#ERROR openning "<<temp<<" file"<<endl;
    if(temp) delete []temp;
    
    temp = p->getValue(MEMM_DISP_FILE_NAME);
    ss.str("");
    ss << outputDirectory << "/" << temp;
    ParamDisp = new std::ofstream(ss.str(), ofstream::out);
    if(ParamDisp->good()) cout<<"\t-"<<temp<<endl;
    else cerr<<"#ERROR openning "<<temp<<" file"<<endl;
    if(temp) delete []temp;
    
    temp = p->getValue(MEMM_POSTERIOR_TYPE);
    if( strcmp("assignment",temp) == 0 ) posterior=1;
    else if( strcmp("seedpool",temp) == 0 ) posterior=2;
    else if( strcmp("assignment_short",temp) == 0 ) posterior=4;
    else posterior=NULL;
    cout<<"\t-"<<temp<<" "<< posterior;
    if (posterior) cout << " exported in file ";
    else cout << " no post-treatment " << endl;
    
    if (posterior)
    {
        temp = p->getValue(MEMM_POSTERIOR_FILE_NAME);
        ss.str("");
        ss << outputDirectory << "/" << temp;
        fposterior = new std::ofstream(ss.str(), ofstream::out);
        if(fposterior->good()) cout<<" "<<temp<<endl;
        else cout << endl <<"#ERROR openning "<<temp<<" file"<<endl;
        if(temp) delete []temp;
    }
    
    cout << "... end reading default options"<<endl;
}
