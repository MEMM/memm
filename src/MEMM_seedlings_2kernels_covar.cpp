/**************************************************************************************************************
 * memm is a bayesian estimation of pollen dispersal kernel and variance in male fecundity.                   *
 * copyright (c) 2009-2016 etienne klein <etienne.klein@paca.inra.fr>                                         *
 *                                                                                                            *
 * this file is part of memm.                                                                                 *
 *                                                                                                            *
 * memm is free software: you can redistribute it and/or modify                                               *
 * it under the terms of the gnu general public license as published by                                       *
 * the free software foundation, either version 3 of the license, or                                          *
 * (at your option) any later version.                                                                        *
 *                                                                                                            *
 * memm is distributed in the hope that it will be useful,                                                    *
 * but without any warranty; without even the implied warranty of                                             *
 * merchantability or fitness for a particular purpose.  see the                                              *
 * gnu general public license for more details.                                                               *
 *                                                                                                            *
 * you should have received a copy of the gnu general public license                                          *
 * along with memm.  if not, see <http://www.gnu.org/licenses/>.                                              *
 **************************************************************************************************************/

#include "MEMM_seedlings_2kernels_covar.hpp"
#include <ctime>
#include <algorithm>
#include <cstdlib>

MEMM_seedlings_2kernels_covar::MEMM_seedlings_2kernels_covar()
{
  cout<< "MEMM : Seedlings_2kernels_covar mode"<<endl;
  pLoiLDD = NULL;
  Vec_FreqLDD.clear();
    Vec_FreqLDD_All.clear();
    Vec_Fecs_All.clear();
    Vec_Fecp_All.clear();
}

MEMM_seedlings_2kernels_covar::~MEMM_seedlings_2kernels_covar()
{
  if(pLoiLDD) delete pLoiLDD;
}

void MEMM_seedlings_2kernels_covar::mcmc(int nbIteration, int thinStep)
{
    
    double lastFecLogLiks, nextFecLogLiks;
    double lastFecLogLikp, nextFecLogLikp;
    double lastFreqLDDLogLik, nextFreqLDDLogLik;
    double previousValue, nextValue;
    double previousValue2, nextValue2;
    double previousLogLik, nextLogLik;
    vector < double > Vec_Previous (Nvar, 0.);
    
    double temp;
    int dim;
    bool inRange;
    
    bool inside;
    int blocSize = 10;
    int Nbloc = NPar/blocSize;
    vector<double> previousBloc1 (NPar, 0.);
    vector<double> previousBloc2 (NPar, 0.);
    vector<double> previousFecp (blocSize, 0.);
    vector<double> nextFecp (blocSize, 0.);
    vector<double> previousFecs (blocSize, 0.);
    vector<double> nextFecs (blocSize, 0.);
    vector<double> previousFreqLDD (blocSize, 0.);
    vector<double> nextFreqLDD (blocSize, 0.);
    double ratio=1;
    vector<int> myvector;
    for (int i=0; i<NPar; ++i) myvector.push_back(i); // Vector of indices to generate permuted samples
    srand ( unsigned ( time(0) ) );
    
    
    
    double Tune=0.2;
    double fineTune=0.05;
    
    bool gamasFixed;
    bool scalesFixed, shapesFixed;
    //    bool kappasFixed, thetasFixed;
    bool migsFixed, pi0sFixed;
    bool alphalddFixed, betalddFixed;
    bool scaleslddFixed, shapeslddFixed;
    
    bool gamaFixed;
    bool scaleFixed, shapeFixed;
    bool migFixed, selfFixed;
    bool pi0Fixed;
    
    
    accept_ratio gamasRatio = allParameters["gamas"]->accept_ratio_;
    accept_ratio scalesRatio = allParameters["scales"]->accept_ratio_;
    accept_ratio shapesRatio = allParameters["shapes"]->accept_ratio_;
    //    accept_ratio kappasRatio = allParameters["kappas"]->accept_ratio_;
    //    accept_ratio thetasRatio = allParameters["thetas"]->accept_ratio_;
    accept_ratio migsRatio = allParameters["migs"]->accept_ratio_;
    accept_ratio pi0sRatio = allParameters["Pi0s"]->accept_ratio_;
    accept_ratio scaleslddRatio = allParameters["scalesLDD"]->accept_ratio_;
    accept_ratio shapeslddRatio = allParameters["shapesLDD"]->accept_ratio_;
    accept_ratio alphalddRatio = allParameters["alphaLDD"]->accept_ratio_;
    accept_ratio betalddRatio = allParameters["betaLDD"]->accept_ratio_;
    
    accept_ratio gamaRatio = allParameters["gama"]->accept_ratio_;
    accept_ratio scaleRatio = allParameters["scale"]->accept_ratio_;
    accept_ratio shapeRatio = allParameters["shape"]->accept_ratio_;
    accept_ratio migRatio = allParameters["mig"]->accept_ratio_;
    accept_ratio selfRatio = allParameters["self"]->accept_ratio_;
    accept_ratio pi0Ratio = allParameters["Pi0"]->accept_ratio_;
    
    gamasFixed = ( gamasRatio(1.0,1.0) == 0 );
    scalesFixed = ( scalesRatio(1.0,1.0) == 0 );
    shapesFixed = ( shapesRatio(1.0,1.0) == 0 );
    //    kappasFixed = ( kappasRatio(1.0,1.0) == 0 );
    //    thetasFixed = ( thetasRatio(1.0,1.0) == 0 );
    migsFixed = ( migsRatio(1.0,1.0) == 0 );
    pi0sFixed = ( pi0sRatio(1.0,1.0) == 0 );
    scalesFixed = ( scalesRatio(1.0,1.0) == 0 );
    shapesFixed = ( shapesRatio(1.0,1.0) == 0 );
    alphalddFixed = ( alphalddRatio(1.0,1.0) == 0 );
    betalddFixed = ( betalddRatio(1.0,1.0) == 0 );
    scaleslddFixed = ( scaleslddRatio(1.0,1.0) == 0 );
    shapeslddFixed = ( shapeslddRatio(1.0,1.0) == 0 );
    
    gamaFixed = ( gamaRatio(1.0,1.0) == 0 );
    scaleFixed = ( scaleRatio(1.0,1.0) == 0 );
    shapeFixed = ( shapeRatio(1.0,1.0) == 0 );
    selfFixed = ( selfRatio(1.0,1.0) == 0 );
    migFixed = ( migRatio(1.0,1.0) == 0 );
    pi0Fixed = ( pi0Ratio(1.0,1.0) == 0 );
    
    int xpourCent = nbIteration/20;
    if(!xpourCent) xpourCent=1;
    
    if ( (pLoi->_IDLoi == MEMM_IDLOI_GAMMA) || (pLoi->_IDLoi == MEMM_IDLOI_LN) ) {
        lastFecLogLikp = pLoi->logLik(NPar, Vec_Fecp,GamA);
        lastFecLogLiks = pLois->logLik(NPar, Vec_Fecs,GamAs);}
    else if (pLoi->_IDLoi == MEMM_IDLOI_ZIGAMMA) {
        lastFecLogLikp = pLoi->logLik(NPar, Vec_Fecp, Pi0, GamA);
        lastFecLogLiks = pLois->logLik(NPar, Vec_Fecs, Pi0s, GamAs);
        cout << " vraisemblance de la zigamma :" << lastFecLogLiks << " " << Pi0s << " " << GamAs << " " << lastFecLogLikp << " " << Pi0 << " " << GamA << endl;
    }
    else std::cerr << " Distribution of individual fecundities not implemented ";
    
    // lastFreqLDDLogLik = pLoiLDD->logLik(NPar,Vec_FreqLDD,AlphaLDD,BetaLDD); //New
    previousLogLik = logLik(3);  //New
    cout << "Calcul de logLik : " << previousLogLik << endl << endl;

    
    double Zero = 0.;
    lastFreqLDDLogLik = pLoiLDD->logLik(NPar, Vec_FreqLDD, Zero, BetaLDD);
    
    //output start value
    if(thinStep){
        
        if ( (pLoi->_IDLoi == MEMM_IDLOI_GAMMA) || (pLoi->_IDLoi == MEMM_IDLOI_LN) )
        {
            *ParamFec << "0 ";
            if ( !gamasFixed )*ParamFec << lastFecLogLiks << " " << GamAs << " ";
            if ( !gamaFixed )*ParamFec << lastFecLogLikp << " " << GamA << " ";
            if ( !betalddFixed )*ParamFec << lastFreqLDDLogLik << " " << BetaLDD;
            *ParamFec << endl;
        }
        else if (pLoi->_IDLoi == MEMM_IDLOI_ZIGAMMA)
        {
            *ParamFec << "0 ";
            if ( !gamasFixed )*ParamFec << lastFecLogLiks << " " << GamAs << " " << Pi0s << " ";
            if ( !gamaFixed )*ParamFec << lastFecLogLikp << " " << GamA << " " << Pi0 << " ";
            if ( !betalddFixed )*ParamFec << lastFreqLDDLogLik << " " << BetaLDD;
            *ParamFec << endl;
        }
        ParamFec->flush();
        
        *IndivFec << "ite" ;
        for (int p=0; p<NPar; p++) {*IndivFec << " Fs_" << p;}
        for (int p=0; p<NPar; p++) {*IndivFec << " Fp_" << p;}
        for (int p=0; p<NPar; p++) {*IndivFec << " LDD_" << p;}
        *IndivFec << endl;
        IndivFec->flush();
        *IndivFecAll  << "ite" ;
        for (int p=0; p<NPar; p++) {*IndivFecAll << " FsAll_" << p;}
        for (int p=0; p<NPar; p++) {*IndivFecAll << " FpAll_" << p;}
        for (int p=0; p<NPar; p++) {*IndivFecAll << " LDDAll_" << p;}
        *IndivFecAll << endl;
        IndivFecAll->flush();
        
        *ParamDisp << "ite" << " " << "previousLogLik"
        << " " << "Scales"
        << " " << "Shapes"
        << " " << "ScalesLDD"
        << " " << "ShapesLDD"
        << " " << "AlphaLDD"
        << " " << "BetaLDD"
        << " " << "Migs"
        << " " << "Scale"
        << " " << "Shape"
        << " " << "Mig"
        << " " << "Self";
        for (int dim=0; dim<3; dim++)
            for (int v=0; v<Nvar; v++)
                *ParamDisp << " " << "In_" << dim << v ;
        for (int dim=0; dim<3; dim++)
            for (int v=0; v<Nvar; v++)
                *ParamDisp << " " << "FixedEff_" << dim << v;
        *ParamDisp <<endl;
        ParamDisp->flush();
        
        
        *IndivFec << "0 " ;
        for (int p=0; p<NPar; p++) {*IndivFec << Vec_Fecs[p] << " ";}
        for (int p=0; p<NPar; p++) {*IndivFec << Vec_Fecp[p] << " ";}
        for (int p=0; p<NPar; p++) {*IndivFec << Vec_FreqLDD[p] << " ";}
        *IndivFec << endl;
        IndivFec->flush();
        *IndivFecAll  << "0 " ;
        for (int p=0; p<NPar; p++) {*IndivFecAll << Vec_Fecs_All[p] << " ";}
        for (int p=0; p<NPar; p++) {*IndivFecAll << Vec_Fecp_All[p] << " ";}
        for (int p=0; p<NPar; p++) {*IndivFecAll << Vec_FreqLDD_All[p] << " ";}
        *IndivFecAll << endl;
        IndivFecAll->flush();
        *ParamDisp << "0 " << previousLogLik
        << " " << Scales
        << " " << Shapes
        << " " << ScalesLDD
        << " " << ShapesLDD
        << " " << AlphaLDD
        << " " << BetaLDD
        << " " << Migs
        << " " << Scale
        << " " << Shape
        << " " << Mig
        << " " << Self;
        for (int dim=0; dim<3; dim++)
            for (int v=0; v<Nvar; v++)
                *ParamDisp << " " << In_FixedEff[dim][v] ;
        for (int dim=0; dim<3; dim++)
            for (int v=0; v<Nvar; v++)
                *ParamDisp << " " << Vec_FixedEff[dim][v] ;
        *ParamDisp <<endl;
        ParamDisp->flush();
        
    }
    
    clock_t c_start = clock();
    clock_t c_end = clock();
    //    cout << endl << " Un calcul de vraisemblance: " << previousLogLik << " lasts " << 1000.0 * (c_end-c_start) / CLOCKS_PER_SEC << " ms" << endl << endl;
    
    if(nbIteration) cout << "it=1..."<< std::endl;
    for(int ite=1; ite<=nbIteration; ite++)
    {
        if (ite % xpourCent == 0) {cout << "it="<<ite << "..."<< std::endl;}
        
        //GamAs- fecundities distribution seeds
        if ( !gamasFixed )
            if ( (pLoi->_IDLoi == MEMM_IDLOI_GAMMA) || (pLoi->_IDLoi == MEMM_IDLOI_LN) )
            {
                nextValue = GamAs*exp(Tune*gauss());
                if( nextValue>mGamAs && nextValue<MGamAs )
                {
                    lastFecLogLiks = pLois->logLik(NPar, Vec_Fecs, GamAs);
                    nextFecLogLiks = pLois->logLik(NPar, Vec_Fecs, nextValue);
                    if( accept() < exp(nextFecLogLiks-lastFecLogLiks) * gamasRatio(GamAs,nextValue) )
                    {
                        GamAs = nextValue;
                        lastFecLogLiks = nextFecLogLiks;
                        pLois->setDParam(MEMM_LOI_GAMA,GamAs);
                    }
                }
                if(thinStep && (ite%thinStep == 0)) *ParamFec << ite << " " << lastFecLogLiks << " " << GamAs << " ";
            }
        
        //GamAs && pi0s jointly - fecundities distribution seeds ZI parameters
        if ( (pLoi->_IDLoi == MEMM_IDLOI_ZIGAMMA) )
        {
            nextValue = Pi0s*exp(Tune*gauss());
            nextValue2 = GamAs*exp(Tune*gauss());
            if( nextValue>mPi0s && nextValue<MPi0s && nextValue2>mGamAs && nextValue2<MGamAs )
            {
                lastFecLogLiks = pLois->logLik(NPar, Vec_Fecs, Pi0s, GamAs);
                nextFecLogLiks = pLois->logLik(NPar, Vec_Fecs, nextValue, nextValue2);
                
                if( accept() < exp(nextFecLogLiks-lastFecLogLiks) * pi0sRatio(Pi0s,nextValue) * gamasRatio(GamAs,nextValue2)  )
                {
                    GamAs = nextValue2;
                    Pi0s = nextValue;
                    lastFecLogLiks = nextFecLogLiks;
                    pLois->setDParam(MEMM_LOI_GAMA,GamAs);
                    pLois->setDParam(MEMM_LOI_ZIPROB,Pi0s);
                }
            }
            if(thinStep && (ite%thinStep == 0)) *ParamFec << ite << " " << lastFecLogLiks << " " << GamAs << " " << Pi0s << " ";
        }
        
        //GamA (GamAp)- fecundities distribution pollen
        if (!gamaFixed)
            if ( (pLoi->_IDLoi == MEMM_IDLOI_GAMMA) || (pLoi->_IDLoi == MEMM_IDLOI_LN) )
            {
                nextValue = GamA*exp(Tune*gauss());
                if( nextValue>mGamA && nextValue<MGamA )
                {
                    lastFecLogLikp = pLoi->logLik(NPar, Vec_Fecp, GamA);
                    nextFecLogLikp = pLoi->logLik(NPar, Vec_Fecp, nextValue);
                    if( accept() < exp(nextFecLogLikp-lastFecLogLikp) * gamaRatio(GamA,nextValue) )
                    {
                        GamA = nextValue;
                        lastFecLogLikp = nextFecLogLikp;
                        pLoi->setDParam(MEMM_LOI_GAMA,GamA);
                    }
                }
                if(thinStep && (ite%thinStep == 0)) *ParamFec << lastFecLogLikp << " " << GamA << " ";
            }
        
        //pi0 - fecundities distribution pollen ZI parameter
        if ( (pLoi->_IDLoi == MEMM_IDLOI_ZIGAMMA) )
        {
            nextValue = Pi0*exp(Tune*gauss());
            nextValue2 = GamA*exp(Tune*gauss());
            if( nextValue>mPi0 && nextValue<MPi0 && nextValue2>mGamA && nextValue2<MGamA )
            {
                lastFecLogLikp = pLoi->logLik(NPar, Vec_Fecp, Pi0, GamA);
                nextFecLogLikp = pLoi->logLik(NPar, Vec_Fecp, nextValue, nextValue2);
                if( accept() < exp(nextFecLogLikp-lastFecLogLikp) * pi0Ratio(Pi0,nextValue) * gamaRatio(GamA,nextValue2))
                {
                    Pi0 = nextValue;
                    GamA = nextValue2;
                    lastFecLogLikp = nextFecLogLikp;
                    pLoi->setDParam(MEMM_LOI_ZIPROB,Pi0);
                    pLoi->setDParam(MEMM_LOI_GAMA,GamA);
                }
            }
            if(thinStep && (ite%thinStep == 0)) *ParamFec << lastFecLogLikp << " " << GamA << " " << Pi0 << " ";
        }
        
        //BetaLDD - FreqLDD distribution parameter
        if ( !betalddFixed )
        {
            lastFreqLDDLogLik = pLoiLDD->logLik(NPar, Vec_FreqLDD, Zero, BetaLDD);
            nextValue2 = BetaLDD * exp(Tune*gauss());
            if( nextValue2>mBetaLDD && nextValue2<MBetaLDD )
            {
                nextFreqLDDLogLik = pLoiLDD->logLik(NPar, Vec_FreqLDD, Zero, nextValue2);
                if( accept() < exp(nextFreqLDDLogLik-lastFreqLDDLogLik) * betalddRatio(BetaLDD,nextValue2))
                {
                    BetaLDD = nextValue2;
                    lastFreqLDDLogLik = nextFreqLDDLogLik;
                    pLoiLDD->setDParam(MEMM_LOI_VAR,BetaLDD);
                }
            }
        }
        if(thinStep && (ite%thinStep == 0)) *ParamFec << lastFreqLDDLogLik << " " << BetaLDD << endl;
        
        
        //        //Vec_Fecs & Vec_Fecp - Individual fecundities seeds and pollen simultaneously // One by one
        //        if ( !gamasFixed || GamAs!=0 || !gamaFixed || GamA!=0 )
        //        for(int p=0 ; p<NPar ; p++)
        //        {
        //            previousValue = Vec_Fecs[p];
        //            previousValue2 = Vec_Fecp[p];
        //            if ( !gamasFixed || GamAs==0 ) {pLois->tirage(Vec_Fecs[p]); Vec_Fecs_All[p] *= Vec_Fecs[p]/previousValue;}
        //            if ( !gamaFixed || GamA==0 ) {pLoi->tirage(Vec_Fecp[p]); Vec_Fecp_All[p] *= Vec_Fecp[p]/previousValue2;}
        //            nextLogLik = logLik();
        //            if(accept()<exp(nextLogLik-previousLogLik)) previousLogLik = nextLogLik;
        //            else
        //            {
        //                if ( !gamasFixed || GamAs==0 ) {Vec_Fecs_All[p] *= previousValue/Vec_Fecs[p];  Vec_Fecs[p] = previousValue;}
        //                if ( !gamaFixed || GamA==0 ) {Vec_Fecp_All[p] *= previousValue2/Vec_Fecp[p]; Vec_Fecp[p] = previousValue2;}
        //            }
        //        }
        //
        
        //Vec_Fecs & Vec_Fecp - Individual fecundities seeds and pollen simultaneously // Block proposals
        
        if ( !gamasFixed || GamAs>0.00001 || !gamaFixed || GamA>0.00001 )
        {
            random_shuffle ( myvector.begin(), myvector.end() );
            for (int bloc=0; bloc<Nbloc; bloc++)
            {
                inside = true;
                previousFecp.clear();
                nextFecp.clear();
                previousFecs.clear();
                nextFecs.clear();
                ratio=1;
                for (vector<int>::iterator it=myvector.begin()+bloc*blocSize; it!=myvector.begin()+(bloc+1)*blocSize; ++it) {
                    previousBloc1[*it] = Vec_Fecs[*it]; previousFecs.push_back(Vec_Fecs[*it]);
                    previousBloc2[*it] = Vec_Fecp[*it]; previousFecp.push_back(Vec_Fecp[*it]);
                    if ( !gamasFixed || GamAs>0.00001)
                    {
                        Vec_Fecs[*it] *= exp(Tune*gauss());
                        Vec_Fecs_All[*it] *= Vec_Fecs[*it]/previousBloc1[*it];
                        inside = inside && (Vec_Fecs[*it]<10000) && (Vec_Fecs[*it]>0.0000001);
                        ratio *= Vec_Fecs[*it]/previousBloc1[*it];
                    }
                    nextFecs.push_back(Vec_Fecs[*it]);
                    if ( !gamaFixed || GamA>0.00001)
                    {
                        Vec_Fecp[*it] *= exp(Tune*gauss());
                        Vec_Fecp_All[*it] *= Vec_Fecp[*it]/previousBloc2[*it];
                        inside = inside && (Vec_Fecp[*it]<10000) && (Vec_Fecp[*it]>0.0000001);
                        ratio *= Vec_Fecp[*it]/previousBloc2[*it];
                    }
                    nextFecp.push_back(Vec_Fecp[*it]);
                }
                
                if (inside) {
                    nextLogLik = logLik();
                    if(accept()< ratio*exp(nextLogLik - previousLogLik +
                                           pLois->logLik(blocSize, nextFecs, GamAs) - pLois->logLik(blocSize, previousFecs, GamAs)+
                                           pLoi->logLik(blocSize, nextFecp, GamA) - pLoi->logLik(blocSize, previousFecp, GamA)))
                    { previousLogLik = nextLogLik; }
                    else
                    {
                        for (vector<int>::iterator it=myvector.begin()+bloc*blocSize; it!=myvector.begin()+(bloc+1)*blocSize; ++it) {
                            Vec_Fecs_All[*it] *= previousBloc1[*it]/Vec_Fecs[*it];Vec_Fecs[*it] = previousBloc1[*it];
                            Vec_Fecp_All[*it] *= previousBloc2[*it]/Vec_Fecp[*it];Vec_Fecp[*it] = previousBloc2[*it];
                        }
                    }
                    
                }
                else
                {
                    for (vector<int>::iterator it=myvector.begin()+bloc*blocSize; it!=myvector.begin()+(bloc+1)*blocSize; ++it) {
                        Vec_Fecs_All[*it] *= previousBloc1[*it]/Vec_Fecs[*it]; Vec_Fecs[*it] = previousBloc1[*it];
                        Vec_Fecp_All[*it] *= previousBloc2[*it]/Vec_Fecp[*it]; Vec_Fecp[*it] = previousBloc2[*it];
                    }
                }
            }
        }
        
        if(thinStep && (ite%thinStep == 0))
        {
            *IndivFec << ite << " " ;
            for (int p=0; p<NPar; p++) {*IndivFec << Vec_Fecs[p] << " ";}
            for (int p=0; p<NPar; p++) {*IndivFec << Vec_Fecp[p] << " ";}
            //*IndivFec << endl;
            //IndivFec->flush();
            *IndivFecAll << ite << " " ;
            for (int p=0; p<NPar; p++) {*IndivFecAll << Vec_Fecs_All[p] << " ";}
            for (int p=0; p<NPar; p++) {*IndivFecAll << Vec_Fecp_All[p] << " ";}
        }
        
        
        //        //Vec_FreqLDD - Individual frequencies of LDD // Propose one by one
        //        if ( !betalddFixed || BetaLDD!=0 )
        //        for(int p=0 ; p<NPar ; p++)
        //        {
        //            previousValue = Vec_FreqLDD[p];
        //            pLoiLDD->tirage(Vec_FreqLDD[p]);
        //
        //                temp = AlphaLDD + Vec_FreqLDD[p];
        //                for (int v=0; v<Nvar; v++) temp += Vec_FixedEff[2][v] * Covar[p][v];
        //                Vec_FreqLDD_All[p] = exp( temp ) / ( 1 + exp( temp ) );
        //            nextLogLik = logLik();
        //            if(accept()<exp(nextLogLik-previousLogLik)) previousLogLik = nextLogLik;
        //            else
        //            {
        //                Vec_FreqLDD[p] = previousValue;
        //                temp = AlphaLDD + Vec_FreqLDD[p];
        //                for (int v=0; v<Nvar; v++) temp += Vec_FixedEff[2][v] * Covar[p][v];
        //                Vec_FreqLDD_All[p] = exp( temp ) / ( 1 + exp( temp ) );
        //            }
        //        }
        
        //Vec_FreqLDD - Individual frequencies of LDD // Block proposals
        if ( !betalddFixed || BetaLDD!=0 )
        {
            random_shuffle ( myvector.begin(), myvector.end() );
            for (int bloc=0; bloc<Nbloc; bloc++)
            {
                inside = true;
                previousFreqLDD.clear();
                nextFreqLDD.clear();
                ratio=1;
                for (vector<int>::iterator it=myvector.begin()+bloc*blocSize; it!=myvector.begin()+(bloc+1)*blocSize; ++it) {
                    previousBloc1[*it] = Vec_FreqLDD[*it];
                    previousFreqLDD.push_back(Vec_FreqLDD[*it]);
                    Vec_FreqLDD[*it] += 0.1 * gauss();
                    temp = AlphaLDD + Vec_FreqLDD[*it];
                    for (int v=0; v<Nvar; v++) temp += Vec_FixedEff[2][v] * Covar[*it][v];
                    Vec_FreqLDD_All[*it] = exp( temp ) / ( 1 + exp( temp ) );
                    //                        inside = inside && (Vec_Fecs[*it]<10000) && (Vec_Fecs[*it]>0.0000001);
                    //                        ratio *= 1.;
                    nextFreqLDD.push_back(Vec_FreqLDD[*it]);
                }
                
                if (inside) {
                    nextLogLik = logLik();
                    if( accept()< ratio*exp(nextLogLik - previousLogLik +
                                            pLoiLDD->logLik(blocSize, nextFreqLDD, Zero, BetaLDD) - pLoiLDD->logLik(blocSize, previousFreqLDD, Zero, BetaLDD)))
                    { previousLogLik = nextLogLik; }
                    else
                    {
                        for (vector<int>::iterator it=myvector.begin()+bloc*blocSize; it!=myvector.begin()+(bloc+1)*blocSize; ++it) {
                            Vec_FreqLDD[*it] = previousBloc1[*it];
                            temp = AlphaLDD + Vec_FreqLDD[*it];
                            for (int v=0; v<Nvar; v++) temp += Vec_FixedEff[2][v] * Covar[*it][v];
                            Vec_FreqLDD_All[*it] = exp( temp ) / ( 1 + exp( temp ) );
                        }
                    }
                    
                }
                else
                {
                    for (vector<int>::iterator it=myvector.begin()+bloc*blocSize; it!=myvector.begin()+(bloc+1)*blocSize; ++it) {
                        Vec_FreqLDD[*it] = previousBloc1[*it];
                        temp = AlphaLDD + Vec_FreqLDD[*it];
                        for (int v=0; v<Nvar; v++) temp += Vec_FixedEff[2][v] * Covar[*it][v];
                        Vec_FreqLDD_All[*it] = exp( temp ) / ( 1 + exp( temp ) );
                    }
                }
            }
        }
        
        if(thinStep && (ite%thinStep == 0))
        {
            for (int p=0; p<NPar; p++) {*IndivFec << Vec_FreqLDD[p] << " ";}
            *IndivFec << endl;
            IndivFec->flush();
            for (int p=0; p<NPar; p++) {*IndivFecAll << Vec_FreqLDD_All[p] << " ";}
            *IndivFecAll << endl;
            IndivFecAll->flush();
        }
        
        //change Scales && Shapes jointly
        if (!shapesFixed || !scalesFixed)
        {
            previousValue = Shapes;
            previousValue2 = Scales;
            ratio = 1;
            if (!shapesFixed) {Shapes = previousValue*exp(Tune*gauss()); ratio *= shapesRatio(previousValue, Shapes);}
            if (!scalesFixed) {Scales = previousValue2*exp(Tune*gauss()); ratio *= scalesRatio(previousValue2, Scales);}
            if( Shapes>mShapes && Shapes<MShapes && Scales>mScales && Scales<MScales )
            {
                as=Scales*exp(gammln(2/Shapes)-gammln(3/Shapes));
                abs=-pow(as,-Shapes);
                Knorm = Shapes/as/as/exp(gammln(2/Shapes));
                nextLogLik = logLik();
                //                cout << ite << " change scales s " << Scales << " " << previousValue2 << " : " << nextLogLik << " vs. " << previousLogLik << endl;
                if( accept() < exp(nextLogLik-previousLogLik) * ratio )
                    previousLogLik = nextLogLik;
                else{
                    if (!shapesFixed) Shapes = previousValue;
                    if (!scalesFixed) Scales = previousValue2;
                    as=Scales*exp(gammln(2/Shapes)-gammln(3/Shapes));
                    abs=-pow(as,-Shapes);
                    Knorm = Shapes/as/as/exp(gammln(2/Shapes));
                }
            }
            else { Shapes = previousValue; Scales = previousValue2;}
        }
        
        // change ScalesLDD && ShapesLDD jointly
        if (!shapeslddFixed || !scaleslddFixed)
        {
            previousValue = ShapesLDD;
            previousValue2 = ScalesLDD;
            ratio = 1;
            if (!shapeslddFixed) { ShapesLDD = previousValue*exp(Tune*gauss()); ratio *= shapeslddRatio(previousValue, ShapesLDD);}
            if (!scaleslddFixed) { ScalesLDD = previousValue2*exp(Tune*gauss()); ratio *= scaleslddRatio(previousValue2, ScalesLDD);}
            if( ScalesLDD>mScalesLDD && ScalesLDD<MScalesLDD && ShapesLDD>mShapesLDD && ShapesLDD<MShapesLDD )
            {
                asLDD = log(ScalesLDD) - ShapesLDD/2;
                KnormLDD = 1/pow( 2*3.14*ShapesLDD , 0.5 );
                nextLogLik = logLik();
                if( accept() < exp(nextLogLik-previousLogLik) * ratio )
                    previousLogLik = nextLogLik;
                else
                {
                    ScalesLDD = previousValue2;
                    ShapesLDD = previousValue;
                    asLDD = log(ScalesLDD) - ShapesLDD/2;
                    KnormLDD = 1/pow( 2*3.14*ShapesLDD , 0.5 );
                }
            }
            else {ScalesLDD = previousValue2; ShapesLDD = previousValue;}
        }
        
        // change Migs
        if ( !migsFixed )
        {
            previousValue = Migs;
            Migs = previousValue*exp(Tune*gauss());
            if( Migs>mMigs && Migs<MMigs)
            {
                nextLogLik = logLik();
                if( accept() < exp(nextLogLik-previousLogLik) * migsRatio(previousValue, Migs))
                    previousLogLik = nextLogLik;
                else Migs = previousValue;
            }
            else Migs = previousValue;
        }
        
        // change Scale & Shape jointly
        if (!shapeFixed || !scaleFixed)
        {
            previousValue = Shape;
            previousValue2 = Scale;
            ratio =1;
            if (!shapeFixed) { Shape = previousValue*exp(Tune*gauss()); ratio *= shapeRatio(previousValue, Shape);}
            if (!scaleFixed) { Scale = previousValue2*exp(Tune*gauss()); ratio *= scaleRatio(previousValue2, Scale);}
            if( Shape>mShape && Shape<MShape && Scale>mScale && Scale<MScale )
            {
                ap=Scale*exp(gammln(2/Shape)-gammln(3/Shape));
                abp=-pow(ap,-Shape);
                nextLogLik = logLik();
                if( accept() < exp(nextLogLik-previousLogLik) * ratio)
                    previousLogLik = nextLogLik;
                else
                {
                    Shape = previousValue;
                    Scale = previousValue2;
                    ap=Scale*exp(gammln(2/Shape)-gammln(3/Shape));
                    abp=-pow(ap,-Shape);
                }
            }
            else { Shape = previousValue; Scale = previousValue;}
        }
        
        // change Mig & Self jointly
        if ( !migFixed || !selfFixed )
        {
            previousValue = Mig;
            previousValue2 = Self;
            ratio =1;
            if ( !migFixed) {Mig = previousValue*exp(Tune*gauss()); ratio *= migRatio(previousValue, Mig);}
            if ( !selfFixed ) {Self = previousValue2*exp(Tune*gauss()); ratio *= selfRatio(previousValue2, Self);}
            if( Mig>mMig && Mig<MMig && Self>mSelf && Self<MSelf)
            {
                nextLogLik = logLik();
                if( accept() < exp(nextLogLik-previousLogLik) * ratio)
                    previousLogLik = nextLogLik;
                else {Mig = previousValue; Self = previousValue2;}
            }
            else {Mig = previousValue; Self = previousValue2;}
        }
        
        
        //        EK - 2019/11    //////////////////////////////////
        //        Ajout des effets fixes des covariables
        //        //////////////////////////////////////////////////
        
        
        //        RJMCMC add and remove fixed effects on Vec_Fecs
        dim = 0;
        for (int v=0; v<Nvar; v++) {
            if ( (PriorIn_FixedEff[dim][v] > 0.0) && (PriorIn_FixedEff[dim][v] <1.0)  && (In_FixedEff[dim][v]) )
            {
                In_FixedEff[dim][v]=false;
                previousValue = Vec_FixedEff[dim][v];
                Vec_FixedEff[dim][v]=0;
                for (int m=0; m<NPar; m++) Vec_Fecs_All[m] *= exp(-previousValue * Covar[m][v]);
                nextLogLik = logLik();
                if( accept() < exp(nextLogLik-previousLogLik) ) previousLogLik = nextLogLik;
                else
                {
                    In_FixedEff[dim][v]=true;
                    Vec_FixedEff[dim][v] = previousValue;
                    for (int m=0; m<NPar; m++) Vec_Fecs_All[m] *= exp(previousValue * Covar[m][v] );
                }
            }
            else if ( (PriorIn_FixedEff[dim][v] > 0.0) && (PriorIn_FixedEff[dim][v] <1.0) )
            {
                In_FixedEff[dim][v]=true;
                Vec_FixedEff[dim][v]= qMean_FixedEff[dim][v] + qSD_FixedEff[dim][v] * gauss();
                for (int m=0; m<NPar; m++) Vec_Fecs_All[m] *= exp( Vec_FixedEff[dim][v] * Covar[m][v] );
                nextLogLik = logLik();
                if( accept() < exp(nextLogLik-previousLogLik) ) previousLogLik = nextLogLik;
                else
                {
                    In_FixedEff[dim][v]=false;
                    Vec_FixedEff[dim][v] = 0;
                    for (int m=0; m<NPar; m++) Vec_Fecs_All[m] *= exp( -Vec_FixedEff[dim][v] * Covar[m][v] );
                }
            }
        }
        
        //        RJMCMC add and remove fixed effects on Vec_Fecp
        if ( !gamaFixed || GamA>0.00001)
        {
            dim = 1;
            for (int v=0; v<Nvar; v++){
                if ( (PriorIn_FixedEff[dim][v] > 0.0) && (PriorIn_FixedEff[dim][v] <1.0)  && (In_FixedEff[dim][v]))
                {
                    In_FixedEff[dim][v]=false;
                    previousValue = Vec_FixedEff[dim][v];
                    Vec_FixedEff[dim][v]=0;
                    for (int m=0; m<NPar; m++) Vec_Fecp_All[m] *= exp(-previousValue * Covar[m][v]);
                    nextLogLik = logLik();
                    if( accept() < exp(nextLogLik-previousLogLik) ) previousLogLik = nextLogLik;
                    else
                    {
                        In_FixedEff[dim][v]=true;
                        Vec_FixedEff[dim][v] = previousValue;
                        for (int m=0; m<NPar; m++) Vec_Fecp_All[m] *= exp(previousValue * Covar[m][v]);
                    }
                }
                else if ( (PriorIn_FixedEff[dim][v] > 0.0) && (PriorIn_FixedEff[dim][v] <1.0) )
                {
                    In_FixedEff[dim][v]=true;
                    Vec_FixedEff[dim][v]= qMean_FixedEff[dim][v] + qSD_FixedEff[dim][v] * gauss();
                    for (int m=0; m<NPar; m++) Vec_Fecp_All[m] *= exp( Vec_FixedEff[dim][v] * Covar[m][v]);
                    nextLogLik = logLik();
                    if( accept() < exp(nextLogLik-previousLogLik) ) previousLogLik = nextLogLik;
                    else
                    {
                        In_FixedEff[dim][v]=false;
                        Vec_FixedEff[dim][v] = 0;
                        for (int m=0; m<NPar; m++) Vec_Fecp_All[m] *= exp( -Vec_FixedEff[dim][v] * Covar[m][v]);
                    }
                }
            }
        }
        
        //        RJMCMC add and remove fixed effects on Vec_FreqLDD
        dim = 2;
        for (int v=0; v<Nvar; v++){
            if ( (PriorIn_FixedEff[dim][v] > 0.0) && (PriorIn_FixedEff[dim][v] <1.0)  && (In_FixedEff[dim][v]))
            {
                In_FixedEff[dim][v]=false;
                previousValue = Vec_FixedEff[dim][v];
                Vec_FixedEff[dim][v]=0;
                for (int m=0; m<NPar; m++) {
                    temp = AlphaLDD + Vec_FreqLDD[m];
                    for (int v=0; v<Nvar; v++) temp += Vec_FixedEff[dim][v] * Covar[m][v];
                    Vec_FreqLDD_All[m] = exp( temp ) / ( 1 + exp( temp ) );
                }
                nextLogLik = logLik();
                if( accept() < exp(nextLogLik-previousLogLik) ) previousLogLik = nextLogLik;
                else
                {
                    In_FixedEff[dim][v]=true;
                    Vec_FixedEff[dim][v] = previousValue;
                    for (int m=0; m<NPar; m++) {
                        temp = AlphaLDD + Vec_FreqLDD[m];
                        for (int v=0; v<Nvar; v++) temp += Vec_FixedEff[dim][v] * Covar[m][v];
                        Vec_FreqLDD_All[m] = exp( temp ) / ( 1 + exp( temp ) );
                    }
                }
            }
            else if ( (PriorIn_FixedEff[dim][v] > 0.0) && (PriorIn_FixedEff[dim][v] <1.0) )
            {
                In_FixedEff[dim][v]=true;
                Vec_FixedEff[dim][v]= qMean_FixedEff[dim][v] + qSD_FixedEff[dim][v] * gauss();
                for (int m=0; m<NPar; m++) {
                    temp = AlphaLDD + Vec_FreqLDD[m];
                    for (int v=0; v<Nvar; v++) temp += Vec_FixedEff[dim][v] * Covar[m][v];
                    Vec_FreqLDD_All[m] = exp( temp ) / ( 1 + exp( temp ) );
                }
                nextLogLik = logLik();
                if( accept() < exp(nextLogLik-previousLogLik) ) previousLogLik = nextLogLik;
                else
                {
                    In_FixedEff[dim][v]=false;
                    Vec_FixedEff[dim][v] = 0;
                    for (int m=0; m<NPar; m++) {
                        temp = AlphaLDD + Vec_FreqLDD[m];
                        for (int v=0; v<Nvar; v++) temp += Vec_FixedEff[dim][v] * Covar[m][v];
                        Vec_FreqLDD_All[m] = exp( temp ) / ( 1 + exp( temp ) );
                    }
                }
            }
        }
        
        
        //        change the fixed effects on Vec_Fecs
        dim = 0;
        inRange=true;
        for (int v=0; v<Nvar; v++){
            Vec_Previous[v]=Vec_FixedEff[dim][v];
            if ((PriorIn_FixedEff[dim][v] > 0) && (In_FixedEff[dim][v]))
            {
                Vec_FixedEff[dim][v] += fineTune * gauss();
                inRange = inRange && (Vec_FixedEff[dim][v] > mFixedEff[dim][v]) && (Vec_FixedEff[dim][v] < MFixedEff[dim][v]);
                //                ratio *= covarRatio[dim][v](Vec_Previous[v], Vec_FixedEff[dim][v])
            }
        }
        if (inRange)
        {
            for (int m=0; m<NPar; m++)
            {
                temp = 0;
                for (int v=0; v<Nvar; v++) temp += Vec_FixedEff[dim][v] * Covar[m][v];
                Vec_Fecs_All[m] = Vec_Fecs[m] * exp( temp );
            }
            nextLogLik = logLik();
            if( accept() < exp(nextLogLik-previousLogLik) ) previousLogLik = nextLogLik;
            else
            {
                for (int v=0; v<Nvar; v++) Vec_FixedEff[dim][v] = Vec_Previous[v];
                for (int m=0; m<NPar; m++)
                {
                    temp = 0;
                    for (int v=0; v<Nvar; v++) temp += Vec_FixedEff[dim][v] * Covar[m][v];
                    Vec_Fecs_All[m] = Vec_Fecs[m] * exp( temp );
                }
            }
        }
        else
        {
            for (int v=0; v<Nvar; v++) Vec_FixedEff[dim][v] = Vec_Previous[v];
        }
        
        //        change the fixed effects on Vec_Fecp
        if ( !gamaFixed || GamA>0.00001)
        {
            dim = 1;
            inRange=true;
            for (int v=0; v<Nvar; v++){
                Vec_Previous[v]=Vec_FixedEff[dim][v];
                if ((PriorIn_FixedEff[dim][v] > 0) && (In_FixedEff[dim][v]))
                {
                    Vec_FixedEff[dim][v] += fineTune * gauss();
                    inRange = inRange && (Vec_FixedEff[dim][v] > mFixedEff[dim][v]) && (Vec_FixedEff[dim][v] < MFixedEff[dim][v]);
                    //                ratio *= covarRatio[dim][v](Vec_Previous[v], Vec_FixedEff[dim][v])
                }
            }
            if (inRange)
            {
                for (int m=0; m<NPar; m++)
                {
                    temp = 0;
                    for (int v=0; v<Nvar; v++) temp += Vec_FixedEff[dim][v] * Covar[m][v];
                    Vec_Fecp_All[m] = Vec_Fecp[m] * exp( temp );
                }
                nextLogLik = logLik();
                if( accept() < exp(nextLogLik-previousLogLik) ) previousLogLik = nextLogLik;
                else
                {
                    for (int v=0; v<Nvar; v++) Vec_FixedEff[dim][v] = Vec_Previous[v];
                    for (int m=0; m<NPar; m++)
                    {
                        temp = 0;
                        for (int v=0; v<Nvar; v++) temp += Vec_FixedEff[dim][v] * Covar[m][v];
                        Vec_Fecp_All[m] = Vec_Fecp[m] * exp( temp );
                    }
                }
            }
            else
            {
                for (int v=0; v<Nvar; v++) Vec_FixedEff[dim][v] = Vec_Previous[v];
            }
        }
        //AlphaLDD - INTERCEPT on Vec_FreqLDD logit model
        if ( !alphalddFixed )
        {
            previousValue = AlphaLDD;
            AlphaLDD += fineTune * gauss() ;
            if( AlphaLDD>mAlphaLDD && AlphaLDD<MAlphaLDD )
            {
                for (int m=0; m<NPar; m++)
                {
                    temp = AlphaLDD + Vec_FreqLDD[m];
                    for (int v=0; v<Nvar; v++) temp += Vec_FixedEff[dim][v] * Covar[m][v];
                    Vec_FreqLDD_All[m] = exp( temp ) / ( 1 + exp( temp ) );
                }
                nextLogLik = logLik();
                //                cout << ite << " change alphaLDD " << alphaLDD << " " << previousValue << " : " << nextLogLik << " vs. " << previousLogLik << endl;
                if( accept() < exp(nextLogLik-previousLogLik) * alphalddRatio(previousValue, AlphaLDD))  previousLogLik = nextLogLik;
                else
                {
                    AlphaLDD = previousValue;
                    for (int m=0; m<NPar; m++)
                    {
                        temp = AlphaLDD + Vec_FreqLDD[m];
                        for (int v=0; v<Nvar; v++) temp += Vec_FixedEff[dim][v] * Covar[m][v];
                        Vec_FreqLDD_All[m] = exp( temp ) / ( 1 + exp( temp ) );
                    }
                }
            }
        }
        
        //         if(thinStep) *ParamFec << lastFreqLDDLogLik << " " << AlphaLDD << " " << BetaLDD << endl;
        
        //        change the fixed effects on Vec_FreqLDD
        dim = 2;
        inRange=true;
        for (int v=0; v<Nvar; v++){
            Vec_Previous[v]=Vec_FixedEff[dim][v];
            if ((PriorIn_FixedEff[dim][v] > 0) && (In_FixedEff[dim][v]))
            {
                Vec_FixedEff[dim][v] += fineTune * gauss();
                inRange = inRange && (Vec_FixedEff[dim][v] > mFixedEff[dim][v]) && (Vec_FixedEff[dim][v] < MFixedEff[dim][v]);
                //                ratio *= covarRatio[dim][v](Vec_Previous[v], Vec_FixedEff[dim][v])
            }
        }
        if (inRange)
        {
            for (int m=0; m<NPar; m++)
            {
                temp = AlphaLDD + Vec_FreqLDD[m];
                for (int v=0; v<Nvar; v++) temp += Vec_FixedEff[dim][v] * Covar[m][v];
                Vec_FreqLDD_All[m] = exp( temp ) / ( 1 + exp( temp ) );
            }
            nextLogLik = logLik();
            if( accept() < exp(nextLogLik-previousLogLik) ) previousLogLik = nextLogLik;
            else
            {
                for (int v=0; v<Nvar; v++) Vec_FixedEff[dim][v] = Vec_Previous[v];
                for (int m=0; m<NPar; m++)
                {
                    temp = AlphaLDD + Vec_FreqLDD[m];
                    for (int v=0; v<Nvar; v++) temp += Vec_FixedEff[dim][v] * Covar[m][v];
                    Vec_FreqLDD_All[m] = exp( temp ) / ( 1 + exp( temp ) );
                }
            }
        }
        else
        {
            for (int v=0; v<Nvar; v++) Vec_FixedEff[dim][v] = Vec_Previous[v];
        }
        
        
        
        if(thinStep && (ite%thinStep == 0))
            //        if(thinStep)
        {
            *ParamDisp << ite<< " " << previousLogLik
            << " " << Scales
            << " " << Shapes
            << " " << ScalesLDD
            << " " << ShapesLDD
            << " " << AlphaLDD
            << " " << BetaLDD
            << " " << Migs
            << " " << Scale
            << " " << Shape
            << " " << Mig
            << " " << Self;
            
            for (int dim=0; dim<3; dim++)
                for (int v=0; v<Nvar; v++)
                    *ParamDisp << " " << In_FixedEff[dim][v] ;
            for (int dim=0; dim<3; dim++)
                for (int v=0; v<Nvar; v++)
                    *ParamDisp << " " << Vec_FixedEff[dim][v] ;
            *ParamDisp <<endl;
            ParamDisp->flush();
        }
        
        if ( thinStep && (ite==1 || ite==10 || ite == 100))
        {c_end = clock();
            cout << endl << ite << " iterations last: " << 1000.0 * (c_end-c_start) / CLOCKS_PER_SEC << " ms" << endl;
        }
    }
}

void MEMM_seedlings_2kernels_covar::mcmc_dyn(int nbIteration, int thinStep)
{
  cerr<<"seedlings 2kernels dynamic mcmc not implemented yet"<<endl;
  exit(0);
}


/***************************** PROTECTED ********************************/

long double MEMM_seedlings_2kernels_covar::logLik(int posterior)
{
    
    long double liktemp = 0;
    double pip = 0;
    double pip2 = 0;
    int pbm = 0;
    
    vector < vector<long double> > matp (NPar, vector <long double> (NPar, 0.) );
    vector < vector<long double> > mats (Ns, vector <long double> (NPar, 0.) );
    vector <long double> totp (NPar);
    vector <long double> tots (Ns);
    
    if (posterior == 3) cout << endl << "Totp(m) :";
    for (int m=0; m<NPar; m++)
    {
        totp[m]=0;
        for (int p=0; p<m; p++)
        {
            matp[m][p]=exp(abp*pow(DistMP[m][p],Shape))*Vec_Fecp_All[p];
            if (isnan(matp[m][p])){matp[m][p]=0; pbm=1;}
            totp[m]+=matp[m][p];
        }
        for (int p=m+1; p<NPar; p++)
        {
            matp[m][p]=exp(abp*pow(DistMP[m][p],Shape))*Vec_Fecp_All[p];
            if (isnan(matp[m][p])){matp[m][p]=0; pbm=1;}
            totp[m]+=matp[m][p];
        }
        matp[m][m]=Self*totp[m]/(1-Mig-Self);
        if (posterior == 3) cout << " " << totp[m];
    }
    
    if (posterior == 3) cout << endl << "Tots(s) :";
    for (int s=0; s<Ns; s++)
    {
        tots[s]=0;
        for (int m=0; m<NPar; m++)
        {
            //mats[s][m]=((1-Vec_FreqLDD[m])*exp(abs*pow(DistSM[s][m],Shapes))/Knorm+(Vec_FreqLDD[m])*exp(absLDD*pow(DistSM[s][m],ShapesLDD))/KnormLDD)*Vec_Fecs[m];
            
            mats[s][m]=Vec_Fecs_All[m]*
            ((1-Vec_FreqLDD_All[m])*exp(abs*pow(DistSM[s][m],Shapes))*Knorm +
             (Vec_FreqLDD_All[m])*exp(-pow((log(DistSM[s][m])-log(ScalesLDD)+ShapesLDD/2),2)/2/ShapesLDD)/DistSM[s][m]/DistSM[s][m]/pow(2*3.14*ShapesLDD,0.5));
            // To Use a mixture of exponential power with bivariate log-normal
            // CARE: Modifications of the 2D-log-normale... ScaleLDD is now the mean dispersl distance
            
            if (isnan(mats[s][m])){mats[s][m]=0; pbm=1;}
            tots[s]+=mats[s][m];
        }
        if (posterior == 3) cout << " " << tots[s];
    }
    
    if (pbm==1) { cerr << " Warning: Overflow. One component of the dispersal matrix was not defined. " << endl;}
    
    if (posterior == 3) cout << endl << "Liktemp :";
    for (int s=0; s<Ns; s++)
    {
        if( ProbMig[s]>0 && tots[s]>0)
        {
            pip=0;pip2=0;
            for (int m=0; m<NbMeres[s]; m++)
            {
                pip += (ProbMeres[s][m].pere_prob)*mats[s][ProbMeres[s][m].pere_pot];
            }
            for (int m=0; m<NbCouples[s]; m++)
            {
                pip2 += (ProbCouples[s][m].couple_prob)*mats[s][ProbCouples[s][m].mere_pot]*matp[ProbCouples[s][m].mere_pot][ProbCouples[s][m].pere_pot]/totp[ProbCouples[s][m].mere_pot];
            }
            if (posterior == 3) cout << " " << ProbMig[s]*Migs+(1-Migs)/tots[s]*(pip*Mig+(1-Mig-Self)*pip2);
            liktemp += log(ProbMig[s]*Migs+(1-Migs)/tots[s]*(pip*Mig+(1-Mig-Self)*pip2));
        }
    }
    
    if (posterior == 3) cout << endl << "Liktemp = " << liktemp << endl;
    return liktemp;
    
}


/*************** INIT ******************/

void MEMM_seedlings_2kernels_covar::loadParentFile(Configuration * p)
{
    
    MEMM::loadParentFile(p);
    
    char * temp;
    
    Nvar = Nqt;
    
    temp = p->getValue(MEMM_COVAR_NUMBER);
    if(temp) Nvar = atoi(temp);
    if(temp) delete []temp;
    
    Covar.resize( NPar, vector<double>( Nvar,0.) );
    
    cout <<endl << "Loading the covariates..."<<endl;
    for (int p=0; p<NPar; p++)
    {
        for (int v=0; v<Nvar; v++)
            Covar[p][v]=allParents[p]->getCoVarQuanti(v);
    }
    cout<<endl;
    
    Vec_FixedEff.resize( 3, vector<double>( Nvar, 0.) );
    mFixedEff.resize( 3, vector<double>( Nvar, -100.) );
    MFixedEff.resize( 3, vector<double>( Nvar, 100.) );
    PriorIn_FixedEff.resize( 3, vector<double>( Nvar, 0.5) );
    In_FixedEff.resize( 3, vector<bool>( Nvar, true) );
    qMean_FixedEff.resize( 3, vector<double>( Nvar, 0.) );
    qSD_FixedEff.resize( 3, vector<double>( Nvar, 1.) );
    
    temp = p->getValue(MEMM_PRIOR_COVAR_FILE);
    ifstream inputPriorCovar(temp);
    if(inputPriorCovar.good())
    {
        cout << endl<<"# Loading priors for covariate effects from file " <<temp<<" ..."<<endl;
        if(temp) delete []temp;
        
        int v;
        for(v=0; v<Nvar && inputPriorCovar.good() ; v++)
        {
            for (int dim=0; dim<3; dim++){
                inputPriorCovar >> PriorIn_FixedEff[dim][v];
                inputPriorCovar >> Vec_FixedEff[dim][v];
                inputPriorCovar >> mFixedEff[dim][v];
                inputPriorCovar >> MFixedEff[dim][v];
                inputPriorCovar >> qMean_FixedEff[dim][v];
                inputPriorCovar >> qSD_FixedEff[dim][v];
            }
        }
        if(v != Nvar && v>0)
            for(v=1; v<Nvar ; v++)
            {
                for (int dim=0; dim<3; dim++){
                    PriorIn_FixedEff[dim][v] = PriorIn_FixedEff[dim][0];
                    Vec_FixedEff[dim][v] = Vec_FixedEff[dim][0];
                    mFixedEff[dim][v] = mFixedEff[dim][0];
                    MFixedEff[dim][v] = MFixedEff[dim][0];
                    qMean_FixedEff[dim][v] = qMean_FixedEff[dim][0];
                    qSD_FixedEff[dim][v] = qSD_FixedEff[dim][0];
                }
            }
        inputPriorCovar.close();
    }
    else
    {   cout << endl<<" File " <<temp<< " unavailable for loading priors of covaraite effects: default values are used instead ..." << endl;}
    
    
    
    for (int dim=0; dim<3; dim++)
        for (int v=0; v<Nvar; v++)
        {
            if (PriorIn_FixedEff[dim][v]==0) In_FixedEff[dim][v]=false;
            cout << dim << "(" << v << ") = " << Vec_FixedEff[dim][v] << " | ";
        }
    cout <<endl << "   covariates loaded OK..."<<endl;
    
    
}


void MEMM_seedlings_2kernels_covar::loadParameters(Configuration *p)
{

  MEMM_seedlings::loadParameters(p);

  Parameter * param_temp;
    char * temp;

  cout<< "Load more and more Parameters..."<<endl;

  // alphaLDD
  param_temp = allParameters["alphaLDD"];
  if(param_temp)
  {
    AlphaLDD = param_temp->INIT;
    mAlphaLDD = param_temp->MIN;
    MAlphaLDD = param_temp->MAX;
    cout << "AlphaLDD : "<< AlphaLDD << " [" <<mAlphaLDD <<"|"<< MAlphaLDD << "]"<<endl;
  }

  // betaLDD
  param_temp = allParameters["betaLDD"];
  if(param_temp)
  {
    BetaLDD = param_temp->INIT;
    mBetaLDD = param_temp->MIN;
    MBetaLDD = param_temp->MAX;
    cout << "BetaLDD : "<< BetaLDD << " [" <<mBetaLDD <<"|"<< MBetaLDD << "]"<<endl;
  }
    
   
  // ScalesLDD 
  param_temp = allParameters["scalesLDD"];
  if(param_temp)
  {
    ScalesLDD = param_temp->INIT;
    mScalesLDD = param_temp->MIN;
    MScalesLDD = param_temp->MAX;
    cout << "ScalesLDD : "<< ScalesLDD << " [" <<mScalesLDD <<"|"<< MScalesLDD << "]"<<endl;
  }
  
  // shapesLDD
  param_temp = allParameters["shapesLDD"];
  if(param_temp)
  {
    ShapesLDD = param_temp->INIT;
    mShapesLDD = param_temp->MIN;
    MShapesLDD = param_temp->MAX;
    cout << "ShapesLDD : "<< ShapesLDD << " [" <<mShapesLDD <<"|"<< MShapesLDD << "]"<<endl;
  }

//  constants for an exponential power
//  asLDD=ScalesLDD*exp(gammln(2/ShapesLDD)-gammln(3/ShapesLDD));
//  absLDD=-pow(asLDD,-ShapesLDD);
//  KnormLDD = ShapesLDD/asLDD/asLDD/exp(gammln(2/ShapesLDD));
    
// Constants for a log-normal
  asLDD = log(ScalesLDD) - ShapesLDD/2; // equal to log(r_m)
//  absLDD=-pow(asLDD,-ShapesLDD);
  KnormLDD = 1/pow( 2*3.14*ShapesLDD , 0.5 ) ; // equal to 1/sqrt(2 pi sigma2)

// Constant for the SDD kernel => necessary because of the mixture model kernel.
  Knorm = Shapes/as/as/exp(gammln(2/Shapes));

  Vec_FreqLDD.resize(NPar, 0. );
  Vec_FreqLDD_All.resize(NPar, 0.1 );
    
    Vec_Fecs_All.resize(NPar, 1. );
    Vec_Fecp_All.resize(NPar, 1. );

    
  pLoiLDD = loadDistribution(p,"individual_frequencies_LDD");
    
    outputDirectory = string(p->getValue(MEMM_DIRECTORY_PATH));
    DIR * dir;
    if( (dir = opendir(outputDirectory.c_str())) != NULL )
    {
      cout << "\t-" << outputDirectory <<endl;
      closedir(dir);
    }
    else {
      cerr<<"#ERROR opening directory "<<outputDirectory <<endl;
      exit(1);
    }
  
    stringstream ss;
     ss << outputDirectory << "/";
    temp = p->getValue(MEMM_IND_FEC_FILE_NAME);
    ss.str("");
    ss << outputDirectory << "/" << temp << "_All.txt";
    IndivFecAll = new std::ofstream(ss.str(), ofstream::out);
    if(IndivFecAll->good()) cout<<"\t-"<<temp<<endl;
    else cerr<<"#ERROR openning "<<temp<<" file"<<endl;
    if(temp) delete []temp;

    
  cout<<"end loading Parameters"<<endl;

}


