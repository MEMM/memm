/**************************************************************************************************************
 * memm is a bayesian estimation of pollen dispersal kernel and variance in male fecundity.                   *
 * copyright (c) 2009-2016 etienne klein <etienne.klein@paca.inra.fr>                                         *
 *                                                                                                            *
 * this file is part of memm.                                                                                 *
 *                                                                                                            *
 * memm is free software: you can redistribute it and/or modify                                               *
 * it under the terms of the gnu general public license as published by                                       *
 * the free software foundation, either version 3 of the license, or                                          *
 * (at your option) any later version.                                                                        *
 *                                                                                                            *
 * memm is distributed in the hope that it will be useful,                                                    *
 * but without any warranty; without even the implied warranty of                                             *
 * merchantability or fitness for a particular purpose.  see the                                              *
 * gnu general public license for more details.                                                               *
 *                                                                                                            *
 * you should have received a copy of the gnu general public license                                          *
 * along with memm.  if not, see <http://www.gnu.org/licenses/>.                                              *
 **************************************************************************************************************/

#include "Configuration.hpp"

Configuration::Configuration(): isOK_(false){}

Configuration::Configuration(const char * filein) {
  ifstream file;
  file.open(filein, ifstream::in);
  if(file.good())
  {
    isOK_=true;
    configurationFileName_ = new char[strlen(filein)+1];
    strcpy(configurationFileName_,filein);
  }
  else isOK_=false;

  file.close();
}

Configuration::~Configuration()
{
  if(configurationFileName_) delete []configurationFileName_;
}

bool Configuration::isOK()
{
  return isOK_;
}

vector<string> Configuration::getPriorsName()
{
  return prior_name;

}

map<string,vector<string>> Configuration::getDistributionLawsNameAndParametersName()
{
  return MEMM_loi::laws_name_and_parameters;
}

