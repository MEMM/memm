#include "Console.hpp"

Console::Console(){}

Console::Console(QWidget *parent) : QWidget(parent)
{
  isRunning = false;

  buttonLineContainer = new QHBoxLayout();
  startStopButton = new QPushButton(tr("RUN"));
  startStopButton->setStyleSheet("background-color: green; font-weight: bold;");
  startStopButton->setSizePolicy(QSizePolicy::Maximum,QSizePolicy::Minimum);
  connect(startStopButton,SIGNAL(clicked()),this,SLOT(runProcess()));
  //connect(startStopButton,SIGNAL(clicked()),parent,SLOT(actionSaveFile()));
  
  buttonLineContainer->addWidget(startStopButton);
  buttonLineContainer->setSizeConstraint(QLayout::SetMinAndMaxSize);

  buttonContainer = new QGroupBox();
  buttonContainer->setStyleSheet("QGroupBox {border:none; margin:0px; padding:0px;}");
  buttonContainer->setLayout(buttonLineContainer);

  console = new QPlainTextEdit();
  console->document()->setMaximumBlockCount(0);
  QPalette p = palette();
  p.setColor(QPalette::Base, Qt::black);
  p.setColor(QPalette::Text, Qt::green);
  console->setPalette(p);
  console->setReadOnly(true);

  mainContainer = new QVBoxLayout();
  mainContainer->addWidget(buttonContainer);
  mainContainer->addWidget(console);
  mainContainer->setAlignment(Qt::AlignTop);
  mainContainer->setSizeConstraint(QLayout::SetMinAndMaxSize);

  setLayout(mainContainer);

  // Create QProcess
  process = new QProcess(this);
  process->setProcessChannelMode(QProcess::MergedChannels);
  connect(process, SIGNAL(readyRead()),this,SLOT(writeConsole()));
  connect(process, SIGNAL(finished(int)),this,SLOT(stopProcess(int)));
  connect(process, &QProcess::errorOccurred, [=](QProcess::ProcessError error)
      {
        cerr<< "ERROR occured... error value = " << error << endl;
        emit stopProcess(error);
      });
  process->setProgram("MEMM");

  initConsole();

}

Console::~Console(){}

void Console::setExec(QString exec)
{
  process->setProgram(exec);
  initConsole();
}

void Console::setParameters(QStringList param)
{
  process->setArguments(param);
  initConsole();
}

void Console::setRunMode(bool state)
{
  startStopButton->setEnabled(state);

  if(state){
    startStopButton->setStyleSheet("QPushButton{ background-color: green; font-weight: bold;} QToolTip{ color: white; background-color: #2a82da; border 1px solid white;}");
    startStopButton->setToolTip("Run Model");
    setStatusTip(tr(""));
  }
  else
  {
    startStopButton->setStyleSheet("QPushButton{ background-color: red; color:black; font-weight: bold;} QToolTip{ color: white; background-color: #2a82da; border 1px solid white;}");
    startStopButton->setToolTip("Save parameters before RUNNING");
    setStatusTip(tr("Save parameters before RUNNING"));
  }

}

/***************** SLOT *****************/

void Console::runProcess()
{
  if(isRunning)
  {
    if(process->state() == QProcess::Running) process->kill();
    isRunning = false;
    startStopButton->setStyleSheet("background-color: green; font-weight: bold;");
    startStopButton->setText("RUN");
  }
  else
  {
    startStopButton->setStyleSheet("background-color: red; font-weight: bold;");
    startStopButton->setText("STOP");
    isRunning = true;

    initConsole();
    process->start();

    if(!process->waitForStarted()) writeConsole("ERROR Failed to Start.\n");
  }

}

void Console::stopProcess(int status)
{
  if(status && !process->exitStatus()) cerr << "ERROR Console Process Crash. value =  "<<process->exitStatus()<<endl;
  isRunning = false;
  startStopButton->setStyleSheet("background-color: green; font-weight: bold;");
  startStopButton->setText("RUN");
}

void Console::writeConsole()
{
  console->moveCursor(QTextCursor::End);
  console->insertPlainText(QString::fromUtf8(process->readAll()));
  console->moveCursor(QTextCursor::End);
}
/******************************** PRIVATE *********************************/
void Console::writeConsole(QString st)
{
  console->moveCursor(QTextCursor::End);
  console->insertPlainText(st);
  console->moveCursor(QTextCursor::End);
}

void Console::cleanConsole()
{
  console->setPlainText("$> ");
}

void Console::initConsole()
{
  cleanConsole();
  QString prompt = process->program();
  prompt.append(" ");
  prompt.append(process->arguments().join(" "));
  prompt.append("\n");
  writeConsole(prompt);
}
