/**************************************************************************************************************
 * memm is a bayesian estimation of pollen dispersal kernel and variance in male fecundity.                   *
 * copyright (c) 2009-2016 etienne klein <etienne.klein@paca.inra.fr>                                         *
 *                                                                                                            *
 * this file is part of memm.                                                                                 *
 *                                                                                                            *
 * memm is free software: you can redistribute it and/or modify                                               *
 * it under the terms of the gnu general public license as published by                                       *
 * the free software foundation, either version 3 of the license, or                                          *
 * (at your option) any later version.                                                                        *
 *                                                                                                            *
 * memm is distributed in the hope that it will be useful,                                                    *
 * but without any warranty; without even the implied warranty of                                             *
 * merchantability or fitness for a particular purpose.  see the                                              *
 * gnu general public license for more details.                                                               *
 *                                                                                                            *
 * you should have received a copy of the gnu general public license                                          *
 * along with memm.  if not, see <http://www.gnu.org/licenses/>.                                              *
 **************************************************************************************************************/

#include "MainWindow.hpp"

MainWindow::MainWindow(ConfigXML * p) : QMainWindow()
{
    haveToSave = false;
    advancedMode = false;

    param = p;
    //setFixedSize(300,150);

    setWindowTitle("MEMM configuration");
    setMinimumSize(DEFAULT_WIDTH,DEFAULT_HEIGHT);
    createMenuAndToolBar();
    addConsoleDock();
    setMainWidget(p);
    initValue(param);
}


MainWindow::~MainWindow()
{
}

void MainWindow::createMenuAndToolBar()
{
    // create a menu File
    QMenu * menuFile = menuBar()->addMenu("&File");

    // create an open, save, save as and quit actions
    QAction * actionOpen = new QAction("&Open",this);
    menuFile->addAction(actionOpen);
    QAction * actionSave = new QAction("&Save",this);
    menuFile->addAction(actionSave);
    QAction * actionSaveAs = new QAction("&Save As...",this);
    menuFile->addAction(actionSaveAs);
    QAction * actionQuit = new QAction("&Quit",this);
//    actionQuit->setIcon(QIcon("quit.png"));
    menuFile->addAction(actionQuit);

    QMenu * menuConsole = menuBar()->addMenu("&Console");
    QAction * actionProg = new QAction("&Program",this);
    menuConsole->addAction(actionProg);
    QAction * actionConsole = new QAction("&Console",this);
    menuConsole->addAction(actionConsole);
  
    QMenu * menuInfo = menuBar()->addMenu("&Info");
    QAction * actionAbout = new QAction("&About",this);
    menuInfo->addAction(actionAbout);

    // create a toolbar using previous actions
    QToolBar * toolBarOpenFile = addToolBar("&Open");
    toolBarOpenFile->addAction(actionOpen);
    QToolBar * toolBarSaveFile = addToolBar("&Save");
    toolBarSaveFile->addAction(actionSave);
    QToolBar * toolBarSaveFileAs = addToolBar("&Save As");
    toolBarSaveFileAs->addAction(actionSaveAs);
    QToolBar * toolBarQuit = addToolBar("&Quit");
    toolBarQuit->addAction(actionQuit);
    QToolBar * toolBarConsole = addToolBar("&Console");
    toolBarConsole->addAction(actionConsole);
    QToolBar * toolBarAdMode = addToolBar("&Advanced Mode");
    QAction * actionAdMode = new QAction("&Advanced Mode",this);
    toolBarAdMode->addAction(actionAdMode);


    // Connect signals and slots on action
    connect(actionOpen, SIGNAL(triggered()), this, SLOT(actionOpenFile()));
    connect(actionSave, SIGNAL(triggered()), this, SLOT(actionSaveFile()));
    connect(actionSaveAs, SIGNAL(triggered()), this, SLOT(actionSaveFileAs()));
    connect(actionQuit, SIGNAL(triggered()), this, SLOT(actionQuit()));
    connect(actionProg, SIGNAL(triggered()), this, SLOT(actionProg()));
    connect(actionConsole, SIGNAL(triggered()), this, SLOT(actionConsole()));
    connect(actionAbout, SIGNAL(triggered()), this, SLOT(actionAbout()));
    connect(actionAdMode, SIGNAL(triggered()), this, SLOT(actionAdMode()));


    statusBarInfo = statusBar();
    setStatusBar(statusBarInfo);
}

void MainWindow::setParam(ConfigXML * p)
{
    if(p != NULL) param = p;
    initValue(param);
}

void MainWindow::cleanInterface()
{
//  iPW->clean();
  pPW->clean();
//  oPW->clean();
//  opPW->clean();
}

QWidget * MainWindow::setMainWidget(ConfigXML * p)
{
    // create the main tab
    tabs = new QTabWidget();

    mPW = new ModelsParamWidget(p);
    iPW = new InputParamWidget(p);
    pPW = new ParamParamWidget(p);
    oPW = new OutputParamWidget(p);
    opPW = new OptionsParamWidget(p);

    tabs->addTab(mPW,"Models");
    tabs->addTab(opPW,"Options");
    tabs->addTab(iPW,"Input");
    tabs->addTab(pPW,"Parametres");
    tabs->addTab(oPW,"Output");

    QMainWindow::setCentralWidget(tabs);

    connect(mPW, SIGNAL(valueChanged(const QString &)), this, SLOT(valueChanged(const QString &)));
    connect(iPW, SIGNAL(valueChanged()), this, SLOT(valueChanged()));
    connect(pPW, SIGNAL(valueChanged()), this, SLOT(valueChanged()));
    connect(oPW, SIGNAL(valueChanged()), this, SLOT(valueChanged()));
    connect(opPW, SIGNAL(valueChanged()), this, SLOT(valueChanged()));

    //connect(this, SIGNAL(adModeChanged(int)), iPW, SLOT(adModeChanged(int)));
    connect(this, SIGNAL(adModeChanged(int)), pPW, SLOT(adModeChanged(int)));
    //connect(this, SIGNAL(adModeChanged(int)), oPW, SLOT(adModeChanged(int)));
    //connect(this, SIGNAL(adModeChanged(int)), opPW, SLOT(adModeChanged(int)));


    return tabs;

}

/********************** SLOT ***********************/
void MainWindow::actionOpenFile()
{
  QString filename;

  if(haveToSave)
  {
    QMessageBox msgBox;
    msgBox.setText("The document has been modified.");
    msgBox.setInformativeText("Do you want to save it?");
    msgBox.setStandardButtons(QMessageBox::Save | QMessageBox::Ignore | QMessageBox::Cancel);
    msgBox.setDefaultButton(QMessageBox::Save);
    int ret = msgBox.exec();

    // check button signal
    switch (ret) {
        case QMessageBox::Save:
            actionSaveFileAs();
            filename = QFileDialog::getSaveFileName(this,tr("Save into File"), "~/", tr("XML files (*.xml)"));
            if(filename != NULL) 
            {
                param->saveFile(filename.toStdString());
                haveToSave = false;
            }
            break;
        case QMessageBox::Ignore:
            break;
        case QMessageBox::Cancel:
            return;
            break;
        default:
            break;
    }
  }  
    cleanInterface();

    int res = 0;
    filename = QFileDialog::getOpenFileName(this,tr("Open Parameters XML file"), "~/", tr("XML files (*.xml)"));
    if(filename != NULL)
    {
        if(param != NULL)res =  param->loadXMLConfig(filename.toStdString());
        if(res)
        {
            initValue(param);
            setWindowTitle(QString("MEMM configuration | "+QFileInfo(filename).fileName()));
            haveToSave = false;
            if(console) console->setRunMode(true);
        }
    }
}

void MainWindow::actionSaveFile()
{
    if(param->getFileName() != "")
    {
      param->save();
      haveToSave = false;
      if(console) console->setRunMode(true);
    }
    else actionSaveFileAs();
}

void MainWindow::actionSaveFileAs()
{
    int res = 0;
    QString filename = QFileDialog::getSaveFileName(this,tr("Save into File "), "~/", tr("XML files (*.xml)"));
    if(filename != NULL) 
    {
        res = param->saveFile(filename.toStdString());
        haveToSave = false;
        if(console) console->setRunMode(true);
        if(res)setWindowTitle(QString("MEMM parameters | "+QFileInfo(filename).fileName()));
        QStringList args;
        args << filename;
        console->setParameters(args);
    }
}

void MainWindow::actionQuit()
{
  if(haveToSave)
  {
    QString filename;
    QMessageBox msgBox;
    msgBox.setText("The document has been modified.");
    msgBox.setInformativeText("Do you want to save it?");
    msgBox.setStandardButtons(QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);
    msgBox.setDefaultButton(QMessageBox::Save);
    int ret = msgBox.exec();

    // check button signal
    switch (ret) {
        case QMessageBox::Save:
            actionSaveFileAs();
            filename = QFileDialog::getSaveFileName(this,tr("Save into File"), "~/", tr("XML files (*.xml)"));
            if(filename != NULL) 
            {
                param->saveFile(filename.toStdString());
                close();
            }
            break;
        case QMessageBox::Discard:
            close();
            break;
        case QMessageBox::Cancel:
            break;
        default:
            break;
    }
  }
  else close();
}

void MainWindow::actionProg()
{
  
  QString binary = QFileDialog::getOpenFileName(this,tr("MEMM binary"), "/");
  if(binary != "") console->setExec(binary);
}

void MainWindow::actionConsole()
{
  if(dock->isHidden()){
    // If first openning, do some animation ;-)
    if(dock->minimumWidth() == 10) {
      QPropertyAnimation *animation = new QPropertyAnimation(dock, "minimumWidth");
      animation->setDuration(1000);
      animation->setStartValue(0);
      animation->setEndValue(DEFAULT_WIDTH/2);
      animation->start();
    }
    dock->show();
  }
  else {
    dock->hide();
  }
}

void MainWindow::actionAbout()
{
    QMessageBox::about(this, tr("About MEMM"),
                       tr("<h2>Mixed Effect Mating Model (MEMM)</h2> V3.0 <br/>"\
                         "<br/>This program implements several Bayesian statistical methods (using MCMC and based on Metropolis-Hasting algorithm) to estimate the pollen and seed dispersal kernels from spatial and genetic data concerning adult plants and sampled seeds.<br/>"\
                          "<br/>It was originally developed by <b>Etienne Klein</b> and <b><a href='http://jeff.biosp.org'>Jean-François Rey</a></b> under GPL V3 license " \
                          "<br/><h3>Links</h3>"\
                          "Project page : <a href='https://informatique-mia.inra.fr/biosp/MEMM'>https://informatique-mia.inra.fr/biosp/MEMM</a><br/>"\
                          "GitLab repository : <a href='https://gitlab.paca.inra.fr/MEMM/memm'>https://gitlab.paca.inra.fr/MEMM/memm</a><br/>"\
                          "Bug report : <a href='https://gitlab.paca.inra.fr/MEMM/memm/issues'>Issues</a><br/>"\
                         ));
}

void MainWindow::actionAdMode()
{
  int sb = QMessageBox::question(this, " Active Advanced Mode", "Would you like to enable advanced mode ?", QMessageBox::Yes, QMessageBox::No);
  switch(sb)
  {
    case QMessageBox::Yes :
      advancedMode = true;
      emit adModeChanged(1);
      break;
    case QMessageBox::No :
      advancedMode = false;
      emit adModeChanged(0);
      break;
  }
}

void MainWindow::valueChanged()
{
  haveToSave = true;
  if(console) console->setRunMode(false);
}

void MainWindow::valueChanged(const QString & m)
{
  pair<string,string> model_info = param->getModel(m.toStdString());
  model_info.second;
  if( param->readXMLConfig(model_info.second) )
  {
    cleanInterface();
    initValue(param);
    setWindowTitle(QString("MEMM configuration | "+m));
    haveToSave = true;
    if(console) console->setRunMode(false);
  }
}
/***************** PRIVATE *************************/
void MainWindow::initValue(ConfigXML * p)
{
    if(p == NULL) return;

    disconnect(iPW, SIGNAL(valueChanged()), this, SLOT(valueChanged()));
    disconnect(pPW, SIGNAL(valueChanged()), this, SLOT(valueChanged()));
    disconnect(oPW, SIGNAL(valueChanged()), this, SLOT(valueChanged()));
    disconnect(opPW, SIGNAL(valueChanged()), this, SLOT(valueChanged()));

    iPW->setParam(p);
    pPW->setParam(p);
    oPW->setParam(p);
    opPW->setParam(p);

    iPW->initValue();
    pPW->initValue();
    oPW->initValue();
    opPW->initValue();

    QStringList args;
    args << QString(p->getFileName().c_str());
    console->setParameters(args);
    
    connect(iPW, SIGNAL(valueChanged()), this, SLOT(valueChanged()));
    connect(pPW, SIGNAL(valueChanged()), this, SLOT(valueChanged()));
    connect(oPW, SIGNAL(valueChanged()), this, SLOT(valueChanged()));
    connect(opPW, SIGNAL(valueChanged()), this, SLOT(valueChanged()));

}

void MainWindow::addConsoleDock()
{
  dock = new QDockWidget(tr("Console"), this);
  //dock->setFeatures(QDockWidget::NoDockWidgetFeatures);
  dock->setAllowedAreas(Qt::RightDockWidgetArea);
  dock->setMinimumWidth(10);
  dock->setBaseSize(DEFAULT_WIDTH/2, DEFAULT_HEIGHT);
  console = new Console(this);
  dock->setWidget(console);
  this->addDockWidget(Qt::RightDockWidgetArea, dock);

  if(param->getFileName() == "") valueChanged(); 

  dock->hide();
}
