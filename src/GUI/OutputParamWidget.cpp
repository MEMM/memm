/**************************************************************************************************************
 * memm is a bayesian estimation of pollen dispersal kernel and variance in male fecundity.                   *
 * copyright (c) 2009-2016 etienne klein <etienne.klein@paca.inra.fr>                                         *
 *                                                                                                            *
 * this file is part of memm.                                                                                 *
 *                                                                                                            *
 * memm is free software: you can redistribute it and/or modify                                               *
 * it under the terms of the gnu general public license as published by                                       *
 * the free software foundation, either version 3 of the license, or                                          *
 * (at your option) any later version.                                                                        *
 *                                                                                                            *
 * memm is distributed in the hope that it will be useful,                                                    *
 * but without any warranty; without even the implied warranty of                                             *
 * merchantability or fitness for a particular purpose.  see the                                              *
 * gnu general public license for more details.                                                               *
 *                                                                                                            *
 * you should have received a copy of the gnu general public license                                          *
 * along with memm.  if not, see <http://www.gnu.org/licenses/>.                                              *
 **************************************************************************************************************/

/*!
 * \file OutputParamWidget.cpp
 * \brief QWidget containing output arguments
 * \author Jean-Francois REY
 * \version 1.0
 * \date 04 Dec 2013
 *
 * The OutputParamWidget class is a widget contianing argument editor for the output
 */

#include "OutputParamWidget.hpp"

OutputParamWidget::OutputParamWidget() : QWidget()
{
    // Main Layout
    vLayout = new QFormLayout();
  
    // Output Directory
    // Line edit and button
    QHBoxLayout * hLayout0;
    QPushButton * cDir0;
    hLayout0 = new QHBoxLayout();
    oLEdit0 = new QLineEdit();
    hLayout0->addWidget(oLEdit0);
    cDir0 = new QPushButton("Directory",this);
    hLayout0->addWidget(cDir0);
    vLayout->addRow(new QLabel("Output directory:"),hLayout0);
    QObject::connect(cDir0,SIGNAL(clicked()),this,SLOT(ChooseOutputDirectory()));
    QObject::connect(oLEdit0,SIGNAL(textChanged(const QString &)),this,SLOT(updateOutputDirectory(const QString &)));
    

    // Gama
    // Line edit and button
    QHBoxLayout * hLayout1;
    QPushButton * cFile1;
    hLayout1 = new QHBoxLayout();
    oLEdit1 = new QLineEdit();
    hLayout1->addWidget(oLEdit1);
    //cFile1 = new QPushButton("File",this);
    //hLayout1->addWidget(cFile1);
    vLayout->addRow(new QLabel("GamA:"),hLayout1);
    //QObject::connect(cFile1,SIGNAL(clicked()),this,SLOT(ChooseGamaFile()));
    QObject::connect(oLEdit1,SIGNAL(textChanged(const QString &)),this,SLOT(updateGamaFile(const QString &)));
    
    // Individual fecundities
    // Line edit and button
    QHBoxLayout * hLayout2;
    QPushButton * cFile2;
    hLayout2 = new QHBoxLayout();
    oLEdit2 = new QLineEdit();
    hLayout2->addWidget(oLEdit2);
    //cFile2 = new QPushButton("File",this);
    //hLayout2->addWidget(cFile2);
    vLayout->addRow(new QLabel("Individual fecundities:"),hLayout2);
    //QObject::connect(cFile2,SIGNAL(clicked()),this,SLOT(ChooseIndFecFile()));
    QObject::connect(oLEdit2,SIGNAL(textChanged(const QString &)),this,SLOT(updateIndFecFile(const QString &)));
 
    // Dispersal parameters
    // Line edit and button
    QHBoxLayout * hLayout3;
    QPushButton * cFile3;   
    hLayout3 = new QHBoxLayout();
    oLEdit3 = new QLineEdit();
    hLayout3->addWidget(oLEdit3);
    //cFile3 = new QPushButton("File",this);
    //hLayout3->addWidget(cFile3);
    vLayout->addRow(new QLabel("Dispersal parameters:"),hLayout3);
    //QObject::connect(cFile3,SIGNAL(clicked()),this,SLOT(ChooseDispParamFile()));
    QObject::connect(oLEdit3,SIGNAL(textChanged(const QString &)),this,SLOT(updateDispParamFile(const QString &)));
  
    QObject::connect(this,SIGNAL(valueChanged()),this,SLOT(checkValidity()));

    setLayout(vLayout);

}

OutputParamWidget::OutputParamWidget(ConfigXML *p) : OutputParamWidget()
{
    param = p;
}

OutputParamWidget::~OutputParamWidget(){}

void OutputParamWidget::setParam(ConfigXML * p)
{
    param = p;
}

void OutputParamWidget::initValue()
{
    // Set edit line with value from param
    oLEdit0->setText(QString(param->getValue(MEMM_DIRECTORY_PATH)));
    oLEdit1->setText(QString((param->getValue(OUTPUT_GAMA_FILE_NAME,OUTPUT_GAMA_FILE_NAME_ATTRIBUT)).c_str()));
    oLEdit2->setText(QString((param->getValue(OUTPUT_IND_FEC_FILE_NAME,OUTPUT_IND_FEC_FILE_NAME_ATTRIBUT)).c_str()));
    oLEdit3->setText(QString((param->getValue(OUTPUT_DISP_FILE_NAME,OUTPUT_DISP_FILE_NAME_ATTRIBUT)).c_str()));

    checkValidity();
}

/***************** SLOT **************************/

void OutputParamWidget::checkValidity()
{
  setStatusTip("");

  if( !directoryIsValid(oLEdit0->text().toStdString().c_str()) )
  {
    oLEdit0->setStyleSheet("border: 1px solid red;");
    setStatusTip(tr("Invalid Output Directory"));
  }
  else oLEdit0->setStyleSheet("");
}

void OutputParamWidget::ChooseOutputDirectory()
{
    QString path = QFileDialog::QFileDialog::getExistingDirectory(this, tr("Select Directory"),
                                                "~/",
                                                QFileDialog::ShowDirsOnly
                                                | QFileDialog::DontResolveSymlinks);
      
    if(path != NULL)
    {
        oLEdit0->setText(path);
        param->setValue(MEMM_DIRECTORY_PATH,path.toStdString().c_str());
        emit valueChanged();
    }
}

void OutputParamWidget::updateOutputDirectory(const QString &text)
{ 
  param->setValue(MEMM_DIRECTORY_PATH,text.toStdString().c_str());
  emit valueChanged();
}

/*
void OutputParamWidget::ChooseGamaFile()
{
    QString filename = QFileDialog::getSaveFileName(this,tr("Save File"), "~/", tr("Text files (*.txt)"));
    if(filename != NULL)
    {
        oLEdit1->setText(filename);
        param->setValue(OUTPUT_GAMA_FILE_NAME,filename.toStdString(),OUTPUT_GAMA_FILE_NAME_ATTRIBUT);
        emit valueChanged();
    }

}
*/
void OutputParamWidget::updateGamaFile(const QString & text)
{
  param->setValue(OUTPUT_GAMA_FILE_NAME,text.toStdString(),OUTPUT_GAMA_FILE_NAME_ATTRIBUT);
  emit valueChanged();
}
/*
void OutputParamWidget::ChooseIndFecFile()
{
    QString filename = QFileDialog::getOpenFileName(this,tr("Save File"), "~/", tr("Text files (*.txt)"));
    if(filename != NULL)
    {
        oLEdit2->setText(filename);
        param->setValue(OUTPUT_IND_FEC_FILE_NAME,filename.toStdString(),OUTPUT_IND_FEC_FILE_NAME_ATTRIBUT);
        emit valueChanged();
    }

}*/

void OutputParamWidget::updateIndFecFile(const QString & text)
{ 
  param->setValue(OUTPUT_IND_FEC_FILE_NAME,text.toStdString(),OUTPUT_IND_FEC_FILE_NAME_ATTRIBUT);
  emit valueChanged();
}
/*
void OutputParamWidget::ChooseDispParamFile()
{
    QString filename = QFileDialog::getSaveFileName(this,tr("Save File"), "~/", tr("Text files (*.txt)"));
    if(filename != NULL)
    {
        oLEdit3->setText(filename);
        param->setValue(OUTPUT_DISP_FILE_NAME,filename.toStdString(),OUTPUT_DISP_FILE_NAME_ATTRIBUT);
        emit valueChanged();
    }
}*/

void OutputParamWidget::updateDispParamFile(const QString & text)
{
  param->setValue(OUTPUT_DISP_FILE_NAME,text.toStdString(),OUTPUT_DISP_FILE_NAME_ATTRIBUT);
  emit valueChanged();
}
