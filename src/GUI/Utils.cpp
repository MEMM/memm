#include "Utils.hpp"

bool fileIsValid(const char *file)
{
  struct stat buffer;
  return (stat (file, &buffer) == 0);
}

bool directoryIsValid(const char *directory)
{
  struct stat buffer;
  return (stat(directory, &buffer) == 0 && S_ISDIR(buffer.st_mode));
}
