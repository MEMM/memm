/**************************************************************************************************************
 * memm is a bayesian estimation of pollen dispersal kernel and variance in male fecundity.                   *
 * copyright (c) 2009-2016 etienne klein <etienne.klein@paca.inra.fr>                                         *
 *                                                                                                            *
 * this file is part of memm.                                                                                 *
 *                                                                                                            *
 * memm is free software: you can redistribute it and/or modify                                               *
 * it under the terms of the gnu general public license as published by                                       *
 * the free software foundation, either version 3 of the license, or                                          *
 * (at your option) any later version.                                                                        *
 *                                                                                                            *
 * memm is distributed in the hope that it will be useful,                                                    *
 * but without any warranty; without even the implied warranty of                                             *
 * merchantability or fitness for a particular purpose.  see the                                              *
 * gnu general public license for more details.                                                               *
 *                                                                                                            *
 * you should have received a copy of the gnu general public license                                          *
 * along with memm.  if not, see <http://www.gnu.org/licenses/>.                                              *
 **************************************************************************************************************/


#include "ModelsParamWidget.hpp"

ModelsParamWidget::ModelsParamWidget(ConfigXML * p) : QWidget() 
{
  if(param_) param_ = p;
  else exit(1);
  
  vLayout_ = new QFormLayout();

  models_ = new QComboBox();

  description_ = new QTextEdit();
  description_->setReadOnly(true);

  
  initValue(); 

  vLayout_->addRow(new QLabel("Model:"),models_);
  QObject::connect(models_,SIGNAL(currentTextChanged(const QString &)),this,SLOT(ChangeModel(const QString &)));
  vLayout_->addRow(new QLabel("Description:"),description_);
  setLayout(vLayout_);


}

ModelsParamWidget::~ModelsParamWidget(){}

void ModelsParamWidget::ChangeModel(const QString & m)
{
  pair<string,string> model_info = param_->getModel(m.toStdString());
  description_->setText(QString::fromStdString(model_info.first));
  emit valueChanged(m);
}

void ModelsParamWidget::setParam(ConfigXML * p)
{
  param_ = p;
}

void ModelsParamWidget::initValue()
{
  map<string,pair<string,string>> models_info = param_->getModels();
  for(map<string,pair<string,string>>::iterator it = models_info.begin(); it != models_info.end(); ++it)
  {
    models_->addItem(QString::fromStdString(it->first));
  }
  ChangeModel(param_->getValue(MEMM_MODE)); 
}
