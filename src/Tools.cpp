/**************************************************************************************************************
 * memm is a bayesian estimation of pollen dispersal kernel and variance in male fecundity.                   *
 * copyright (c) 2009-2016 etienne klein <etienne.klein@paca.inra.fr>                                         *
 *                                                                                                            *
 * this file is part of memm.                                                                                 *
 *                                                                                                            *
 * memm is free software: you can redistribute it and/or modify                                               *
 * it under the terms of the gnu general public license as published by                                       *
 * the free software foundation, either version 3 of the license, or                                          *
 * (at your option) any later version.                                                                        *
 *                                                                                                            *
 * memm is distributed in the hope that it will be useful,                                                    *
 * but without any warranty; without even the implied warranty of                                             *
 * merchantability or fitness for a particular purpose.  see the                                              *
 * gnu general public license for more details.                                                               *
 *                                                                                                            *
 * you should have received a copy of the gnu general public license                                          *
 * along with memm.  if not, see <http://www.gnu.org/licenses/>.                                              *
 **************************************************************************************************************/

/*! \file Tools.cpp
 * \brief Tools
 * \author Jean-Francois Rey
 * \date 16 Dec 2013
 */

#include "Tools.hpp"

std::string Tools::convertPointToComma(std::string str)
{
    size_t pos = str.find_last_of('.');
    if(pos!=std::string::npos) str[pos]=',';    
    return str;
}

std::string Tools::convertCommaToPoint(std::string str)
{
    size_t pos = str.find_last_of(',');
    if(pos!=std::string::npos) str[pos]='.';    
    return str;
}

/*
template <typename T>
std::string Tools::to_string_with_precision(const T a_value, const int n)
{
  std::ostringstream out;
  out << std::setprecision(n) << a_value;
  return out.str();
}*/
