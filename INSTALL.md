# MEMM INSTALL

This is install instruction using source code.  
Package installation is available for tags versions and some from continious delivery on gitlab deposits.

## Windows

> ! Avoid using name and path with __space__ !

### Using msys2 (standard)

1. Download, install and launch (32 or 64 bits) [msys2](https://www.msys2.org/)  
> To use 64 bits installation replace __32__ by __64__ and __i686__ by __x86\_64__  

2. MSYS2 configuration (32 bits) :  
  2.1 ```pacman -Syu```  
  2.2 ```pacman -S mingw-w64-i686-toolchain mingw32/mingw-w64-i686-cmake mingw32/mingw-w64-i686-boost mingw32/mingw-w64-i686-libxml2 msys/make mingw32/mingw-w64-i686-qt5-static mingw32/mingw-w64-i686-jasper```  

3. Configuration (32 bits) :  
  ```bash
  cmake . -G "MSYS Makefiles" -DBUILD_STATIC=ON -DBUILD_GUI=ON -DBUILD_DIST=OFF -DCMAKE_BUILD_TYPE=Release -DARCH=i686 -DCMAKE_C_FLAGS="-m32" -DCMAKE_CXX_FLAGS="-m32 -DLIBXML_STATIC" -DQt5_DIR=/mingw32/qt5-static/lib/cmake/Qt5/
  ``` 

4. Compilation :
  ```bash
  make
  ```  

5. Install :  
  ```bash
  make install
  ```  


#### Create installer

* step 2 : You need to install NSIS before creating package.  
```bash
pacman -S mingw32/mingw-w64-i686-nsis
``` 

* step 3 :  
```bash
cmake . -G "MSYS Makefiles" -DBUILD_STATIC=ON -DBUILD_GUI=ON -DBUILD_DIST=ON -DCMAKE_BUILD_TYPE=Release -DARCH=i686 -DCMAKE_C_FLAGS="-m32" -DCMAKE_CXX_FLAGS="-m32 -DLIBXML_STATIC" -DQt5_DIR=/mingw32/qt5-static/lib/cmake/Qt5/
```  

* step 5 :  
```bash
cpack -G NSIS
```  


### Using MinGW (old method)

1. install MinGW  
2. install dependencies over MinGW installer  
3. install cmake  
4. configuration :  
  4.1 Using MinGW makefile ``` MSDOS> cmake CMakeList.txt -G "MinGW Makefiles" ```  
  4.2 Using CodeBlock makefile ``` MSDOS> cmake CMakeList.txt -G "CodeBlocks - MinGW Makefiles"```  
5. Compilation : ``` mingw32-make all ```  

--------------------------------------------------------------------------------

## __MAC OS__

> This install instruction is for Mac OSX >= 10.9 (>= Mavericks)

> Install Xcode from apple store before anything else.  
 > 10.14
Run '''xcode-select --install'''  
Run '''sudo installer -pkg /Library/Developer/CommandLineTools/Packages/macOS_SDK_headers_for_macOS_10.14.pkg -target /'''

> No more static libraries -> BUILD_STATIC=OFF

### Standard installation

1. __cmake__  
  1.1 Download cmake from [https://cmake.org/](https://cmake.org/)  
  1.2 Launch cmake, go to _Tools -> How to Install For Command Line Use_ and install it.  

2. __boost__  
  2.1 Download boost and untar  
  2.2 :boost/$> ./bootstrp.sh toolset=clang  
  2.3 :boost/$> ./b2 toolset=clang  
  2.4 :boost/$> sudo ./b2 install  

3. __Qt5__  [Optional]   
  3.1 Download online installer open source version of QT5  
  3.2 launch installer and install it (Tools and clang)  

4. Configure :  
  ```bash
  export MACOSX_DEPLOYMENT_TARGET=10.9 && cmake . -DBOOST_ROOT=/Path/to/boost/ -DQt5_DIR=/Path/to/Qt/5.x.x/clang_64/lib/cmake/Qt5/ -DCMAKE_BUILD_TYPE=Release -DBUILD_GUI=ON -DBUILD_DIST=ON -DBUILD_STATIC=OFF -DCMAKE_OSX_DEPLOYMENT_TARGET=10.9
  ```  

5. Compilation :  
  ```bash
  make
  ```  

6. Install :  
  ```bash sudo make install```  

7. Create bundle
  ```bash
  cpack -G Bundle
  ```  

### Using homebrew

> Seems that xcode is not needed (include in homebrew).

1. Install dependencies  
```bash
brew install cmake boost qt
```

2. Configure and install project  
```bash
export MACOSX_DEPLOYMENT_TARGET=10.9 && cmake . -DCMAKE_BUILD_TYPE=Release -DBUILD_GUI=ON -DBUILD_DIST=ON -DBUILD_STATIC=OFF -DCMAKE_OSX_DEPLOYMENT_TARGET=10.9 -DQt5_DIR=/usr/local/opt/qt/lib/cmake/Qt5/
make
sudo make install
# create bundle
cpack -G Bundle
```

### Using macport

1. Install dependencies  
```bash
sudo port install cmake libxml2 boost qt5
```

2. Configure and install project  
```bash
export MACOSX_DEPLOYMENT_TARGET=10.9 && cmake . -DCMAKE_BUILD_TYPE=Release -DBUILD_GUI=ON -DBUILD_DIST=ON -DBUILD_STATIC=OFF -DCMAKE_OSX_DEPLOYMENT_TARGET=10.9
make
sudo make install
# create bundle
cpack -G Bundle
```

--------------------------------------------------------------------------------

## Linux

### Debian/Ubuntu

1. Dependencies  
```bash
sudo apt-get install g++ \
              cmake \
              libxml2-dev libxml2
              pkg-config \
              libboost-dev \
              libc6 \
              libicu-dev \
              libglib2.0-0 \
              libqt5core5a libqt5gui5 qtbase5-dev libqt5widgets5 \
              libstdc++6 \
              libx11-6 \
              build-essential \
              zlib1g-dev \
              liblzma-dev
```  

2. Configuration, compilation and installation  
```bash
cmake . -DBUILD_GUI=ON -DBUILD_STATIC=OFF -DBUILD_DIST=OFF -DCMAKE_BUILD_TYPE=Release 
make
sudo make install
```

3. Packaging  
```bash
cmake . -DBUILD_GUI=ON -DBUILD_STATIC=OFF -DBUILD_DIST=ON -DCMAKE_BUILD_TYPE=Release 
make clean && make
cpack
cpack -G DEB
```

