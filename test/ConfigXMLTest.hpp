#ifndef __CONFIG_XML_TEST__
#define __CONFIG_XML_TEST__

#include <gtest/gtest.h>
#include <iostream>
#include <fstream>
#include "../include/ConfigXML.hpp"

class ConfigXMLTest : public ::testing::Test {

    protected:
    ConfigXMLTest();
    
    virtual ~ConfigXMLTest();

    virtual void SetUp() {
        // Code here will be called immediately after the constructor (right
            //     // before each test).
     }
    
    virtual void TearDown() {
    // Code here will be called immediately after each test (right
    // before the destructor).
    }

    ConfigXML * pxml;
    unordered_map<string, Parameter *> parameters;
};

#endif

