#include <math.h>
#include <vector>
#include <fstream>
#include <iostream>
#include "../include/MEMM_util.h"
#include <cmath>
#include <gtest/gtest.h>

TEST(gammlnTest,gammlnNegativeInput) {
    EXPECT_TRUE(isnan(gammln(-1)));
}
TEST(gammlnTest,gammlnZeroInput) {
    EXPECT_TRUE(isinf(gammln(0)));

}
TEST(gammlnTest,gammlnPositiveInput) {
    EXPECT_FLOAT_EQ(0.57236493,gammln(0.5));
    EXPECT_FLOAT_EQ(0.0,gammln(1));
    EXPECT_FLOAT_EQ(-0.12078224,gammln(1.5));
    EXPECT_FLOAT_EQ(-4.4408921e-16,gammln(2));
    EXPECT_FLOAT_EQ(8.5251617,gammln(8));
    EXPECT_FLOAT_EQ(10.604603,gammln(9));
    EXPECT_FLOAT_EQ(12.801827,gammln(10));
    EXPECT_FLOAT_EQ(359.13422,gammln(100));
}
