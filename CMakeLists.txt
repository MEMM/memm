#########################################
# fIle: CMakeList.txt                   #
# Package: MEMM                         #
#########################################

# -----------------------------------
# Name and version 
# -----------------------------------
cmake_minimum_required (VERSION 2.8.9)
project (MEMM)
SET(MEMM_VERSION_MAJOR 3)
SET(MEMM_VERSION_MINOR 0)

# -----------------------------------
# VARIABLES & OPTIONS & MACRO
# -----------------------------------

# set architecture i386, amd64...
if(NOT ARCH)
  SET(ARCH ${CMAKE_SYSTEM_PROCESSOR})
endif()



#uninstall target
configure_file(
  "${CMAKE_CURRENT_SOURCE_DIR}/cmake_uninstall.cmake.in"
  "${CMAKE_CURRENT_BINARY_DIR}/cmake_uninstall.cmake"
  IMMEDIATE @ONLY)

set (CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin)
set (CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)
set (CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)

set(CMAKE_AUTOMOC ON)
set(CMAKE_INCLUDE_CURRENT_DIR ON)
include_directories (include)
include_directories (include/GUI)

OPTION(BUILD_STATIC "Build static MEMM version" OFF)
OPTION(BUILD_DIST "Change parameters for building package" OFF)
OPTION(BUILD_GUI "Build MEMM GUI" ON)
OPTION(BUILD_TEST "Build unit tests" OFF)
OPTION(BUILD_DOC "Build Doxygen doc" OFF)

if(BUILD_TEST)
  MESSAGE("Unit Tests : ON")
  add_custom_target(tests)
  macro(run_test test_target)
  add_custom_target(${test_target}_runtest
    COMMAND "${CMAKE_BINARY_DIR}/bin/${test_target}"
    DEPENDS ${test_target}
    WORKING_DIRECTORY "${CMAKE_SOURCE_DIR}/test/")
  add_dependencies(tests ${test_target}_runtest)
endmacro()
else ()
  MESSAGE("Unit Tests : OFF")
endif(BUILD_TEST)

# -----------------------------------
# BINARIES
# -----------------------------------
set(CMAKE_MACOSX_RPATH "ON")
set(MEMM_SRC src/Parameter.cpp src/Population.cpp src/graine.cpp src/locus.cpp src/genotype.cpp  src/individu.cpp  src/parent.cpp src/MEMM_loi.cpp src/MEMM_logNormal.cpp src/MEMM_gauss.cpp src/MEMM_gamma.cpp src/MEMM_beta.cpp src/MEMM_zigamma.cpp src/MEMM_util.cpp src/MEMM.cpp src/MEMM_logLik.cpp src/Configuration.cpp src/ConfigTXT.cpp src/ConfigXML.cpp src/XMLInterface.cpp src/MEMM_seedlings.cpp src/MEMM_seedlings_2kernels.cpp src/MEMM_seedlings_2kernels_covar.cpp src/MEMM_coloniz.cpp src/MEMM_coloniz_dendro.cpp src/MEMM_coloniz_dendro_multipop.cpp src/MEMM_logLik_multiSpe_covar.cpp src/MEMM_logLik_multiSpe_covar_predict.cpp src/Tools.cpp)

if(BUILD_STATIC)
  MESSAGE("Build static version : ON")
  MESSAGE(WARINING "Static mode is for the local architecture")
  SET(CMAKE_FIND_LIBRARY_SUFFIXES ".a;.so")
  SET(BUILD_SHARED_LIBS OFF)
  add_executable(MEMM src/main.cpp)
  add_library (MEMMLIB STATIC ${MEMM_SRC})
  target_link_libraries (MEMM MEMMLIB)
  add_definitions("-D_STATIC_")
  
  if(NOT APPLE)
    if(WIN32)
	    SET(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -static -static-libgcc -static-libstdc++")# --disable-shared --enable-static")
      SET_TARGET_PROPERTIES(MEMM PROPERTIES LINK_SEARCH_START_STATIC 1)
      SET_TARGET_PROPERTIES(MEMM PROPERTIES LINK_SEARCH_END_STATIC 1)
    endif(WIN32)
    if(UNIX)
      # on unix like compile as dynamic with std libraies and static for other (libxlm2, boost, QT5)
      SET(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -static-libgcc -static-libstdc++")
      SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fPIE")
      #SET_TARGET_PROPERTIES(MEMM PROPERTIES LINK_SEARCH_START_STATIC 1)
      #SET_TARGET_PROPERTIES(MEMM PROPERTIES LINK_SEARCH_END_STATIC 1)
    endif(UNIX)
  endif(NOT APPLE)
else(BUILD_STATIC)
  MESSAGE("Build static version : OFF")
  add_executable(MEMM src/main.cpp)
  add_library (MEMMLIB SHARED ${MEMM_SRC})
  target_link_libraries (MEMM MEMMLIB)
  #SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fPIC")
endif(BUILD_STATIC)

if(BUILD_GUI)
  MESSAGE("Build MEMM_GUI : ON")
  add_executable(MEMM_GUI 
    src/GUI/Utils.cpp include/GUI/Utils.hpp
    src/GUI/MainWindow.cpp include/GUI/MainWindow.hpp
    src/GUI/Console.cpp include/GUI/Console.hpp
    src/GUI/InputParamWidget.cpp include/GUI/InputParamWidget.hpp
    src/GUI/ParamParamWidget.cpp include/GUI/ParamParamWidget.hpp
    src/GUI/OutputParamWidget.cpp include/GUI/OutputParamWidget.hpp
    src/GUI/OptionsParamWidget.cpp include/GUI/OptionsParamWidget.hpp
    src/GUI/ModelsParamWidget.cpp include/GUI/ModelsParamWidget.hpp
    src/GUI/ParameterLayout.cpp include/GUI/ParameterLayout.hpp
    src/GUI/DistributionLayout.cpp include/GUI/DistributionLayout.hpp
    src/GUI/main_GUI.cpp
    )
  target_link_libraries(MEMM_GUI MEMMLIB)
  if(BUILD_STATIC)
    SET_TARGET_PROPERTIES(MEMM_GUI PROPERTIES LINK_SEARCH_START_STATIC 1)
    SET_TARGET_PROPERTIES(MEMM_GUI PROPERTIES LINK_SEARCH_END_STATIC 1)
  endif(BUILD_STATIC)
else(BUILD_GUI)
  MESSAGE("Build MEMM_GUI : OFF")
endif(BUILD_GUI)

if(BUILD_TEST)
enable_testing()
add_executable(MEMM_TEST
    test/MEMM_utilTest.cpp
    test/MEMM_gammaTest.hpp
    test/MEMM_gammaTest.cpp
    test/ConfigTXTTest.cpp
    test/ConfigXMLTest.cpp
    test/runTest.cpp
    )
target_link_libraries(MEMM_TEST MEMMLIB)
#add_test(NAME TEST
#    WORKING_DIRECTORY "${CMAKE_SOURCE_DIR}/test/"
#    COMMAND "${CMAKE_BINARY_DIR}/bin/MEMM_TEST" )
run_test(MEMM_TEST)
endif(BUILD_TEST)

# -----------------------------------
# DEPENDENCIES
# -----------------------------------
SET(CMAKE_MODULE_PATH ${CMAKE_SOURCE_DIR}/cmake)
#INCLUDE(MACRO)
FIND_PACKAGE(Boost 1.49.0 REQUIRED)
if (Boost_FOUND)
    
    set(Boost_DEBUG OFF)
    if(BUILD_STATIC)
      set(Boost_USE_STATIC_LIBS       ON)
      set(Boost_USE_STATIC_RUNTIME    ON)
    else(BUILD_STATIC)
      SET(Boost_USE_STATIC_LIBS OFF)
    endif(BUILD_STATIC)
    set(Boost_USE_MULTITHREADED      ON)
    include_directories (${Boost_INCLUDE_DIRS})
    target_link_libraries (MEMMLIB ${Boost_LIBRARIES}) 
    if(BUILD_TEST)
      target_link_libraries (MEMM_TEST ${Boost_LIBRARIES}) 
    endif(BUILD_TEST)
endif (Boost_FOUND)

## Libxml
find_package (LibXml2 2.7.0 REQUIRED)
if (LIBXML2_FOUND)
    MESSAGE("-- libxml-2.0 version : ${LIBXML2_VERSION_STRING}")
    include_directories (${LIBXML2_INCLUDE_DIR})
    add_definitions(${LIBXML2_DEFINITIONS})
    #link_directories (${LIBXML2_LIBRARY_DIR})
    #if(BUILD_GUI)
    #  target_link_libraries (MEMM_GUI ${LIBXML2_LIBRARIES})
    #endif(BUILD_GUI)
    target_link_libraries (MEMMLIB ${LIBXML2_LIBRARIES})
    if(BUILD_STATIC)
	    add_definitions(${LIBXML2_DEFINITIONS} -DLIBXML_STATIC)
    if(WIN32)
	    target_link_libraries(MEMMLIB -lz -llzma -lm -lws2_32 -liconv)
    else(WIN32)
      target_link_libraries(MEMMLIB ${LIBXML2_LIBRARIES} -lz -llzma -lm -licuio -licui18n -licuuc -licudata -ldl)
      MESSAGE("  libxml2 static linking : -DLIBXML_STATIC ${LIBXML2_LIBRARIES} -licui18n -licuuc -licudata -lz -llzma -lm -licuio -ldl")
    endif(WIN32)
    endif(BUILD_STATIC)
endif (LIBXML2_FOUND)

## Qt
if(BUILD_GUI)
  find_package (Qt5 REQUIRED Widgets)
  if (Qt5_FOUND)
    set (CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} ${Qt5Widgets_EXECUTABLE_COMPILE_FLAGS}")
    MESSAGE("-- Qt5 version : ${Qt5_VERSION}") 
    target_link_libraries (MEMM_GUI Qt5::Core Qt5::Gui Qt5::Widgets)
    add_definitions(${Qt5Widgets_DEFINITIONS})
    include_directories (${Qt5Widgets_INCLUDE_DIRS})
    set_target_properties(Qt5::Core PROPERTIES MAP_IMPORTED_CONFIG_COVERAGE "RELEASE")
    SET(Qt5_PATH "${Qt5_DIR}/../../../")
    if(BUILD_STATIC)
      MESSAGE("Cannot compile GUI as static ? : run cmake with -DBUILD_GUI=OFF or use QT compiled as static")
      #SET(QT_USE_QTPLUGIN TRUE)
      add_definitions("-DQT_STATICPLUGIN -DQT_STATIC_BUILD")
    if(WIN32)
      target_link_libraries(MEMM_GUI Qt5::WinMain Qt5::QWindowsIntegrationPlugin -pthread -lwinpthread -liconv -llzma -lz)
      #target_link_libraries(MEMM_GUI -L${QT5_PATH}/share/qt5/plugins/platforms/ -lqwindows -L${QT5_PATH}/lib/ -lQt5PlatformSupport -lwinspool -lshlwapi -lcomdlg32 -loleaut32 -limm32 -lwinmm -lglu32 -lopengl32 -lgdi32 -lole32 -luuid -lws2_32 -ladvapi32 -lshell32 -luser32 -lkernel32 )
    endif(WIN32)
    if(UNIX AND NOT APPLE)
      #target_link_libraries(MEMM_GUI -L${QT5_PATH}/plugins/platforms/ libQt5PlatformSupport.a libqxcb.a  -L${QT5_PATH}/lib/ libqtharfbuzzng.a -Wl,-Bstatic -lpng -Wl,-Bstatic -ljpeg -Wl,-Bdynamic -lc -Wl,-Bdynamic -lglib-2.0 -Wl,-Bdynamic -lGL -Wl,-Bdynamic -lpthread -Wl,-Bdynamic -lglib-2.0 -Wl,-Bdynamic -ldl )
      #OK ca marche bien !
      target_link_libraries(MEMM_GUI ${Qt5_PATH}/plugins/platforms/libqxcb.a -lX11-xcb -lXi -lSM -lICE -lxcb -L${Qt5_PATH}/lib/ -lxcb-static -lQt5PlatformSupport -lfontconfig -lfreetype -lXrender -lXext -lX11 -ludev -lmtdev -lEGL -lQt5Gui -ljpeg -lpng -lqtharfbuzzng -lQt5DBus -lQt5Core -lz -lm -ldl -pthread -lgthread-2.0 -lglib-2.0 -lrt -lGL -lpthread)
    endif(UNIX AND NOT APPLE)
    endif(BUILD_STATIC)
    #qt5_use_modules(MEMM_GUI Widgets Core Gui)
  endif (Qt5_FOUND)
endif(BUILD_GUI)

## Doxygen : add a target to generate API documentation with Doxygen
if(BUILD_DOC)
  MESSAGE("Doxygen : ON")
  find_package(Doxygen REQUIRED)
if(DOXYGEN_FOUND)
    configure_file (${CMAKE_CURRENT_SOURCE_DIR}/Doxyfile.in ${CMAKE_CURRENT_BINARY_DIR}/Doxyfile @ONLY)
    add_custom_target (doc
        ${DOXYGEN_EXECUTABLE} ${CMAKE_CURRENT_BINARY_DIR}/Doxyfile
        WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
        COMMENT "Generating API documentation with Doxygen" VERBATIM
    )
endif(DOXYGEN_FOUND)
else()
  MESSAGE("Doxygen : OFF")
endif(BUILD_DOC)

## Google Test
if(BUILD_TEST)
  find_package(GTest REQUIRED)
if(GTEST_FOUND)
    include_directories(${GTEST_INCLUDE_DIR})
    target_link_libraries (MEMM_TEST ${GTEST_LIBRARIES} ${GTEST_MAIN_LIBRARIES} pthread)
endif(GTEST_FOUND)
endif(BUILD_TEST)


# -----------------------------------
# COMPIL OPTIONS
# -----------------------------------
if (CMAKE_BUILD_TYPE MATCHES "Debug")
    add_definitions("-DDEBUG")
    set (CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -Wall -Wextra -g -ggdb -Wconversion -O0")
    MESSAGE("Debug mode : ON")
else ()
    set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -O3 -w")
    MESSAGE("Debug mode : OFF")
endif ()

if(BUILD_DIST)
  add_definitions("-DBUILD_DIST")
  MESSAGE("Build Dist options : ON")
else()
  MESSAGE("Build Dist options : OFF")
endif(BUILD_DIST)

set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")

#--------------------------------------
# INSTALL & UNINSTALL
#--------------------------------------

# Windows install
if(WIN32 AND NOT UNIX)

  install(TARGETS MEMM
        RUNTIME DESTINATION .
        COMPONENT applications
        )
  install(TARGETS MEMMLIB
        LIBRARY DESTINATION .
        ARCHIVE DESTINATION .
	      COMPONENT libraries
        )

  if(BUILD_GUI)
    install(TARGETS MEMM_GUI
    RUNTIME DESTINATION .
    COMPONENT applications
    )
  endif(BUILD_GUI)

  INSTALL(FILES data/Parameters.xml data/seedlings/ParametersSeedling.xml data/AlisierDesc.txt data/AlisierPar.txt data/freqall.txt data/LocusError.txt data/seedlings/seedlings.txt data/seedlings/parentsSeedlings.txt
        DESTINATION data )
  INSTALL(FILES R/MCMCVisu.R DESTINATION R)
  INSTALL(FILES MEMM.ico DESTINATION images)
  INSTALL(FILES LICENSE AUTHORS doc/ReadMe_v3_0.pdf
    DESTINATION doc
    COMPONENT applications)
  SET (MEMM_DATA_FILES_PATH "${CMAKE_INSTALL_PREFIX}/data/")
  add_definitions(-DMEMM_DATA_PATH="${MEMM_DATA_FILES_PATH}")
 
# linux and mac os 
else(WIN32 AND NOT UNIX)

  install(TARGETS MEMM
        RUNTIME DESTINATION bin
        COMPONENT applications
        )
  install(TARGETS MEMMLIB
        LIBRARY DESTINATION lib
        ARCHIVE DESTINATION lib/static
	      COMPONENT libraries
        )

  if(BUILD_GUI)
    install(TARGETS MEMM_GUI
    RUNTIME DESTINATION bin
    COMPONENT applications
  )
  endif(BUILD_GUI)

  INSTALL(FILES data/Parameters.xml data/seedlings/ParametersSeedling.xml data/AlisierDesc.txt data/AlisierPar.txt data/freqall.txt data/LocusError.txt data/seedlings/seedlings.txt data/seedlings/parentsSeedlings.txt
        DESTINATION share/memm)
      INSTALL( FILES R/MCMCVisu.R DESTINATION share/memm/)
  INSTALL(FILES memm.desktop DESTINATION share/applications)
  #INSTALL(FILES memm-term.desktop DESTINATION share/applications)
  INSTALL(FILES MEMM.png DESTINATION share/icons)

  SET (MEMM_DATA_FILES_PATH "${CMAKE_INSTALL_PREFIX}/share/memm/")
  add_definitions(-DMEMM_DATA_PATH="${MEMM_DATA_FILES_PATH}")

  INSTALL(FILES LICENSE AUTHORS doc/ReadMe_v3_0.pdf
    DESTINATION share/doc/memm/
    COMPONENT applications)

  add_custom_target(uninstall
    COMMAND ${CMAKE_COMMAND} -P ${CMAKE_CURRENT_BINARY_DIR}/cmake_uninstall.cmake)
endif(WIN32 AND NOT UNIX)

#--------------------------------------
# CPACK
#--------------------------------------
if(APPLE)
 SET(CPACK_COMPONENTS_ALL applications libraries)
  #if(NOT BUILD_STATIC)
    #install(TARGETS MEMMLIB DESTINATION lib)
  #endif(NOT BUILD_STATIC)
  if(BUILD_DIST)
    install(DIRECTORY "${Qt5_PATH}/lib/QtPrintSupport.framework" "${Qt5_PATH}/lib/QtCore.framework" "${Qt5_PATH}/lib/QtWidgets.framework" "${Qt5_PATH}/lib/QtGUI.framework"
	    DESTINATION ../Frameworks
    	FILES_MATCHING
    	PATTERN "*"
      PATTERN "Headers/*" EXCLUDE
	    PATTERN "*_debug" EXCLUDE)
    install(DIRECTORY "${Qt5_PATH}/plugins/platforms/"
      DESTINATION ../Plugins/platforms/
      FILES_MATCHING
      PATTERN "*.dylib"
      PATTERN "*_debug.dylib" EXCLUDE)
    #install(FILES qt.conf DESTINATION ../Resources/)
  endif(BUILD_DIST)
endif(APPLE)

if(BUILD_DIST)
  SET(CPACK_PACKAGE_CONTACT "Jean-François Rey<jean-francois.rey@inra.fr>")
  SET(CPACK_PACKAGE_DESCRIPTION_SUMMARY "This computer program is a bayesian estimation of pollen dispersal kernel and the variance in male fecundity from spatial and genetic data (microsatellites) concerning adult plants and sampled seeds (see Klein et al. 2008 Molecular Ecology).")
  SET(CPACK_PACKAGE_NAME ${PROJECT_NAME})
  SET(CPACK_PACKAGE_VERSION "${MEMM_VERSION_MAJOR}.${MEMM_VERSION_MINOR}.0")
  SET(CPACK_PACKAGE_FILE_NAME "${CPACK_PACKAGE_NAME}-${CPACK_PACKAGE_VERSION}-${CMAKE_SYSTEM_NAME}-${ARCH}")
  #    target_link_libraries(MEMM_GUI )
  #SET(CPACK_PACKAGE_EXECUTABLES MEMM MEMM_GUI)
  SET(CPACK_PACKAGE_ICON "MEMM.png")
  #SET(CPACK_RESOURCE_FILE_LICENSE "LICENSE")
  #SET(CPACK_RESOURCE_FILE_README "README")

  if(UNIX)
    IF(${ARCH} STREQUAL "x86_64")
      SET(CPACK_DEBIAN_PACKAGE_ARCHITECTURE amd64)
    ELSE()
      SET(CPACK_DEBIAN_PACKAGE_ARCHITECTURE i386)
    ENDIF(${ARCH} STREQUAL "x86_64")
    #SET (CPACK_DEBIAN_PACKAGE_ARCHITECTURE ${ARCH})
    if(NOT BUILD_STATIC)
      SET (CPACK_DEBIAN_PACKAGE_DEPENDS "libxml2 (>= 2.7), libboost-dev (>= 1.49.0)")
      SET (CPACK_DEBIAN_PACKAGE_SUGGESTS "qt5-default (>= 5.2.0)")
    ENDIF(NOT BUILD_STATIC)
  endif(UNIX)

  if(APPLE)
    SET(CPACK_PACKAGE_ICON "MEMM.icns")
    SET(CPACK_BUNDLE_NAME ${PROJECT_NAME})
    SET(CPACK_BUNDLE_ICON "MEMM.icns")
    SET(CPACK_BUNDLE_PLIST "Info.plist")
    SET(CPACK_BUNDLE_STARTUP_COMMAND "MEMM.sh")
  endif(APPLE)

  if(WIN32)
    set(CPACK_INSTALL_PREFIX ${CMAKE_INSTALL_PREFIX})
	  set(CPACK_PACKAGE_INSTALL_DIRECTORY "${PROJECT_NAME}")
    set(CPACK_RESOURCE_FILE_LICENSE ${CMAKE_SOURCE_DIR}\\\\LICENSE)
	  set(CPACK_RESOURCE_FILE_LICENSE_PROVIDED "1")
	  set(CPACK_NSIS_DISPLAY_NAME "${PROJECT_NAME}")
    set(CPACK_NSIS_MODIFY_PATH ON)
    #SET(CPACK_PACKAGE_ICON "Unspecified\\\\share\\\\icons\\\\MEMM.png")
    SET(CPACK_PACKAGE_ICON "${CMAKE_SOURCE_DIR}\\\\MEMM.ico")
	  set(CPACK_PACKAGE_EXECUTABLES "MEMM;MEMM bash;MEMM_GUI;MEMM Interface")

    set(CPACK_NSIS_MUI_ICON ${CMAKE_SOURCE_DIR}\\\\MEMM.ico)
    set(CPACK_NSIS_INSTALLED_ICON_NAME "bin\\\\MEMM.exe")    
    set(CPACK_SOURCE_STRIP_FILES 1)

  endif(WIN32)

  INCLUDE(CPack)
endif(BUILD_DIST)
