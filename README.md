# MEMM

[![pipeline status](https://gitlab.paca.inra.fr/MEMM/memm/badges/master/pipeline.svg)](https://gitlab.paca.inra.fr/MEMM/memm/commits/master)

# Demo

## DESCRIPTION

MEMM (Mixed Effects Mating Model) implemtents a Bayesian statistic method to estimate the pollen dispersal kernel and the variance in male fecundity from spatial and genetic data (microsatellites) concerning adult plants and sampled seeds (see Klein et al. 2008 Molecular Ecology).

## DEPENDENCIES

* gcc/g++ (gcc >= 4.8.0)
    Available from : http://gcc.gnu.org

    The GNU compiler Collection.

* cmake (cmake >= 2.8.9)
    Available from : http://cmake.org

    The cross-platform build system.

* boost (boost >= 1.49.0)
    Available from : http://www.boost.org

    C++ libraries.

* libxml2 (libxml-2.0 >= 2.5.0)
    Available from : http://xmlsoft.org/

    The XML C parser and toolkit.

* Qt5 (Qt >= 5.2)
    Available from : http://qt-project.org

    The croos-platform application framework.

* Doxygen (doxygen >= 1.8.0) (optional)
    Available from : http://www.doxygen.org

    Generate documentation from source code.

* GTest (GTest => 1.6.0) (optional)
    Available from : https://code.google.com/p/googletest/

    Google framework for writting C++ tests.


## INSTALL

See the [INSTALL](INSTALL.md) file for the compilation and installation procedure.

__BUILD OPTIONS__

The 'cmake' script supports the following options, among others :

-DCMAKE\_BUILD\_TYPE=Debug|Release
    To enable compilation type Debug or Release mode.

-G "<Generator>"
    To change make file generator (by default use one for the system).
    See cmake help to know Generators available.

-DBUILD\_STATIC=ON|OFF (Default=OFF)
  Enable to build static binaries

-DBUILD\_GUI=ON|OFF (Default=ON)
  enable to build the MEMM GUI

-DBUILD\_TEST=ON|OFF (DEFAULT=OFF)
    Enable or disable unit test building

-DBUILD\_DOC=ON|OFF (Default=OFF)
    Enable or disable documentations generation (Doxygen)
-DBUILD\_DIST=ON|OFF (Default=OFF)
    Enable or disable parameters for packaging
    
See the output of 'cmake --help' for details on additionnal supported options.

## ACKNOWLEDGEMENTS

Many thanks to Jean-François Rey for useful help.

## KNOWN BUGS

Many.

## COPYRIGHT
  GNU General Public License v3.
  See the LICENSE file.
  copyright (c) 2009-2018 etienne klein <etienne.klein@inra.fr> and contributors
  INRA BioSP Avignon, FRANCE

## AUTHORS
  This sotfware is written by Etienne Klein <etienne.klein@inra.fr>
  and Jean-François Rey <jean-francois.rey@inra.fr>
  with contributions from Olivier Bonnefon <olivier.bonnefon@inra.fr>


